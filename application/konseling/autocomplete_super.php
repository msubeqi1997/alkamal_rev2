<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");

$term = trim(strip_tags($_GET['term']));
$super = $_GET['surper'];
$row_set = array();
$pos = new model_konseling();
$poin = $pos->getDetailPoin($super);
$start = $poin[1]['start'];
$end = $poin[1]['end'];
$level = "SP".$super;
$data = $pos->searchSuperSantri($term,$start,$end,$level);
if($super !=''){
  foreach ($data[1] as $row) {
	$row['nis']=htmlentities(stripslashes($row['nis']));
	$row['nama']=htmlentities(stripslashes($row['nama_lengkap']));
	
	$row_set[] = $row;
  }
}
echo json_encode($row_set);
?>