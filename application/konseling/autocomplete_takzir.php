<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_konseling();
$data = $pos->searchTakzir($term);

foreach ($data[1] as $row) {
	$row['takzir']=htmlentities(stripslashes($row['hukuman']));
	
	$row_set[] = $row;
}

echo json_encode($row_set);
?>