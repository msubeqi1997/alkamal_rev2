<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	$smt = $tahun[1]['semester'];
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'simpan_amnesti')
	{
		$iditem = $_POST['token'];
		$tanggal = display_to_sql($_POST['tanggal']);
		$poin = $_POST['poin'];
		$note = $_POST['note'];
		$user = $_SESSION['sess_id'];
		$sts = '1';
		
		$array = $pos->saveAmnesti($iditem,$tanggal,$poin,$note,$tapel,$smt,$sts,$user);
		
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'select_siswa')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'akumulasi')
	{
		$id_item=$_POST['id'];
		$data = $pos->getAkumulasiAmnesti($id_item);
		$array['poin'] = $data[1]['poin'];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'get_riwayat_amnesti')
	{
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenis_jenjang = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		$search = $_POST['search'];
		$array = $pos->getBukuAmnesti($var,$search);
		$data = $array[1];
		
		$i=0;
		foreach ($data as $row) {
			
			$detail = '<button  type="submit" id_item="'.$row['uuid'].'"  title="Detail data" class="btn btn-sm btn-success btndetail "  id="btndetail'.$row['uuid'].'"  ><i class="glyphicon glyphicon-list-alt"></i></button>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat'];
			$data[$i]['firqoh'] =$row['nama_asrama'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['detail'] =$detail;
			$data[$i]['jumlah'] =$row['total']-$row['poin'];
			$i++;
		}
		$datax = array('data' => $data);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	
	if($method == 'delete_data')
	{
		$id_item=$_POST['id_item'];
		$user = $_SESSION['sess_id'];
		$data = $pos->deleteDataAmnesti($id_item,$user);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'detail_amnesti_siswa')
	{
		$id_item=$_POST['id_item'];
		$fetch = $pos->getDetailPelanggar($id_item);
		$data = $fetch[1];
		$array['nis'] = $data['nis'];
		$array['nama'] = $data['nama_lengkap'];
		$array['uuid'] = $data['uuid'];
		echo json_encode($array);
	}
	
	if($method == 'get_detail_amnesti')
	{
		$id_siswa=$_POST['idsiswa'];
		$array = $pos->getDetailAmnesti($id_siswa);
		$data = $array[1];
		
		$i=0;
		foreach ($data as $row) {
			
			$button = '<button  type="submit" id_item="'.$row['auto'].'" id_data="'.$row['id_siswa'].'"  title="Hapus data" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$row['auto'].'"  ><i class="fa fa-trash"></i></button>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['tanggal'] =display_to_report($row['input_at']);
			$data[$i]['edit'] =$button;
			$i++;
		}
		$datax = array('data' => $data);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	
} else {
	exit('No direct access allowed.');
}