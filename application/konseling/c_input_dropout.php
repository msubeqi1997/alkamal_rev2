<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	$smt = $tahun[1]['semester'];
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'simpan')
	{
		$siswa = $_POST['idsiswa'];
		$nama_siswa = $_POST['nama_siswa'];
		$nomor = $_POST['txtsurat'];
		$tanggal = display_to_sql($_POST['txttanggal']);
		$note = $_POST['txtnote'];
		$user = $_SESSION['sess_id'];
		$sts = '1';
		$alasan = 'Dropout Pelanggaran';
		
		$IDjenjang = $pos->getJenjangSiswa($siswa);
		$updateStatus = $pos->updateStatusKesiswaan($IDjenjang[1]['id'],$alasan);
		
		$folder_file = '../../files/file_konseling/';
		$tmp = $_FILES['file_attach']['tmp_name'];
		$tipe = $_FILES['file_attach']['type'];
		$nama = $_FILES['file_attach']['name'];
		$extention = pathinfo($nama, PATHINFO_EXTENSION);
		$tanggal = date('Y-m-d');
		
		
		$nama_file = 'SK_'.$tanggal.'_'.$nama_siswa.'.'.$extention;
		UploadCompress($nama_file, $folder_file, $tmp);
				
		$array = $pos->saveDropout($siswa,$nomor,$tanggal,$tapel,$smt,$nama_file,$note,$sts,$user);
		
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'select_siswa')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	
	if($method == 'get_riwayat_surat')
	{
		$first=display_to_sql($_POST['first']);
		$last=display_to_sql($_POST['last']);
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenis_jenjang = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat_kelas = ']=$_POST['tingkat'];}
		$search = $_POST['search'];
		$array = $pos->getBukuDropout($first,$last,$var,$search);
		$data = $array[1];
		$i=0;
		foreach ($data as $row) {
			$action = '<div class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a id="berkas'.$row['autoid'].'" target="_blank" href="../../files/file_konseling/'.$row['file'].'"><i class="fa fa-download"></i>Download surat</a></li>
                      </ul>
                    </div>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat_kelas'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['tanggal'] =display_to_report($row['tanggal_surat']);
			$data[$i]['nomor'] =$row['nomor_surat'];
			$data[$i]['button'] =$action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	if($method == 'delete_skorsing')
	{
		$id_item=$_POST['id_item'];
		$user = $_SESSION['sess_id'];
		$data = $pos->deleteSkorsing($id_item,$user);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'terima_surat')
	{
		$id_item=$_POST['id_item'];
		$tanggal = date('Y-m-d');
		$data = $pos->terimaSuper($id_item,$tanggal);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}