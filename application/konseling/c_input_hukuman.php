<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	$smt = $tahun[1]['semester'];
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'simpan_hukuman')
	{
		$iditem = $_POST['token'];
		$jenis = $_POST['jenis'];
		$tanggal = display_to_sql($_POST['tanggal']);
		$poin = $_POST['poin'];
		$kronologi = $_POST['kronologi'];
		$bimbingan = $_POST['bimbingan'];
		$takzir = $_POST['takzir'];
		$klasifikasi = $_POST['klasifikasi'];
		$user = $_SESSION['sess_id'];
		$sts = '1';
		
		$array = $pos->saveTakzir($iditem,$jenis,$klasifikasi,$tanggal,$poin,$kronologi,$bimbingan,$takzir,$tapel,$smt,$sts,$user);
		
		$data = $pos->getAkumulasiPoin($iditem);
		$result['poin'] = $data[1]['poin'];
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'select_siswa')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'cek_akumulasi')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getAkumulasiPoin($id_item);
		$array['poin'] = $data[1]['poin'];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'level_sp')
	{
		$val=$_POST['val'];
		$id=$_POST['id'];
		$data = $pos->level_sp($val);
		$check = $pos->check_sp($id,$data[1]['sp']);
		$cetak = '';
		if($data[1]['sp'] == 'SKORSING'){
			$skorsing = $pos->check_skorsing($id);	
			$cetak = (!empty($skorsing[1]['skorsing']))?'sudah':'belum';
		}
		
		$array['check_sp'] = (!empty($check[1]['cetak']))?true:false;
		$array['level'] = (!empty($data[1]['sp']))?$data[1]['sp']:'';
		$array['cetak'] = $cetak;
		$array['notif'] = (empty($check[1]['cetak']) && !empty($data[1]['sp']))?"Akumulasi poin sebesar ".$val." poin, saatnya cetak ".$data[1]['sp']:"";
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'select_siswa_sp')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$sp = $pos->level_sp($data[1]['poin']);
		
		$array['level'] = (!empty($sp[1]['sp']))?$sp[1]['sp']:'';;
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		
		echo json_encode($array);
	}
	
	if($method == 'get_detail_jenis')
	{
		$id_item=$_POST['id'];
		$data = $pos->getJenisHukuman($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'get_riwayat_pelanggaran')
	{
		$first=display_to_sql($_POST['first']);
		$last=display_to_sql($_POST['last']);
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenis_jenjang = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		$search = $_POST['search'];
		$array = $pos->getDataPelanggaran($first,$last,$var,$search);
		$data = $array[1];
		
		$i=0;
		foreach ($data as $row) {
			
			$button = '<button  type="submit" id_item="'.$row['auto'].'"  title="Hapus data" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$row['auto'].'"  ><i class="fa fa-trash"></i></button>';
			$detail = '<button  type="submit" id_item="'.$row['auto'].'"  title="Detail data" class="btn btn-sm btn-success btndetail "  id="btndetail'.$row['auto'].'"  ><i class="glyphicon glyphicon-list-alt"></i></button>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['tanggal'] =display_to_report($row['input_at']);
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat'];
			$data[$i]['firqoh'] =$row['nama_asrama'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['detail'] =$detail;
			$data[$i]['button'] =$button;
			$i++;
		}
		$datax = array('data' => $data);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	if($method == 'delete_data')
	{
		$id_item=$_POST['id_item'];
		$user = $_SESSION['sess_id'];
		$data = $pos->deleteDataPelanggaran($id_item,$user);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'detail_pelanggaran_siswa')
	{
		$id_item=$_POST['id_item'];
		$fetch = $pos->getDetailPelanggaranSiswa($id_item);
		$data = $fetch[1];
		$array['nis'] = $data['nis'];
		$array['nama'] = $data['nama_lengkap'];
		$array['firqoh'] = $data['nama_asrama'];
		$array['jenjang'] = $data['jenis_jenjang']." ".$data['jenis_kelamin']." / Tingkat ".$data['tingkat'];
		$array['mdk'] = $data['mdk'];
		$array['tanggal'] = display_to_report($data['input_at']);;
		$array['jenis'] = $data['jenis_pelanggaran'];
		$array['klasifikasi'] = $data['klasifikasi'];
		$array['poin'] = $data['poin_pelanggaran'];
		$array['takzir'] = $data['hukuman'];
		$array['kronologi'] = $data['kronologi'];
		$array['bimbingan'] = $data['bimbingan'];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}