<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	$smt = $tahun[1]['semester'];
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'simpan')
	{
		$iditem = $_POST['token'];
		$first = display_to_sql($_POST['first']);
		$last = date('Y-m-d',strtotime('+7 days',strtotime($first)));
		$nomor = $_POST['nomor'];
		$poin = $_POST['poin'];
		$user = $_SESSION['sess_id'];
		$input_at = date('Y-m-d');
		$sts = '1';
		
		$array = $pos->saveSkorsing($iditem,$first,$last,$nomor,$tapel,$smt,$poin,$user,$input_at);
		
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['token'] = $array[2];
		echo json_encode($result);
	}
	
	if($method == 'attachment')
	{
		$id_item=$_POST['id_item'];
		$nama_siswa=$_POST['nama_siswa'];
		$folder_file = '../../files/file_konseling/';
		$tmp = $_FILES['file_attach']['tmp_name'];
		$tipe = $_FILES['file_attach']['type'];
		$nama = $_FILES['file_attach']['name'];
		$extention = pathinfo($nama, PATHINFO_EXTENSION);
		$nama_file = '';
		$tanggal = date('Y-m-d');
		
		$detail = $pos->detailSkorsing($id_item);
		if (!empty($tmp)) { //jika ada gambar
			if(!empty($detail[1]['file'])){unlink($folder_file.$detail[1]['file']);}
			$nama_file = 'skorsing_'.$id_item.'_'.$tanggal.'_'.$nama_siswa.'.'.$extention;
			UploadCompress($nama_file, $folder_file, $tmp);
		}
		
		$data = $pos->terimaSkorsing($id_item,$tanggal,$nama_file);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'select_siswa')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'get_riwayat_surat')
	{
		$first=display_to_sql($_POST['first']);
		$last=display_to_sql($_POST['last']);
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenis_jenjang = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		$search = $_POST['search'];
		$array = $pos->getBukuSkorsing($first,$last,$tapel,$var,$search);
		$data = $array[1];
		$i=0;
		foreach ($data as $row) {
			$hapus = '<button  type="submit" id_item="'.$row['autoid'].'"  title="Hapus surat" class="btn btn-sm btn-danger btndelete "><i class="fa fa-trash"></i></button>';
			$download = '<button  type="submit" id_item="'.$row['autoid'].'"  title="Download surat" class="btn btn-sm btn-primary btndownload "><i class="fa fa-download"></i></button>';
			$action = '<div class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a id_item="'.$row['autoid'].'" class="btnupload " data-nis="'.$row['nis'].'" data-nama="'.$row['nama_lengkap'].'" data-nomor="'.$row['nomor_surat'].'"><i class="fa fa-upload"></i> Upload surat</a></li>
                        <li><a id_item="'.$row['autoid'].'" class="btndownload "><i class="fa fa-download"></i> Download surat</a></li>
                        <li><a id_item="'.$row['autoid'].'" class="btndelete " ><i class="fa fa-trash"></i> Hapus surat</a></li>
                      </ul>
                    </div>';
			if(empty($row['tanggal_kembali'])){
				$terima = '';
			}else{
				$extension=strtolower(strrchr($row['file'] , '.'));
				  if (preg_match("/$extension/i", '.jpg.jpeg.gif.png')) {
					$terima = '<a id="berkas'.$row['auto'].'" src="../../files/file_konseling/'.$row['file'].'" onclick="return fullimage(berkas'.$row['auto'].');">['.$row['tanggal_kembali'].']</a>';
				  }else{
					$terima = '<a id="berkas'.$row['auto'].'" target="_blank" href="../../files/file_konseling/'.$row['file'].'">['.$row['tanggal_kembali'].']</a>';
				  }
			}
			$data[$i]['urutan'] =$i+1;
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat'];
			$data[$i]['firqoh'] =$row['nama_asrama'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['first'] =display_to_report($row['tanggal_mulai']);
			$data[$i]['last'] =display_to_report($row['tanggal_akhir']);;
			$data[$i]['terima'] =$terima;
			$data[$i]['button'] =$action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	if($method == 'delete_skorsing')
	{
		$id_item=$_POST['id_item'];
		$user = $_SESSION['sess_id'];
		$data = $pos->deleteSkorsing($id_item,$user);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'terima_surat')
	{
		$id_item=$_POST['id_item'];
		$tanggal = date('Y-m-d');
		$data = $pos->terimaSuper($id_item,$tanggal);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}