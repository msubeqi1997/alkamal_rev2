<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	$smt = $tahun[1]['semester'];
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'simpan_super')
	{
		$iditem = $_POST['token'];
		$tanggal = display_to_sql($_POST['tanggal']);
		$duedate = date('Y-m-d',strtotime('+7 days',strtotime($tanggal)));
		$keterangan = $_POST['note'];
		$sp = $_POST['sp'];
		$poin = $_POST['poin'];
		$user = $_SESSION['sess_id'];
		$sts = '1';
		$count_sp = $pos->countSP($sp);
		$urut_sp = $count_sp[1]['jumlah']+1;
		$bulan = date('n');
		$tahun = date('Y');
		$tingkat_sp = substr($sp, 2, 1);
		$nomor_surat = sprintf("%03d",$urut_sp)."/SPK".$tingkat_sp."/PPTA/".getRomawi($bulan)."/".$tahun;
		
		$array = $pos->saveSuper($iditem,$tanggal,$duedate,$keterangan,$nomor_surat,$tapel,$smt,$poin,$sp,$user);
		
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['token'] = $array[2];
		echo json_encode($result);
	}
	
	if($method == 'select_siswa')
	{
		$id_item=$_POST['siswa_id'];
		$data = $pos->getDetailPelanggar($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'get_riwayat_surat')
	{
		$first=display_to_sql($_POST['first']);
		$last=display_to_sql($_POST['last']);
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenis_jenjang = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		if($_POST['level']  != ''){$var['sp_ke = ']=$_POST['level'];}
		$search = $_POST['search'];
		
		$array = $pos->getBukuSuper($first,$last,$var,$search);
		$data = $array[1];
		$i=0;
		foreach ($data as $row) {
			$over = '';
			
			$ext = explode('.',$row['file']);
			if($row['tanggal_kembali'] == ''){
				$now = strtotime(date('Y-m-d')); // or your date as well
				$datediff = $now - strtotime($row['duedate']);
				$over = round($datediff / (60 * 60 * 24))." hari";
				$upload = '';
				$terima = '<button  type="submit" id_item="'.$row['auto'].'"  data-nis="'.$row['nis'].'" data-nama="'.$row['nama_lengkap'].'" title="Terima surat" class="btn btn-sm btn-info btnreply "><i class="fa fa-mail-reply"></i> Terima</button>';}
			else{ 
				$tglterima=display_to_report($row['tanggal_kembali']);
				
				$now = strtotime($row['tanggal_kembali']); // or your date as well
				$your_date = strtotime('+7 days',strtotime($row['tanggal_surat']));
				$datediff = $now - $your_date;
				$over = round($datediff / (60 * 60 * 24))." hari";
				$upload = '<li><a id_item="'.$row['auto'].'" class="btnupload " data-nis="'.$row['nis'].'" data-nama="'.$row['nama_lengkap'].'" data-tanggal="'.display_to_report($row['tanggal_surat']).'"><i class="fa fa-upload"></i> Upload surat</a></li>';
				if(!empty($row['file'])){
				  $extension=strtolower(strrchr($row['file'] , '.'));
				  if (preg_match("/$extension/i", '.jpg.jpeg.gif.png')) {
					$terima = '<a id="berkas'.$row['auto'].'" src="../../files/file_konseling/'.$row['file'].'" onclick="return fullimage(berkas'.$row['auto'].');">['.$tglterima.']</a>';
				  }else{
					$terima = '<a id="berkas'.$row['auto'].'" target="_blank" href="../../files/file_konseling/'.$row['file'].'">['.$tglterima.']</a>';
				  }
				}else{
				  $terima = "[".$tglterima."]";
				}
			}
			$hapus = '<button  type="submit" id_item="'.$row['auto'].'"  title="Hapus surat" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$row['auto'].'"  ><i class="fa fa-trash"></i></button>';
			$download = '<button  type="submit" id_item="'.$row['auto'].'"  title="Download surat" class="btn btn-sm btn-primary btndownload "><i class="fa fa-download"></i></button>';
			
			
			$action = '<div class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        '.$upload.'
                        <li><a id_item="'.$row['auto'].'" class="btndownload "><i class="fa fa-download"></i> Download surat</a></li>
                        <li><a id_item="'.$row['auto'].'" class="btndelete " ><i class="fa fa-trash"></i> Hapus surat</a></li>
                      </ul>
                    </div>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat'];
			$data[$i]['firqoh'] =$row['nama_asrama'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['level'] =$row['sp_ke'];
			$data[$i]['tanggal'] =display_to_report($row['tanggal_surat']);
			$data[$i]['duedate'] =display_to_report($row['duedate']);
			$data[$i]['over'] =($over > 0)?$over:'';
			$data[$i]['admin'] =$row['admin'];
			$data[$i]['button'] =$terima;
			$data[$i]['action'] =$action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	if($method == 'delete_surat')
	{
		$id_item=$_POST['id_item'];
		$user = $_SESSION['sess_id'];
		$data = $pos->deleteSuper($id_item,$user);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'terima_surat')
	{
		$id_item=$_POST['id_item'];
		$nama_siswa=$_POST['nama_siswa'];
		$tanggal = date('Y-m-d');
		$folder_file = '../../files/file_konseling/';
		$tmp = $_FILES['file_surat']['tmp_name'];
		$tipe = $_FILES['file_surat']['type'];
		$nama = $_FILES['file_surat']['name'];
		$extention = pathinfo($nama, PATHINFO_EXTENSION);
		$nama_file = '';
		$user = $_SESSION['sess_id'];
		
		if (!empty($tmp)) { //jika ada gambar
			$nama_file = 'sp_'.$id_item.'_'.$tanggal.'_'.$nama_siswa.'.'.$extention;
			UploadCompress($nama_file, $folder_file, $tmp);
		}
		
		$data = $pos->terimaSuper($id_item,$tanggal,$user,$nama_file);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'attachment')
	{
		$id_item=$_POST['id_item'];
		$nama_siswa=$_POST['nama_siswa'];
		$folder_file = '../../files/file_konseling/';
		$tmp = $_FILES['file_attach']['tmp_name'];
		$tipe = $_FILES['file_attach']['type'];
		$nama = $_FILES['file_attach']['name'];
		$extention = pathinfo($nama, PATHINFO_EXTENSION);
		$nama_file = '';
		$user = $_SESSION['sess_id'];
		
		$detail = $pos->detailSuper($id_item);
		if (!empty($tmp)) { //jika ada gambar
			if(!empty($detail[1]['file'])){unlink($folder_file.$detail[1]['file']);}
			$nama_file = 'sp_'.$id_item.'_'.$detail[1]['tanggal_kembali'].'_'.$nama_siswa.'.'.$extention;
			UploadCompress($nama_file, $folder_file, $tmp);
		}
		
		$data = $pos->terimaSuper($id_item,$detail[1]['tanggal_kembali'],$user,$nama_file);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
} else {
	exit('No direct access allowed.');
}

function get_file_extension($file_name) {
	return substr(strrchr($file_name,'.'),1);
}

function getRomawi($bln){
                switch ($bln){
                    case 1: 
                        return "I";
                        break;
                    case 2:
                        return "II";
                        break;
                    case 3:
                        return "III";
                        break;
                    case 4:
                        return "IV";
                        break;
                    case 5:
                        return "V";
                        break;
                    case 6:
                        return "VI";
                        break;
                    case 7:
                        return "VII";
                        break;
                    case 8:
                        return "VIII";
                        break;
                    case 9:
                        return "IX";
                        break;
                    case 10:
                        return "X";
                        break;
                    case 11:
                        return "XI";
                        break;
                    case 12:
                        return "XII";
                        break;
                }
}