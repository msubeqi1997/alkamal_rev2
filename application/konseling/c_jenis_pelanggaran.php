<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'getdata'){
		$array = $pos->getJenisPelanggaran();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit jenis pelanggaran" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			if($key['aktif'] == '1'){$active = '<button  type="submit" id_item="'.$key['auto'].'"  title="Jenis pelanggaran aktif" class="btn btn-sm btn-success btnactive "  id="btnactive'.$key['auto'].'"  >active</button>';}
			else{$active = '<button  type="submit" id_item="'.$key['auto'].'"  title="Aktifasi jenis pelanggaran" class="btn btn-sm btn-warning btnactive "  id="btnactive'.$key['auto'].'"  >activate</button>';}
			$data[$i]['button'] = $button;
			$data[$i]['active'] = $active;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$plot = $_POST['plot'];
		$poin = $_POST['poin'];
		$aktif = $_POST['aktif'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJenisPelanggaran($name,$plot,$poin,$aktif);
		}
		else
		{
			$array = $pos->updateJenisPelanggaran($iditem,$name,$plot,$poin,$aktif);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailJenisPelanggaran($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'activate')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->activateJenisPelanggaran($id_item);
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}