<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_konseling();
	$method=$_POST['method'];
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'getdata'){
		$array = $pos->getJenisTakzir();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$data[$i]['button'] = $action = '<td>
                        <div class="btn-group">
                          <button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit jenis takzir" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>
                          <button  type="submit" id_item="'.$key['auto'].'"  title="Tombol hapus jenis takzir" class="btn btn-sm btn-danger btndelete "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-trash"></i></button>
						</div>
                       </td>';
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJenisTakzir($name);
		}
		else
		{
			$array = $pos->updateJenisTakzir($iditem,$name);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailJenisTakzir($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'delete')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->deleteJenisTakzir($id_item);
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}