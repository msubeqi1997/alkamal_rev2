<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_konseling();
	$tahun = $pos->activeTapel();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	$first=display_to_sql($_POST['first']);
	$last=display_to_sql($_POST['last']);
	$var = array();
	if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
								  $var['jenis_kelamin = ']=strtolower($exp[1]);
								  $var['jenis_jenjang = ']=strtolower($exp[0]);
								}
	if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
	if($_POST['tingkat_kelas']  != ''){$var['tingkat = ']=$_POST['tingkat_kelas'];}
	$search = $_POST['txtname'];
	$array = $pos->getBukuDropout($first,$last,$var,$search);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'DAFTAR SANTRI DROPOUT')
	->setCellValue('A5', 'NO')
	->setCellValue('B5', 'NO INDUK')
	->setCellValue('C5', 'NAMA SANTRI')
	->setCellValue('D5', 'JENJANG')
	->setCellValue('E5', 'TINGKAT')
	->setCellValue('F5', 'MDK')
	->setCellValue('G5', 'SURAT KEPUTUSAN')
	->setCellValue('H5', 'TANGGAL SURAT')
	->setCellValue('I5', 'ADMIN')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=6;
		
	foreach($data as $row) {
		
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('A'.$i, $num)
		  ->setCellValue('B'.$i, $row['nis'])
		  ->setCellValue('C'.$i, $row['nama_lengkap'])
		  ->setCellValue('D'.$i, $row['jenis_jenjang']." ".$row['jenis_kelamin'])
		  ->setCellValue('E'.$i, "Tingkat ".$row['tingkat_kelas'])
		  ->setCellValue('E'.$i, $row['mdk'])
		  ->setCellValue('G'.$i, $row['nomor_surat'])
		  ->setCellValue('H'.$i, display_to_report($row['tanggal_surat']))
		  ->setCellValue('I'.$i, $row['admin'])
		;
		$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	