<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_konseling();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	$first=display_to_sql($_POST['first']);
	$last=display_to_sql($_POST['last']);
	$var = array();
	if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
								  $var['jenis_kelamin = ']=strtolower($exp[1]);
								  $var['jenis_jenjang = ']=strtolower($exp[0]);
								}
	if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
	if($_POST['tingkat_kelas']  != ''){$var['tingkat = ']=$_POST['tingkat_kelas'];}
	if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
	if($_POST['level']  != ''){$var['sp_ke = ']=$_POST['level'];}
	$search = $_POST['txtname'];
	
	$array = $pos->getBukuSuper($first,$last,$var,$search);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'DAFTAR SURAT PERINGATAN KELUAR')
	->setCellValue('B5', 'NO')
	->setCellValue('C5', 'TANGGAL')
	->setCellValue('D5', 'NO INDUK')
	->setCellValue('E5', 'NAMA SANTRI')
	->setCellValue('F5', 'KODE FIRQOH')
	->setCellValue('G5', 'NAMA FIRQOH')
	->setCellValue('H5', 'JENJANG')
	->setCellValue('I5', 'TINGKAT')
	->setCellValue('J5', 'MDK')
	->setCellValue('K5', 'LEVEL SP')
	->setCellValue('L5', 'NOMOR SURAT')
	->setCellValue('M5', 'TANGGAL SURAT')
	->setCellValue('N5', 'TANGGAL KEMBALI')
	->setCellValue('O5', 'OVER')
	->setCellValue('P5', 'DITERIMA')
	->setCellValue('Q5', 'PENERIMA')
	->setCellValue('R5', 'PENGINPUT')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=6;
		
	foreach($data as $row) {
		$over = '';
		$kembali = ($row['tanggal_kembali'] == "")?" ":display_to_report($row['tanggal_kembali']);
		$startdate = display_to_report($row['tanggal_surat']);
		$duedate = strtotime('+7 days',strtotime($row['tanggal_surat']));

		if($kembali == ' '){
			$now = strtotime(date('Y-m-d')); // or your date as well
			$datediff = $now - $duedate;
			$over = round($datediff / (60 * 60 * 24))." hari";
			$terima='belum';
		}
		else{ 
			$terima=$kembali;
			$now = strtotime($row['tanggal_kembali']); // or your date as well
			$your_date = strtotime('+7 days',strtotime($row['tanggal_surat']));
			$datediff = $now - $your_date;
			$over = round($datediff / (60 * 60 * 24))." hari";
		}
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('B'.$i, $num)
		  ->setCellValue('C'.$i, display_to_report($row['input_at']))
		  ->setCellValue('D'.$i, $row['nis'])
		  ->setCellValue('E'.$i, $row['nama_lengkap'])
		  ->setCellValue('F'.$i, $row['kode_firqoh'])
		  ->setCellValue('G'.$i, $row['nama_asrama'])
		  ->setCellValue('H'.$i, $row['jenis_jenjang']." ".$row['jenis_kelamin'])
		  ->setCellValue('I'.$i, "Tingkat ".$row['tingkat'])
		  ->setCellValue('J'.$i, $row['mdk'])
		  ->setCellValue('K'.$i, $row['sp_ke'])
		  ->setCellValue('L'.$i, $row['nomor_sp'])
		  ->setCellValue('M'.$i, display_to_report($row['tanggal_surat']))
		  ->setCellValue('N'.$i, display_to_report($row['duedate']))
		  ->setCellValue('O'.$i, $over)
		  ->setCellValue('P'.$i, $terima)
		  ->setCellValue('Q'.$i, $row['penerima'])
		  ->setCellValue('R'.$i, $row['admin'])
		;
		$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	