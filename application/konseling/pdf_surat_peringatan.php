<?php
//error_reporting(0);
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();
/*
$idc = isset($_POST['id_item'])?$_POST['id_item']:'';
$pos = new model_konseling();
$array = $pos->getDetailPelanggar($idc);
$data = $array[1];
$tgl_ttd= tanggal_indo($data['tertanggal']);
*/
$kelamin = array('0'=>'Perempuan','1'=>'Laki-laki');
$stopword = array('KOTA','KABUPATEN');

$output= '
  <!DOCTYPE html>
  <html lang="id">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  </head>
  <body>
  <div class="kertas">
  <div class="kop-surat">
  <img src="../../image/kop-alkamal.jpg" alt="Kop surat">
  </div>
  <div class="judul-surat">
  <div class="tingkat-surat">
  Tingkat '.$tingkat_surat.'
  </div>
  <h1>Surat Pernyataan</h1>
  <div class="nomor-surat">Nomor: '.$surat['nomor_sp'].'</div>
  </div>
  <p>Bismillahirrohmanirrohim <br />Dengan ini saya, </p>
  <table class="regular lm-20">
  <tbody>
  <tr>
  <td class="label">Nama/NIS</td>
  <td>:</td>
  <td>'. $santri['nama_lengkap'].'/'.$santri['no_induk'].'</td>
  </tr>
  <tr>
  <td class="label">TTL</td>
  <td>:</td>
  <td>'. $santri['tempat_lahir'].', '.date('d/m/Y',strtotime($santri['tanggal_lahir'])).'</td>
  </tr>
  <tr>
  <td class="label">Kelas/Sekolah</td>
  <td>:</td>
  <td>3/SMK</td>
  </tr>
  <tr>
  <td class="label">Orang Tua</td>
  <td>:</td>
  <td>'. $santri['ayah'].'</td>
  </tr>
  <tr>
  <td class="label">Alamat</td>
  <td>:</td>
  <td>'. $santri['alamat'].'</td>
  </tr>
  </tbody>
  </table>
  <p>Menyatakan telah melanggar Undang – Undang / Peraturan Pon. Pes. Terpadu Al Kamal pada:</p>
  <table class="regular lm-20">
  <tbody>
  <tr>
  <td class="label">Hari/Tanggal</td>
  <td>:</td>
  <td>'.date('d/m/Y',strtotime($surat['tanggal_surat'])).'</td>
  </tr>
  <tr>
  <td class="label">Bentuk Pelanggaran/Kasus</td>
  <td>:</td>
  <td>Pengambilan</td>
  </tr>
  <tr>
  <td class="label">Klasifikasi Pelanggaran</td>
  <td>:</td>
  <td>Berat</td>
  </tr>
  </tbody>
  </table>
  <p>
  Sehubungan dengan pelanggaran tersebut di atas, saya <strong>menyatakan berjanji</strong> untuk merubah sikap dan perilaku saya ke arah yang lebih baik dan tidak akan mengulangi pelanggaran ini secara khusus dan pelanggaran terhadap Undang – Undang / Peraturan Pon. Pes. Terpadu Al Kamal secara umum. Untuk selanjutnya saya bersedia menjalani sanksi yang telah menjadi keputusan Majlis Mudabbir Pon. Pes. Terpadu Al Kamal.
  </p>
  <p>
  Demikian Surat Pernyataan ini saya buat dan saya tandatangani dalam keadaan sadar tanpa ada unsur paksaan dari pihak manapun.
  </p>
  <div class="tgl-surat">Blitar, '.date('d/m/Y').'</div>

  <table class="tanda-tangan col-2">
  <tr>
  <td>Orang Tua/Wali</td>
  <td>Yang Menyatakan</td>
  </tr>
  <tr>
  <td><div class="garis-nama">'.$santri['ayah'].'</div></td>
  <td><div class="garis-nama">'.$santri['nama_lengkap'].'</div></td>
  </tr>
  </table>
  <h3>Saksi-saksi</h3>
  <h3>Mudabbir Firqoh '.$kamar['nama_asrama'].' <br />
  Departemen Keamanan</h3>
  <table class="tanda-tangan col-4">
  <tr>
  <td>Koordinator</td>
  <td>Anggota 1</td>
  <td>Anggota 2</td>
  <td>Anggota 3</td>
  </tr>
  <tr>
  <td><div class="garis-nama">'.$peng_asrama['keamanan_ko'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['keamanan_1'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['keamanan_2'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['keamanan_3'].'</div></td>
  </tr>
  </table>
  <h3>Mudabbir Al Yaumi Firqoh '.$kamar['nama_asrama'].'</h3>
  <table class="tanda-tangan col-4">
  <tr>
  <td>Ketua</td>
  <td>Sekretaris</td>
  <td>Bendahara</td>
  <td>Wakil Bendahara</td>
  </tr>
  <tr>
  <td><div class="garis-nama">'.$peng_asrama['ketua'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['sekretaris'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['bendahara_1'].'</div></td>
  <td><div class="garis-nama">'.$peng_asrama['bendahara_2'].'</div></td>
  </tr>
  </table>

  <h3>Mengetahui, <br />
  Mudabbir Markazy PPTA</h3>
  <table class="tanda-tangan col-2">
  <tr>
  <td>Koord. Keamanan Markazy</td>
  <td>Wali Firqoh '.$kamar['nama_asrama'].'</td>
  </tr>
  <tr>
  <td><div class="garis-nama">'.$peng_pusat['keamanan_1'].'</div></td>
  <td><div class="garis-nama">'.$kamar['nama_lengkap'].'</div></td>
  </tr>
  </table>

  <div class="catatan">
  Catatan : Setiap minta tandatangan Surat Pernyataan dimasukkan dalam stop map merah dan digandakan menjadi 3 lembar. (stop map 1 / asli  dipegang oleh pelanggar, 2 diserahkan ke Idaroh Firqoh dan 3 diserahkan ke Idaroh Markaz ), serta berstempel asli Firqoh dan Markaz.
  </div>

  </div>

  <style>
  @page {
    /* width = 21cm */
    size: A4;
    margin: 1cm 1cm 0.5cm;

  }
  @media print{
    table{
      width: 19cm;
    }

  }
  .kertas{
    width: 100%;
    margin: 0 auto;
    font-size: 11pt;
  }
  .kop-surat img{
    width:100%;
  }
  table{
    width: 100%;
  }
  table tr td{
    padding: 1px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr th{
    padding: 3px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr td.label{
    font-weight: bold;
    width: 30%;
  }
  table tr td.label + td{
    width: 1%;
  }
  table tr td.label + td + td{
    width: 69%;
  }
  table.formulir{
    margin-bottom: 20px;
  }
  .judul-surat{
    text-align: center;
    position: relative;
  }
  .nomor-surat{
    margin:0;
  }
  .judul-surat h1{
    border-bottom: 1px solid #000;
    font-size: 12pt;
    display: inline;
    padding: 0 5px;
    margin-bottom:0;
    margin-top: 5px;
    text-transform: uppercase;
    letter-spacing: 2px;
    line-height: 11pt;
  }
  .tingkat-surat{
    position: absolute;
    top: 3px;
    left: 0;
    font-size: 14pt;
    font-weight: bold;
    padding: 2px 5px;
    border: 1px solid #000;
  }
  h3{
    text-align: center;
    font-size: 11pt;
    margin:0 0 3px;
    line-height: 11pt;
  }
  .lm-20{
    margin-left: 20px;
  }
  .tgl-surat{
    text-align: right;
    padding-right: 40px;
  }
  .tanda-tangan{
    margin-bottom: 10px;
  }

  .tanda-tangan tr td{
    vertical-align: bottom;
    text-align: center;
    padding: 0;
  }
  .tanda-tangan tr:first-child td{
    vertical-align: top;
    height:  62px;
  }
  .garis-nama{
    padding: 0 5px 0;
    text-align:center;
    display: inline;
    border-bottom: 1px solid #000;
    min-width: 100px;
  }
  .catatan{
    font-style: italic;
    margin-top: 10px;
    font-size: 10pt;
  }
  table.col-2 tr td{
    width: 50%;
  }
  table.col-4 tr td{
    width: 25%;
  }
  p{
    margin:5px 0;
    text-align: justify;
  }
  </style>

  </body>
  </html>
  ';
  
$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file=''.$data['nama_lengkap'].'.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}