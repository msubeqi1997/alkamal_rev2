<?php
//error_reporting(0);
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();

$idc = isset($_POST['id_item'])?$_POST['id_item']:'';
$pos = new model_konseling();
$data_super = $pos->detailSkorsing($idc);
$detail = $data_super[1];
$array = $pos->getDataSurat($detail['id_siswa']);
$data = $array[1];

$peng_markazy = $pos->getPengurusMarkazy();
$rois = $peng_markazy[1];

$html ='
  <!DOCTYPE html>
  <html lang="id">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  </head>
  <body>
	<div class="kertas">
	  <div class="kop-surat">
		<img src="../../image/kop-surat.png" alt="Kop surat">
	  </div>
		
		<div class="judul-surat">
		  
		  <h1>SURAT KEPUTUSAN ROIS MARKAZY</h1><br/>
		  <h1 class="underlined">PONDOK PESANTREN TERPADU AL KAMAL</h1>
		  <div class="nomor-surat">Nomor: '.$detail['nomor_surat'].'</div>
		</div>
		<div class="judul-surat">
		  
		  <h1>TENTANG</h1><br/>
		  <h1>SKORSING SEMENTARA SANTRI</h1>
		  
		</div>
		<br/>
		<p>Yang bertanda tangan di bawah ini Rois Markazy Pondok Pesantren Terpadu Al Kamal Kunir Wonodadi Blitar setelah: </p>
		
		<p><h2>MEMPERHATIKAN</h2></p>
		<p class="lm-15">1. Frekwensi kasus yang dilakukan oleh yang bersangkutan.</p>
		<p class="lm-15">2. Akumulasi skor pelanggaran yang dilakukan termasuk kategori berat.</p>
		<p class="lm-15">3. Surat peringatan yang sudah ditandatangani oleh santri yang bersangkutan.</p>
		<br/>
		<p><h2>MENGINGAT</h2></p>
		<p class="lm-15">1. UU/Peraturan Pondok Pensantren Terpadu Al Kamal.</p>
		<p class="lm-15">2. Demi menjaga kedisiplinan, ketertiban, dan nama baik Pondok Pesantren Terpadu Al Kamal.</p>
		<p class="lm-15">3. Surat Pernyataan Orang tua/Wali Santri dan santri dengan Pondok Pesantren Terpadu Al Kamal.</p>
		
		<br/>
		<p><h2>MEMUTUSKAN</h2></p>
		<p>Bahwa santri dengan identitas sebagai berikut:</p>
		<table class="regular lm-20">
		  <tbody>
		  <tr>
		  <td class="lwidth">Nama/NIS</td>
		  <td class="swidth">:</td>
		  <td>'.$data['nama_lengkap'].' / '.$data['nis'].'</td>
		  </tr>
		  <tr>
		  <td class="">Tempat, tanggal lahir</td>
		  <td>:</td>
		  <td>'.$data['tempat_lahir'].', </td>
		  </tr>
		  <tr>
		  <td class="">Tingkat/Sekolah</td>
		  <td>:</td>
		  <td> Tingkat '.$data['tingkat'].' / '.$data['sekolah_tujuan'].' </td>
		  </tr>
		  <tr>
		  <td class="">Jenjang</td>
		  <td>:</td>
		  <td> '.$data['jenis_jenjang'].' '.$data['jenis_kelamin'].'</td>
		  </tr>
		  <tr>
		  <td class="">Firqoh</td>
		  <td>:</td>
		  <td> '.$data['nama_asrama'].'</td>
		  </tr>
		  <tr>
		  <td class="">Orang Tua</td>
		  <td>:</td>
		  <td>'.$data['ayah'].'</td>
		  </tr>
		  <tr>
		  <td class="">Alamat</td>
		  <td>:</td>
		  <td> '.$data['alamat'].' '.ucwords(strtolower($data['kabupaten_name'])).' '.ucwords(strtolower($data['propinsi_name'])).'</td>
		  </tr>
		  </tbody>
		</table>
		<br/>
		<table class="regular">
		  <tbody>
		  <tr>
		  <td class="swidth">1. </td>
		  <td>Dilakukan skorsing dari seluruh kegiatan di Pondok Pesantren Terpadu Al Kamal selama 7(tujuh) hari, terhitung sejak tanggal '.tanggal_indo($detail['tanggal_mulai']).' sampai tanggal '.tanggal_indo($detail['tanggal_akhir']).' dengan maksud agar ada pembinaan langsung oleh orang tua/wali.</td>
		  </tr>
		  <tr>
		  <td>2. </td>
		  <td>Selama menjalani masa skorsing, santri wajib lapor ke Pondok Pesantren Terpadu Al Kamal dengan menandatangani form yang telah disediakan setelah pulang dari sekolah formal.</td>
		  </tr>
		  <tr>
		  <td>3. </td>
		  <td>Jika selama menjalani masa skorsing dan setelahnya di pandang tidak berubah menjadi lebih baik, maka dapat diterbitkan Surat Keputusan Pengasuh tetang Penyerahan Amanat Santri ke Orang tua/Wali santri.</td>
		  </tr>
		</table>
		<br/>
		<p>Demikian surat keputusan ini di buat, semoga dapat dijadikan maklum adanya.</p>
		<br/>
		<br/>
		
		<table class="tanda-tangan col-2">
		  <tr>
		  <td>Blitar, '.tanggal_indo($detail['input_at']).'<br/>Rois Pondok Pesantren Terpadu Al Kamal</td>
		  </tr>
		  <tr>
		  <td><b>'.$rois['ketua'].'</b></td>
		  </tr>
		</table>
		
	</div>

  <style>
  @page {
    /* width = 21cm */
	size: 8.5in 12.99in;
    margin: 1cm 1cm 0.5cm;

  }
  @media print{
    table{
      width: 19cm;
    }

  }
  .kertas{
    width: 100%;
    margin: 0 auto;
    font-size: 11pt;
  }
  
  .kertas.ganti-halaman{
	page-break-before: always;
  }
		  
  .kop-surat img{
    width:100%;
  }
  table{
    width: 100%;
  }
  
  .tablebordered {border-collapse:collapse; }
  .tablebordered th { color:#000; vertical-align:middle;}
  .tablebordered td, .tablebordered th { padding:3px;border:1px solid #000; }
  table tr td{
    padding: 1px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr th{
    padding: 3px 0;
    vertical-align: top;
    text-align: center;
  }
  table tr td.label{
    font-weight: bold;
    width: 30%;
  }
  table tr td.label + td{
    width: 1%;
  }
  table tr td.label + td + td{
    width: 69%;
  }
  table.formulir{
    margin-bottom: 20px;
  }
  .judul-surat{
    text-align: center;
    position: relative;
	margin-top: 10px;
  }
  .nomor-surat{
    margin:0;
  }
  
  h1{
    font-size: 12pt;
    display: inline;
    padding: 0 5px;
    margin-bottom:0;
    margin-top: 5px;
    text-transform: uppercase;
    letter-spacing: 1px;
    line-height: 11pt;
  }
  
  h2{
    font-size: 12pt;
    display: inline;
    padding: 0;
    margin-bottom:15px;
    margin-top: 0;
    text-transform: uppercase;
    letter-spacing: 1px;
    line-height: 11pt;
  }
  
  .underlined{
    border-bottom: 1px solid #000;
  }
  .tingkat-surat{
    position: absolute;
    top: 13px;
    left: 0;
    font-size: 14pt;
    font-weight: bold;
    padding: 2px 5px;
   
  }
  .pengembalian-surat{
    position: absolute;
    top: 142px;
    right: 0;
    font-size: 10pt;
	padding: 0px 5px;
	width:235px;
    border: 1px solid #dd4b39;
  }
  h3{
    text-align: center;
    font-size: 1pt;
    margin:0 0 3px;
    line-height: 11pt;
  }
  .blank{
    margin-top:40px;
  }
  .lm-15{
    margin-left: 20px;
  }
  .lm-20{
    margin-left: 20px;
  }
  
  .lwidth{
	width:140px;
  }
  .swidth{
	width:12px;
  }
  .tgl-surat{
    text-align: right;
    padding-right: 40px;
  }
  .tanda-tangan{
    margin-bottom: 10px;
  }

  .tanda-tangan tr td{
    vertical-align: bottom;
    text-align: center;
    padding: 20px 0;
  }
  .tanda-tangan tr:first-child td{
    vertical-align: top;
    height:  62px;
  }
  .garis-nama{
    padding: 0 5px 0;
    text-align:center;
    display: inline;
    border-bottom: 1px solid #000;
    min-width: 100px;
  }
  .catatan{
    font-style: italic;
    margin-top: 10px;
    font-size: 10pt;
  }
  table.col-2 tr td{
    width: 50%;
  }
  table.col-4 tr td{
    width: 25%;
  }
  p{
    margin:5px 0;
    text-align: justify;
  }
  </style>

  </body>
  </html>
  ';
 
$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file='surat_skorsing.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

function daftar_hari($hari){	
	$daftar_hari = array(
	 'Sunday' => 'Ahad',
	 'Monday' => 'Senin',
	 'Tuesday' => 'Selasa',
	 'Wednesday' => 'Rabu',
	 'Thursday' => 'Kamis',
	 'Friday' => 'Jumat',
	 'Saturday' => 'Sabtu'
	);
	return $daftar_hari[$hari];
}

