<?php
//error_reporting(0);
require_once ("../model/dbconn.php");
require_once ("../model/model_konseling.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();

$idc = isset($_POST['id_item'])?$_POST['id_item']:'';
$pos = new model_konseling();
$data_super = $pos->detailSuper($idc);
$detail = $data_super[1];
$array = $pos->getDataSurat($detail['id_siswa']);
$data = $array[1];
$kamar = explode(',', $data['pembina_kamar']);
if($data['wali']==''){ $wali_santri = $data['ayah'];} else{ $wali_santri = $data['wali']; }

$peng_firqoh = $pos->getPengurusAsrama($data['id_asrama']);
$peng_markazy = $pos->getPengurusMarkazy();
$mudabir = $peng_firqoh[1];
$rois = $peng_markazy[1];

$riwayat_pelanggaran = $pos->getRiwayatPelanggaran($detail['id_siswa']);
$riwayat = array();
$total_poin = 0;
foreach($riwayat_pelanggaran[1] as $row){
	$namahari = date('l', strtotime($row['input_at']));
	$riwayat[] ='<tr>
				  <td class="center">'.$row['urutan'].'</td>
				  <td>'.daftar_hari($namahari).' / '.date('d-m-Y', strtotime($row['input_at'])).'</td>
				  <td>'.$row['jenis_pelanggaran'].'</td>
				  <td class="center">'.$row['klasifikasi'].'</td>
				  <td class="center">'.$row['poin_pelanggaran'].'</td>
				</tr>';
	$total_poin = $total_poin + $row['poin_pelanggaran'];
}

$tingkat_sp = substr($detail['sp_ke'], 2, 1);
$amnesti = $pos->getAkumulasiAmnesti($detail['id_siswa']);
$totalakhir=$total_poin-$amnesti[1]['poin'];
$html ='
  <!DOCTYPE html>
  <html lang="id">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  </head>
  <body>
	<div class="kertas">
	  <div class="kop-surat">
		<img src="../../image/kop-surat.png" alt="Kop surat">
	  </div>
		<div class="pengembalian-surat">
			<table class="regular">
			<tr>
			<td>Diberikan tanggal</td>
			<td>:</td>
			<td> '.tanggal_indo($detail['tanggal_surat']).'</td>
			</tr>
			<tr>
			<td>Dikembalikan tanggal</td>
			<td>:</td>
			<td> '.tanggal_indo($detail['duedate']).'</td>
			</tr>
			</table>
		</div>
		<div class="judul-surat">
		  <div class="tingkat-surat">
			SP '.$tingkat_sp.'
		  </div>
		  <h1>Surat Pernyataan</h1>
		  <div class="nomor-surat">Nomor: '.$detail['nomor_sp'].'</div>
		</div>
		<p>Bismillahirrohmanirrohim <br />Dengan ini saya, </p>
		<table class="regular lm-20">
		  <tbody>
		  <tr>
		  <td class="lwidth">Nama/NIS</td>
		  <td class="swidth">:</td>
		  <td>'.$data['nama_lengkap'].' / '.$data['nis'].'</td>
		  </tr>
		  <tr>
		  <td class="">Tempat, tanggal lahir</td>
		  <td>:</td>
		  <td>'.$data['tempat_lahir'].', '.tanggal_indo($data['tanggal_lahir']).'</td>
		  </tr>
		  <tr>
		  <td class="">Tingkat/Sekolah</td>
		  <td>:</td>
		  <td> Tingkat '.$data['tingkat'].' / '.$data['sekolah_tujuan'].' </td>
		  </tr>
		  <tr>
		  <td class="">Jenjang</td>
		  <td>:</td>
		  <td> '.$data['jenis_jenjang'].' '.$data['jenis_kelamin'].'</td>
		  </tr>
		  <tr>
		  <td class="">Firqoh</td>
		  <td>:</td>
		  <td> '.$data['nama_asrama'].'</td>
		  </tr>
		  <tr>
		  <td class="">Orang Tua</td>
		  <td>:</td>
		  <td>'.$data['ayah'].'</td>
		  </tr>
		  <tr>
		  <td class="">Alamat</td>
		  <td>:</td>
		  <td> '.$data['alamat'].' '.ucwords(strtolower($data['kabupaten_name'])).' '.ucwords(strtolower($data['propinsi_name'])).'</td>
		  </tr>
		  </tbody>
		</table>
		<p>Menyatakan telah melanggar Undang – Undang / Peraturan Pondok Pesantren Terpadu Al Kamal dengan total akumulasi skor sebanyak <b>'.$detail['akumulasi_poin'].' poin</b>.</p>
		<p>Sehubungan dengan pelanggaran – pelanggaran yang telah saya lakukan, maka saya menyatakan berjanji untuk merubah sikap dan perilaku saya ke arah yang lebih baik dan tidak akan mengulangi melanggar Undang – Undang / Peraturan Pondok Pesantren Terpadu Al Kamal. Untuk selanjutnya saya bersedia menjalani sanksi yang telah menjadi keputusan Majlis Mudabbir Pondok Pesantren Terpadu Al Kamal. </p>
		<p>Demikian Surat Pernyataan ini saya buat dan saya tandatangani dalam keadaan sadar tanpa ada unsur paksaan dari pihak manapun.</p>
		<div class="tgl-surat">Blitar, '.tanggal_indo($detail['tanggal_surat']).'</div>
		<table class="tanda-tangan col-2">
		  <tr>
		  <td>Orang Tua/Wali</td>
		  <td>Yang Menyatakan</td>
		  </tr>
		  <tr>
		  <td class="bold">'.$wali_santri.'</td>
		  <td class="bold">'.$data['nama_lengkap'].'</td>
		  </tr>
		</table>
		<h3 style="text-align:left;">Saksi-saksi</h3>
		<h3>Mudabbir Al Yaumi Firqoh </h3>
		<table class="tanda-tangan col-4">
		  <tr>
		  <td>Ketua</td>
		  <td>Sekretaris</td>
		  <td>Bendahara</td>
		  
		  </tr>
		  <tr>
		  <td class="bold">'.$mudabir['ketua'].'</td>
		  <td class="bold">'.$mudabir['sekretaris'].'</td>
		  <td class="bold">'.$mudabir['bendahara_1'].'</td>
		  </tr>
		</table>

		<h3>Mengetahui, <br />	  Mudabbir Markazy PPTA</h3>
		<table class="tanda-tangan col-2">
		  <tr>
		  <td>Rois Aam</td>
		  <td>Co. Amni Markazy</td>
		  <td>Pembina Firqoh/Konselor</td>
		  </tr>
		  <tr>
		  <td class="bold">'.$rois['ketua'].'</td>
		  <td class="bold">'.$rois['keamanan_1'].'</td>
		  <td class="bold">'.$data['pembina_asrama'].'</td>
		  </tr>
		</table>';
if($tingkat_sp=='3' || $tingkat_sp=='2'){			
	$html .='	<h3>Pembina Markazy</h3>
		<table class="tanda-tangan col-2">
		  <tr>
		  <td>Pembina 1</td>
		  <td>Pembina 2</td>
		  <td>Pembina 3</td>
		  </tr>
		  <tr>
		  <td class="bold">'.$rois['pembina_1'].'</td>
		  <td class="bold">'.$rois['pembina_2'].'</td>
		  <td class="bold">'.$rois['pembina_3'].'</td>
		  </tr>
		</table>';
}
if($tingkat_sp=='3'){	
	$html .='<h3>Mengetahui, <br />	  Pengasuh PPTA</h3>
		<table class="tanda-tangan col-2 blank">
		  <tr>
		  <td class="bold">'.$rois['pengasuh_1'].'</td>
		  </tr>
		  
		</table>';
}
$html .='
	  <div class="kertas ganti-halaman">
		<div class="kop-surat">
			<img src="../../image/kop-surat.png" alt="Kop surat">
		</div>
		<div class="judul-surat">
		  <h1>LAMPIRAN SURAT PERNYATAAN</h1>
		</div>
		
		<table class="regular blank">
		  <tbody>
			  <tr>
			  <td class="lwidth">Nama/NIS</td>
			  <td class="swidth">:</td>
			  <td>'.$data['nama_lengkap'].' / '.$data['nis'].'</td>
			  </tr>
			  <tr>
			  <td class="">Tingkat/Sekolah</td>
			  <td>:</td>
			  <td> Tingkat '.$data['tingkat'].' / '.$data['sekolah_tujuan'].' </td>
			  </tr>
			  <tr>
			  <td class="">Jenjang</td>
			  <td>:</td>
			  <td> '.$data['jenis_jenjang'].' '.$data['jenis_kelamin'].'</td>
			  </tr>
			  <tr>
			  <td class="">Firqoh</td>
			  <td>:</td>
			  <td> '.$data['nama_asrama'].'</td>
			  </tr>
			  
		  </tbody>
		</table>
		
		<table class="tablebordered" style="margin-top:20px;">
		  <thead>
			<tr class="tableheader">
				<th style="width:30px">NO</th>
				<th style="width:87px">HARI/TANGGAL</th>
				<th style="width:140px">JENIS PELANGGARAN</th>
				<th style="width:140px">KLASIFIKASI PELANGGARAN</th>
				<th style="width:50px">SKOR</th>
			</tr>
		  </thead>
		  <tbody>
			'.implode($riwayat).'
			<tr>
			  <td></td>
			  <td colspan="3" style="text-align:center; font-weight:bold;"> JUMLAH POIN</td>
			  <td class="center" style="font-weight:bold;">'.$total_poin.'</td>
			</tr>
			<tr>
			  <td></td>
			  <td colspan="3" style="text-align:center; font-weight:bold;"> AMNESTI</td>
			  <td class="center" style="font-weight:bold;">'.$amnesti[1]['poin'].'</td>
			</tr>
			<tr>
			  <td></td>
			  <td colspan="3" style="text-align:center; font-weight:bold;"> TOTAL POIN SAAT INI</td>
			  <td class="center" style="font-weight:bold;">'.$totalakhir.'</td>
			</tr>
		  </tbody>
		</table>
		
		<div class="tgl-surat blank">Blitar, '.tanggal_indo($detail['tanggal_surat']).'</div>
		<table class="tanda-tangan col-2 blank">
		  <tr>
			  <td>Wali santri</td>
			  <td>Nama santri</td>
		  </tr>
		  <tr>
			  <td class="bold">'.$wali_santri.'</td>
			  <td class="bold">'.$data['nama_lengkap'].'</td>
		  </tr>
		</table>
		<br/>
		<br/>
		<div class="catatan">
		  Catatan : (1) Setiap minta tandatangan Surat Pernyataan dimasukkan dalam stop map merah dan digandakan menjadi 3 lembar. (stop map 1 /asli  dipegang oleh pelanggar, 2 diserahkan ke Idaroh Firqoh dan 3 diserahkan ke Idaroh Markaz ), serta berstempel asli Firqoh dan Markaz. (2)Jika dalam mengumpulkan SP ini melebihi batas akhir yang telah ditentukan, maka dianggap tidak serius dalam membuat pernyataan (klasifikasi kasus B)
		</div>
	  </div>
	</div>

  <style>
  @page {
    /* width = 21cm */
	size: 8.5in 12.99in;
    margin: 0.5cm 1cm 0.5cm;

  }
  @media print{
    table{
      width: 19cm;
    }

  }
  .kertas{
    width: 100%;
    margin: 0 auto;
    font-size: 11pt;
  }
  
  .kertas.ganti-halaman{
	page-break-before: always;
  }
		  
  .kop-surat img{
    width:100%;
  }
  table{
    width: 100%;
  }
  
  .tablebordered {border-collapse:collapse; }
  .tablebordered th { color:#000; vertical-align:middle;}
  .tablebordered td, .tablebordered th { padding:3px;border:1px solid #000; }
  table tr td{
    padding: 1px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr th{
    padding: 3px 0;
    vertical-align: top;
    text-align: center;
  }
  table tr td.label{
    font-weight: bold;
    width: 30%;
  }
  table tr td.label + td{
    width: 1%;
  }
  table tr td.label + td + td{
    width: 69%;
  }
  table.formulir{
    margin-bottom: 20px;
  }
  .judul-surat{
    text-align: center;
    position: relative;
	margin-top: 10px;
  }
  .nomor-surat{
    margin:0;
  }
  .judul-surat h1{
    border-bottom: 1px solid #000;
    font-size: 12pt;
    display: inline;
    padding: 0 5px;
    margin-bottom:0;
    margin-top: 5px;
    text-transform: uppercase;
    letter-spacing: 2px;
    line-height: 11pt;
  }
  .tingkat-surat{
    position: absolute;
    top: 13px;
    left: 0;
    font-size: 14pt;
    font-weight: bold;
    padding: 2px 5px;
    border: 1px solid #000000;
	width:75px;
  }
  .pengembalian-surat{
    position: absolute;
    top: 142px;
    right: 0;
    font-size: 10pt;
	padding: 0px 5px;
	width:235px;
    border: 1px solid #dd4b39;
  }
  h3{
    text-align: center;
    font-size: 11pt;
    margin:0 0 3px;
    line-height: 11pt;
	font-weight:reguler;
  }
  .bold {
	 font-weight: bold; 
  }
  .center {
	text-align:center;
  }
  .blank{
    margin-top:40px;
  }
  .lm-20{
    margin-left: 20px;
  }
  .lwidth{
	width:140px;
  }
  .swidth{
	width:12px;
  }
  .tgl-surat{
    text-align: right;
    padding-right: 40px;
  }
  .tanda-tangan{
    margin-bottom: 10px;
  }

  .tanda-tangan tr td{
    vertical-align: bottom;
    text-align: center;
    padding: 0;
  }
  .tanda-tangan tr:first-child td{
    vertical-align: top;
    height:  62px;
  }
  .garis-nama{
    border-bottom: 1px solid #000;
  }
  .catatan{
    font-style: italic;
    margin-top: 10px;
    font-size: 10pt;
  }
  table.col-2 tr td{
    width: 50%;
  }
  table.col-4 tr td{
    width: 25%;
  }
  p{
    margin:5px 0;
    text-align: justify;
  }
  </style>

  </body>
  </html>
  ';
 
$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file='surat_peringatan.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

function daftar_hari($hari){	
	$daftar_hari = array(
	 'Sunday' => 'Ahad',
	 'Monday' => 'Senin',
	 'Tuesday' => 'Selasa',
	 'Wednesday' => 'Rabu',
	 'Thursday' => 'Kamis',
	 'Friday' => 'Jumat',
	 'Saturday' => 'Sabtu'
	);
	return $daftar_hari[$hari];
}

