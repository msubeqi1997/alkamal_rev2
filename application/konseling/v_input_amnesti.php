<?php ob_start(); 
$titlepage="Amnesti Santri";
$idsmenu=42; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();
$conn = new model_data();
$asrama = $conn->getAsrama();
$jenis = $pos->getJenisPelanggaran();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	AMNESTI
	<small>form input amnesti</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
			<div class="pull-right">
			  <button type="submit" title="Buku Surat" class="btn btn-primary bg-navy" id="btnopenriwayat" ><i class="fa fa-search"></i> Buku amnesti</button> 				
			</div>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row" >
		  	<div class="col-md-6">
			  <div class="form-group">
				<label>Firqoh</label>
				<select class="form-control" id="txtasrama" name="txtasrama" style="width: 100%;">
				  <option>Pilih Firqoh</option>
				  <?php
				  foreach($asrama[1] as $opt){
					echo '<option value="'.$opt['auto'].'">'.$opt['nama_asrama'].'</option>';
				  }
				  ?>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-6">
			  <div class="form-group">
				<label>Kamar</label>
				<select class="form-control" id="txtkamar" name="txtkamar" style="width: 100%;">
				  <option value="">Pilih Firqoh dahulu</option>
				  
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-md-12 form-horizontal">
				<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_input_hukuman.php">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
	<input type="hidden" id="txtnilaipoin" name="txtnilaipoin" value="">
	<input type="hidden" id="txtjenis" name="txtjenis" value="">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-9 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Jenjang / Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi poin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtakumulasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi amnesti</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtamnesti" class="text-danger bold"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Poin total</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="pointotal" class="bold"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Jumlah amnesti</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control" name="jumlah_amnesti" id="jumlah_amnesti" required>
						<input type="hidden" id="jumlah_poin" value="0">
					   
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Tanggal</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control txtperiode tgl" name="tanggal_keluar" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
				
			</div><!-- /.col -->
			<div class="col-md-3">
				<div class="hero-widget box-profile" style="margin:0 0 10px 0">
					<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
				</div><!-- /.box -->
			</div>
			<div class="col-md-12">
				<div class="form-group">
				  <label>Keterangan</label>
				  <textarea class="form-control" rows="3" id="txtketerangan" name="txtketerangan" placeholder="..."></textarea>
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer" id="check_notif">
		  <button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

	<div id="modallasttrans" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:1140px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Buku amnesti</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" class="form-horizontal" target="_blank" action="import_data_amnesti.php" >
				  <input type="hidden" name="method" value="excel">
				  <div class="row">
					<div class="col-md-12">
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Jenjang</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenjang" name="jenjang" >
							  <option value="">Semua</option>
							  <option value="ula putra">Ula Putra</option>
							  <option value="ula putri">Ula Putri</option>
							  <option value="wustho putra">Wustho Putra</option>
							  <option value="wustho putri">Wustho Putri</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >MDK/Bukan</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenis" name="jenis" >
							  <option value="">Semua</option>
							  <option value="tidak">Bukan MDK</option>
							  <option value="mdk">MDK</option>
							</select>
						</div>
					  </div>				
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Tingkat</label>
						<div class="col-sm-3">
							<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
							  <option value="">Semua</option>
							  <option value="1">Tingkat 1</option>
							  <option value="2">Tingkat 2</option>
							  <option value="3">Tingkat 3</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >Firqoh</label>
						<div class="col-sm-3">
							<select class="form-control" id="asrama" name="asrama" >
							  <option value="">Semua</option>
							  <?php
							  foreach ($asrama[1] as $opt) {
								echo "<option value='".$opt['auto']."' ".$selected."> ".$opt['nama_asrama']." </option>";
							  }
							?>
							</select>
						</div>
						
					  </div>
					  
					  <div class="form-group"> 
						<label class="col-sm-1  control-label" >Nama/NIS</label>
						<div class="col-sm-3"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
						<div class="col-sm-6">
						  <button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
						  <button type="submit" title="Download data" class="btn btn-primary " id="btndownload" ><i class="fa fa-download"></i> Download</button>
						</div>
					  </div>
					</div>				
				  </div><!-- /.row -->
				  </form>
				  	
					<hr>
					<div class="box-body table-responsive no-padding" >
						<table id="table_last_transaction" class="table  table-bordered table-hover table-striped" >
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th>NIS</th>
									<th>Nama</th>
									<th>Firqoh</th>
									<th>Jenjang</th>
									<th>Tingkat</th>
									<th>MDK</th>
									<th>Akumulasi Poin</th>
									<th>Amnesti</th>
									<th>Poin</th>
									<th>Detail</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div id="modaldetailpelanggaran" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Detail Data pelanggaran</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <div class="row">
					<div class="col-md-9 form-horizontal">
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nomor Induk</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailnis"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nama lengkap</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailnama"></span>
							</div>
						</div>
						
						<div class="box-body table-responsive no-padding" >
							<table id="table_detail_amnesti" class="table  table-bordered table-hover table-striped" >
								<thead>
									<tr class="tableheader">
										<th style="width:30px">#</th>
										<th>Tanggal</th>
										<th>Poin amnesti</th>
										<th>Admin</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div><!-- /.col -->
				  </div><!-- /.row -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
	function goBack() {
		window.history.back();
	}

	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});

	$(document).ready( function () 
	{
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_amnesti.php", { term: $('#txtsearchitem').val(), asrama: $('#txtasrama').val(), kamar: $('#txtkamar').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl><dt>"+item.nis + " - "+item.nama+"</dt></dl>"  )
			.appendTo( ul );
		};
		
	
		$('#tanggal_keluar, #txtfirstperiod, #txtlastperiod, #tanggal_keluarsp').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		//Datemask dd/mm/yyyy
        $("#tanggal_keluar, #txtfirstperiod, #txtlastperiod, #tanggal_keluarsp").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
		$('#txtasrama').on('change', function() {
			var value = {
				asrama: this.value,
				method : "get_kamar_asrama"
			};
			$.ajax({
				url : "../pesantren/c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var respons = jQuery.parseJSON(data);
					if(respons.data.length>0){
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Pilih kamar </option>");
						$.each(respons.data, function (key, val) {
						  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
						})
					}else{
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});

	});
	
	function temptabel(id){
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_input_amnesti.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				var data = data.data;
				
				$('#txtidsiswa').val(data.uuid);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#txtjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#txtmdk').html(data.mdk);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',urls);
				akumulasi(data.uuid,data.poin);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function akumulasi(id,total){
		var value = {
			id:id,
			method : "akumulasi"
		};
		$.ajax(
		{
			url : "c_input_amnesti.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				var jumlah_poin = parseInt(total)+parseInt(data.poin);
				$('#txtamnesti').html(data.poin);
				$('#txtakumulasi').html(jumlah_poin);
				$('#pointotal').html(total);
				$('#jumlah_poin').val(jumlah_poin);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#txtnis').html('');				
		$('#txtnama').html('');
		$('#txtfirqoh').html('');
		$('#txtjenjang').html('');
		$('#txtmdk').html('');
		$('#txtakumulasi').html('');
		$('#txtamnesti').html('');
		$('#tanggal_keluar').val('');
		$('#txtketerangan').val('');
		$('#jumlah_amnesti').val('');
		$('#jumlah_poin').val('0');
		$('#pointotal').html('');
		$('#preview_siswa').attr('src', '');
	}
	
    	
	$(document).on( "click","#btnsimpan", function() {
		var token = $("#txtidsiswa").val();
		var tanggal = $("#tanggal_keluar").val();
		var jumlah_amnesti = parseInt($('#jumlah_amnesti').val());
		var jumlah_poin = parseInt($('#jumlah_poin').val());
		var note = $('#txtketerangan').val();
		
		if(token == '' || tanggal == '' || jumlah_amnesti==''){
			$.notify({
				message: "Isian bertanda * wajib diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#tanggal_keluar");
			return;
		}
		if(jumlah_amnesti > jumlah_poin){
			$.notify({
				message: "Amnesti tidak boleh lebih dari poin pelanggaran."
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#jumlah_amnesti");
			return;
		}
		
		swal({   
		title: "Simpan data amnesti santri",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			tanggal:tanggal,
			poin:jumlah_amnesti,
			note:note,
			method : "simpan_amnesti"
		  };
		  $("#btnsimpan").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_input_amnesti.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsimpan").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses simpan berhasil');
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Proses simpan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsimpan").prop('disabled', false);
			}
		  });
		});
	});
	
	$(document).on("click","#btnopenriwayat",function(){
	  $("#modallasttrans").modal("show");
	  $("#table_last_transaction tbody").html("");
	});
	
	$(document).on("click",".btndetail",function(){
		$("#modaldetailpelanggaran").modal("show");
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "detail_amnesti_siswa"
		};
		$.ajax(
		{
			url : "c_input_amnesti.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				
				$('#detailnis').html(data.nis);				
				$('#detailnama').html(data.nama);
				
				tbldetail(data.uuid);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	});
	
	function tbldetail(idsiswa){
		var par = {
			idsiswa : idsiswa,
			method : "get_detail_amnesti"
		};
		
		$("#table_detail_amnesti").DataTable().destroy();
		
		$('#table_detail_amnesti').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_amnesti.php",
				"type": "POST",
				"data":par,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "tanggal" },
			{ "data": "poin_amnesti" },
			{ "data": "admin" },
			{ "data": "edit" },
			]
		});
	}
	
	$(document).on("click","#btnfilter",function(){
		tblsearch();
	});
	
	
	function tblsearch(){
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var asrama = $('#asrama').val();
		var search = $('#txtname').val();
		var value = {
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			asrama : asrama,
			search : search,
			method : "get_riwayat_amnesti"
		};
		
		$("#table_last_transaction").DataTable().destroy();
		
		$('#table_last_transaction').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_amnesti.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "firqoh" },
			{ "data": "jenjang" },
			{ "data": "tingkat" },
			{ "data": "jenis" },
			{ "data": "total" },
			{ "data": "poin" },
			{ "data": "jumlah" },
			{ "data": "detail" },
			]
		});
	}
	
	$(document).on("click",".btndelete",function(){
		var id_item = $(this).attr('id_item');
		var id_data = $(this).attr('id_data');
		var value = {
			id_item : id_item,
			method : "delete_data"
		};
		swal({   
		title: "Hapus amnesti",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Hapus",   
		closeOnConfirm: true }, 
		function(){
		  $.ajax(
		  {
			url : "c_input_amnesti.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses hapus berhasil');
					tbldetail(id_data);				
				}else{
					$.notify({
						message: "Proses hapus gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
			}
		  });
		});
	});
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}	
	
</script>
</body>
</html>
