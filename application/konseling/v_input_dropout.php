<?php ob_start(); 
$titlepage="Dropout Santri";
$idsmenu=32; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	DROPOUT
	<small>proses dropout santri</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
			<div class="pull-right">
			  <button type="submit" title="Buku Surat" class="btn btn-primary bg-navy" id="btnopenriwayat" ><i class="fa fa-search"></i> Buku dropout</button> 				
			</div>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row" >
			<div class="col-md-12 form-horizontal">
			  <div class="form-group"> 
				<div class="col-sm-2 control-label">
					<b>Pilih Santri</b>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
				</div>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_input_dropout.php" enctype="multipart/form-data">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<input type="hidden" name="nama_siswa" id="nama_siswa" value="">
	<input type="hidden" id="method" name="method" value="simpan">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-9 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Jenjang / Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi poin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtakumulasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
					  <b>Nomor surat keputusan</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <input type="text" class="form-control" name="txtsurat" id="txtsurat" value="" required />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Tanggal surat</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control" name="txttanggal" id="txttanggal" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
				<div class="form-group"> 
					<label class="col-sm-4  control-label"><b>Berkas surat</b><span class="text-danger"> *</span></label>
					<div class="col-sm-8">
						<input type="file" name="file_attach" id="file_attach"  required />
					</div>
				</div>
			</div><!-- /.col -->	
			<div class="col-md-3">
				<div class="hero-widget box-profile" style="margin:0 0 10px 0">
					<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
				</div><!-- /.box -->
			</div>
				<div class="col-md-12">
					<div class="form-group">
					  <label>Keterangan</label>
					  <textarea class="form-control" rows="3" id="txtnote" name="txtnote" placeholder="..."></textarea>
					</div>
				</div><!-- /.col -->
			
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer">
		  <button type="submit" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

	<div id="modallasttrans" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Riwayat dropout santri</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" class="form-horizontal" target="_blank" action="import_data_dropout.php" >
				  <input type="hidden" name="method" value="excel">
				  <div class="row">
					<div class="col-md-12">
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Jenjang</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenjang" name="jenjang" >
							  <option value="">Semua</option>
							  <option value="ula putra">Ula Putra</option>
							  <option value="ula putri">Ula Putri</option>
							  <option value="wustho putra">Wustho Putra</option>
							  <option value="wustho putri">Wustho Putri</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >MDK/Bukan</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenis" name="jenis" >
							  <option value="">Semua</option>
							  <option value="tidak">Bukan MDK</option>
							  <option value="mdk">MDK</option>
							</select>
						</div>
					  </div>				
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Tingkat</label>
						<div class="col-sm-3">
							<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
							  <option value="">Semua</option>
							  <option value="1">Tingkat 1</option>
							  <option value="2">Tingkat 2</option>
							  <option value="3">Tingkat 3</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >Nama/NIS</label>
						<div class="col-sm-3"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
					  </div>
					  
					  <div class="form-group"> 
						
						<label class="col-sm-1  control-label">Tanggal</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control txtperiode tgl" id="txtfirstperiod"  name="first" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> -
						  <input type="text" class="form-control txtperiode tgl" id="txtlastperiod"  name="last" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						  <button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
						  <button type="submit" title="Download data" class="btn btn-primary " id="btndownload" ><i class="fa fa-download"></i> Download</button>
						</div>
					  </div>
					</div>				
				  </div><!-- /.row -->
				  </form>
					<hr>
					<div class="box-body table-responsive no-padding">
						<table id="table_last_transaction" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th>NIS</th>
									<th>Nama</th>
									<th>Jenjang</th>
									<th>Tingkat</th>
									<th>MDK</th>
									<th>Nomor surat</th>
									<th>Tanggal surat</th>
									<th>Admin</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}
	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});
	
	$(document).ready( function () 
	{
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_dropout.php", { term: $('#txtsearchitem').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.uuid);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl><dt>"+item.nis + " - "+item.nama+"</dt></dl>"  )
			.appendTo( ul );
		};
		
		$('#txttanggal, #txtfirstperiod, #txtlastperiod').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		//Datemask dd/mm/yyyy
        $("#txttanggal, #txtfirstperiod, #txtlastperiod").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	});
	
	
	function temptabel(id){
		var sp = $('#txtsuper').val();
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_input_dropout.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				var data = data.data;
				
				$('#txtidsiswa').val(data.uuid);				
				$('#nama_siswa').val(data.nama_lengkap);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtakumulasi').html(data.poin);					
				$('#txtpoin').val(data.poin);					
				$('#txtfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#txtjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#txtmdk').html(data.mdk);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',urls);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#nama_siswa').val('');				
		$('#txtpoin').val('');				
		$('#txtnis').html('');				
		$('#txtnama').html('');
		$('#txtakumulasi').html('');
		$('#txtfirqoh').html('');
		$('#txtjenjang').html('');
		$('#txtmdk').html('');
		$('#txttanggal').val('');
		$('#txtnote').val('');
		$('#file_attach').val('');
		$('#txtsurat').val('');
		$('#preview_siswa').attr('src', '');
	}
	
	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#target").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin dropout santri?",
			function (result) {
			  if (result == true) {
				$("#btnsimpan").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Dropout santri berhasil.");
		$("#btnsimpan").prop('disabled', false);
		newitem();
	  }
	  else{
		bootbox.alert(msg);
		$("#btnsimpan").prop('disabled', false);
		newitem();
	  }
	};
	
	function tblsearch(){
		var first = $("#txtfirstperiod").val();
		var last = $("#txtlastperiod").val();
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var search = $('#txtname').val();
		var value = {
			first : first,
			last : last,
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			search : search,
			method : "get_riwayat_surat"
		};
		
		$("#table_last_transaction").DataTable().destroy();
		
		$('#table_last_transaction').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_dropout.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "jenjang" },
			{ "data": "tingkat" },
			{ "data": "jenis" },
			{ "data": "nomor" },
			{ "data": "tanggal" },
			{ "data": "admin" },
			{ "data": "button" },
			]
		});
	}
	$(document).on("click","#btnopenriwayat",function(){
	  $("#modallasttrans").modal("show");
	  $("#table_last_transaction tbody").html("");
	});
	$(document).on("click","#btnfilter",function(){
		tblsearch();
	});
	
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
</script>
</body>
</html>
