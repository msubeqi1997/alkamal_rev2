<?php ob_start(); 
$titlepage="Pelanggaran Santri";
$idsmenu=29; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();
$conn = new model_data();
$asrama = $conn->getAsrama();
$jenis = $pos->getJenisPelanggaran();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	HUKUMAN
	<small>form input pelanggaran</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
			<div class="pull-right">
			  <button type="submit" title="Buku Surat" class="btn btn-primary bg-navy" id="btnopenriwayat" ><i class="fa fa-search"></i> Data pelanggaran</button> 				
			  <button type="button" title="Poin santri" class="btn btn-primary bg-navy" id="btndatapoin" ><i class="fa fa-download"></i> Poin Pelanggaran</button> 				
			</div>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row" >
		  	<div class="col-md-6">
			  <div class="form-group">
				<label>Firqoh</label>
				<select class="form-control" id="txtasrama" name="txtasrama" style="width: 100%;">
				  <option>Pilih Firqoh</option>
				  <?php
				  foreach($asrama[1] as $opt){
					echo '<option value="'.$opt['auto'].'">'.$opt['nama_asrama'].'</option>';
				  }
				  ?>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-6">
			  <div class="form-group">
				<label>Kamar</label>
				<select class="form-control" id="txtkamar" name="txtkamar" style="width: 100%;">
				  <option value="">Pilih Firqoh dahulu</option>
				  
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  <div class="row">
			<div class="col-md-12 form-horizontal">
				<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_input_hukuman.php">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
	<input type="hidden" id="txtnilaipoin" name="txtnilaipoin" value="">
	<input type="hidden" id="txtjenis" name="txtjenis" value="">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-9 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Jenjang / Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi poin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtakumulasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Level SP</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtlevelsp" class="text-danger bold"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Tanggal</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control txtperiode tgl" name="tanggal_keluar" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Jenis pelanggaran</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-6">
						<select class="form-control" id="txtpelanggaran" name="txtpelanggaran" style="width: 100%;" required>
						  <option value="">Jenis pelanggaran</option>
						  <?php
						  foreach($jenis[1] as $row){
							echo '<option value="'.$row['auto'].'">'.$row['jenis'].'</option>';
						  }
						  ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Klasifikasi pelanggaran</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-6">
					  <b>: </b> <span id="txtklasifikasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Poin pelanggaran</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-6">
					  <b>: </b> <span id="txtpoin"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Takzir</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-8">
					  <input type="text" class="form-control" name="txttakzir" id="txttakzir"  value="">
					</div>
				</div>
			</div><!-- /.col -->
			<div class="col-md-3">
				<div class="hero-widget box-profile" style="margin:0 0 10px 0">
					<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
				</div><!-- /.box -->
			</div>
			<div class="col-md-12">
				<div class="form-group">
				  <label>Kronologi</label>
				  <textarea class="form-control" rows="3" id="txtkronologi" name="kronologi" placeholder="..."></textarea>
				</div>
				<div class="form-group">
				  <label>Bimbingan</label>
				  <textarea class="form-control" rows="3" id="txtbimbingan" name="bimbingan" placeholder="..."></textarea>
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer" id="check_notif">
		  <button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

	<div id="modallasttrans" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:1140px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Data pelanggaran</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" class="form-horizontal" target="_blank" action="import_data_pelanggaran.php" >
				  <input type="hidden" name="method" value="excel">
				  <div class="row">
					<div class="col-md-12">
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Jenjang</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenjang" name="jenjang" >
							  <option value="">Semua</option>
							  <option value="ula putra">Ula Putra</option>
							  <option value="ula putri">Ula Putri</option>
							  <option value="wustho putra">Wustho Putra</option>
							  <option value="wustho putri">Wustho Putri</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >MDK/Bukan</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenis" name="jenis" >
							  <option value="">Semua</option>
							  <option value="tidak">Bukan MDK</option>
							  <option value="mdk">MDK</option>
							</select>
						</div>
					  </div>				
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Tingkat</label>
						<div class="col-sm-3">
							<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
							  <option value="">Semua</option>
							  <option value="1">Tingkat 1</option>
							  <option value="2">Tingkat 2</option>
							  <option value="3">Tingkat 3</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >Firqoh</label>
						<div class="col-sm-3">
							<select class="form-control" id="asrama" name="asrama" >
							  <option value="">Semua</option>
							  <?php
							  foreach ($asrama[1] as $opt) {
								echo "<option value='".$opt['auto']."' ".$selected."> ".$opt['nama_asrama']." </option>";
							  }
							?>
							</select>
						</div>
						
					  </div>
					  
					  <div class="form-group"> 
						<label class="col-sm-1  control-label" >Nama/NIS</label>
						<div class="col-sm-3"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
						<label class="col-sm-1  control-label">Tanggal</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control txtperiode tgl" id="txtfirstperiod"  name="first" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> -
						  <input type="text" class="form-control txtperiode tgl" id="txtlastperiod"  name="last" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						  <button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
						  <button type="submit" title="Download data" class="btn btn-primary " id="btndownload" ><i class="fa fa-download"></i> Download</button>
						</div>
					  </div>
					</div>				
				  </div><!-- /.row -->
				  </form>
				  	
					<hr>
					<div class="box-body table-responsive no-padding" >
						<table id="table_last_transaction" class="table  table-bordered table-hover table-striped" >
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th>Tanggal</th>
									<th>NIS</th>
									<th>Nama</th>
									<th>Firqoh</th>
									<th>Jenjang</th>
									<th>Tingkat</th>
									<th>MDK</th>
									<th>Pelanggaran</th>
									<th>Takzir</th>
									<th>Admin</th>
									<th>Detail</th>
									<th style="width:70px">Edit</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div id="modaldetailpelanggaran" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Detail Data pelanggaran</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <div class="row">
					<div class="col-md-9 form-horizontal">
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nomor Induk</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailnis"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nama lengkap</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailnama"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Firqoh</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailfirqoh"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Jenjang / Tingkat</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailjenjang"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>MDK</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailmdk"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Tanggal</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailtanggal"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Jenis pelanggaran</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailjenispelanggaran"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Klasifikasi pelanggaran</b>
							</div>
							<div class="col-sm-6">
							  <b>: </b> <span id="detailklasifikasi"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Poin pelanggaran</b>
							</div>
							<div class="col-sm-6">
							  <b>: </b> <span id="detailpoin"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Takzir</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailtakzir"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Kronologi</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailkronologi"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Bimbingan</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="detailbimbingan"></span>
							</div>
						</div>
					</div><!-- /.col -->
				  </div><!-- /.row -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div id="modalshortcutsp" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Input Surat Peringatan</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <div class="row">
					<div class="col-md-9 form-horizontal">
						<input type="hidden" id="txtidsiswasp" name="idsiswa" class="" value="">
						<input type="hidden" id="txtlevelsp" name="txtlevelsp" value="">
						<input type="hidden" id="txtpoinsp" name="txtpoin" value="">
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nomor Induk</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spnis"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Nama lengkap</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spnama"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Firqoh</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spfirqoh"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Jenjang / Tingkat</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spjenjang"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>MDK</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spmdk"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Level SP</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="splevel"></span>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Akumulasi Poin</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spakumulasipoin"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 control-label">
								<b>Tanggal surat</b><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-4">
							  <div class="input-group date" id="datepicker">
								<input type="text" class="form-control txtperiode tgl" name="tanggal_keluar" id="tanggal_keluarsp" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-4">
								<b>Due date</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="spduedate"></span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
							  <label>Keterangan</label>
							  <textarea class="form-control" rows="3" id="txtnotesp" name="txtnotesp" placeholder="..."></textarea>
							</div>
						</div><!-- /.col -->
					</div><!-- /.col -->
				  </div><!-- /.row -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="btnsimpansp" class="btn btn-info pull-right"><i class="fa fa-save"></i> Simpan </button>
					<span id="infoproses"></span>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
	function goBack() {
		window.history.back();
	}

	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});

	$(document).ready( function () 
	{
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_search.php", { term: $('#txtsearchitem').val(), asrama: $('#txtasrama').val(), kamar: $('#txtkamar').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl><dt>"+item.nis + " - "+item.nama+"</dt></dl>"  )
			.appendTo( ul );
		};
		
		$( "#txttakzir" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_takzir.php", { term: $('#txttakzir').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					$("#txttakzir").val(ui.item.hukuman);
					return false;
				}
		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl>"+item.hukuman +"</dl>"  )
			.appendTo( ul );
		};
		
		
	
		$('#tanggal_keluar, #txtfirstperiod, #txtlastperiod, #tanggal_keluarsp').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		//Datemask dd/mm/yyyy
        $("#tanggal_keluar, #txtfirstperiod, #txtlastperiod, #tanggal_keluarsp").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
		$('#txtasrama').on('change', function() {
			var value = {
				asrama: this.value,
				method : "get_kamar_asrama"
			};
			$.ajax({
				url : "../pesantren/c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var respons = jQuery.parseJSON(data);
					if(respons.data.length>0){
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Pilih kamar </option>");
						$.each(respons.data, function (key, val) {
						  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
						})
					}else{
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});

		$('#txtpelanggaran').on('change', function() {
			var value = {
				id: this.value,
				method : "get_detail_jenis"
			};
			$.ajax({
				url : "c_input_hukuman.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					var data = data.data;
					
					$('#txtpoin').html('');
					$('#txtklasifikasi').html('');
					$('#txttakzir').val('');
					$('#txtpoin').html(data.poin);
					$('#txtklasifikasi').html(data.klasifikasi);
					$('#txtjenis').val(data.jenis);
					$('#txtnilaipoin').val(data.poin);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
	});
	
	function temptabel(id){
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				var data = data.data;
				
				$('#txtidsiswa').val(data.uuid);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#txtjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#txtmdk').html(data.mdk);
				$('#txtakumulasi').html(data.poin);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',urls);
				
				levelsp(data.uuid,data.poin);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function levelsp(id,val){
		var value = {
			id:id,
			val: val,
			method : "level_sp"
		};
		$.ajax(
		{
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				
				$('#txtlevelsp').html("<b>"+data.level+"</b></br>&nbsp;&nbsp; "+data.notif);
				if(data.check_sp == false && data.level != ''){
					$('#check_notif').html('');
					
					if(data.level == 'SKORSING'){
					  if(data.cetak == 'sudah'){
						$('#check_notif').append('<button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>');
					  }else {
						$('#check_notif').html('Silahkan proses '+data.level+' di halaman input '+data.level+' untuk siswa tersebut');	
					  }
					}else if(data.level == 'DROPOUT'){
					  $('#check_notif').html('Silahkan proses '+data.level+' di halaman input '+data.level+' untuk siswa tersebut');	
					  $('#check_notif').append('<button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>');
					}
					else{
					  $('#check_notif').html('Silahkan cetak '+data.level+' dahulu untuk input pelanggaran selanjutnya');	
					  $('#check_notif').append('<button type="button" title="Input SP" id_siswa="'+id+'" class="btn btn-primary pull-right" id="inputsp" ><i class="fa fa-edit"></i> Input SP </button>');
					}
					
				}else{
					$('#check_notif').html('');
					$('#check_notif').append('<button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>');
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function cek_akumulasi(id,poin){
		var value = {
			id:id,
			val:poin,
			method : "level_sp"
		};
		$.ajax(
		{
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				
				if(data.check_sp == false && data.level != ''){
					if(data.level == 'SKORSING' || data.level == 'DROPOUT'){
					  
					}else{inputsp(id);}
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#txtnis').html('');				
		$('#txtnama').html('');
		$('#txtfirqoh').html('');
		$('#txtjenjang').html('');
		$('#txtmdk').html('');
		$('#txtlevelsp').html('');
		$('#txtakumulasi').html('');
		$('#tanggal_keluar').val('');
		$("#txtpelanggaran option[value='']").prop('selected', true);
		$('#txtpoin').html('');
		$('#txtklasifikasi').html('');
		$('#txttakzir').val('');
		$('#txtkronologi').val('');
		$('#txtbimbingan').val('');
		$('#txtjenis').val('');
		$('#txtnilaipoin').val('');
		$('#preview_siswa').attr('src', '');
		$('#check_notif').html('');
		$('#check_notif').append('<button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>');
	}
	
	function newsp(){
		$('#txtidsiswasp').val('');				
		$('#txtlevelsp').val('');				
		$('#txtpoinsp').val('');				
		$('#spnis').html('');				
		$('#spnama').html('');
		$('#spfirqoh').html('');
		$('#spjenjang').html('');
		$('#spmdk').html('');
		$('#splevel').html('');
		$('#spakumulasipoin').html('');
		$('#tanggal_keluarsp').val('');
		$('#spduedate').html('');
		$('#txtnotesp').val('');
	}
    	
	$(document).on( "click","#btnsimpan", function() {
		var token = $("#txtidsiswa").val();
		var tanggal = $("#tanggal_keluar").val();
		var takzir = $('#txttakzir').val();
		var kronologi = $('#txtkronologi').val();
		var bimbingan = $('#txtbimbingan').val();
		var jenis = $('#txtjenis').val();
		var poin = $('#txtnilaipoin').val();
		var klasifikasi = $('#txtklasifikasi').html();
		
		if(token == '' || tanggal == '' || takzir=='' || jenis ==''){
			$.notify({
				message: "Isian bertanda * wajib diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtpelanggaran");
			return;
		}
		
		swal({   
		title: "Simpan data pelanggaran santri",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			tanggal:tanggal,
			takzir:takzir,
			kronologi:kronologi,
			bimbingan:bimbingan,
			jenis:jenis,
			klasifikasi:klasifikasi,
			poin:poin,
			method : "simpan_hukuman"
		  };
		  $("#btnsimpan").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsimpan").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses simpan berhasil');
					newitem();
					set_focus("#txtsearchitem");
					cek_akumulasi(token,data.poin);
				}else{
					$.notify({
						message: "Proses simpan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsimpan").prop('disabled', false);
			}
		  });
		});
	});
	
	$(document).on("click","#btnopenriwayat",function(){
	  $("#modallasttrans").modal("show");
	  $("#table_last_transaction tbody").html("");
	});
	
	$(document).on("click",".btndetail",function(){
		$("#modaldetailpelanggaran").modal("show");
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "detail_pelanggaran_siswa"
		};
		$.ajax(
		{
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				
				$('#detailnis').html(data.nis);				
				$('#detailnama').html(data.nama);
				$('#detailfirqoh').html(data.firqoh);
				$('#detailjenjang').html(data.jenjang);
				$('#detailmdk').html(data.mdk);
				$('#detailtanggal').html(data.tanggal);
				$('#detailjenispelanggaran').html(data.jenis);
				$('#detailklasifikasi').html(data.klasifikasi);
				$('#detailpoin').html(data.poin);
				$('#detailtakzir').html(data.takzir);
				$('#detailkronologi').html(data.kronologi);
				$('#detailbimbingan').html(data.bimbingan);
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	});
	
	$(document).on("click","#btnfilter",function(){
		tblsearch();
	});
	
	$(document).on("click","#inputsp",function(){
		var id_item = $(this).attr('id_siswa');
		inputsp(id_item);
	});
	
	$('#tanggal_keluarsp').on('blur', function() {
		var array = this.value.split("-");
		var dt = new Date(array[1]+"/"+array[0]+"/"+array[2]);
		
		dt.setDate( dt.getDate() + 7 );
		var dd = dt.getDate();
		var mm = dt.getMonth() + 1;
		var y = dt.getFullYear();

		var someFormattedDate = ("0" + dd).slice(-2)+ '-' + ("0" + mm).slice(-2)+ '-' + y;
		$('#spduedate').html(someFormattedDate);
	});
	
	$(document).on( "click","#btnsimpansp", function() {
		var token = $("#txtidsiswasp").val();
		var tanggal = $("#tanggal_keluarsp").val();
		var duedate = $("#spduedate").html();
		var note = $('#txtnotesp').val();
		var sp = $('#txtlevelsp').val();
		var poin = $('#txtpoinsp').val();
		
		if(token == '' || tanggal == ''){
			$.notify({
				message: "Isian bertanda * wajib diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			return;
		}
		
		swal({   
		title: "Simpan surat peringatan",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			tanggal:tanggal,
			duedate:duedate,
			note:note,
			sp:sp,
			poin:poin,
			method : "simpan_super"
		  };
		  $("#btnsimpan").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsimpansp").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					
					$.notify('Proses simpan berhasil');
					printpdf(data.token);
					$("#modalshortcutsp").modal("hide");
					newsp();
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Proses simpan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsimpansp").prop('disabled', false);
			}
		  });
		});
	});
	
	function tblsearch(){
		var first = $("#txtfirstperiod").val();
		var last = $("#txtlastperiod").val();
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var asrama = $('#asrama').val();
		var search = $('#txtname').val();
		var value = {
			first : first,
			last : last,
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			asrama : asrama,
			search : search,
			method : "get_riwayat_pelanggaran"
		};
		
		$("#table_last_transaction").DataTable().destroy();
		
		$('#table_last_transaction').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_hukuman.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "tanggal" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "firqoh" },
			{ "data": "jenjang" },
			{ "data": "tingkat" },
			{ "data": "jenis" },
			{ "data": "jenis_pelanggaran" },
			{ "data": "hukuman" },
			{ "data": "admin" },
			{ "data": "detail" },
			{ "data": "button" },
			]
		});
	}
	
	$(document).on("click",".btndelete",function(){
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "delete_data"
		};
		swal({   
		title: "Hapus data pelanggaran",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Hapus",   
		closeOnConfirm: true }, 
		function(){
		  $.ajax(
		  {
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses hapus berhasil');
					tblsearch();				
				}else{
					$.notify({
						message: "Proses hapus gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});
					set_focus("#txtname");
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
			}
		  });
		});
	});
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
	
	function printpdf(token) {
		var id_item = token;
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "pdf_test.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	}
	
	function inputsp(id){
		$("#modalshortcutsp").modal("show");
		var id_item = id;
		var value = {
			siswa_id : id_item,
			method : "select_siswa_sp"
		};
		$.ajax(
		{
			url : "c_input_hukuman.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newsp();
				var rest = jQuery.parseJSON(data);
				var data = rest.data;
				
				$('#txtidsiswasp').val(data.uuid);				
				$('#txtlevelsp').val(rest.level);				
				$('#txtpoinsp').val(data.poin);				
				$('#spnis').html(data.nis);				
				$('#spnama').html(data.nama_lengkap);
				$('#spfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#spjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#spmdk').html(data.mdk);
				$('#spakumulasipoin').html(data.poin);
				$('#splevel').html(rest.level);
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	$(document).on( "click","#btndatapoin", function() {
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_data_poin_santri.php";

		document.body.appendChild(mapForm);
		mapForm.submit();
	});
</script>
</body>
</html>
