<?php ob_start(); 
$titlepage="Skorsing Santri";
$idsmenu=31; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();
$conn = new model_data();
$asrama = $conn->getAsrama();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	SKORSING
	<small>release surat skorsing</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
			<div class="pull-right">
			  <button type="submit" title="Buku Surat" class="btn btn-primary bg-navy" id="btnopenriwayat" ><i class="fa fa-search"></i> Buku skorsing</button> 				
			</div>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row" >
			<div class="col-md-12 form-horizontal">
			  <div class="form-group"> 
				<div class="col-sm-2 control-label">
					<b>Pilih Santri</b>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
				</div>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_input_super.php">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<input type="hidden" id="txtpoin" name="txtpoin" value="">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-9 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi poin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtakumulasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Jenjang / Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Tanggal skorsing</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control" name="txtfirstperiod" id="txtfirstperiod" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Akhir skorsing</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-8">
					  <b>: </b> <span id="spduedate"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
					  <b>Nomor surat</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <input type="text" class="form-control" name="txtsurat" id="txtsurat" value="" required>
					</div>
				</div>
			</div><!-- /.col -->
			<div class="col-md-3">
				<div class="hero-widget box-profile" style="margin:0 0 10px 0">
					<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
				</div><!-- /.box -->
			</div>
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer">
		  <button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

	<div id="modallasttrans" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:1140px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Riwayat surat skorsing</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" class="form-horizontal" target="_blank" action="import_surat_skorsing.php" >
				  <input type="hidden" name="method" value="excel">
				  <div class="row">
					<div class="col-md-12">
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Jenjang</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenjang" name="jenjang" >
							  <option value="">Semua</option>
							  <option value="ula putra">Ula Putra</option>
							  <option value="ula putri">Ula Putri</option>
							  <option value="wustho putra">Wustho Putra</option>
							  <option value="wustho putri">Wustho Putri</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >MDK/Bukan</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenis" name="jenis" >
							  <option value="">Semua</option>
							  <option value="tidak">Bukan MDK</option>
							  <option value="mdk">MDK</option>
							</select>
						</div>
					  </div>				
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Tingkat</label>
						<div class="col-sm-3">
							<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
							  <option value="">Semua</option>
							  <option value="1">Tingkat 1</option>
							  <option value="2">Tingkat 2</option>
							  <option value="3">Tingkat 3</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >Firqoh</label>
						<div class="col-sm-3">
							<select class="form-control" id="asrama" name="asrama" >
							  <option value="">Semua</option>
							  <?php
							  foreach ($asrama[1] as $opt) {
								echo "<option value='".$opt['auto']."' ".$selected."> ".$opt['nama_asrama']." </option>";
							  }
							?>
							</select>
						</div>
						
					  </div>
					  
					  <div class="form-group"> 
						<label class="col-sm-1  control-label" >Nama/NIS</label>
						<div class="col-sm-3"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
						<label class="col-sm-1  control-label">Tanggal</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control txtperiode tgl" id="txtfirstperiod"  name="first" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> -
						  <input type="text" class="form-control txtperiode tgl" id="txtlastperiod"  name="last" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						  <button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
						  <button type="submit" title="Download data" class="btn btn-primary " id="btndownload" ><i class="fa fa-download"></i> Download</button>
						</div>
					  </div>
					</div>				
				  </div><!-- /.row -->
				  </form>
					<hr>
					<div class="box-body table-responsive no-padding">
						<table id="table_last_transaction" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th>NIS</th>
									<th>Nama</th>
									<th>Firqoh</th>
									<th>Jenjang</th>
									<th>Tingkat</th>
									<th>MDK</th>
									<th>Nomor surat</th>
									<th>Mulai</th>
									<th>Sampai</th>
									<th>Terima</th>
									<th>Admin</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
		
	<div id="modalattachment" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Upload absensi skorsing</h4>
				</div>
				<!--modal header-->
				<form method="post" id="attachment" action="c_input_skorsing.php" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9 form-horizontal">
						  
							<input type="hidden" name="method" class="" value="attachment">
							<input type="hidden" name="id_item" id="txtiditem" class="" value="">
							<input type="hidden" name="nama_siswa" id="attach_siswa" value="">
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nomor Induk</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachnis"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nama lengkap</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachnama"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nomor Surat</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachnomor"></span>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-sm-3  control-label"><b>File</b></label>
								<div class="col-sm-8">
									<input type="file" name="file_attach" id="file_attach"  />
								</div>
							</div>
						  
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btnattachsave" class="btn btn-info pull-right"><i class="fa fa-save"></i> Simpan </button>
					<span id="infoproses"></span>
					
				</div>
				</form>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">              
		  <div class="modal-body">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<img src="" class="imagepreview" style="width: 100%;" >
		  </div>
		</div>
	  </div>
	</div>
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}
	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});
	
	$(document).ready( function () 
	{
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_skorsing.php", { term: $('#txtsearchitem').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.uuid);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl><dt>"+item.nis + " - "+item.nama+"</dt></dl>"  )
			.appendTo( ul );
		};
		
		$('#txtfirstperiod, #txtlastperiod, #txtfirstsearch, #txtlastsearch').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		//Datemask dd/mm/yyyy
        $("#txtfirstperiod, #txtlastperiod, #txtfirstsearch, #txtlastsearch").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	});
	
	function temptabel(id){
		var sp = $('#txtsuper').val();
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_input_skorsing.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				var data = data.data;
				
				$('#txtidsiswa').val(data.uuid);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtakumulasi').html(data.poin);					
				$('#txtpoin').val(data.poin);					
				$('#txtfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#txtjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#txtmdk').html(data.mdk);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',urls);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
	
	function fullimage(id){
		//var x = document.getElementsById().getAttribute("class");
		$('.imagepreview').attr('src', $(id).attr('src'));
		$('#imagemodal').modal('show');   
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#txtpoin').val('');				
		$('#txtnis').html('');				
		$('#txtnama').html('');
		$('#txtakumulasi').html('');
		$('#txtfirqoh').html('');
		$('#txtjenjang').html('');
		$('#txtmdk').html('');
		$('#spduedate').html('');
		$('#txtfirstperiod').val('');
		$('#txtsurat').val('');
		$('#preview_siswa').attr('src', '');
	}
    
	$('#txtfirstperiod').on('blur', function() {
		var array = this.value.split("-");
		var dt = new Date(array[1]+"/"+array[0]+"/"+array[2]);
		
		dt.setDate( dt.getDate() + 7 );
		var dd = dt.getDate();
		var mm = dt.getMonth() + 1;
		var y = dt.getFullYear();

		var someFormattedDate = dd + '-' + mm + '-' + y;
		$('#spduedate').html(someFormattedDate);
	});
	
	$(document).on( "click","#btnsimpan", function() {
		var token = $("#txtidsiswa").val();
		var first = $("#txtfirstperiod").val();
		var nomor = $('#txtsurat').val();
		var poin = $('#txtpoin').val();
		
		if(token == '' || token == null){
			$.notify({
				message: "Santri belum dipilih"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsearchitem");
			return;
		}
		
		if(first == ''){
			$.notify({
				message: "Tanggal skorsing belum diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtfirstperiod");
			return;
		}
		
		if(nomor == '' || nomor == null){
			$.notify({
				message: "Nomor surat skorsing belum diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsurat");
			return;
		}
		
		swal({   
		title: "Simpan surat skorsing",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			first:first,
			nomor:nomor,
			poin:poin,
			method : "simpan"
		  };
		  $("#btnsimpan").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_input_skorsing.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsimpan").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses simpan berhasil');
					printpdf(data.token);
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Proses simpan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsimpan").prop('disabled', false);
			}
		  });
		});
	});
	
	function tblsearch(){
		var first = $("#txtfirstperiod").val();
		var last = $("#txtlastperiod").val();
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var asrama = $('#asrama').val();
		var search = $('#txtname').val();
		var value = {
			first : first,
			last : last,
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			asrama : asrama,
			search : search,
			method : "get_riwayat_surat"
		};
		
		$("#table_last_transaction").DataTable().destroy();
		
		$('#table_last_transaction').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_skorsing.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "firqoh" },
			{ "data": "jenjang" },
			{ "data": "tingkat" },
			{ "data": "jenis" },
			{ "data": "nomor_surat" },
			{ "data": "first" },
			{ "data": "last" },
			{ "data": "terima" },
			{ "data": "admin" },
			{ "data": "button" },
			]
		});
	}
	
	$('#txtfirstperiodx').on('blur', function() {
		var array = this.value.split("-");
		var dt = new Date(array[1]+"/"+array[0]+"/"+array[2]);
		
		dt.setDate( dt.getDate() + 7 );
		var dd = dt.getDate();
		var mm = dt.getMonth() + 1;
		var y = dt.getFullYear();

		var someFormattedDate = ("0" + dd).slice(-2)+ '-' + ("0" + mm).slice(-2)+ '-' + y;
		$('#txtlastperiod').val(someFormattedDate);
	});
	
	$(document).on("click","#btnopenriwayat",function(){
	  $("#modallasttrans").modal("show");
	  $("#table_last_transaction tbody").html("");
	});
	$(document).on("click","#btnfilter",function(){
		tblsearch();
	});
	
	$(document).on("click",".btndelete",function(){
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "delete_skorsing"
		};
		swal({   
		title: "Hapus surat skorsing",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Hapus",   
		closeOnConfirm: true }, 
		function(){
		  $.ajax(
		  {
			url : "c_input_skorsing.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses hapus berhasil');
					tblsearch();				
				}else{
					$.notify({
						message: "Proses hapus gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});
					set_focus("#txtname");
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
			}
		  });
		});
	});

	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "pdf_surat_skorsing.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
	
	$(document).on("click",".btnupload",function(){
		$("#modalattachment").modal("show");
		var id = $(this).attr('id_item');
		$('#attachnis').html('');
		$('#attachnama').html('');
		$('#attachnomor').html('');
		$('#file_attach').val('');
		$('#txtiditem').val('');
		$("#btnattachsave").prop('disabled', false);
		var nis = $(this).data('nis');
		var nama = $(this).data('nama');
		var nomor = $(this).data('nomor');
		$('#attachnis').html(nis);
		$('#attachnama').html(nama);
		$('#attachnomor').html(nomor);
		$('#txtiditem').val(id);
		$('#attach_siswa').val(nama);
		
	});
	
	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#attachment").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin upload surat?",
			function (result) {
			  if (result == true) {
				$("#btnattachsave").prop('disabled', true);
				$('#attachment').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. upload surat selesai.");
		$("#modalattachment").modal("hide");
		tblsearch();
	  }
	  else{
		bootbox.alert(msg);
		$("#btnattachsave").prop('disabled', false);
	  }
	}
	
	function printpdf(token) {
		var id_item = token;
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "pdf_surat_skorsing.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	}
</script>
</body>
</html>
