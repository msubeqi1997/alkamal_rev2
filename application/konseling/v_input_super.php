<?php ob_start(); 
$titlepage="Surat Peringatan";
$idsmenu=30; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();
$conn = new model_data();
$asrama = $conn->getAsrama();
$jenis = $pos->getJenisPelanggaran();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	SURAT PERINGATAN
	<small>release surat peringatan</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
			<div class="pull-right">
			  <button type="submit" title="Buku Surat" class="btn btn-primary bg-navy" id="btnopenriwayat" ><i class="fa fa-search"></i> Buku surat</button> 				
			</div>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row" >
			<div class="col-md-12 form-horizontal">
			  <div class="form-group"> 
				<div class="col-sm-2 control-label">
					<b>Surat peringatan</b>
				</div>
				<div class="col-sm-8">
					<select class="form-control" id="txtsuper" name="txtsuper" style="width: 100%;" required>
					  <option value="">Surat peringatan</option>
					  <option value="1">Surat peringatan 1</option>
					  <option value="2">Surat peringatan 2</option>
					  <option value="3">Surat peringatan 3</option>
					</select>
				</div>
			  </div>
			  <div class="form-group"> 
				<div class="col-sm-2 control-label">
					<b>Pilih Santri</b>
				</div>
				<div class="col-sm-8">
					<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
				</div>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_input_super.php">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<input type="hidden" id="txtsp" name="txtsp" value="">
	<input type="hidden" id="txtpoin" name="txtpoin" value="">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-9 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Jenjang / Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Level SP</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtlevelsp"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Akumulasi poin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtakumulasi"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4 control-label">
						<b>Tanggal surat</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control txtperiode tgl" name="tanggal_keluar" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Due date</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="spduedate"></span>
					</div>
				</div>
			</div><!-- /.col -->
			<div class="col-md-3">
				<div class="hero-widget box-profile" style="margin:0 0 10px 0">
					<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
				</div><!-- /.box -->
			</div>
			<div class="col-md-12">
				<div class="form-group">
				  <label>Keterangan</label>
				  <textarea class="form-control" rows="3" id="txtnote" name="txtnote" placeholder="..."></textarea>
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer" id="check_notif">
		  <button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

	<div id="modallasttrans" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:1240px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Riwayat surat peringatan</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" class="form-horizontal" target="_blank" action="import_surat_peringatan.php" >
				  <input type="hidden" name="method" value="excel">
				  <div class="row">
					<div class="col-md-12">
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Jenjang</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenjang" name="jenjang" >
							  <option value="">Semua</option>
							  <option value="ula putra">Ula Putra</option>
							  <option value="ula putri">Ula Putri</option>
							  <option value="wustho putra">Wustho Putra</option>
							  <option value="wustho putri">Wustho Putri</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >MDK/Bukan</label>
						<div class="col-sm-3">
							<select class="form-control" id="jenis" name="jenis" >
							  <option value="">Semua</option>
							  <option value="tidak">Bukan MDK</option>
							  <option value="mdk">MDK</option>
							</select>
						</div>
					  </div>				
					  <div class="form-group"> 
						<label class="col-sm-1  control-label">Tingkat</label>
						<div class="col-sm-3">
							<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
							  <option value="">Semua</option>
							  <option value="1">Tingkat 1</option>
							  <option value="2">Tingkat 2</option>
							  <option value="3">Tingkat 3</option>
							</select>
						</div>
						<label class="col-sm-1  control-label" >Firqoh</label>
						<div class="col-sm-3">
							<select class="form-control" id="asrama" name="asrama" >
							  <option value="">Semua</option>
							  <?php
							  foreach ($asrama[1] as $opt) {
								echo "<option value='".$opt['auto']."' ".$selected."> ".$opt['nama_asrama']." </option>";
							  }
							?>
							</select>
						</div>
						
					  </div>
					  
					  <div class="form-group"> 
						<label class="col-sm-1  control-label" >Nama/NIS</label>
						<div class="col-sm-3"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
						<label class="col-sm-1  control-label">Level SP</label>
						<div class="col-sm-3">
							<select class="form-control" id="level" name="level" >
							  <option value="">Semua</option>
							  <option value="SP1">SP 1</option>
							  <option value="SP2">SP 2</option>
							  <option value="SP3">SP 3</option>
							</select>
						</div>
						
					  </div>
					  <div class="form-group"> 
						
						<label class="col-sm-1  control-label">Tanggal</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control txtperiode tgl" id="txtfirstperiod"  name="first" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> -
						  <input type="text" class="form-control txtperiode tgl" id="txtlastperiod"  name="last" value=""  style="width:100px " data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						  <button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
						  <button type="submit" title="Download data" class="btn btn-primary " ><i class="fa fa-download"></i> Download</button>
						</div>
					  </div>
					</div>				
				  </div><!-- /.row -->
				  </form>
					<hr>
					<div class="box-body table-responsive no-padding">
						<table id="table_last_transaction" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th >Tanggal</th>
									<th >NIS</th>
									<th >Nama</th>
									<th>Firqoh</th>
									<th>Jenjang</th>
									<th>Tingkat</th>
									<th>MDK</th>
									<th>Level SP</th>
									<th >Duedate</th>
									<th >Over</th>
									<th >Admin</th>
									<th >Terima</th>
									<th style="width:70px">Edit</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div id="modalfeedback" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Pengembalian surat peringatan</h4>
				</div>
				<!--modal header-->
				<form method="post" id="feedback" action="c_input_super.php" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9 form-horizontal">
						  
							<input type="hidden" name="method" class="" value="terima_surat">
							<input type="hidden" name="id_item" id="txtidsuper" class="" value="">
							<input type="hidden" name="nama_siswa" id="nama_siswa" value="">
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nomor Induk</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="feedbacknis"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nama lengkap</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="feedbacknama"></span>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-sm-3  control-label"><b>Surat</b></label>
								<div class="col-sm-8">
									<input type="file" name="file_surat" id="file_surat"  />
								</div>
							</div>
						  
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btnfeedbacksave" class="btn btn-info pull-right"><i class="fa fa-save"></i> Simpan </button>
					<span id="infoproses"></span>
					
				</div>
				</form>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div id="modalattachment" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Upload surat peringatan</h4>
				</div>
				<!--modal header-->
				<form method="post" id="attachment" action="c_input_super.php" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9 form-horizontal">
						  
							<input type="hidden" name="method" class="" value="attachment">
							<input type="hidden" name="id_item" id="txtiditem" class="" value="">
							<input type="hidden" name="nama_siswa" id="attach_siswa" value="">
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nomor Induk</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachnis"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nama lengkap</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachnama"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Tanggal Surat</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="attachtanggal"></span>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-sm-3  control-label"><b>Surat</b></label>
								<div class="col-sm-8">
									<input type="file" name="file_attach" id="file_attach"  />
								</div>
							</div>
						  
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btnattachsave" class="btn btn-info pull-right"><i class="fa fa-save"></i> Simpan </button>
					<span id="infoproses"></span>
					
				</div>
				</form>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">              
		  <div class="modal-body">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<img src="" class="imagepreview" style="width: 100%;" >
		  </div>
		</div>
	  </div>
	</div>
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}
	
	$(document).on('hidden.bs.modal', '.modal', function () {
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});
	
	$(document).ready( function () 
	{
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_super.php", { term: $('#txtsearchitem').val(), surper: $('#txtsuper').val()}, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.uuid);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dt>"+item.nis + " - "+item.nama+"</dt>"  )
			.appendTo( ul );
		};
		
		$('#tanggal_keluar, #txtfirstperiod, #txtlastperiod').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		//Datemask dd/mm/yyyy
        $("#tanggal_keluar, #txtfirstperiod, #txtlastperiod").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
	});
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
	
	function fullimage(id){
		//var x = document.getElementsById().getAttribute("class");
		$('.imagepreview').attr('src', $(id).attr('src'));
		$('#imagemodal').modal('show');   
	}
	
	function temptabel(id){
		var sp = $('#txtsuper').val();
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var data = jQuery.parseJSON(data);
				var data = data.data;
				
				$('#txtidsiswa').val(data.uuid);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtfirqoh').html(data.kode_firqoh+" - "+data.nama_asrama);
				$('#txtjenjang').html(data.jenis_jenjang+" "+data.jenis_kelamin+" / Tingkat "+data.tingkat);
				$('#txtmdk').html(data.mdk);
				$('#txtakumulasi').html(data.poin);					
				$('#txtpoin').val(data.poin);
				$('#txtlevelsp').html("SP "+sp);
				$('#txtsp').val("SP"+sp);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',urls);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function levelsp(id,val){
		var value = {
			id:id,
			val: val,
			method : "level_sp"
		};
		$.ajax(
		{
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				
				$('#txtlevelsp').html(data.level+"</br>&nbsp;&nbsp; "+data.notif);
				if(data.check_sp == false && data.level != ''){
					$('#check_notif').html('');
					$('#check_notif').html('Silahkan cetak SP dahulu untuk input pelanggaran selanjutnya');
					$('#check_notif').append('<button type="button" title="Input SP" id_siswa="'+id+'" class="btn btn-primary pull-right" id="inputsp" ><i class="fa fa-edit"></i> Input SP </button>');
				}else{
					$('#check_notif').html('');
					$('#check_notif').append('<button type="button" id="btnsimpan" data-btn="simpan" class="btn btn-danger padding pull-right"> <i class="fa fa-save"></i> Simpan </button>');
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#txtsp').val('');				
		$('#txtpoin').val('');				
		$('#txtnis').html('');				
		$('#txtnama').html('');
		$('#txtfirqoh').html('');
		$('#txtjenjang').html('');
		$('#txtmdk').html('');
		$('#txtlevelsp').html('');
		$('#txtakumulasi').html('');
		$('#spduedate').html('');
		$('#tanggal_keluar').val('');
		$('#txtnote').val('');
		$('#preview_siswa').attr('src', '');
	}
    
	$('#tanggal_keluar').on('blur', function() {
		var array = this.value.split("-");
		var dt = new Date(array[1]+"/"+array[0]+"/"+array[2]);
		
		dt.setDate( dt.getDate() + 7 );
		var dd = dt.getDate();
		var mm = dt.getMonth() + 1;
		var y = dt.getFullYear();

		var someFormattedDate = dd + '-' + mm + '-' + y;
		$('#spduedate').html(someFormattedDate);
	});
	
	$(document).on( "click","#btnsimpan", function() {
		var token = $("#txtidsiswa").val();
		var tanggal = $("#tanggal_keluar").val();
		var note = $('#txtnote').val();
		var sp = $('#txtsp').val();
		var poin = $('#txtpoin').val();
		
		if(token == '' || tanggal == ''){
			$.notify({
				message: "Isian bertanda * wajib diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtpelanggaran");
			return;
		}
		
		swal({   
		title: "Simpan surat peringatan",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			tanggal:tanggal,
			note:note,
			sp:sp,
			poin:poin,
			method : "simpan_super"
		  };
		  $("#btnsimpan").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsimpan").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses simpan berhasil');
					printpdf(data.token);
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Proses simpan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsimpan").prop('disabled', false);
			}
		  });
		});
	});
	
	function tblsearch(){
		var first = $("#txtfirstperiod").val();
		var last = $("#txtlastperiod").val();
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var asrama = $('#asrama').val();
		var level = $('#level').val();
		var search = $('#txtname').val();
		var value = {
			first : first,
			last : last,
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			level : level,
			asrama : asrama,
			search : search,
			method : "get_riwayat_surat"
		};
		
		$("#table_last_transaction").DataTable().destroy();
		
		$('#table_last_transaction').DataTable({
			"paging": false,
			"lengthChange": false,
			"searching": false,
			"ordering": false,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_input_super.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "tanggal" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "firqoh" },
			{ "data": "jenjang" },
			{ "data": "tingkat" },
			{ "data": "jenis" },
			{ "data": "level" },
			{ "data": "duedate" },
			{ "data": "over" },
			{ "data": "admin" },
			{ "data": "button" },
			{ "data": "action" },
			]
		});
	}
	$(document).on("click","#btnopenriwayat",function(){
	  $("#modallasttrans").modal("show");
	  $("#table_last_transaction tbody").html("");
	});
	$(document).on("click","#btnfilter",function(){
		tblsearch();
	});
	
	$(document).on("click",".btndelete",function(){
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "delete_surat"
		};
		swal({   
		title: "Hapus surat peringatan",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Hapus",   
		closeOnConfirm: true }, 
		function(){
		  $.ajax(
		  {
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Proses hapus berhasil');
					tblsearch();				
				}else{
					$.notify({
						message: "Proses hapus gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});
					set_focus("#txtname");
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
			}
		  });
		});
	});

	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "pdf_test.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
	function printpdf(token) {
		var id_item = token;
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "pdf_test.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	}
	
	$(document).on("click",".btnreply",function(){
		$("#modalfeedback").modal("show");
		var id_item = $(this).attr('id_item');
		$('#feedbacknis').html('');
		$('#feedbacknama').html('');
		$('#file_surat').val('');
		$("#btnfeedbacksave").prop('disabled', false);
		var nis = $(this).data('nis');
		var nama = $(this).data('nama');
		$('#feedbacknis').html(nis);
		$('#feedbacknama').html(nama);
		$('#txtidsuper').val(id_item);
		$('#nama_siswa').val(nama);
		
	});
	
	$(document).on("click",".btnupload",function(){
		$("#modalattachment").modal("show");
		var id = $(this).attr('id_item');
		$('#attachnis').html('');
		$('#attachnama').html('');
		$('#attachtanggal').html('');
		$('#file_attach').val('');
		$('#txtiditem').val('');
		$("#btnattachsave").prop('disabled', false);
		var nis = $(this).data('nis');
		var nama = $(this).data('nama');
		var tanggal = $(this).data('tanggal');
		$('#attachnis').html(nis);
		$('#attachnama').html(nama);
		$('#attachtanggal').html(tanggal);
		$('#txtiditem').val(id);
		$('#attach_siswa').val(nama);
		
	});
	
	$(function () {
		var options = {
			success: suksesDialog
		}
		$("#feedback").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin upload surat?",
			function (result) {
			  if (result == true) {
				$("#btnfeedbacksave").prop('disabled', true);
				$('#feedback').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. upload surat selesai.");
		$("#modalfeedback").modal("hide");
		tblsearch();
	  }
	  else{
		bootbox.alert(msg);
		$("#btnfeedbacksave").prop('disabled', false);
	  }
	};
	
	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#attachment").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin upload surat?",
			function (result) {
			  if (result == true) {
				
				$("#btnattachsave").prop('disabled', true);
				$('#attachment').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. upload surat selesai.");
		$("#modalattachment").modal("hide");
		tblsearch();
	  }
	  else{
		bootbox.alert(msg);
		$("#btnattachsave").prop('disabled', false);
	  }
	};
		
	
	$(document).on("click",".btnreplyx",function(){
		var id_item = $(this).attr('id_item');
		var value = {
			id_item : id_item,
			method : "terima_surat"
		};
		swal({   
		title: "Terima surat peringatan kembali",   
		text: "Apakah anda yakin melanjutkan proses?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Iya",   
		closeOnConfirm: true }, 
		function(){
		  $.ajax(
		  {
			url : "c_input_super.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Surat sudah diterima');
					tblsearch();				
				}else{
					$.notify({
						message: "Proses gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
			}
		  });
		});
	});
</script>
</body>
</html>
