<?php 
$titlepage="Jenis Takzir";
$idsmenu=27; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_konseling.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_konseling();

?>
<section class="content-header">
  <h1>
	TAKZIR
	<small>data master</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Jenis Takzir</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Jenis</button>
					<br>
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					  <button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
			</div>
			<br/>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Jenis Takzir</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Tambah Jenis Takzir</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Jenis Takzir</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="ex: hukuman"> 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_jenis_takzir.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "hukuman" },
				{ "data": "button" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			$("#modalmasteritem").modal('show');
			newitem();
		});
		
		function newitem()
		{
			$(".modal-title").html("");
			$(".modal-title").html("Tambah Jenis Takzir");
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			set_focus("#txtname");
			
		}
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Jenis takzir kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			var value = {
				id_item: id_item,
				item_name: item_name,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_jenis_takzir.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.ceksat == 0){
						$.notify(data.error);
					}else{
						if(data.crud == 'N'){
							if(data.result == 1){
								$.notify('Proses simpan berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								newitem();				
							}else{
								$.notify({
									message: "Proses simpan gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});
								set_focus("#txtname");
							}
						}else if(data.crud == 'E'){
							if(data.result == 1){
								$.notify('Proses update berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								$("#modalmasteritem").modal("hide");
							}else{
								$.notify({
									message: "Proses update gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});					
								set_focus("#txtname");
							}
						}else{
							$.notify({
								message: "Invalid request"
							},{
								type: 'danger',
								delay: 8000,
							});	
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on( "click",".btnedit", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "get_detail_item"
			};
			$.ajax(
			{
				url : "c_jenis_takzir.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$(".modal-title").html("");
					$(".modal-title").html("Edit Jenis Takzir");
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.auto);
					$("#txtname").val($.trim(data.hukuman));
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btndelete", function() {
			var id_item = $(this).attr("id_item");
			swal({   
			title: "Hapus jenis takzir",   
			text: "Apakah anda yakin akan menghapus jenis takzir?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Hapus",   
			closeOnConfirm: true }, 
			function(){
			  var value = {
				id_item: id_item,
				method : "delete"
			  };
			  $.ajax(
			  {
				url : "c_jenis_takzir.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil.result == true){
						$.notify('Proses berhasil');
						var table = $('#table_item').DataTable(); 
						table.ajax.reload( null, false );
					}else{
						$.notify({
							message: "Proses gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			  });
			});
		});
		
		$(document).on( "click","#btndownload", function() {
		
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "import_takzir.php";
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	</script>
</body>
</html>
