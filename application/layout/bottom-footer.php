<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
	
	$(document).ready( function (){
		<?php 
		
		$session_value=(isset($_SESSION['id_top_menu']))?$_SESSION['id_top_menu']:'0'; ?>
		var idMenu = '<?php echo $session_value; ?>';
		menu(idMenu);
		
        $('.topnav').click(function(e) {
			var navID = $(this).attr('data-id');
			menu(navID);
		});
		
		
	});
	
	function menu(id){
		$('#main-navigation').html('');
		var value = {
			id_item: id,
			method : "get_menu"
		};
		$.ajax(
		{
			url : "../layout/utility.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var hasil = jQuery.parseJSON(data);
				$('#main-navigation').append(hasil.menu);
				var url = document.URL.split("/");
				var href = "/"+url[3]+"/"+url[4]+"/"+url[5]+"/"+url[6];
				
				$('.treeview-menu li a').each(function(){
					var $this = $(this);
					if($this.attr('href').indexOf(href) !== -1){
						$this.parents('.treeview').addClass('active');
						$this.parents('li').addClass('active');
					}
				})
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/fastclick/fastclick.min.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/sweetalert.min.js" type="text/javascript"></script>
<script src="../../dist/js/session_checker.js" type="text/javascript"></script>
<script src="../../dist/js/hotkey.js"></script>
<script src="../../dist/js/jquery.dataTables.min.js"></script>
<script src="../../dist/js/dataTables.bootstrap.min.js"></script>
<script src="../../dist/js/dataTables.responsive.min.js"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>