</head>
<body class="hold-transition skin-black-light sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header"> 
	  <!-- Logo -->
        <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>KM</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>AL</b>KAMAL</span>
        </a>
		
    <nav class="navbar navbar-static-top">
	  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </a>
      <div class="container-fluid">
        <div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
			<i class="fa fa-bars"></i>
		  </button>
		</div>
        <!-- main navigation bar disini -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <?php
			$level ='';
			$sv = new utility(); 
            $top = $sv->getTopMenu();
            $nav = $top[1];
			foreach($nav as $row){
			  $access=$_SESSION['sess_level_access'];
              $arrayaccess=explode(",", $access);
			    if(in_array($row['id'], $arrayaccess))
                {
			?>
				<li class="dropdown"><a  href="#" class="dropdown-toggle topnav" data-toggle="dropdown" data-id="<?php echo $row['id']; ?>"><?php echo $row['menu_name']; ?> <span class="caret"></span></a>
			<?php
				}
			}
			?>
		  </ul>
		</div> 
 <!-- /.navbar-collapse --> 
 <!-- ./ main navigation bar -->
		<?php $path = '../../files/images_user/'.$_SESSION['photo_profile']; $avatar = (file_exists($path))?$_SESSION['photo_profile']:'avatar5.png'; ?>
		<div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  <img src="../../files/images_user/<?php echo $avatar; ?>" class="user-image" alt="User Image">
			  <span class="hidden-xs"><?php echo $_SESSION['sess_username']; ?></span>
			</a> 
			<ul class="dropdown-menu"> 
			  <!-- User image -->
			  <li class="user-header">
				<img src="../../files/images_user/<?php echo $avatar; ?>" class="img-circle" alt="User Image">
				<p>
				  <?php echo $_SESSION['sess_username']; ?>
				</p>
			  </li>              
			  <!-- Menu Footer-->
			  <li class="user-footer">
				<div class="pull-left">
				  <a href="<?php echo $sitename.'application/utility/v_ubah_password.php'; ?>" title="Change Password " class="btn btn-default btn-flat">Change Password</a>
				</div>
				<div class="pull-right">
				<a href="<?php echo $sitename.'application/main/logout.php'; ?>" title="Close Application" class="btn btn-default btn-flat">Logout</a>
				</div>
			  </li>
			</ul>
		   </li>
		   <li>
			<a class="titles" href="<?php echo $sitename.'application/main/logout.php'; ?>" ><i class="titles fa fa-sign-out" title="Close Application"  ></i></a>
		   </li>
		  </ul>
		</div><!--  /.<div class="navbar-custom-menu">-->
	  </div><!-- ./container -->
	</nav>
</header>

	  <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../../files/images_user/<?php echo $avatar; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['sess_username']; ?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu" id="main-navigation">
			<li class="header">MAIN NAVIGATION</li>
			
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
	  
<!-- main content -->
<div class="content-wrapper">
  <!--<div class="container-fluid"> -->
  