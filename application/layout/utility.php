<?php
error_reporting(0);
session_start();
include "../../library/config.php";
require_once ("../model/dbconn.php");
require_once ("../model/utility.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) || isset($_POST['method']))
{
	$method=$_POST['method'];
	$sv = new utility();
	if($method == 'get_menu'){
		unset($_SESSION['id_top_menu']);
		$menu_id = $_POST['id_item'];
		$html_menu = '';
		
		$_SESSION['id_top_menu'] = $menu_id;
		$menutitle = $sv->getTopMenubyId($menu_id);
		
		$data = $sv->getMenu($menu_id);
		$key = $data[1];
			$html_menu ='<li class="header"> '.$menutitle[1]['menu_name'].' NAVIGATION</li>';
		foreach($key as $menu) 
        {
            $id_menu=$menu['id_menu'];
            $query_sub_menu =  $sv->getSubMenu($id_menu);
                             
            $html_menu .= '<li class="treeview">
						  <a href="#">
							<i class="'.$menu['icon'].'"></i>
							<span>'.$menu['name_menu'].'</span>
							<i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class="treeview-menu">';
            
              
			foreach($query_sub_menu[1] as $sub_menu)
			{  
			 $mymenu=$_SESSION['sess_h_menu'];
			 $arramymenu=explode(",", $mymenu);

			 if(in_array($sub_menu['id_sub_menu'], $arramymenu))
			 {    
				$html_menu .= '<li class="">
									<a href="'.$sitename.$sub_menu['url'].'" target="'.$sub_menu['target'].'"><i class="fa fa-circle-o"></i> '.$sub_menu['name_sub_menu'].'</a>
								</li>';
			 }
			} 
              
			$html_menu .= ' </ul>
						</li>';
		}
	 
		$result['menu'] = $html_menu;
		//$result['stat'] = $arramymenu;
		echo json_encode($result);
	}
}