<?php
session_start();
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/utility.php");

$username = $_POST['username'];
$password = $_POST['password'];
if (empty($username) && empty($password)) {
	header('location:login.php?error=1');
	//break;
} else if (empty($username)) {
	header('location:login.php?error=2');
	//break;
} else if (empty($password)) {
	header('location:login.php?error=3');
	//break;
}
$sv = new utility();
$data = $sv->getLogin($username,$password);
if ($data[2] == 1) 
{
	
	$_SESSION['sess_username'] = $username;
	$_SESSION['sess_id'] = $data[1]['id_user'];
	$_SESSION['sess_level_access'] = $data[1]['level_access'];
	$_SESSION['sess_h_menu'] = $data[1]['h_menu'];
	$_SESSION['sess_uniqid'] = uniqid();
	$_SESSION['nama_sekolah'] = $data[1]['nama_sekolah'];
	$_SESSION['photo_profile'] = (!empty($data[1]['avatar']))?$data[1]['avatar']:'avatar5.png';
	$iduser = $_SESSION['sess_id'];
	
	$update_sess = $sv->updateUniqLogin($data[1]['id_user'],$_SESSION['sess_uniqid']);
	if($data[1]['url'] == ''){
	  header('location:../main/index.php');	
	}else{
	  header('location:'.$sitename.''.$data[1]['url'].'');	
	}
	
}
else
{
	header('location:login.php?error=4');
}
?>
