<?php
function UploadImageZ($fupload_name, $folder, $tmpname){
	//ambil extensi file
	$extension=strtolower(strrchr($fupload_name, '.'));

  //direktori gambar
  $vdir_upload = $folder;
    if (is_dir($vdir_upload)){ //jika folder penyimpanan ada
        $vfile_upload = $vdir_upload . $fupload_name;
    }else { //JIKA FOLDER BELUM ADA
        mkdir($vdir_upload, 0777,true); //BUAT FOLDERNYA
        $vfile_upload = $vdir_upload . $fupload_name;
    }

  //Simpan gambar dalam ukuran sebenarnya
  move_uploaded_file($tmpname, $vfile_upload);
  
  //identitas file asli
  //cek extensi
  switch ($extension){
  	case '.jpeg':
  	case '.jpg':
  		$im_src = imagecreatefromjpeg($vfile_upload);
  		break;
  	case '.gif':
  		$im_src = imagecreatefromgif($vfile_upload);
  		break;
  	case '.png':
  		$im_src = imagecreatefrompng($vfile_upload);
  		break;
  	default:
  		$im_src=false;
  		break;
  }
  $src_width = imageSX($im_src);
  $src_height = imageSY($im_src);

  //Simpan dalam versi small 110 pixel
  //Set ukuran gambar hasil perubahan
  $dst_width = 354;
  $dst_height = ($dst_width/$src_width)*$src_height;

  //proses perubahan ukuran
  $im = imagecreatetruecolor($dst_width,$dst_height);
  imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

  //Simpan gambar
  $imageQuality=100;	
  //cek extensi
  switch ($extension){
  	case '.jpeg':
  	case '.jpg':
  		if (imagetypes() & IMG_JPG) {
  		imagejpeg($im,$vdir_upload.$fupload_name,$imageQuality);
  		}
  		break;
  	case '.gif':
  		if (imagetypes() & IMG_GIF) {
  		imagegif($im,$vdir_upload.$fupload_name);
  		}
  		break;
  	case '.png':
  		$scaleQuality = round(($imageQuality/100) * 9);
  		$invertScaleQuality = 9 - $scaleQuality;
  		if (imagetypes() & IMG_PNG) {
  		imagepng($im,$vdir_upload.$fupload_name,$invertScaleQuality);
  		}
  		break;
  	default:
  		
  		break;
  }
  
  
  //Hapus gambar di memori komputer
  imagedestroy($im_src);
  imagedestroy($im);
}

function UploadCompress($fupload_name, $folder, $tmpname){
	//ambil extensi file
	$extension=strtolower(strrchr($fupload_name, '.'));

  //direktori gambar
  $vdir_upload = $folder;
    if (is_dir($vdir_upload)){ //jika folder penyimpanan ada
        $vfile_upload = $vdir_upload . $fupload_name;
    }else { //JIKA FOLDER BELUM ADA
        mkdir($vdir_upload, 0777,true); //BUAT FOLDERNYA
        $vfile_upload = $vdir_upload . $fupload_name;
    }

  //Simpan gambar dalam ukuran sebenarnya
  move_uploaded_file($tmpname, $vfile_upload);
  
  //identitas file asli
  //cek extensi
  switch ($extension){
  	case '.jpeg':
  	case '.jpg':
  		$im_src = imagecreatefromjpeg($vfile_upload);
  		break;
  	case '.gif':
  		$im_src = imagecreatefromgif($vfile_upload);
  		break;
  	case '.png':
  		$im_src = imagecreatefrompng($vfile_upload);
  		break;
  	default:
  		$im_src=false;
  		break;
  }
  $src_width = imageSX($im_src);
  $src_height = imageSY($im_src);

  //Simpan dalam versi small 110 pixel
  //Set ukuran gambar hasil perubahan
  $dst_width = 354;
  $dst_height = ($dst_width/$src_width)*$src_height;

  //proses perubahan ukuran
  $im = imagecreatetruecolor($src_width,$src_height);
  imagecopyresampled($im, $im_src, 0, 0, 0, 0, $src_width, $src_height, $src_width, $src_height);

  //Simpan gambar
  $imageQuality=75;	
  //cek extensi
  switch ($extension){
  	case '.jpeg':
  	case '.jpg':
  		if (imagetypes() & IMG_JPG) {
  		imagejpeg($im,$vdir_upload.$fupload_name,$imageQuality);
  		}
  		break;
  	case '.gif':
  		if (imagetypes() & IMG_GIF) {
  		imagegif($im,$vdir_upload.$fupload_name);
  		}
  		break;
  	case '.png':
  		$scaleQuality = round(($imageQuality/100) * 9);
  		$invertScaleQuality = 9 - $scaleQuality;
  		if (imagetypes() & IMG_PNG) {
  		imagepng($im,$vdir_upload.$fupload_name,$invertScaleQuality);
  		}
  		break;
  	default:
  		
  		break;
  }
  
  
  //Hapus gambar di memori komputer
  imagedestroy($im_src);
  imagedestroy($im);
}
function UploadFileZ($fupload_name, $folder, $tmpname){
	//ambil extensi file
	$extension=strtolower(strrchr($fupload_name, '.'));
	
  //direktori file
  $vdir_upload = $folder.'_'.$extension.'/';
  if (is_dir($vdir_upload)){ //jika folder penyimpanan ada
  	$vfile_upload = $vdir_upload . $fupload_name;
  }else { //JIKA FOLDER BELUM ADA
	mkdir($vdir_upload, 0777,true); //BUAT FOLDERNYA
	$vfile_upload = $vdir_upload . $fupload_name;
  }
  //Simpan file dalam ukuran sebenarnya
  move_uploaded_file($tmpname, $vfile_upload);
}
function UploadCoverZ($fupload_name, $folder, $tmpname){
	
	//direktori file
	$vdir_upload = $folder;
	if (is_dir($vdir_upload)){ //jika folder penyimpanan ada
		$vfile_upload = $vdir_upload . $fupload_name;
	}else { //JIKA FOLDER BELUM ADA
		mkdir($vdir_upload, 0777,true); //BUAT FOLDERNYA
		$vfile_upload = $vdir_upload . $fupload_name;
	}
	//Simpan file dalam ukuran sebenarnya
	move_uploaded_file($tmpname, $vfile_upload);
}



function UploadGaleriVideo($fupload_name){
  //direktori file
  $vdir_upload = "../../../galeri_video/";
  $vfile_upload = $vdir_upload . $fupload_name;

  //Simpan gambar dalam ukuran sebenarnya
  move_uploaded_file($_FILES["fvideo"]["tmp_name"], $vfile_upload);
}
function UploadGaleriVideo_sc($fupload_name){
  //direktori file
  $vdir_upload = "../../../galeri_video/";
  $vfile_upload = $vdir_upload . $fupload_name;

  //Simpan gambar dalam ukuran sebenarnya
  move_uploaded_file($_FILES["fscreenvideo"]["tmp_name"], $vfile_upload);
}
