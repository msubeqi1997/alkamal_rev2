<?php include "../../library/config.php"; ?>
<?php $titlepage="Dashboard"; ?>
<?php
require_once("../model/dbconn.php");
require_once("../model/utility.php");
include "../layout/top-header.php"; 
include "../../library/check_login.php";

include "../layout/header.php"; 
$now = new DateTime();
$tgl = $now->format('Y-m-d'); 
unset($_SESSION['id_top_menu']);
?>
<section style="margin-bottom:10px;" class="content-header">
	<h1>
		Dashboard
		<small>Page</small>
	</h1>
</section>
<section class="content-main">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid box-danger">
				<?php
				$pos = new utility();
				$sekolah = $pos->getrefsytem();
				$name = $sekolah[1]['nama_sekolah'];
				$address = $sekolah[1]['alamat_sekolah'];
				$email = $sekolah[1]['email_sekolah'];
				?>
				<div class="box-header">
					<h1 class="box-title"><?php echo $name;?></h1>
				</div> <!-- end of box-header -->
				<div class="box-body">
					<h4><?php echo $address;?></h4>
					<h5>Email : <?php echo $email;?></h5>
				</div> <!-- end of box-body -->
			</div><!-- end of box box-solid bg-light-blue-gradiaent -->
		</div>
	</div><!-- row -->
	<div class="row">
		<div class="col-md-9">
			
		</div><!-- end of col-md-8 -->
		<div class="col-md-3">
			
		</div><!-- end of col md 4 -->
	</div><!-- end of row -->
	</section>
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script src="../../dist/js/redirect.js"></script>
	<script>
		$(document).on("keyup keydown","#txtsearchitem",function(){
			var searchitem = $("#txtsearchitem").val();
			value={
				term : searchitem,
			}
			$.ajax(
			{
				url : "../master/c_search_item.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#table_search tbody").html(data.data)
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});

	</script>

</body>
</html>
