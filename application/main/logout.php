<?php
session_start();
include "../../library/config.php";
require_once ("../model/dbconn.php");
require_once ("../model/utility.php");
if (!isset($_SESSION['sess_username']) or 
	!isset($_SESSION['sess_id']) or
	!isset($_SESSION['sess_h_menu']) or
	!isset($_SESSION['sess_uniqid']) )
{	
	header('Location: '.$sitename.'application/main/login.php');
}

unset($_SESSION['sess_username']);
unset($_SESSION['sess_id']);
unset($_SESSION['sess_h_menu']);
unset($_SESSION['sess_uniqid']);
unset($_SESSION['nama_sekolah']);
header('Location: '.$sitename.'application/main/login.php');
?>