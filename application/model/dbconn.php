<?php
$dbuserx='root';
$dbpassx='';
class dbconn {
	public $dblocal;
	public function __construct()
	{

	}
	public function initDBO()
	{
		global $dbuserx,$dbpassx;
		try {
			$this->dblocal = new PDO("mysql:host=localhost;dbname=alkamal_rev;charset=latin1",$dbuserx,$dbpassx,array(PDO::ATTR_PERSISTENT => true));
			$this->dblocal->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e)
		{
			die("Can not connect database");
		}
		
	}
	
}

function getId() {
	list($usec, $sec) = explode(" ", microtime());
	return $sec . substr($usec, 2);
	// akan menghasilkan angka seperti 133758407476815300 18 digit selama 200 tahun ke depan. Jika lebih dari itu akan menjadi 17 digit dan seterusnya.
}
?>
