 <?php
class model_data extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
	
	/****************************** START SETTING TAHUN PELAJARAN & DATA POKOK ************/
	public function getTapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.* FROM z_tahun_ajaran t, 
        (SELECT @rownum := 0) r ORDER BY thn_ajaran_id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailTapel($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from z_tahun_ajaran a where a.thn_ajaran_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activeTapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM z_tahun_ajaran WHERE akademik='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function hari()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id,hari FROM z_hari");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updateTanggalAkademik($tahun,$ganjilmulai,$ganjilselesai,$genapmulai,$genapselesai,$semester,$hari_masuk)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_tahun_ajaran SET ganjilmulai =:gm, ganjilselesai =:gs, genapmulai =:nm, genapselesai =:ns, semester =:smt, hari_masuk =:hari, akademik ='1' WHERE thn_ajaran_id =:id;");
	   $stmt->bindParam("id",$tahun);
	   $stmt->bindParam("gm",$ganjilmulai);
	   $stmt->bindParam("gs",$ganjilselesai);
	   $stmt->bindParam("nm",$genapmulai);
	   $stmt->bindParam("ns",$genapselesai);
	   $stmt->bindParam("smt",$semester);
	   $stmt->bindParam("hari",$hari_masuk);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function activateTapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_tahun_ajaran SET akademik = (case when thn_ajaran_id = :id then '1' when thn_ajaran_id != :id then '0' end);");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getTingkatPendidikan()
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM z_tingkat_pendidikan t ORDER BY t.id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getPropinsi()
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM provinces t ORDER BY t.name ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getKabupaten($propinsi)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.id, t.name FROM regencies t WHERE t.province_id=:propinsi ORDER BY t.name ASC");
       $stmt->bindParam("propinsi",$propinsi);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getSatuanPendidikan($id)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM psb_tujuan_sekolah t WHERE t.satuan_pendidikan !=:id ORDER BY t.auto ASC");
       $stmt->bindParam("id",$id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getJenjangPendidikan($id)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM psb_tujuan_sekolah t WHERE t.kode_sekolah =:id");
       $stmt->bindParam("id",$id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getNamaSiswa($nomor){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT nama FROM pes_data_alumni WHERE induk=:induk");
		 $stmt->bindParam("induk",$nomor);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getKelengkapan()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT t.* FROM psb_kelengkapan t WHERE aktif='1'");
       $stmt->bindParam("id",$jalur_id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	/****************** END SETTING TAHUN PELAJARAN ***************************/
	
	public function getDataSiswa(){
		
		$db = $this->dblocal;
		$array = $this->activeTapel();
	    $tapel = $array[1]['thn_ajaran_id'];
		// initilize all variable
		$params = $columns = $totalRecords = $data = array();

		$params = $_REQUEST;

		//define index of column
		$columns = array( 
			0 =>'urutan',
			1 =>'nis', 
			2 => 'nama_lengkap',
			3 => 'alamat',
			4 => 'jenjang',
			5 => 'jenis',
			6 => 'tingkat_kelas',
			7 => 'tujuan',
		);

		$where = $sqlTot = $sqlRec = "";

		// check search value exist
		if( !empty($params['search']['value']) ) {   
			$where .=" WHERE ";
			$where .=" ( nis LIKE '%".$params['search']['value']."%' ";    
			$where .=" OR nama_lengkap LIKE '%".$params['search']['value']."%' ";
			$where .=" OR alamat LIKE '%".$params['search']['value']."%' ";
			$where .=" OR propinsi_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR kabupaten_name LIKE '%".$params['search']['value']."%' ";
			$where .=" OR tujuan LIKE '%".$params['search']['value']."%' ";
			$where .=" OR jenjang LIKE '%".$params['search']['value']."%' ";
			$where .=" OR jenis LIKE '%".$params['search']['value']."%' ";
			$where .=" OR tingkat_kelas LIKE '%".$params['search']['value']."%' )";
		}

		// getting total number records without any search
		$sql = "SELECT * FROM (SELECT urutan, jid, jenis,uuid, nis, nama_lengkap, alamat,propinsi_name, kabupaten_name, tujuan, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang, CONCAT ('Tingkat',' ',tingkat_kelas) AS tingkat_kelas FROM 
				(SELECT @rownum := @rownum + 1 AS urutan,j.id as jid, j.uuid, j.nis, (SELECT tingkat FROM z_tingkat_siswa WHERE id_siswa=s.uuid AND id_tahun_akademik='".$tapel."' GROUP BY id_siswa) AS tingkat_kelas,  
				s.id, s.nama_lengkap, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
				s.alamat, MAX(j.jenjang_pendidikan), (CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
				(CASE WHEN j.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis,
				(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS tujuan,
				(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
				(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name              
				FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid,(SELECT @rownum := 0) r WHERE j.status_kesiswaan='siswa' GROUP BY j.uuid) d)data ";
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}


		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		$number = $db->prepare($sqlTot);
		$number->execute();
		$totalRecords = $number->rowCount();
		
		$array = $db->prepare($sqlRec);
		$array->execute();
		$queryRecords = $array->fetchAll(PDO::FETCH_ASSOC);
		
		//$data[] = $queryRecords;
		
		$json_data = array(
				"draw"            => intval( $params['draw'] ),   
				"recordsTotal"    => intval( $totalRecords ),  
				"recordsFiltered" => intval($totalRecords),
				"data"            => $queryRecords   // total data array
				);

		return $json_data;  // send data as json format
	}
	
	public function getSiswaPendaftar($token){
		$db = $this->dblocal;
		$array = $this->activeTapel();
	    $tapel = $array[1]['thn_ajaran_id'];
		try
		{
		 $stmt = $db->prepare("SELECT s.*,j.*, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang, r.nama_asrama,
						(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS tujuan,
						(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
						(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name  
						FROM 
							(SELECT uuid, (CASE WHEN kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, nama_lengkap, kelamin, alamat, tempat_lahir, tanggal_lahir, photo_siswa, propinsi, kabupaten FROM z_siswa WHERE uuid=:uuid) s 
						JOIN 
							(SELECT MAX(jenjang_pendidikan) AS jenjang_pendidikan, uuid, id AS jid, nis, jenis, tujuan_sekolah,(CASE WHEN jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND uuid=:uuid) j 
						ON s.uuid=j.uuid LEFT JOIN (SELECT id_siswa, (SELECT nama_asrama FROM pes_asrama WHERE auto=a.asrama) AS nama_asrama FROM
							(SELECT id_siswa, k.asrama FROM pes_pembagian_kamar p JOIN pes_kamar k ON(p.id_kamar=k.auto) WHERE id_siswa=:uuid AND id_tahun_akademik=:tapel) a) r
						ON s.uuid=r.id_siswa
						");
		 $stmt->bindParam("uuid",$token);
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getEditSiswa($token){
		$db = $this->dblocal;
		$array = $this->activeTapel();
	    $tapel = $array[1]['thn_ajaran_id'];
		try
		{
		 $stmt = $db->prepare("SELECT s.*, j.jenjang_pendidikan, j.nis, j.id AS jid, j.tujuan_sekolah, j.jenis AS mdk, j.status_kesiswaan, 
							  (SELECT tingkat FROM z_tingkat_siswa WHERE id_siswa=s.uuid AND id_tahun_akademik=:tapel GROUP BY id_siswa) AS tingkat_kelas  
							  FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid WHERE j.status_kesiswaan='siswa' AND j.id=:uuid");
		 $stmt->bindParam("uuid",$token);
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getKodeSiswa($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT j.uuid, 
		 (SELECT photo_siswa FROM z_siswa WHERE uuid=j.uuid) AS photo_siswa, 
		 (SELECT photo_wali FROM z_siswa WHERE uuid=j.uuid) AS photo_wali FROM z_jenjang_siswa j WHERE j.id=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getRiwayatPendidikan($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.* FROM z_siswa_riwayat_pendidikan s WHERE s.uuid=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function deleteRiwayatPendidikan($uuid){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_siswa_riwayat_pendidikan WHERE uuid=:uuid");
		$stmt->bindParam("uuid",$uuid);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveRiwayatPendidikan($berkas){
	  
	  $keys = array();
	  foreach($berkas as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_siswa_riwayat_pendidikan(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($berkas as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveSiswa($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_siswa(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateSiswa($uuid,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE z_siswa SET ".implode(', ',$sets)." WHERE uuid='".$uuid."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertJenjangSiswa($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_jenjang_siswa(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertTingkatSiswa($data){
	  
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_tingkat_siswa(".implode(', ',$keys).",id_tahun_akademik) VALUES(".$par.",:tapel)";
	  	  
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->bindParam('tapel', $tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateJenjangSiswa($jid,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE z_jenjang_siswa SET ".implode(', ',$sets)." WHERE id='".$jid."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateTingkatSiswa($id_siswa,$tingkat){
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("UPDATE z_tingkat_siswa SET tingkat=:tingkat WHERE id_siswa=:siswa AND id_tahun_akademik=:tapel ");
		$stmt->bindParam('tapel', $tapel);
		$stmt->bindParam('siswa', $id_siswa);
		$stmt->bindParam('tingkat', $tingkat);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteTingkatSiswa($id_siswa){
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_tingkat_siswa WHERE id_siswa=:siswa AND id_tahun_akademik=:tapel ");
		$stmt->bindParam('tapel', $tapel);
		$stmt->bindParam('siswa', $id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getBerkasSiswa($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT kelengkapan, tipe, html_value FROM z_siswa_berkas WHERE uuid=:token ");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}

	
	public function getImportSiswa()
    {
      $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $stmt = $db->prepare("SELECT * FROM (SELECT id,nis, jenis, nama_lengkap, alamat, propinsi_name, kabupaten_name, sekolah_tujuan, 
							tempat_lahir,tanggal_lahir,kelamin,saudara,anak_ke,abk,no_hp,kode_pos,ayah ,pendidikan_ayah,pekerjaan_ayah,hp_ayah,
							ibu,pendidikan_ibu,pekerjaan_ibu,hp_ibu,wali,pekerjaan_wali,telp_wali,alamat_wali,hubungan_wali, 
							CONCAT(jenjang,' ',jenis_kelamin) AS jenjang, CONCAT ('Tingkat',' ',tingkat_kelas) AS tingkat_kelas FROM 
							(SELECT s.id, s.no_pendaftaran, s.nama_lengkap,s.alamat, s.tempat_lahir,s.tanggal_lahir,s.kelamin,s.saudara,s.anak_ke,s.abk,s.no_hp,
							s.kode_pos,s.ayah ,s.pendidikan_ayah,s.pekerjaan_ayah,s.hp_ayah,s.ibu,s.pendidikan_ibu,s.pekerjaan_ibu,s.hp_ibu,
							s.wali,s.pekerjaan_wali,s.telp_wali,s.alamat_wali,s.hubungan_wali, MAX(j.jenjang_pendidikan), j.nis,
							(SELECT tingkat FROM z_tingkat_siswa WHERE id_siswa=s.uuid AND id_tahun_akademik=:tapel GROUP BY id_siswa) AS tingkat_kelas,
							(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
							(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
							(CASE WHEN j.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis,
							(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS sekolah_tujuan,
							(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
							(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name
							FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid WHERE j.status_kesiswaan='siswa' GROUP BY j.uuid) d)data");
       $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	
	public function getReportSiswa(array $var = array(),$search)
    {
      $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$search.'%" OR nis LIKE "%'.$search.'%")');}
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, nis, nama_lengkap, CONCAT(alamat,' ',kabupaten_name,' ',propinsi_name) as alamat_lengkap, CONCAT(jenjang_pendidikan,' ',jenis_kelamin) AS jenjang, jenis, tingkat, status, nama_asrama, nama_kamar, sekolah_tujuan, id_asrama, id_kamar, tujuan_sekolah,
							tempat_lahir,tanggal_lahir,kelamin,saudara,anak_ke,abk,no_hp,alamat,kode_pos,ayah ,pendidikan_ayah,pekerjaan_ayah,hp_ayah,
							ibu,pendidikan_ibu,pekerjaan_ibu,hp_ibu,wali,pekerjaan_wali,telp_wali,alamat_wali,hubungan_wali,photo_siswa, photo_wali, kabupaten_name, propinsi_name FROM 
							(
								SELECT s.id, s.no_pendaftaran, s.nama_lengkap,s.alamat, s.tempat_lahir,s.tanggal_lahir,s.kelamin,s.saudara,s.anak_ke,s.abk,s.no_hp,
								s.kode_pos,s.ayah ,s.pendidikan_ayah,s.pekerjaan_ayah,s.hp_ayah,s.ibu,s.pendidikan_ibu,s.pekerjaan_ibu,s.hp_ibu,
								s.wali,s.pekerjaan_wali,s.telp_wali,s.alamat_wali,s.hubungan_wali,s.photo_siswa,s.photo_wali,MAX(j.jenjang_pendidikan) AS jenjang, j.nis,j.jenis,k.nama_asrama, k.nama_kamar, t.tingkat,(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name, 
								(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
								(CASE WHEN j.jenjang_pendidikan='2' THEN 'ula' ELSE 'wustho' END) AS jenjang_pendidikan, 
								(CASE WHEN s.kelamin='0' THEN 'putri' ELSE 'putra' END) AS jenis_kelamin, 
								(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS sekolah_tujuan, 
								j.tujuan_sekolah, j.status_kesiswaan AS status, id_asrama, id_kamar FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
								JOIN (SELECT a.auto AS id_asrama, a.nama_asrama, k.nama_kamar, k.auto AS id_kamar, p.id_siswa FROM pes_asrama a 
								JOIN pes_kamar k ON k.asrama=a.auto JOIN pes_pembagian_kamar p ON p.id_kamar=k.auto WHERE p.id_tahun_akademik=:tapel) k ON k.id_siswa=s.uuid 
								JOIN (SELECT DISTINCT(id_siswa) AS id_siswa, tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel) t ON t.id_siswa=s.uuid GROUP BY j.uuid)
							d,(SELECT @rownum := 0) r WHERE ".implode(' AND ',$query_arr)."");
       $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   //$stat[2] = implode(' AND ',$query_arr);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function pencarianSiswa(array $var = array(),$status,$search)
    {
	  $column_siswa=array('s.uuid','s.nama_lengkap','s.kelamin','s.tempat_lahir','s.tanggal_lahir','s.saudara','s.anak_ke','s.abk','s.alamat','s.email','s.no_hp','s.kode_pos','s.ayah','s.tanggal_lahir_ayah','s.tempat_lahir_ayah','s.alamat_ayah','s.hp_ayah','s.telp_ayah','s.pekerjaan_ayah','s.pendidikan_ayah',
						's.ibu','s.tanggal_lahir_ibu','s.tempat_lahir_ibu','s.hp_ibu','s.pekerjaan_ibu','s.pendidikan_ibu','s.wali','s.tanggal_lahir_wali','s.tempat_lahir_wali','s.telp_wali','s.pekerjaan_wali','s.pendidikan_wali','s.hubungan_wali','s.alamat_wali','s.photo_siswa','s.photo_wali');
      $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$search.'%" OR nis LIKE "%'.$search.'%")');}
	  $where=(!empty($query_arr))?'WHERE ':'';
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $sql = "SELECT @rownum := @rownum + 1 AS urutan, d.* FROM (
							SELECT ".implode(',',$column_siswa).", j.jenis, j.tujuan_sekolah as id_sekolah, j.nis, k.id_asrama, k.id_kamar, k.nama_asrama, k.nama_kamar, t.tingkat,
							(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name, 
							(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
							(CASE WHEN j.jenjang='2' THEN 'ula' ELSE 'wustho' END) AS jenjang_pendidikan, 
							(CASE WHEN s.kelamin='0' THEN 'putri' ELSE 'putra' END) AS jenis_kelamin, 
							(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS sekolah_tujuan 
							FROM z_siswa s JOIN (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis, jenis, tujuan_sekolah FROM z_jenjang_siswa WHERE status_kesiswaan=:status GROUP BY uuid) j ON s.uuid=j.uuid
							JOIN (SELECT a.auto AS id_asrama, a.nama_asrama, k.nama_kamar, k.auto AS id_kamar, p.id_siswa FROM pes_asrama a JOIN pes_kamar k ON k.asrama=a.auto JOIN pes_pembagian_kamar p ON p.id_kamar=k.auto WHERE p.id_tahun_akademik=:tapel) k ON k.id_siswa=s.uuid
							JOIN (SELECT DISTINCT(id_siswa) AS id_siswa, tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel) t ON t.id_siswa=s.uuid
							) d,(SELECT @rownum := 0) r ".$where." ".implode(' AND ',$query_arr)."";
        $stmt = $db->prepare($sql);
	   $stmt->bindParam("tapel",$tapel);
       $stmt->bindParam("status",$status);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   $stat[2] = $sql;
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function pencarianAlumni(array $var = array(),$status,$search)
    {
	  $column_siswa=array('s.uuid','s.nama_lengkap','s.kelamin','s.tempat_lahir','s.tanggal_lahir','s.saudara','s.anak_ke','s.abk','s.alamat','s.email','s.no_hp','s.kode_pos','s.ayah','s.tanggal_lahir_ayah','s.tempat_lahir_ayah','s.alamat_ayah','s.hp_ayah','s.telp_ayah','s.pekerjaan_ayah','s.pendidikan_ayah',
						's.ibu','s.tanggal_lahir_ibu','s.tempat_lahir_ibu','s.hp_ibu','s.pekerjaan_ibu','s.pendidikan_ibu','s.wali','s.tanggal_lahir_wali','s.tempat_lahir_wali','s.telp_wali','s.pekerjaan_wali','s.pendidikan_wali','s.hubungan_wali','s.alamat_wali','s.photo_siswa','s.photo_wali');
      $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$search.'%" OR nis LIKE "%'.$search.'%")');}
	  $where=(!empty($query_arr))?'WHERE ':'';
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $sql = "SELECT @rownum := @rownum + 1 AS urutan, d.* FROM (
					SELECT ".implode(',',$column_siswa).", j.jenis, j.tujuan_sekolah as id_sekolah, j.nis, k.id_asrama, k.id_kamar, k.nama_asrama, k.nama_kamar,
					(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name, 
					(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
					(CASE WHEN j.jenjang='2' THEN 'ula' ELSE 'wustho' END) AS jenjang_pendidikan, 
					(CASE WHEN s.kelamin='0' THEN 'putri' ELSE 'putra' END) AS jenis_kelamin, 
					(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS sekolah_tujuan 
					FROM z_siswa s JOIN (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis, jenis, tujuan_sekolah FROM z_jenjang_siswa WHERE status_kesiswaan=:status GROUP BY uuid, jenjang_pendidikan) j ON s.uuid=j.uuid
				) d,(SELECT @rownum := 0) r ".$where." ".implode(' AND ',$query_arr)."";
        $stmt = $db->prepare($sql);
	   $stmt->bindParam("tapel",$tapel);
       $stmt->bindParam("status",$status);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   $stat[2] = $sql;
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getJenjangSiswa()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT j.*, s.id AS id_siswa,MAX(j.jenjang_pendidikan) AS jenjang FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid WHERE j.status_kesiswaan='siswa' GROUP BY j.uuid");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateNIS($query)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare($query);
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = 'Success update';
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	/****************************** END DATA SISWA ************/
	/****************************** START DATA PEGAWAI ************/
	
	public function getDataPegawai(){
		$db = $this->dblocal;
		
		// initilize all variable
		$params = $columns = $totalRecords = $data = array();

		$params = $_REQUEST;

		//define index of column
		$columns = array( 
			0 =>'urutan',
			1 =>'nip', 
			2 => 'nama_lengkap',
			3 => 'jenis',
			4 => 'ttl',
			5 => 'alamat',
			6 => 'aktif',
			7 => 'tempat_lahir',
			8 => 'tanggal_lahir',
		);

		$where = $sqlTot = $sqlRec = "";

		// check search value exist
		if( !empty($params['search']['value']) ) {   
			$where .=" WHERE ";
			$where .=" ( urutan LIKE '%".$params['search']['value']."%' ";    
			$where .=" OR nip LIKE '%".$params['search']['value']."%' ";
			$where .=" OR nama_lengkap LIKE '%".$params['search']['value']."%' ";
			$where .=" OR jenis LIKE '%".$params['search']['value']."%' ";
			$where .=" OR ttl LIKE '%".$params['search']['value']."%' ";
			$where .=" OR alamat LIKE '%".$params['search']['value']."%' ";
			$where .=" OR aktif LIKE '%".$params['search']['value']."%' ";
			$where .=" OR tempat_lahir LIKE '%".$params['search']['value']."%' ";
			$where .=" OR tanggal_lahir LIKE '%".$params['search']['value']."%' )";
		}

		// getting total number records without any search
		$sql = "SELECT id_pegawai, nip, nama_lengkap, jenis, ttl, tempat_lahir, tanggal_lahir, aktif, alamat,urutan FROM
				(SELECT @rownum := @rownum + 1 AS urutan, (CASE WHEN p.kelamin='0' THEN 'Perempuan' ELSE 'Laki-laki' END) AS jenis, 
				CONCAT(p.tempat_lahir,', ',p.tanggal_lahir) AS ttl, p.* FROM z_pegawai p,(SELECT @rownum := 0) r) data ";
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}


		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		$number = $db->prepare($sqlTot);
		$number->execute();
		$totalRecords = $number->rowCount();
		
		$array = $db->prepare($sqlRec);
		$array->execute();
		$queryRecords = $array->fetchAll(PDO::FETCH_ASSOC);
		
		//$data[] = $queryRecords;
		
		$json_data = array(
				"draw"            => intval( $params['draw'] ),   
				"recordsTotal"    => intval( $totalRecords ),  
				"recordsFiltered" => intval($totalRecords),
				"data"            => $queryRecords   // total data array
				);

		return $json_data;  // send data as json format
	}
	
	public function savePegawai($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_pegawai(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDataPegawaiImport(){
		$db = $this->dblocal;
		try
		{
			$stmt = $db->prepare ("SELECT p.nip, p.nama_lengkap, p.nik, p.tempat_lahir, p.tanggal_lahir, p.kelamin, p.agama, p.alamat, 
			p.kode_pos, p.no_hp, p.pendidikan, p.jurusan, p.lulus, p.email, p.nikah, p.pasangan, 
			(Select pendidikan FROM z_tingkat_pendidikan where id=p.pendidikan) as tingkat_pendidikan FROM z_pegawai p");
			$stmt->execute();
			$stat[0] = true;
			$stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $stat;
		}
		catch(PDOException $ex)
		{
		  $stat[0] = false;
		  $stat[1] = $ex->getMessage();
		  return $stat;
		}
	}
		
	public function getDetailPegawai($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT * FROM z_pegawai WHERE id_pegawai=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updatePegawai($id,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE z_pegawai SET ".implode(', ',$sets)." WHERE id_pegawai='".$id."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getPegawaiAktif()
    {
      $db = $this->dblocal;
      $stmt = $db->prepare("SELECT id_pegawai, nama_lengkap FROM z_pegawai WHERE aktif='aktif 'ORDER BY nama_lengkap ASC");
       $stmt->execute();
       $stat = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
    
	}
	/****************************** END DATA PEGAWAI ************/
	/****************************** START MUTASI SISWA ************/
	public function autoCompleteSearch($term)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT s.uuid, s.nama_lengkap, j.nis, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang
							FROM 
								(SELECT uuid, (CASE WHEN kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, nama_lengkap FROM z_siswa) s 
							JOIN 
								(SELECT  DISTINCT(uuid) AS uuid, nis, (CASE WHEN jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang FROM z_jenjang_siswa WHERE status_kesiswaan='siswa') j 
							ON s.uuid=j.uuid WHERE j.nis LIKE :term or s.nama_lengkap LIKE :term ORDER BY j.nis ASC ");
	   $stmt->bindParam("term",$trm);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailSiswa($token)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT jid, nis, nama_lengkap, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang,kelamin, alamat, tempat_lahir, tanggal_lahir, photo_siswa FROM 
				(SELECT j.nis, j.id AS jid, s.nama_lengkap, kelamin, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
				(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang, s.alamat, s.tempat_lahir, s.tanggal_lahir, s.photo_siswa
				FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid WHERE s.uuid=:token) data ");
	   $stmt->bindParam("token",$token);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveMutasiSiswa($token, $data){
		$sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE z_jenjang_siswa SET ".implode(', ',$sets)." WHERE id='".$token."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success update!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	/****************************** END DATA MUTASI SISWA ************/
	/****************************** START ASRAMA ************/
	
	public function getAsrama()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.*, 
	   (SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=t.wali_asrama) AS pembina,
	   (SELECT COUNT(auto) FROM pes_kamar WHERE asrama = t.auto) AS jumlah_kamar
	   FROM pes_asrama t,(SELECT @rownum := 0) r ORDER BY nama_asrama ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getAsramabyTipe($tipe)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT auto,nama_asrama FROM pes_asrama WHERE tipe=:tipe ORDER BY nama_asrama ASC");
       $stmt->bindParam('tipe', $tipe);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveAsrama($name,$kode,$tipe,$pembina)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_asrama(nama_asrama, kode_firqoh, tipe, wali_asrama) VALUES(:nama,:kode,:tipe,:wali)");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('kode', $kode);
       $stmt->bindParam('tipe', $tipe);
       $stmt->bindParam('wali', $pembina);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateAsrama($id,$name,$kode,$tipe,$pembina)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_asrama SET nama_asrama=:nama, kode_firqoh=:kode, tipe=:tipe, wali_asrama=:wali WHERE auto=:id");
       $stmt->bindParam('nama', $name);
	   $stmt->bindParam('kode', $kode);
       $stmt->bindParam('tipe', $tipe);
       $stmt->bindParam('wali', $pembina);
       $stmt->bindParam('id', $id);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailAsrama($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_asrama WHERE auto=:id ");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPengurusAsrama($idc)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT (SELECT nama_asrama FROM pes_asrama WHERE auto=:id) AS nama_asrama,p.* 
	   FROM pes_pengurus_asrama p WHERE p.asrama=:id AND p.id_tahun_ajaran=:thn");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("thn",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}

	public function getPengurus($id)
	{
	  $db = $this->dblocal;
	 try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_pengurus_asrama");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function savePengurusAsrama($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO pes_pengurus_asrama(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$db->beginTransaction(); 
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = $db->lastInsertId();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updatePengurusAsrama($id,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE pes_pengurus_asrama SET ".implode(', ',$sets)." WHERE auto='".$id."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getPengurusMarkazy()
	{
	  $db = $this->dblocal;
	 try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_pengurus_markazy LIMIT 1");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function savePengurusMarkazy($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO pes_pengurus_markazy(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$db->beginTransaction(); 
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = $db->lastInsertId();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updatePengurusMarkazy($id,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE pes_pengurus_markazy SET ".implode(', ',$sets)." WHERE auto='".$id."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	/****************************** END ASRAMA ************/
	/****************************** START KAMAR ASRAMA ************/
	
	public function getKamar()
    {
      $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.*, 
	   (SELECT nama_asrama FROM pes_asrama WHERE auto = t.asrama) AS nama_asrama,
	   (SELECT COUNT(id_siswa) AS terisi FROM pes_pembagian_kamar WHERE id_tahun_akademik=:tapel AND id_kamar=t.auto) AS terisi
	   FROM pes_kamar t,(SELECT @rownum := 0) r ORDER BY nama_asrama ASC");
       $stmt->bindParam('tapel', $tapel);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailKamar($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT t.*,(SELECT nama_asrama FROM pes_asrama WHERE auto=t.asrama) AS nama_asrama FROM pes_kamar t WHERE t.auto=:id ");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveKamar($name,$kapasitas,$pembina,$asrama)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_kamar(nama_kamar, kapasitas_kamar, pembina, asrama) VALUES(:nama, :kapasitas, :pembina, :asrama)");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('kapasitas', $kapasitas);
       $stmt->bindParam('pembina', $pembina);
       $stmt->bindParam('asrama', $asrama);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateKamar($id,$name,$kapasitas,$pembina,$asrama)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_kamar SET nama_kamar=:nama, kapasitas_kamar=:kapasitas, pembina=:pembina, asrama=:asrama WHERE auto=:id");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('kapasitas', $kapasitas);
       $stmt->bindParam('pembina', $pembina);
       $stmt->bindParam('asrama', $asrama);
       $stmt->bindParam('id', $id);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getKamarbyAsrama($id)
    {
      $db = $this->dblocal;
	  try
      {
       $stmt = $db->prepare("SELECT t.* FROM pes_kamar t WHERE t.asrama=:id");
       $stmt->bindParam('id', $id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getPenghuniKamar($id)
    {
      $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
      {
       $stmt = $db->prepare("SELECT m.*, t.id_rombel FROM
							(SELECT id_rombel, id_siswa FROM pes_pembagian_kamar WHERE id_tahun_akademik=:tapel AND id_kamar=:id GROUP BY id_siswa) t 
							JOIN
							( SELECT s.uuid, s.nama_lengkap, j.nis, j.jenis FROM (SELECT  uuid, nama_lengkap FROM z_siswa) s 
							JOIN (SELECT  MAX(jenjang_pendidikan) AS jenjang, uuid, nis, jenis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j ON s.uuid=j.uuid) m ON t.id_siswa=m.uuid");
       $stmt->bindParam('id', $id);
       $stmt->bindParam('tapel', $tapel);
	   
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getUngroup($kelamin,$jenjang)
    {
      $db = $this->dblocal;
	  try
      {
       $stmt = $db->prepare("SELECT MAX(b.jenjang_pendidikan) AS jenjang, b.uuid, b.id, a.nama_lengkap, b.nis, a.jenis 
	   FROM z_siswa a JOIN z_jenjang_siswa b ON a.uuid=b.uuid WHERE b.status_kesiswaan='siswa' AND b.group_kamar='ungroup' AND a.kelamin=:kelamin AND b.jenjang_pendidikan=:jenjang  
	   GROUP BY b.uuid ORDER BY a.nama_lengkap ASC");
	   $stmt->bindParam('kelamin', $kelamin);
	   $stmt->bindParam('jenjang', $jenjang);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getNonRombel($kelamin,$jenjang,$tingkat,$term)
	{
	  if($term == '' || $term == NULL){
		$search = '';  
	  }else{
		$search = " AND d.nama_lengkap LIKE '%".$term."%'";
	  }
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.* FROM (
					SELECT m.* FROM
					(SELECT id_siswa FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND tingkat=:tingkat GROUP BY id_siswa) t 
					JOIN
					( SELECT s.uuid, s.nama_lengkap, j.nis, j.jenis FROM (SELECT  uuid, nama_lengkap FROM z_siswa WHERE kelamin=:kelamin) s 
					JOIN (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis,jenis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenjang_pendidikan=:jenjang GROUP BY uuid) j
					ON s.uuid=j.uuid) m ON t.id_siswa=m.uuid) d 
					LEFT OUTER JOIN (SELECT id_siswa FROM pes_pembagian_kamar WHERE id_tahun_akademik=:tapel) r ON r.id_siswa=d.uuid WHERE r.id_siswa IS NULL 
					".$search." ");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->bindParam("kelamin",$kelamin);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSearchUngroup($kelamin,$jenjang,$term)
    {
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
      {
       $stmt = $db->prepare("SELECT MAX(b.jenjang_pendidikan) AS jenjang, b.uuid, b.id, a.nama_lengkap, a.jenis, b.nis 
	   FROM z_siswa a JOIN z_jenjang_siswa b ON a.uuid=b.uuid WHERE b.status_kesiswaan='siswa' AND b.group_kamar='ungroup' AND a.kelamin=:kelamin AND b.jenjang_pendidikan=:jenjang
	   AND a.nama_lengkap LIKE :term OR b.nis LIKE :term GROUP BY b.uuid ORDER BY a.nama_lengkap ASC");
	   $stmt->bindParam('kelamin', $kelamin);
	   $stmt->bindParam('jenjang', $jenjang);
	   $stmt->bindParam('term', $trm);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveCheckin($id_siswa,$kamar)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("call saveCheckin(:tapel,:kamar,:id_siswa)");
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("kamar",$kamar);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveCheckout($id_item,$kamar)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("call saveCheckout(:tapel,:kamar,:id_item)");
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("kamar",$kamar);
		$stmt->bindParam("id_item",$id_item);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getFilterMutasi(array $var = array()){
		$db = $this->dblocal;
		
		$query_arr=array();
		foreach($var as $col => $val){
			$query_arr[] = "".$col."='".$val."'";
		}
		
		$where = (!empty($query_arr))?'WHERE ':'';
		
		try
		{
		 $stmt = $db->prepare("SELECT * FROM (SELECT MAX(b.jenjang_pendidikan), b.uuid, b.jenis, b.tingkat_kelas, a.nama_lengkap, a.kelamin, b.nis, b.jenjang_pendidikan, b.tanggal_keluar, b.alasan_keluar, 
							   (CASE WHEN a.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
							   (CASE WHEN b.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
							   (CASE WHEN b.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk 
							   FROM z_siswa a JOIN z_jenjang_siswa b ON a.uuid=b.uuid WHERE b.status_kesiswaan='keluar' GROUP BY b.uuid, b.jenjang_pendidikan ORDER BY b.tanggal_keluar ASC) data ".$where." ".implode(' AND ',$query_arr)."");
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
		 //$stat[2] = $where." ".implode(' AND ',$query_arr);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function countPenghuniKamar($kamar)
    {
      $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
      try
      {
       $stmt = $db->prepare("SELECT * FROM 
	   (SELECT t.auto, t.kapasitas_kamar, (SELECT COUNT(id_siswa) AS terisi FROM pes_pembagian_kamar WHERE id_tahun_akademik=:tapel AND id_kamar=t.auto) AS terisi FROM pes_kamar t) tbl 
	   WHERE auto=:kamar");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('kamar', $kamar);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}

	
}