 <?php
class model_konseling extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
	
	/****************************** START SETTING TAHUN PELAJARAN & DATA POKOK ************/
	
	public function activeTapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM z_tahun_ajaran WHERE akademik='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************************** START JENIS PELANGGARAN ************/
	public function getKlasifikasiPelanggaran()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_klasifikasi_pelanggaran");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getJenisPelanggaran()
	{
	  $db = $this->dblocal;
	  try
	  {
		// "
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,a.*, b.klasifikasi AS bobot FROM pes_jenis_pelanggaran a JOIN pes_klasifikasi_pelanggaran b ON a.klasifikasi=b.id,(SELECT @rownum := 0) r ORDER BY a.jenis ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailJenisPelanggaran($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_pelanggaran WHERE auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveJenisPelanggaran($name,$plot,$poin,$aktif)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_jenis_pelanggaran(jenis, klasifikasi, poin, aktif) VALUES(:nama,:plot,:poin,:aktif)");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('plot', $plot);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('aktif', $aktif);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateJenisPelanggaran($iditem,$name,$plot,$poin,$aktif)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_jenis_pelanggaran SET jenis=:nama, klasifikasi=:plot, poin=:poin, aktif=:aktif WHERE auto=:id");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('plot', $plot);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('aktif', $aktif);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function activateJenisPelanggaran($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE pes_jenis_pelanggaran SET aktif=case aktif when '1' then '0'
                             when '0' then '1' end WHERE auto=:id;");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************************** START JENIS TAKZIR ************/
	public function getJenisTakzir()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,a.* FROM pes_jenis_takzir a,(SELECT @rownum := 0) r ORDER BY a.hukuman ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveJenisTakzir($name)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_jenis_takzir(hukuman) VALUES(:nama)");
       $stmt->bindParam('nama', $name);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateJenisTakzir($iditem,$name)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_jenis_takzir SET hukuman=:nama WHERE auto=:id");
       $stmt->bindParam('nama', $name);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function deleteJenisTakzir($iditem)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("DELETE FROM pes_jenis_takzir WHERE auto=:id");
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailJenisTakzir($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_takzir WHERE auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************************** START AKUMULASI POIN ************/
	public function getPoin()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,a.* FROM pes_akumulasi_poin a,(SELECT @rownum := 0) r");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailPoin($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_akumulasi_poin WHERE auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updatePoin($iditem,$start,$end)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_akumulasi_poin SET start=:start, end=:end WHERE auto=:id");
       $stmt->bindParam('start', $start);
       $stmt->bindParam('end', $end);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	/****************************** START INPUT HUKUMAN ************/
	public function autoCompleteSearch($term,$asrama,$kamar)
	{
	  $trm = "%".$term."%";
	  $query='';
	  if($kamar != ''){$query .=" AND k.id_kamar='".$kamar."'";}
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT s.id,s.uuid,s.nama_lengkap,s.nis 
	   FROM (SELECT  DISTINCT(b.uuid) AS uuid, b.id, a.nama_lengkap, b.nis FROM z_siswa a JOIN z_jenjang_siswa b ON a.uuid=b.uuid WHERE b.status_kesiswaan='siswa' ) s 
	   JOIN pes_pembagian_kamar k ON s.uuid=k.id_siswa WHERE k.id_tahun_akademik=:tapel ".$query." ) tbl WHERE tbl.nis LIKE :term or tbl.nama_lengkap LIKE :term ORDER BY tbl.nama_lengkap ASC ");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDataPoinSiswa()
	{
	  $db = $this->dblocal;
	 $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT urutan, jenis, nis, nama_lengkap, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang, CONCAT ('Tingkat',' ',tingkat_kelas) AS tingkat_kelas, nama_asrama, poin FROM 
				(SELECT @rownum := @rownum + 1 AS urutan, j.nis, (SELECT tingkat FROM z_tingkat_siswa WHERE id_siswa=s.uuid AND id_tahun_akademik=:tapel GROUP BY id_siswa) AS tingkat_kelas,  
				s.id, s.nama_lengkap, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
				MAX(j.jenjang_pendidikan), (CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
				(CASE WHEN j.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis,
                 (SELECT SUM(poin_pelanggaran)-(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=s.uuid AND sts='1') FROM pes_hukuman_santri WHERE id_siswa=j.uuid AND sts='1') AS poin, a.nama_asrama
				FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
                LEFT JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=s.uuid  
                 ,(SELECT @rownum := 0) r WHERE j.status_kesiswaan='siswa' GROUP BY j.uuid) d ");
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   //$stat[2] = $where;
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDataPelanggaran($first,$last,array $var = array(),$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, s.nama_lengkap, h.jenis_pelanggaran, h.hukuman, h.poin_pelanggaran, h.input_at, h.auto, a.id_asrama, a.nama_asrama, a.kode_firqoh, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
					(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
					(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=h.id_siswa GROUP BY id_siswa) AS tingkat,
					(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
					(SELECT username FROM m_user WHERE id_user=h.input_by) as admin
				  FROM pes_hukuman_santri h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid 
				  JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid JOIN z_siswa s ON h.id_siswa=s.uuid
				  WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.input_at BETWEEN :first AND :last GROUP BY h.auto ORDER BY h.input_at ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam('last', $last);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   //$stat[2] = $where;
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailPelanggar($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
					(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
					(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
					(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=:id GROUP BY id_siswa) AS tingkat,
					(SELECT SUM(poin_pelanggaran)-(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=j.uuid AND sts='1') FROM pes_hukuman_santri WHERE id_siswa=j.uuid AND sts='1') AS poin,
					j.nis, s.nama_lengkap, s.tempat_lahir, s.tanggal_lahir, s.kelamin, s.photo_siswa, s.ayah, s.wali, a.kode_firqoh, a.nama_asrama
			   FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
			   JOIN (SELECT s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid
			   WHERE j.uuid=:id AND j.status_kesiswaan='siswa'");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDataSurat($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
					(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
					(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
					(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=:id GROUP BY id_siswa) AS tingkat,
					(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=j.tujuan_sekolah) AS sekolah_tujuan,
					(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
					(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
					(SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=a.wali_asrama) AS pembina_asrama,
					a.pembina AS pembina_kamar, j.nis, s.nama_lengkap, s.tempat_lahir, s.tanggal_lahir, s.kelamin, s.alamat, s.photo_siswa, s.ayah, s.wali, a.id_asrama, a.kode_firqoh, a.nama_asrama
			   FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
			   JOIN (SELECT s.auto AS id_asrama, s.nama_asrama, s.kode_firqoh, s.wali_asrama, p.id_siswa, k.pembina FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid
			   WHERE j.uuid=:id AND j.status_kesiswaan='siswa'");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPengurusAsrama($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_pengurus_asrama WHERE asrama=:id AND id_tahun_ajaran=:tapel");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPengurusMarkazy()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_pengurus_markazy");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getRiwayatPelanggaran($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, input_at, jenis_pelanggaran, klasifikasi, poin_pelanggaran FROM pes_hukuman_santri,(SELECT @rownum := 0) r WHERE sts='1' AND id_siswa=:id ORDER BY input_at ASC");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function countSP($sp)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT COUNT(auto) AS jumlah FROM pes_surat_peringatan WHERE sp_ke=:sp");
	   $stmt->bindParam("sp",$sp);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updateNomorSurat($id,$nomor)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE pes_surat_peringatan SET nomor_sp=:nomor WHERE auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("nomor",$nomor);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'success update!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function level_sp($val)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT upper(SP(:val)) AS sp");
	   $stmt->bindParam("val",$val);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getJenisHukuman($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT j.auto, j.jenis, j.poin, k.klasifikasi FROM pes_jenis_pelanggaran j JOIN pes_klasifikasi_pelanggaran k ON j.klasifikasi=k.id WHERE j.auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function searchTakzir($term)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_takzir WHERE hukuman LIKE :term ORDER BY hukuman ASC ");
	   $stmt->bindParam("term",$trm);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveTakzir($iditem,$jenis,$klasifikasi,$tanggal,$poin,$kronologi,$bimbingan,$takzir,$tapel,$smt,$sts,$user)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_hukuman_santri(id_tahun_akademik,semester,id_siswa,jenis_pelanggaran,klasifikasi,poin_pelanggaran,hukuman,kronologi,bimbingan,sts,input_by,input_at) 
	   VALUES(:tapel,:smt,:token,:jenis,:klasifikasi,:poin,:hukuman,:kronologi,:bimbingan,:sts,:input_by,:input_at)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('token', $iditem);
       $stmt->bindParam('jenis', $jenis);
       $stmt->bindParam('klasifikasi', $klasifikasi);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('hukuman', $takzir);
       $stmt->bindParam('kronologi', $kronologi);
       $stmt->bindParam('bimbingan', $bimbingan);
       $stmt->bindParam('sts',$sts);
       $stmt->bindParam('input_by', $user);
       $stmt->bindParam('input_at', $tanggal);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getAkumulasiPoin($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, 
							(SELECT SUM(poin_pelanggaran)-(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=j.uuid AND sts='1') FROM pes_hukuman_santri WHERE id_siswa=j.uuid AND sts='1') AS poin
							FROM z_jenjang_siswa j WHERE j.uuid=:id AND j.status_kesiswaan='siswa'");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteDataPelanggaran($iditem,$user)
    {
      date_default_timezone_set('Asia/Jakarta');
	  $note = 'delete by '.$user.' at '.date('Y-m-d h:i:s').'';
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_hukuman_santri SET sts='0', sts_note=:note WHERE auto=:id");
       $stmt->bindParam('note', $note);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success delete!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailPelanggaranSiswa($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT MAX(j.jenjang_pendidikan) AS jenjang, s.nama_lengkap, j.nis, d.jenis_pelanggaran, d.klasifikasi, d.poin_pelanggaran, d.hukuman, d.kronologi, d.bimbingan, d.input_at, a.nama_asrama,
								(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
								(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
								(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
								(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=s.uuid GROUP BY id_siswa) AS tingkat
							FROM pes_hukuman_santri d
							JOIN z_siswa s ON s.uuid=d.id_siswa JOIN z_jenjang_siswa j ON j.uuid=d.id_siswa 
							JOIN (SELECT s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid
							WHERE j.status_kesiswaan='siswa' AND d.auto=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	####################################### AMNESTI ########################################
	
	public function autoCompleteAmnesti($term,$asrama,$kamar)
	{
	  $trm = "%".$term."%";
	  $query='';
	  if($kamar != ''){$query .=" AND k.id_kamar='".$kamar."'";}
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT s.id,s.uuid,s.nama_lengkap,s.nis 
	   FROM (SELECT  DISTINCT(b.uuid) AS uuid, b.id, a.nama_lengkap, b.nis FROM z_siswa a JOIN z_jenjang_siswa b ON a.uuid=b.uuid WHERE b.status_kesiswaan='siswa' ) s 
	   JOIN pes_pembagian_kamar k ON s.uuid=k.id_siswa JOIN (SELECT DISTINCT(id_siswa) as siswa FROM pes_hukuman_santri WHERE sts='1') h ON h.siswa=s.uuid WHERE k.id_tahun_akademik=:tapel ".$query." ) tbl WHERE tbl.nis LIKE :term or tbl.nama_lengkap LIKE :term ORDER BY tbl.nama_lengkap ASC ");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getAkumulasiAmnesti($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, 
							(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=j.uuid AND sts='1') AS poin
							FROM z_jenjang_siswa j WHERE j.uuid=:id AND j.status_kesiswaan='siswa'");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveAmnesti($iditem,$tanggal,$poin,$note,$tapel,$smt,$sts,$user)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_amnesti_santri(id_tahun_akademik,semester,id_siswa,poin_amnesti,note,sts,input_by,input_at) 
	   VALUES(:tapel,:smt,:token,:poin,:note,:sts,:input_by,:input_at)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('token', $iditem);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('note', $note);
       $stmt->bindParam('sts',$sts);
       $stmt->bindParam('input_by', $user);
       $stmt->bindParam('input_at', $tanggal);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBukuAmnesti(array $var = array(),$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare(" SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, s.uuid, s.nama_lengkap, 
								a.id_asrama, a.nama_asrama, a.kode_firqoh,(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
								(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
								(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=h.id_siswa GROUP BY id_siswa) AS tingkat,
								(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk, h.poin, p.total
							  FROM (SELECT auto, id_siswa, SUM(poin_amnesti) as poin FROM pes_amnesti_santri WHERE sts='1' GROUP BY id_siswa) h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid 
							  JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid 
							  JOIN z_siswa s ON h.id_siswa=s.uuid LEFT JOIN (SELECT auto, id_siswa, SUM(poin_pelanggaran) as total FROM pes_hukuman_santri WHERE sts='1' GROUP BY id_siswa) p ON h.id_siswa=p.id_siswa 
							  WHERE j.status_kesiswaan='siswa' GROUP BY h.id_siswa ORDER BY j.nis ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailAmnesti($id_siswa)
	{
	  $db = $this->dblocal;
	  
	  try
	  {
	   $stmt = $db->prepare("SELECT a.auto, a.id_siswa, a.poin_amnesti, a.input_at, (SELECT username FROM m_user WHERE id_user=a.input_by) as admin FROM pes_amnesti_santri a WHERE a.sts='1' AND a.id_siswa=:id ORDER BY a.input_at ASC");
	   $stmt->bindParam("id",$id_siswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteDataAmnesti($iditem,$user)
    {
	  date_default_timezone_set('Asia/Jakarta');
	  $note = 'delete by '.$user.' at '.date('Y-m-d h:i:s').'';
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_amnesti_santri SET sts='0', sts_note=:note WHERE auto=:id");
       $stmt->bindParam('note', $note);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success delete!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	
	####################################### PANGGILAN WALI ########################################
	
	public function savePanggilanWali($iditem,$tanggal,$wali,$hubungan,$poin,$note,$tapel,$smt,$sts,$user)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_panggilan_ortu(id_tahun_akademik,semester,id_siswa,wali,hubungan,akumulasi_poin,bimbingan,sts,input_by,input_at) 
	   VALUES(:tapel,:smt,:token,:wali,:hubungan,:poin,:note,:sts,:input_by,:input_at)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('token', $iditem);
       $stmt->bindParam('wali', $wali);
       $stmt->bindParam('hubungan', $hubungan);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('note', $note);
       $stmt->bindParam('sts',$sts);
       $stmt->bindParam('input_by', $user);
       $stmt->bindParam('input_at', $tanggal);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBukuPanggilanWali($first,$last,array $var = array(),$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, s.nama_lengkap, h.akumulasi_poin, h.input_at, h.auto, h.wali, h.hubungan, h.bimbingan, a.id_asrama, a.nama_asrama, a.kode_firqoh, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
					(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
					(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=h.id_siswa GROUP BY id_siswa) AS tingkat,
					(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
					(SELECT username FROM m_user WHERE id_user=h.input_by) as admin
				  FROM pes_panggilan_ortu h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid 
				  JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid JOIN z_siswa s ON h.id_siswa=s.uuid
				  WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.input_at BETWEEN :first AND :last GROUP BY h.auto ORDER BY h.input_at ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailPanggilanOrtu($id_item)
	{
	  $db = $this->dblocal;
	  
	  try
	  {
	   //$stmt = $db->prepare("SELECT s.nama_lengkap, a.auto, a.id_siswa, a.bimbingan,a.wali, a.hubungan, a.input_at, (SELECT username FROM m_user WHERE id_user=a.input_by) as admin FROM pes_panggilan_ortu a JOIN z_siswa s ON s.uuid=a.id_siswa WHERE a.sts='1' AND a.auto=:id ORDER BY a.input_at ASC");
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, j.nis, s.nama_lengkap, a.bimbingan,a.wali, a.hubungan, a.input_at
			   FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
			   JOIN pes_panggilan_ortu a ON a.id_siswa=s.uuid WHERE a.sts='1' AND a.auto=:id");
	   $stmt->bindParam("id",$id_item);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteDataPanggilanOrtu($iditem,$user)
    {
	  date_default_timezone_set('Asia/Jakarta');
	  $note = 'delete by '.$user.' at '.date('Y-m-d h:i:s').'';
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_panggilan_ortu SET sts='0', sts_note=:note WHERE auto=:id");
       $stmt->bindParam('note', $note);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success delete!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	####################################### ----- ################################################
	public function check_sp($id,$val)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT check_sp_print(:id,:val) AS 'cetak'");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("val",$val);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function check_skorsing($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT DISTINCT(id_siswa) AS skorsing FROM pes_skorsing WHERE id_siswa=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function searchSuperSantri($term,$start,$end,$level)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT nama_lengkap, uuid, nis, poin, id_siswa FROM ( SELECT (SELECT nama_lengkap FROM z_siswa WHERE uuid=sts.uuid) AS nama_lengkap, sts.* 
							FROM (SELECT j.uuid, j.nis, (SELECT SUM(poin_pelanggaran)-(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=j.uuid AND sts='1') FROM pes_hukuman_santri WHERE id_siswa=j.uuid AND sts='1') AS poin FROM pes_hukuman_santri h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.sts='1' GROUP BY h.id_siswa) sts 
							WHERE poin BETWEEN :start AND :end ) tbl LEFT OUTER JOIN (SELECT id_siswa FROM pes_surat_peringatan WHERE sts='1' AND sp_ke=:level) sp ON tbl.uuid=sp.id_siswa WHERE id_siswa is NULL AND (nama_lengkap LIKE :term OR nis LIKE :term) ");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam("start",$start);
	   $stmt->bindParam("end",$end);
	   $stmt->bindParam("level",$level);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailCetakSP($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.uuid, MAX(j.jenjang_pendidikan) AS jenjang, j.nis, s.nama_lengkap, s.tanggal_lahir, s.tempat_lahir, s.alamat, s. kelamin, s.ayah, s.wali, s.photo_siswa, a.kode_firqoh, a.nama_asrama,
					(SELECT tingkat FROM tbl_tingkat_siswa WHERE id_tahun_akademik='8' AND id_siswa='155167969353970000' GROUP BY id_siswa) AS tingkat,
					(SELECT SUM(poin_pelanggaran) FROM pes_hukuman_santri WHERE id_siswa=j.uuid AND sts='1') AS poin
			   FROM z_jenjang_siswa j JOIN z_siswa s ON s.uuid=j.uuid 
			   JOIN (SELECT s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik='8') a ON a.id_siswa=j.uuid
			   WHERE j.uuid='155167969353970000' AND j.status_kesiswaan='siswa'");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	
	public function searchSkorsingSantri($term,$start,$end)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT nama_lengkap, uuid, nis, tbl.poin, id_siswa FROM ( SELECT (SELECT nama_lengkap FROM z_siswa WHERE uuid=sts.uuid) AS nama_lengkap, sts.* 
							FROM (SELECT j.uuid, j.nis, (SUM(h.poin_pelanggaran)-(SELECT IFNULL(SUM(poin_amnesti),0) FROM pes_amnesti_santri WHERE id_siswa=j.uuid AND sts='1')) AS poin FROM pes_hukuman_santri h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.sts='1' GROUP BY h.id_siswa) sts 
							WHERE poin BETWEEN :start AND :end ) tbl LEFT OUTER JOIN (SELECT id_siswa, poin FROM pes_skorsing WHERE sts='1') sp ON tbl.uuid=sp.id_siswa WHERE id_siswa is NULL AND (nama_lengkap LIKE :term OR nis LIKE :term) ");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam("start",$start);
	   $stmt->bindParam("end",$end);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveSuper($token,$tanggal,$duedate,$keterangan,$nomor_surat,$tapel,$smt,$poin,$sp,$user)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_surat_peringatan(id_tahun_akademik,semester,id_siswa,tanggal_surat,duedate,akumulasi_poin,sp_ke,nomor_sp,keterangan,input_by) 
	   VALUES(:tapel,:smt,:token,:tanggal,:duedate,:poin,:sp,:nomor_sp,:keterangan,:input_by)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('token', $token);
       $stmt->bindParam('tanggal', $tanggal);
       $stmt->bindParam('duedate', $duedate);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('sp', $sp);
       $stmt->bindParam('nomor_sp', $nomor_surat);
       $stmt->bindParam('keterangan', $keterangan);
       $stmt->bindParam('input_by', $user);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
	   $stat[2] = $db->lastInsertId();
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBukuSuper($first,$last,array $var = array(),$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, s.nama_lengkap, h.auto, h.tanggal_surat, h.nomor_sp, h.duedate, h.sp_ke, h.pengembalian, h.tanggal_kembali, h.file, h.input_at,
							a.id_asrama, a.nama_asrama, a.kode_firqoh,(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
							(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
							(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=h.id_siswa GROUP BY id_siswa) AS tingkat,
							(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
							(SELECT username FROM m_user WHERE id_user=h.input_by) as admin,
							(SELECT username FROM m_user WHERE id_user=h.penerima) as penerima
						  FROM pes_surat_peringatan h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid 
						  JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid JOIN z_siswa s ON h.id_siswa=s.uuid
						  WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.input_at BETWEEN :first AND :last GROUP BY h.auto ORDER BY h.input_at ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function detailSuper($iditem)
    {
	  $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT * FROM pes_surat_peringatan WHERE auto=:id");
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function deleteSuper($iditem,$user)
    {
	  date_default_timezone_set('Asia/Jakarta');
	  $note = 'delete by '.$user.' at '.date('Y-m-d h:i:s').'';
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_surat_peringatan SET sts='0', sts_note=:note WHERE auto=:id");
       $stmt->bindParam('note', $note);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success delete!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function terimaSuper($iditem,$tanggal,$user,$file)
    {
	  $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_surat_peringatan SET tanggal_kembali=:date, penerima=:penerima, file=:file WHERE auto=:id");
       $stmt->bindParam('date', $tanggal);
       $stmt->bindParam('id', $iditem);
       $stmt->bindParam('penerima', $user);
       $stmt->bindParam('file', $file);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function cetakSuper($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT p.tanggal_surat,p.sp_ke,p.nomor_sp, p.keterangan, 
	   s.uuid, s.nama_lengkap, s.kelamin, s.tempat_lahir, s.tanggal_lahir, s.alamat, s.ayah
	   ");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveSkorsing($iditem,$first,$last,$nomor,$tapel,$smt,$poin,$user,$input_at)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_skorsing(id_tahun_akademik,semester,id_siswa,tanggal_mulai,tanggal_akhir,nomor_surat,poin,input_by,input_at) 
	   VALUES(:tapel,:smt,:token,:first,:last,:nomor,:poin,:input_by,:input_at)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('token', $iditem);
       $stmt->bindParam('first', $first);
       $stmt->bindParam('last', $last);
       $stmt->bindParam('nomor', $nomor);
       $stmt->bindParam('poin', $poin);
       $stmt->bindParam('input_by', $user);
       $stmt->bindParam('input_at', $input_at);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
	   $stat[2] = $db->lastInsertId();
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBukuSkorsing($first,$last,$tapel,$var,$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, s.nama_lengkap, h.autoid, h.tanggal_mulai, h.tanggal_akhir, h.nomor_surat,h.tanggal_kembali,h.accept_by,h.file,
							a.id_asrama, a.kode_firqoh, a.nama_asrama, (CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
							(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
							(SELECT tingkat FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=h.id_siswa GROUP BY id_siswa) AS tingkat,
							(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
							(SELECT username FROM m_user WHERE id_user=h.input_by) as admin
						  FROM pes_skorsing h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid 
						  JOIN (SELECT s.auto as id_asrama, s.nama_asrama, s.kode_firqoh, p.id_siswa FROM pes_asrama s JOIN pes_kamar k ON s.auto=k.asrama JOIN pes_pembagian_kamar p ON k.auto=p.id_kamar WHERE p.id_tahun_akademik=:tapel) a ON a.id_siswa=j.uuid JOIN z_siswa s ON h.id_siswa=s.uuid
						  WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.tanggal_mulai BETWEEN :first AND :last GROUP BY h.autoid ORDER BY h.input_at ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function detailSkorsing($iditem)
    {
	  $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT * FROM pes_skorsing WHERE autoid=:id");
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function terimaSkorsing($iditem,$tanggal,$file)
    {
	  $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_skorsing SET tanggal_kembali=:date, file=:file WHERE autoid=:id");
       $stmt->bindParam('date', $tanggal);
       $stmt->bindParam('id', $iditem);
       $stmt->bindParam('file', $file);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success update!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function deleteSkorsing($iditem,$user)
    {
      date_default_timezone_set('Asia/Jakarta');
	  $note = 'delete by '.$user.' at '.date('Y-m-d h:i:s').'';
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("UPDATE pes_skorsing SET sts='0', sts_note=:note WHERE autoid=:id");
       $stmt->bindParam('note', $note);
       $stmt->bindParam('id', $iditem);
	   $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success delete!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function searchDropoutSantri($term,$start,$end)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT nama_lengkap, uuid, nis, tbl.poin FROM ( SELECT (SELECT nama_lengkap FROM z_siswa WHERE uuid=sts.uuid) AS nama_lengkap, sts.* 
							FROM (SELECT j.uuid, j.nis, SUM(h.poin_pelanggaran) AS poin FROM pes_hukuman_santri h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid WHERE h.sts='1' AND j.status_kesiswaan='siswa' AND h.sts='1' GROUP BY h.id_siswa) sts 
							WHERE poin BETWEEN :start AND :end OR EXISTS (SELECT DISTINCT(s.id_siswa) AS id_siswa FROM pes_skorsing s WHERE s.id_siswa=uuid AND s.sts='1')) tbl WHERE nama_lengkap LIKE :term OR nis LIKE :term ");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam("start",$start);
	   $stmt->bindParam("end",$end);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveDropout($siswa,$nomor,$tanggal,$tapel,$smt,$file,$note,$sts,$input_by)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("INSERT INTO pes_dropout(id_tahun_akademik,semester,id_siswa,nomor_surat,tanggal_surat,file,keterangan,input_by) 
	   VALUES(:tapel,:smt,:siswa,:nomor,:tanggal,:file,:note,:input_by)");
       $stmt->bindParam('tapel', $tapel);
       $stmt->bindParam('smt', $smt);
       $stmt->bindParam('siswa', $siswa);
       $stmt->bindParam('nomor', $nomor);
       $stmt->bindParam('tanggal', $tanggal);
       $stmt->bindParam('file', $file);
       $stmt->bindParam('note', $note);
       $stmt->bindParam('input_by', $input_by);
       $stmt->execute();
       $stat[0] = true;
	   $stat[1] = "Success save!";
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBukuDropout($first,$last,array $var = array(),$search)
	{
	  $db = $this->dblocal;
	  $query_arr=array();
	  foreach($var as $col => $val){
		$query_arr[] = "".$col." '".$val."'";
	  }
	  
	  if($search  != ''){ array_push($query_arr,'(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")');}
	  $where = (!empty($query_arr))?'WHERE':'';
	  
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM (SELECT MAX(j.jenjang_pendidikan), j.nis, j.jenis, j.tingkat_kelas, s.nama_lengkap, h.autoid, h.tanggal_surat, h.nomor_surat,h.input_by,h.file,
							(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin, 
							(CASE WHEN j.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenis_jenjang,
							(CASE WHEN j.jenis='tidak' THEN 'Bukan MDK' ELSE 'MDK' END) AS mdk,
							(SELECT username FROM m_user WHERE id_user=h.input_by) as admin
						  FROM pes_dropout h JOIN z_jenjang_siswa j ON h.id_siswa=j.uuid JOIN z_siswa s ON h.id_siswa=s.uuid
						  WHERE h.sts='1' AND h.tanggal_surat BETWEEN :first AND :last GROUP BY h.autoid ORDER BY h.tanggal_surat ASC) data ".$where." ".implode(' AND ',$query_arr)."");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getJenjangSiswa($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT MAX(jenjang_pendidikan), id FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND uuid=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updateStatusKesiswaan($id,$alasan)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_jenjang_siswa SET status_kesiswaan='keluar', alasan_keluar=:alasan WHERE id=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("alasan",$alasan);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Success';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
}
	