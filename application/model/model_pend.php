 <?php
class model_pend extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
	
	/****************************** START SETTING TAHUN PELAJARAN & DATA POKOK ************/
	public function getTapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.* FROM z_tahun_ajaran t, 
        (SELECT @rownum := 0) r ORDER BY thn_ajaran_id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDetailTapel($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from z_tahun_ajaran a where a.thn_ajaran_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activeTapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM z_tahun_ajaran WHERE akademik='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function hari()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id,hari FROM z_hari");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	//KELAS
	public function getKelas()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, id_kelas, kelas, 
							(SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=z_kelas.wali_kelas) AS wali_kelas,
							jenjang,
							(CASE WHEN tingkat='1' THEN 'Tingkat 1' WHEN tingkat='2' THEN 'Tingkat 2' WHEN tingkat='3' THEN 'Tingkat 3' END) AS tingkat
							FROM z_kelas, (SELECT @rownum := 0) r ORDER BY urut, tingkat ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	
	public function getPegawaiAktif()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, id_pegawai, nip, nama_lengkap FROM z_pegawai, (SELECT @rownum := 0) r WHERE aktif='aktif' ORDER BY nip ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveKelas($name,$wali,$jenjang,$tingkat,$order){
		
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("INSERT INTO z_kelas (kelas,tingkat,jenjang,wali_kelas,id_model_jadwal,urut) VALUES(:name,:tingkat,:jenjang,:wali,'1',:order);");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("jenjang",$jenjang);
		 $stmt->bindParam("wali",$wali);
		 $stmt->bindParam("order",$order);
		 
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailKelas($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select id_kelas, kelas, tingkat, jenjang, wali_kelas, 
		 (SELECT concat(nip,' - ',nama_lengkap) FROM z_pegawai WHERE id_pegawai=a.wali_kelas) AS nama_wali
		 from z_kelas a where a.id_kelas = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateKelas($id,$name,$wali,$jenjang,$tingkat,$order){
		
				
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE z_kelas SET kelas = :name, tingkat = :tingkat, jenjang = :jenjang, wali_kelas = :wali, urut = :order WHERE id_kelas = :id;");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("jenjang",$jenjang);
		 $stmt->bindParam("wali",$wali);
		 $stmt->bindParam("order",$order);
		 $stmt->bindParam("id",$id);
		 
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $jenjang;
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getWaliKelas($jenjang,$tingkat,$tapel)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT k.id_kelas, k.kelas, w.id_guru, (SELECT CONCAT(nip,' - ',nama_lengkap) FROM z_pegawai WHERE id_pegawai=w.id_guru) AS wali 
							FROM z_kelas k LEFT JOIN (SELECT * FROM tbl_wali_kelas WHERE id_tahun_akademik=:tapel) w ON k.id_kelas=w.id_kelas WHERE jenjang=:jenjang AND tingkat=:tingkat ORDER BY k.kelas ASC");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function insertWaliKelas($data){
	  $sql = "INSERT INTO tbl_wali_kelas(id_tahun_akademik,id_guru,id_kelas) VALUES ".implode(', ',$data)." ";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteWaliKelas($tapel){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM tbl_wali_kelas WHERE id_tahun_akademik=:tapel");
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	/****************** END KELAS ***************************/
	/****************** START SANTRI ***************************/
	
	public function getDataSantri(){
		$db = $this->dblocal;
		$array = $this->activeTapel();
	    $tapel = $array[1]['thn_ajaran_id'];
		// initilize all variable
		$params = $columns = $totalRecords = $data = array();

		$params = $_REQUEST;

		//define index of column
		$columns = array( 
			0 =>'urutan',
			1 =>'nis', 
			2 => 'nama_lengkap',
			3 => 'alamat_lengkap',
			4 => 'jenjang_pendidikan',
			5 => 'tingkat_kelas',
		);

		$where = $sqlTot = $sqlRec = "";

		// check search value exist
		if( !empty($params['search']['value']) ) {   
			$where .=" WHERE ";
			$where .=" ( nis LIKE '%".$params['search']['value']."%' ";    
			$where .=" OR nama_lengkap LIKE '%".$params['search']['value']."%' ";
			$where .=" OR alamat_lengkap LIKE '%".$params['search']['value']."%' ";
			$where .=" OR jenjang_pendidikan LIKE '%".$params['search']['value']."%' ";
			$where .=" OR tingkat_kelas LIKE '%".$params['search']['value']."%' )";
		}

		// getting total number records without any search
		$sql = "SELECT @rownum := @rownum + 1 AS urutan, data.* FROM
				(SELECT s.uuid,s.nama_lengkap, concat(s.alamat,' ',s.kabupaten_name,' ',s.propinsi_name) AS alamat_lengkap, j.jid, j.nis, j.jenjang_pendidikan, j.tingkat_kelas, 'settle' AS tipe FROM
				(SELECT uuid, nama_lengkap, alamat, (SELECT name FROM provinces WHERE id=propinsi) AS propinsi_name,
				(SELECT name FROM regencies WHERE id=kabupaten) AS kabupaten_name FROM z_siswa) s
				JOIN
				(SELECT MAX(id) AS jid, uuid, nis, (CASE WHEN jenis='mdk' THEN 'mdk' WHEN jenjang_pendidikan='2' THEN 'ula' WHEN jenjang_pendidikan='3' THEN 'wustho' END) AS jenjang_pendidikan, tingkat_kelas FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j 
				ON s.uuid=j.uuid
				UNION ALL
				SELECT s.uuid,s.nama_lengkap, concat(s.alamat,' ',s.kabupaten_name,' ',s.propinsi_name) AS alamat_lengkap, j.jid, j.nis, j.jenjang_pendidikan, j.tingkat_kelas, 'nonsettle' AS tipe FROM 
				(SELECT uuid, nama_lengkap, alamat, (SELECT name FROM provinces WHERE id=propinsi) AS propinsi_name, (SELECT name FROM regencies WHERE id=kabupaten) AS kabupaten_name FROM tbl_santri) s 
				JOIN (SELECT MAX(id) AS jid, uuid, nis, jenjang_pendidikan, tingkat_kelas FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j ON s.uuid=j.uuid) data,(SELECT @rownum := 0) r ";
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}


		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		$number = $db->prepare($sqlTot);
		$number->execute();
		$totalRecords = $number->rowCount();
		
		$array = $db->prepare($sqlRec);
		$array->execute();
		$queryRecords = $array->fetchAll(PDO::FETCH_ASSOC);
		
		//$data[] = $queryRecords;
		
		$json_data = array(
				"draw"            => intval( $params['draw'] ),   
				"recordsTotal"    => intval( $totalRecords ),  
				"recordsFiltered" => intval($totalRecords),
				"data"            => $queryRecords   // total data array
				);

		return $json_data;  // send data as json format
	}
	
	public function saveRiwayatPendidikan($berkas){
	  
	  $keys = array();
	  foreach($berkas as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO tbl_santri_riwayat_pendidikan(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($berkas as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertJenjangSantri($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO tbl_jenjang_santri(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveSantri($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO tbl_santri(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getKodeSantri($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT j.uuid, (SELECT photo_siswa FROM tbl_santri WHERE uuid=j.uuid) AS photo_siswa, 
		 (SELECT photo_wali FROM tbl_santri WHERE uuid=j.uuid) AS photo_wali FROM tbl_jenjang_santri j WHERE j.id=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailSantri($uuid){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.*,j.* FROM tbl_santri s JOIN 
		 (SELECT id AS jid, uuid, nis,jenjang_pendidikan,tingkat_kelas,status_kesiswaan FROM tbl_jenjang_santri WHERE uuid = :uuid) j 
		 ON s.uuid=j.uuid WHERE s.uuid=:uuid");
		 $stmt->bindParam("uuid",$uuid);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getNilaiRaportSiswa($kelas,$siswa)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare( "SELECT m.*, ROUND(n.rata,2) as rata FROM
	   (SELECT s.id_kelompok_mapel, s.mapel FROM z_kelompok_mapel m JOIN z_mata_pelajaran s ON m.id=s.id_mapel) m
	   LEFT JOIN 
	   (SELECT m.id_kelompok_mapel, AVG(nilai) AS rata FROM z_master_nilai m 
		 LEFT JOIN (SELECT id_nilai, nilai FROM z_detail_nilai WHERE id_siswa=:siswa AND id_periode=:tapel) n 
		 ON m.id_master_nilai=n.id_nilai GROUP BY m.id_kelompok_mapel
	   ) n ON m.id_kelompok_mapel=n.id_kelompok_mapel");
	   
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("siswa",$siswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}

	public function getRiwayatPendidikan($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.* FROM tbl_santri_riwayat_pendidikan s WHERE s.uuid=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateSantri($uuid,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE tbl_santri SET ".implode(', ',$sets)." WHERE uuid='".$uuid."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateJenjangSantri($jid,$data){
	  
	  $sets = array();
	  foreach($data as $param => $val){
		$sets[] = "".$param." = :".$param."";
	  }
	  $sql = "UPDATE tbl_jenjang_santri SET ".implode(', ',$sets)." WHERE id='".$jid."'";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteRiwayatPendidikan($uuid){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM tbl_santri_riwayat_pendidikan WHERE uuid=:uuid");
		$stmt->bindParam("uuid",$uuid);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getKelasRombel($jenjang,$tingkat)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_kelas, kelas FROM z_kelas WHERE jenjang=:jenjang AND tingkat=:tingkat");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	
	
	public function getNonRombel($jenjang,$tingkat,$satuan,$jenis,$term)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  if($term == NULL){
			$search = '';
		}else{
			$search = "d.nama_lengkap LIKE '%".$term."%' OR d.nis LIKE '%".$term."%' AND ";
		}
	  try
	  {
	   $stmt = $db->prepare("SELECT d.* FROM (
								SELECT s.uuid, s.nama_lengkap, j.nis, 'nonsettle' AS tipe FROM tbl_santri s JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' AND jenjang_pendidikan=:jenjang AND tingkat_kelas=:tingkat GROUP BY uuid) j ON s.uuid=j.uuid
								UNION ALL
								SELECT s.uuid, s.nama_lengkap, j.nis, 'settle' AS tipe FROM z_siswa s JOIN 
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenis=:jenis AND jenjang_pendidikan=:satuan AND tingkat_kelas=:tingkat GROUP BY uuid) j ON s.uuid=j.uuid
							) d
							LEFT OUTER JOIN (SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel) r ON r.id_siswa=d.uuid WHERE ".$search." r.id_siswa IS NULL");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->bindParam("satuan",$satuan);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSiswaRombel($kelas,$term)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  if($term == NULL){
		$search = '';
	}else{
		$search = "WHERE sd.nama_lengkap LIKE '%".$term."%' OR sd.nis LIKE '%".$term."%' ";
	}
	  try
	  {
	   $stmt = $db->prepare (" SELECT * FROM (SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid
							UNION ALL
							SELECT  d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid) sd ".$search." ");
	   
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveCheckin($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("INSERT INTO tbl_rombel(id_tahun_akademik,id_kelas,id_siswa) VALUES(:tapel,:id_kelas,:id_siswa)");
		$stmt->bindParam("id_kelas",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteRombelSiswa($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:id_kelas AND id_siswa=:id_siswa");
		$stmt->bindParam("id_kelas",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertTransisi($data){
	  $sql = "INSERT INTO tbl_transisi(id_tahun_akademik,id_siswa,id_kelas_awal,tingkat_awal,id_kelas_baru,tingkat_baru) VALUES ".implode(', ',$data)." ";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteTransisi($tapel,$tingkat_awal,$kelas_awal){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM tbl_transisi WHERE id_tahun_akademik=:tapel AND id_kelas_awal=:id_kelas AND tingkat_awal=:tingkat");
		$stmt->bindParam("id_kelas",$kelas_awal);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("tingkat",$tingkat_awal);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertSiswaLulus($data){
	  $sql = "INSERT INTO tbl_siswa_lulus(id_tahun_akademik,id_siswa,id_jenjang,id_tingkat,id_kelas) VALUES ".implode(', ',$data)." ";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteTblLulus($tapel,$tingkat_awal,$kelas_awal){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM tbl_siswa_lulus WHERE id_tahun_akademik=:tapel AND id_kelas=:id_kelas AND id_tingkat=:tingkat");
		$stmt->bindParam("id_kelas",$kelas_awal);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("tingkat",$tingkat_awal);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getSiswaTransisi($tingkat,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  $kelas_baru = (empty($kelas))?'':'AND id_kelas_baru="'.$kelas.'"';
	  try
	  {
	   $stmt = $db->prepare("SELECT sis.*, k.kelas FROM 	
							  (	SELECT d.uuid, d.nis, s.nama_lengkap, d.id_transisi, d.id_kelas_baru FROM z_siswa s JOIN
								( SELECT * FROM
								(SELECT id as id_transisi, id_siswa, id_kelas_baru FROM tbl_transisi WHERE tingkat_baru=:tingkat ".$kelas_baru." ) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid 
								UNION ALL
								SELECT d.uuid, d.nis, s.nama_lengkap, d.id_transisi, d.id_kelas_baru FROM tbl_santri s JOIN
								( SELECT * FROM
								(SELECT id as id_transisi, id_siswa, id_kelas_baru FROM tbl_transisi WHERE tingkat_baru=:tingkat ".$kelas_baru.") r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
							  ) sis LEFT JOIN z_kelas k ON k.id_kelas=sis.id_kelas_baru ORDER BY k.kelas ASC");
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteKelasTujuan($id_transisi){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("UPDATE tbl_transisi SET id_kelas_baru=NULL WHERE id=:id");
		$stmt->bindParam("id",$id_transisi);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	############################# KELOMPOK MAPEL ########################################
	
	public function getKelompokMapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urut,t.* FROM z_kelompok_mapel t, 
        (SELECT @rownum := 0) r ORDER BY urutan ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveJenisKM($name,$alias,$urutan)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO z_kelompok_mapel(kelompok_mapel,alias,urutan) VALUES(:name,:alias,:urutan)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("alias",$alias);
		$stmt->bindParam("urutan",$urutan);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailKM($id)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT * FROM z_kelompok_mapel WHERE id_kelompok_mapel=:id");
	   $stmt->bindParam("id",$id);
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateJenisKM($iditem,$name,$alias,$urutan)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("UPDATE z_kelompok_mapel SET kelompok_mapel=:name, alias=:alias, urutan=:urutan WHERE id_kelompok_mapel=:id");
		$stmt->bindParam("id",$iditem);
		$stmt->bindParam("name",$name);
		$stmt->bindParam("alias",$alias);
		$stmt->bindParam("urutan",$urutan);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getListMapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urut,(SELECT kelompok_mapel FROM z_kelompok_mapel WHERE id_kelompok_mapel=t.id_kelompok_mapel) AS kitab,t.* 
	   FROM z_mata_pelajaran t,(SELECT @rownum := 0) r ORDER BY mapel, jenjang ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveMapel($name,$alias,$standar,$cat,$jenjang)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO z_mata_pelajaran(mapel,alias,capaian,id_kelompok_mapel,jenjang) VALUES(:name,:alias,:standar,:cat,:jenjang)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("alias",$alias);
		$stmt->bindParam("standar",$standar);
		$stmt->bindParam("cat",$cat);
		$stmt->bindParam("jenjang",$jenjang);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailMapel($id)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT * FROM z_mata_pelajaran WHERE id_mapel=:id");
	   $stmt->bindParam("id",$id);
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function updateMapel($iditem,$name,$alias,$standar,$cat,$jenjang)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("UPDATE z_mata_pelajaran SET mapel=:name, alias=:alias, capaian=:standar, id_kelompok_mapel=:cat, jenjang=:jenjang WHERE id_mapel=:id");
		$stmt->bindParam("id",$iditem);
		$stmt->bindParam("name",$name);
		$stmt->bindParam("alias",$alias);
		$stmt->bindParam("standar",$standar);
		$stmt->bindParam("cat",$cat);
		$stmt->bindParam("jenjang",$jenjang);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	
	######################## JADWAL PELAJARAN ################################
	public function hariJadwal()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT h.hari, h.id as hari_ke FROM z_model_jadwal_jam j JOIN z_hari h ON h.id=j.hari GROUP BY h.id");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jamJadwal($hari)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id, jamke, TIME_FORMAT(jam_mulai, '%H:%i') as jam_mulai, TIME_FORMAT(jam_selesai, '%H:%i') as jam_selesai FROM z_model_jadwal_jam  WHERE hari=:hari ORDER BY jamke ASC");
	   $stmt->bindParam("hari",$hari);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jadwalMapel($id_model,$id_kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_jadwal_mapel, (SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=m.id_pegawai) as tutor, 
						(SELECT mapel FROM z_mata_pelajaran WHERE id_mapel=m.id_mapel) AS subject, m.id_mapel 
						FROM z_jadwal_mapel m WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model");
	   $stmt->bindParam("model",$id_model);
	   $stmt->bindParam("kelas",$id_kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getTutor()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_pegawai, nip, nama_lengkap FROM z_pegawai WHERE aktif='aktif'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getMapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_mapel as id, mapel as nama_mapel FROM z_mata_pelajaran ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jamKosong($hari,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.id, j.jamke, TIME_FORMAT(j.jam_mulai, '%H:%i') as jam_mulai, TIME_FORMAT(j.jam_selesai, '%H:%i') as jam_selesai FROM z_model_jadwal_jam j
							LEFT OUTER JOIN (SELECT id_model_jadwal_jam FROM z_jadwal_mapel WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) t ON j.id=t.id_model_jadwal_jam 
							WHERE j.hari=:hari AND t.id_model_jadwal_jam IS NULL ORDER BY j.jamke ASC");
	   $stmt->bindParam("hari",$hari);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function cekJadwal($kelas,$model,$tutor)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_jadwal_mapel FROM z_jadwal_mapel WHERE id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model AND id_pegawai=:tutor");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->bindParam("tutor",$tutor);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function simpanJadwal($mapel,$kelas,$model,$tutor)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("INSERT INTO z_jadwal_mapel(id_mapel,id_kelas,id_tahun_ajaran,id_model_jadwal_jam,id_pegawai) VALUES(:mapel,:kelas,:tapel,:model,:tutor)");
	   $stmt->bindParam("mapel",$mapel);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->bindParam("tutor",$tutor);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Success save!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteJadwal($kelas,$model)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM z_jadwal_mapel WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	################################ Input Nilai ###############################
	
	public function getJenisNilai()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM z_jenis_nilai ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailJenisNilai($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_nilai WHERE id_jenis=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSubjekbyKelas($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.id_mapel as id, m.mapel as subjek FROM z_jadwal_mapel j JOIN z_mata_pelajaran m ON m.id_mapel=j.id_mapel 
							WHERE j.id_tahun_ajaran=:tapel AND j.id_kelas=:kelas GROUP BY j.id_mapel");
	   $stmt->bindParam('kelas',$kelas);
	   $stmt->bindParam('tapel',$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSubSubjek($subjek)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_sub_mapel as id, sub_mapel FROM pes_sub_mapel_program WHERE id_mapel=:id");
	   $stmt->bindParam('id',$subjek);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getMapelTutor()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_mapel_program ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailSubMapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT m.mapel, m.id_mapel FROM z_mata_pelajaran m WHERE m.id_mapel=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function cekNamaNilai($nama,$subsubjek,$jenis,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_master_nilai as nilai FROM z_master_nilai WHERE nama_nilai=:nama AND id_mapel=:mapel
							 AND id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_jenis=:jenis");
	   $stmt->bindParam("nama",$nama);
	   $stmt->bindParam("mapel",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteDetailNilaiSiswa($id)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_detail_nilai WHERE id_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function simpanMasterNilai($nama,$subsubjek,$jenis,$kelas,$tanggal,$user)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("INSERT INTO z_master_nilai(nama_nilai,id_mapel,id_kelas,id_tahun_ajaran,id_jenis,tanggal,input_by) 
							VALUES(:nama,:mapel,:kelas,:tapel,:jenis,:tanggal,:user)");
	   $stmt->bindParam("nama",$nama);
	   $stmt->bindParam("mapel",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->bindParam("user",$user);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $db->lastInsertId();
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	
	public function saveNilaiSiswa($data){
	  
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['master'].'","'.$d['mapel'].'","'.$tapel.'","'.$d['siswa'].'","'.$d['nilai'].'")';
	  }
	  
	  $sql = "INSERT INTO z_detail_nilai (id_nilai,mapel,id_periode,id_siswa,nilai) VALUES ".implode(',', $insert_values);
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getListNilai($kelas,$subsubjek,$jenis)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_master_nilai, nama_nilai, tanggal FROM z_master_nilai WHERE id_mapel=:mapel
							 AND id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_jenis=:jenis ORDER BY tanggal ASC");
	   $stmt->bindParam("mapel",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteNilaiSiswa($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_master_nilai WHERE id_master_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stmt = $db->prepare("DELETE FROM z_detail_nilai WHERE id_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailNamaNilai($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id_master_nilai as id, m.nama_nilai, m.id_tahun_ajaran,m.id_kelas, 
								(SELECT kelas FROM z_kelas WHERE id_kelas=m.id_kelas) as kelas,
								(SELECT mapel FROM z_mata_pelajaran WHERE id_mapel=m.id_mapel) AS subjek,
								(SELECT jenis_nilai FROM z_jenis_nilai WHERE id_jenis=m.id_jenis) AS jenis 
							FROM z_master_nilai m WHERE m.id_master_nilai=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getNilaiSiswaEdit($kelas,$id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT l.*,n.nilai FROM
							(SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
								UNION ALL
								SELECT d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
							) l
							LEFT JOIN 
                            (SELECT id_siswa, nilai FROM z_detail_nilai WHERE id_nilai=:id) n 
                            ON l.uuid=n.id_siswa ORDER BY l.nis ASC
							");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	############################## Absensi Santri #####################################
	
	public function getAbsenSiswa($kelas,$tanggal)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT l.*,a.absen FROM
							(SELECT d.uuid as nomor, d.nis, s.nama_lengkap FROM z_siswa s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
								UNION ALL
								SELECT d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
							) l 
							LEFT JOIN (SELECT id_siswa, absen FROM z_absen_siswa WHERE tanggal=:tanggal ) a ON l.nomor=a.id_siswa ORDER BY l.nis ASC");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function countAbsenSiswa($siswa)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT 
								COUNT(CASE WHEN absen='H' THEN 1 ELSE NULL END) AS H,
								COUNT(CASE WHEN absen='A' THEN 1 ELSE NULL END) AS A,
								COUNT(CASE WHEN absen='S' THEN 1 ELSE NULL END) AS S,
								COUNT(CASE WHEN absen='I' THEN 1 ELSE NULL END) AS I 
							FROM z_absen_siswa WHERE id_tahun_akademik=:tapel AND id_siswa=:siswa");
	   $stmt->bindParam("siswa",$siswa);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteAbsenSantri($tanggal,$kelas)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM z_absen_siswa WHERE tanggal=:tanggal AND kelas=:kelas");
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveAbsenSiswa($data){
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['siswa'].'","'.$d['tanggal'].'","'.$d['absen'].'","'.$d['kelas'].'","'.$tapel.'")';
	  }
	  
	  $sql = "INSERT INTO z_absen_siswa (id_siswa,tanggal,absen,kelas,id_tahun_akademik) VALUES ".implode(',', $insert_values);
	  
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getAbsenTutor($tanggal)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.*, a.absen FROM
							(SELECT j.id_pegawai, p.nip, p.nama_lengkap FROM z_jadwal_mapel j JOIN z_model_jadwal_jam m ON j.id_model_jadwal_jam=m.id JOIN z_pegawai p ON j.id_pegawai=p.id_pegawai 
							WHERE m.hari=DAYOFWEEK(:tanggal)  AND j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai) d 
							LEFT JOIN (SELECT id_pegawai, absen FROM z_absen_pengajar WHERE tanggal=:tanggal) a ON d.id_pegawai=a.id_pegawai ORDER BY d.nip ASC");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteAbsenTutor($tanggal)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM z_absen_pengajar WHERE tanggal=:tanggal");
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveAbsenTutor($data){
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['pegawai'].'","'.$d['tanggal'].'","'.$d['absen'].'")';
	  }
	  
	  $sql = "INSERT INTO z_absen_pengajar (id_pegawai,tanggal,absen) VALUES ".implode(',', $insert_values);
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getReportAbsenSiswa($kelas,$first,$last)
	{
	  $db = $this->dblocal;
	  $tahun = $this->activeTapel();
	  $ta = $tahun[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT s.*,a.absen, a.tanggal  FROM      
         (SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
			( SELECT * FROM
			(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
			LEFT JOIN
			(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
			ON j.uuid=r.id_siswa
			) d ON d.uuid=s.uuid
			UNION ALL
			SELECT d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
			( SELECT * FROM
			(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
			LEFT JOIN
			(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
			ON j.uuid=r.id_siswa
			) d ON d.uuid=s.uuid
         ) s 
         LEFT JOIN (SELECT id_siswa, absen, DATE_FORMAT(tanggal,'%d-%m') as tanggal FROM z_absen_siswa WHERE tanggal BETWEEN :first AND :last GROUP BY tanggal,id_siswa) a ON s.uuid=a.id_siswa ORDER BY s.nis ASC");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$ta);
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getReportAbsenTutor($first,$last)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.nip,d.nama_lengkap,j.jadwal_hari,a.tanggal, a.absen FROM
							(SELECT j.id_pegawai, p.nip, p.nama_lengkap FROM z_jadwal_mapel j JOIN z_pegawai p ON j.id_pegawai=p.id_pegawai WHERE j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai) d
							LEFT JOIN 
							(SELECT j.id_pegawai, m.hari as jadwal_hari FROM z_jadwal_mapel j JOIN z_model_jadwal_jam m ON j.id_model_jadwal_jam=m.id WHERE j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai, m.hari) j
							ON d.id_pegawai=j.id_pegawai 
							LEFT JOIN (SELECT id_pegawai, absen, DATE_FORMAT(tanggal, '%d-%m') as tanggal, dayofweek(tanggal) as hari FROM z_absen_pengajar WHERE tanggal BETWEEN :first AND :last) a 
							ON d.id_pegawai=a.id_pegawai AND a.hari=j.jadwal_hari ORDER BY d.nip,a.tanggal");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getheaderrekapsubmapel($kelas,$subsubjek)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id_mapel, m.mapel, j.jenis_nilai, n.id_master_nilai, n.nama_nilai FROM z_mata_pelajaran m CROSS JOIN z_jenis_nilai j 
		LEFT JOIN (SELECT * FROM z_master_nilai WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) n 
		ON n.id_jenis=j.id_jenis AND n.id_mapel=m.id_mapel WHERE m.id_mapel=:subsubjek");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("subsubjek",$subsubjek);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getrekapsubmapel($kelas,$subsubjek)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT s.uuid, s.nis,s.nama_lengkap, m.id_master_nilai, n.nilai FROM 
		( 	SELECT m.id_mapel, m.mapel, j.jenis_nilai, n.id_master_nilai, n.nama_nilai FROM z_mata_pelajaran m CROSS JOIN z_jenis_nilai j 
			LEFT JOIN (SELECT * FROM z_master_nilai WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) n 
			ON n.id_jenis=j.id_jenis AND n.id_mapel=m.id_mapel WHERE m.id_mapel=:subsubjek
		) m
		CROSS JOIN 
		(  	SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
			( SELECT * FROM
			(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
			LEFT JOIN
			(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
			ON j.uuid=r.id_siswa
			) d ON d.uuid=s.uuid
			UNION ALL
			SELECT d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
			( SELECT * FROM
			(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
			LEFT JOIN
			(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
			ON j.uuid=r.id_siswa
			) d ON d.uuid=s.uuid
     	) s
		LEFT JOIN 
		( 	SELECT a.id_nilai, a.id_siswa, ROUND(a.nilai,2) as nilai FROM z_detail_nilai a JOIN z_master_nilai b ON a.id_nilai=b.id_master_nilai 
			WHERE b.id_tahun_ajaran=:tapel AND b.id_mapel=:subsubjek
		) n 
     	ON s.uuid=n.id_siswa AND n.id_nilai=m.id_master_nilai");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("subsubjek",$subsubjek);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function autoCompleteSearch($term,$kelas)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
								UNION ALL
								SELECT d.uuid, d.nis, s.nama_lengkap FROM tbl_santri s JOIN
								( SELECT * FROM
								(SELECT id_siswa FROM tbl_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
								LEFT JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' GROUP BY uuid) j
								ON j.uuid=r.id_siswa
								) d ON d.uuid=s.uuid
							) d ON d.uuid=s.uuid WHERE d.nis LIKE :term or s.nama_lengkap LIKE :term ORDER BY s.nama_lengkap ASC
							");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->bindParam('kelas', $kelas);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function generateDate($first,$last)
	{
	  $db = $this->dblocal;
	  
	  try
	  {
	   $sql = "set @i = -1;";
	   $stmt = $db->prepare($sql);
	   $stmt->execute();
	   $stmt = $db->prepare("select DATE_FORMAT(gen_date, '%d-%m') as date, dayofweek(gen_date) as hari from
							(select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date from
							(select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
							(select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
							(select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
							(select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
							(select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
							Where gen_date between :first and :last ORDER BY gen_date DESC");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
}
	