 <?php
class model_program extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
	
	/****************************** START SETTING TAHUN PELAJARAN & DATA POKOK ************/
	public function getTahunAkademik()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT * FROM z_tahun_ajaran WHERE akademik='1'");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getTapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.id_periode as thn_ajaran_id, t.periode as thn_ajaran, semester, aktif FROM pes_periode_program t, 
        (SELECT @rownum := 0) r ORDER BY periode ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveTapel($item,$semester){
		$db = $this->dblocal;
		try
		{
		  $stmt = $db->prepare("INSERT INTO pes_periode_program(periode,semester) VALUES(UPPER(:thn_ajaran),:semester)");
		  $stmt->bindParam("thn_ajaran",$item);
		  $stmt->bindParam("semester",$semester);
		  $stmt->execute();
		  $stat[0] = true;
		  $stat[1] = "Success save!";
		  $stat[2] =  $stmt->rowCount();
		  return $stat;
		}
		catch(PDOException $ex)
		{
		  $stat[0] = false;
		  $stat[1] = $ex->getMessage();
		  return $stat;
		}
	  }
	  
	  public function updateTapel($id,$name,$semester)
	  {
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE pes_periode_program SET periode = UPPER(:name), semester=:semester WHERE id_periode= :id;");
		 $stmt->bindParam("id",$id);
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("semester",$semester);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success Edit!";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	  }
	
	public function getDetailTapel($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select id_periode as thn_ajaran_id, periode as thn_ajaran, semester from pes_periode_program where id_periode = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activateTapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE pes_periode_program SET aktif = (case when id_periode = :id then '1' when id_periode != :id then '0' end);");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function activeTapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_periode as thn_ajaran_id, periode as thn_ajaran, semester FROM pes_periode_program WHERE aktif='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function hari()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id,hari FROM z_hari");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	//KELAS
	public function getKelas()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, id_kelas, kelas, tingkat, nama_ruang							
							FROM pes_kelas, (SELECT @rownum := 0) r ORDER BY urut, tingkat ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveKelas($name,$tingkat,$ruang){
		
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("INSERT INTO pes_kelas (kelas,tingkat,nama_ruang,aktif) VALUES(:name,:tingkat,:ruang,'1');");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("ruang",$ruang);
		 
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailKelas($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select id_kelas, kelas, tingkat, nama_ruang
		 from pes_kelas a where a.id_kelas = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateKelas($iditem,$name,$tingkat,$ruang){
		
				
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE pes_kelas SET kelas = :name, tingkat = :tingkat, nama_ruang = :ruang WHERE id_kelas = :id;");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("ruang",$ruang);
		 $stmt->bindParam("id",$iditem);
		 
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	
	/****************** END KELAS ***************************/
	/****************** PESERTA PROGRAM INTENSIF ***************************/
	
	public function getNonRombel($jenjang,$tingkat,$satuan,$jenis,$term)
	{
	  if($term == '' || $term == NULL){
		$search = '';  
	  }else{
		$search = " AND d.nama_lengkap LIKE '%".$term."%'";
	  }
	  
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.* FROM (
							  SELECT s.uuid, s.nama_lengkap, j.nis, 'settle' AS tipe FROM z_siswa s JOIN 
							  (SELECT  MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenis=:jenis AND jenjang_pendidikan=:satuan GROUP BY uuid) j ON s.uuid=j.uuid
							  JOIN (SELECT id_siswa FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND tingkat=:tingkat GROUP BY id_siswa) t
							  ON s.uuid=t.id_siswa
							) d
							LEFT OUTER JOIN (SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel) r ON r.id_siswa=d.uuid WHERE r.id_siswa IS NULL ".$search." ");
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->bindParam("satuan",$satuan);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSiswaRombel($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid
							");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveCheckin($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("INSERT INTO pes_rombel(id_tahun_akademik,id_kelas,id_siswa) VALUES(:tapel,:id_kelas,:id_siswa)");
		$stmt->bindParam("id_kelas",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteRombelSiswa($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:id_kelas AND id_siswa=:id_siswa");
		$stmt->bindParam("id_kelas",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	
	####################### JADWAL PELAJARAN ################################
	
	public function hariJadwal()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT h.hari, h.id as hari_ke FROM pes_model_jadwal_jam j JOIN z_hari h ON h.id=j.hari GROUP BY h.id");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jamJadwal($hari)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id, jamke, TIME_FORMAT(jam_mulai, '%H:%i') as jam_mulai, TIME_FORMAT(jam_selesai, '%H:%i') as jam_selesai FROM pes_model_jadwal_jam  WHERE hari=:hari ORDER BY jamke ASC");
	   $stmt->bindParam("hari",$hari);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jamKosong($hari,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.id, j.jamke, TIME_FORMAT(j.jam_mulai, '%H:%i') as jam_mulai, TIME_FORMAT(j.jam_selesai, '%H:%i') as jam_selesai FROM pes_model_jadwal_jam j
							LEFT OUTER JOIN (SELECT id_model_jadwal_jam FROM pes_jadwal_mapel WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) t ON j.id=t.id_model_jadwal_jam 
							WHERE j.hari=:hari AND t.id_model_jadwal_jam IS NULL ORDER BY j.jamke ASC");
	   $stmt->bindParam("hari",$hari);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function jadwalMapel($id_model,$id_kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_jadwal_mapel, (SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=m.id_pegawai) as tutor, 
						(SELECT nama_mapel FROM pes_mapel_program WHERE id=m.id_mapel) AS subject, m.id_mapel 
						FROM pes_jadwal_mapel m WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model");
	   $stmt->bindParam("model",$id_model);
	   $stmt->bindParam("kelas",$id_kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getMapel()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_mapel_program ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailMapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_mapel_program WHERE id=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getTutor()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_pegawai, nip, nama_lengkap FROM z_pegawai WHERE aktif='aktif'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function cekJadwal($kelas,$model,$tutor)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_jadwal_mapel FROM pes_jadwal_mapel WHERE id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model AND id_pegawai=:tutor");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->bindParam("tutor",$tutor);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function simpanJadwal($mapel,$kelas,$model,$tutor)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("INSERT INTO pes_jadwal_mapel(id_mapel,id_kelas,id_tahun_ajaran,id_model_jadwal_jam,id_pegawai) VALUES(:mapel,:kelas,:tapel,:model,:tutor)");
	   $stmt->bindParam("mapel",$mapel);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->bindParam("tutor",$tutor);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Success save!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteJadwal($kelas,$model)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM pes_jadwal_mapel WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_model_jadwal_jam=:model");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("model",$model);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	############################## Absensi Santri #####################################
	
	public function getAbsenSiswa($kelas,$tanggal)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT l.uuid as nomor, l.nis, l.nama_lengkap, a.absen FROM
							(SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid
							) l 
							LEFT JOIN (SELECT id_siswa, absen FROM pes_absen_santri_progam WHERE tanggal=:tanggal ) a ON l.uuid=a.id_siswa ORDER BY l.nis ASC");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function countAbsenSiswa($siswa)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT 
								COUNT(CASE WHEN absen='H' THEN 1 ELSE NULL END) AS H,
								COUNT(CASE WHEN absen='A' THEN 1 ELSE NULL END) AS A,
								COUNT(CASE WHEN absen='S' THEN 1 ELSE NULL END) AS S,
								COUNT(CASE WHEN absen='I' THEN 1 ELSE NULL END) AS I 
							FROM pes_absen_santri_progam WHERE id_tahun_akademik=:tapel AND id_siswa=:siswa");
	   $stmt->bindParam("siswa",$siswa);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteAbsenSantri($tanggal,$kelas)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM pes_absen_santri_progam WHERE tanggal=:tanggal AND kelas=:kelas");
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveAbsenSiswa($data){
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['siswa'].'","'.$d['tanggal'].'","'.$d['absen'].'","'.$d['kelas'].'","'.$tapel.'")';
	  }
	  
	  $sql = "INSERT INTO pes_absen_santri_progam (id_siswa,tanggal,absen,kelas,id_tahun_akademik) VALUES ".implode(',', $insert_values);
	  
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getAbsenTutor($tanggal)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.*, a.absen FROM
							(SELECT j.id_pegawai, p.nip, p.nama_lengkap FROM pes_jadwal_mapel j JOIN pes_model_jadwal_jam m ON j.id_model_jadwal_jam=m.id JOIN z_pegawai p ON j.id_pegawai=p.id_pegawai 
							WHERE m.hari=DAYOFWEEK(:tanggal)  AND j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai) d 
							LEFT JOIN (SELECT id_pegawai, absen FROM pes_absen_pengajar_progam WHERE tanggal=:tanggal) a ON d.id_pegawai=a.id_pegawai ORDER BY d.nip ASC");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteAbsenTutor($tanggal)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM pes_absen_pengajar_progam WHERE tanggal=:tanggal");
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = 'Deleted!';
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveAbsenTutor($data){
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['pegawai'].'","'.$d['tanggal'].'","'.$d['absen'].'")';
	  }
	  
	  $sql = "INSERT INTO pes_absen_pengajar_progam (id_pegawai,tanggal,absen) VALUES ".implode(',', $insert_values);
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	################################ Input Nilai ###############################
	
	public function getJenisNilai()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_nilai ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailJenisNilai($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_jenis_nilai WHERE id_jenis=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSubjekbyKelas($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT j.id_mapel as id, m.nama_mapel as subjek FROM pes_jadwal_mapel j JOIN pes_mapel_program m ON m.id=j.id_mapel 
							WHERE j.id_tahun_ajaran=:tapel AND j.id_kelas=:kelas GROUP BY j.id_mapel");
	   $stmt->bindParam('kelas',$kelas);
	   $stmt->bindParam('tapel',$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSubSubjek($subjek)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT id_sub_mapel as id, sub_mapel FROM pes_sub_mapel_program WHERE id_mapel=:id");
	   $stmt->bindParam('id',$subjek);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getMapelTutor()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_mapel_program ");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailSubMapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT m.nama_mapel, s.id_sub_mapel, s.sub_mapel FROM pes_mapel_program m JOIN pes_sub_mapel_program s ON s.id_mapel=m.id WHERE s.id_sub_mapel=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function cekNamaNilai($nama,$subjek,$subsubjek,$jenis,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_master_nilai as nilai FROM pes_m_nilai WHERE nama_nilai=:nama AND id_mapel=:mapel AND id_sub_mapel=:id_sub
							 AND id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_jenis=:jenis");
	   $stmt->bindParam("nama",$nama);
	   $stmt->bindParam("mapel",$subjek);
	   $stmt->bindParam("id_sub",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function simpanMasterNilai($nama,$subjek,$subsubjek,$jenis,$kelas,$tanggal,$user)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("INSERT INTO pes_m_nilai(nama_nilai,id_mapel,id_sub_mapel,id_kelas,id_tahun_ajaran,id_jenis,tanggal,input_by) 
							VALUES(:nama,:mapel,:id_sub,:kelas,:tapel,:jenis,:tanggal,:user)");
	   $stmt->bindParam("nama",$nama);
	   $stmt->bindParam("mapel",$subjek);
	   $stmt->bindParam("id_sub",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("tanggal",$tanggal);
	   $stmt->bindParam("user",$user);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $db->lastInsertId();
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	
	public function saveNilaiSiswa($data){
	  
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$d['master'].'","'.$d['mapel'].'","'.$tapel.'","'.$d['siswa'].'","'.$d['nilai'].'")';
	  }
	  
	  $sql = "INSERT INTO pes_detail_nilai (id_nilai,mapel,id_periode,id_siswa,nilai) VALUES ".implode(',', $insert_values);
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getListNilai($kelas,$subjek,$subsubjek,$jenis)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT id_master_nilai, nama_nilai, tanggal FROM pes_m_nilai WHERE id_mapel=:mapel AND id_sub_mapel=:id_sub
							 AND id_kelas=:kelas AND id_tahun_ajaran=:tapel AND id_jenis=:jenis ORDER BY tanggal ASC");
	   $stmt->bindParam("mapel",$subjek);
	   $stmt->bindParam("id_sub",$subsubjek);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function generateDate($first,$last)
	{
	  $db = $this->dblocal;
	  
	  try
	  {
	   $sql = "set @i = -1;";
	   $stmt = $db->prepare($sql);
	   $stmt->execute();
	   $stmt = $db->prepare("select DATE_FORMAT(gen_date, '%d-%m') as date, dayofweek(gen_date) as hari from
							(select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) gen_date from
							(select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
							(select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
							(select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
							(select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
							(select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
							Where gen_date between :first and :last ORDER BY gen_date DESC");
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getReportAbsenSiswa($kelas,$first,$last)
	{
	  $db = $this->dblocal;
	  $tahun = $this->getTahunAkademik();
	  $ta = $tahun[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT s.*,a.absen, a.tanggal  FROM      
         (SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
            ( SELECT * FROM
               (SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
               LEFT JOIN
               (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
               ON j.uuid=r.id_siswa
            ) d ON d.uuid=s.uuid
            
         ) s 
         LEFT JOIN (SELECT id_siswa, absen, DATE_FORMAT(tanggal,'%d-%m') as tanggal FROM pes_absen_santri_progam WHERE tanggal BETWEEN :first AND :last GROUP BY tanggal,id_siswa) a ON s.uuid=a.id_siswa ORDER BY s.nis ASC");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$ta);
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getReportAbsenTutor($first,$last)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.nip,d.nama_lengkap,j.jadwal_hari,a.tanggal, a.absen FROM
							(SELECT j.id_pegawai, p.nip, p.nama_lengkap FROM pes_jadwal_mapel j JOIN z_pegawai p ON j.id_pegawai=p.id_pegawai WHERE j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai) d
							LEFT JOIN 
							(SELECT j.id_pegawai, m.hari as jadwal_hari FROM pes_jadwal_mapel j JOIN pes_model_jadwal_jam m ON j.id_model_jadwal_jam=m.id WHERE j.id_tahun_ajaran=:tapel GROUP BY j.id_pegawai, m.hari) j
							ON d.id_pegawai=j.id_pegawai 
							LEFT JOIN (SELECT id_pegawai, absen, DATE_FORMAT(tanggal, '%d-%m') as tanggal, dayofweek(tanggal) as hari FROM pes_absen_pengajar_progam WHERE tanggal BETWEEN :first AND :last) a 
							ON d.id_pegawai=a.id_pegawai AND a.hari=j.jadwal_hari ORDER BY d.nip,a.tanggal");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("first",$first);
	   $stmt->bindParam("last",$last);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getmapelrekap($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id, m.nama_mapel, s.sub_mapel, j.kode, n.id_master_nilai, n.nama_nilai FROM pes_mapel_program m 
							JOIN pes_sub_mapel_program s ON m.id=s.id_mapel CROSS JOIN pes_jenis_nilai j 
							LEFT JOIN (SELECT * FROM pes_m_nilai WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) n 
							ON n.id_jenis=j.id_jenis AND n.id_sub_mapel=s.id_sub_mapel AND n.id_mapel=m.id");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getdetailbysubsubjek($subsubjek)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id, m.nama_mapel, s.sub_mapel FROM pes_mapel_program m 
	  	JOIN pes_sub_mapel_program s ON m.id=s.id_mapel WHERE s.id_sub_mapel=:subsubjek");
	   $stmt->bindParam("subsubjek",$subsubjek);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getheaderrekapsubmapel($kelas,$subjek,$subsubjek)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id, m.nama_mapel, s.sub_mapel, j.jenis_nilai, n.id_master_nilai, n.nama_nilai FROM pes_mapel_program m 
	  	JOIN pes_sub_mapel_program s ON m.id=s.id_mapel CROSS JOIN pes_jenis_nilai j 
		LEFT JOIN (SELECT * FROM pes_m_nilai WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) n 
		ON n.id_jenis=j.id_jenis AND n.id_sub_mapel=s.id_sub_mapel AND n.id_mapel=m.id WHERE s.id_sub_mapel=:subsubjek");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("subsubjek",$subsubjek);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getrekapsubmapel($kelas,$subjek,$subsubjek)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  $tahun = $this->getTahunAkademik();
	  $ta = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT s.uuid, s.nis,s.nama_lengkap, m.id_master_nilai, n.nilai FROM 
	( 	SELECT m.id, m.nama_mapel, s.sub_mapel, j.jenis_nilai, n.id_master_nilai, n.nama_nilai FROM pes_mapel_program m 
	  	JOIN pes_sub_mapel_program s ON m.id=s.id_mapel CROSS JOIN pes_jenis_nilai j 
		LEFT JOIN (SELECT * FROM pes_m_nilai WHERE id_kelas=:kelas AND id_tahun_ajaran=:tapel) n 
		ON n.id_jenis=j.id_jenis AND n.id_sub_mapel=s.id_sub_mapel AND n.id_mapel=m.id WHERE s.id_sub_mapel=:subsubjek
    ) m
      CROSS JOIN 
    (SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
		( SELECT * FROM
		(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:ta AND id_kelas=:kelas) r
		LEFT JOIN
		(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
		ON j.uuid=r.id_siswa
		) d ON d.uuid=s.uuid
     	) s
       LEFT JOIN 
       ( SELECT a.id_nilai, a.id_siswa, ROUND(a.nilai,2) as nilai FROM pes_detail_nilai a JOIN pes_m_nilai b ON a.id_nilai=b.id_master_nilai WHERE b.id_tahun_ajaran=:tapel AND b.id_mapel=:subjek AND b.id_sub_mapel=:subsubjek) n 
     	ON s.uuid=n.id_siswa AND n.id_nilai=m.id_master_nilai");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("subjek",$subjek);
	   $stmt->bindParam("subsubjek",$subsubjek);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("ta",$ta);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function autoCompleteSearch($term,$kelas)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid WHERE d.nis LIKE :term or s.nama_lengkap LIKE :term ORDER BY s.nama_lengkap ASC
							");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->bindParam('kelas', $kelas);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailSiswa($kelas,$idSiswa)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap, s.ayah, s.wali FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas ) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid WHERE s.uuid=:siswa
							");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("siswa",$idSiswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getNilaiRaportSiswa($kelas,$siswa)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT m.*, ROUND(n.rata,2) as rata FROM
							  (SELECT s.id_sub_mapel, s.sub_mapel FROM pes_mapel_program m JOIN pes_sub_mapel_program s ON m.id=s.id_mapel) m
							  LEFT JOIN 
							  (SELECT m.id_sub_mapel, AVG(nilai) AS rata FROM pes_m_nilai m 
								LEFT JOIN (SELECT id_nilai, nilai FROM pes_detail_nilai WHERE id_siswa=:siswa AND id_periode=:tapel) n 
								ON m.id_master_nilai=n.id_nilai GROUP BY m.id_sub_mapel
							  ) n ON m.id_sub_mapel=n.id_sub_mapel");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("siswa",$siswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteNilaiSiswa($id)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM pes_m_nilai WHERE id_master_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stmt = $db->prepare("DELETE FROM pes_detail_nilai WHERE id_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteDetailNilaiSiswa($id)
	{
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM pes_detail_nilai WHERE id_nilai=:id");
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailNamaNilai($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT m.id_master_nilai as id, m.nama_nilai, m.id_tahun_ajaran,m.id_kelas, 
								(SELECT kelas FROM pes_kelas WHERE id_kelas=m.id_kelas) as kelas,
								(SELECT nama_mapel FROM pes_mapel_program WHERE id=m.id_mapel) AS subjek,
								(SELECT sub_mapel FROM pes_sub_mapel_program WHERE id_sub_mapel=m.id_sub_mapel) AS subsubjek,
								(SELECT jenis_nilai FROM pes_jenis_nilai WHERE id_jenis=m.id_jenis) AS jenis 
							FROM pes_m_nilai m WHERE m.id_master_nilai=:id");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getNilaiSiswaEdit($kelas,$id)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap, n.nilai FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM pes_rombel WHERE id_tahun_akademik=:tapel AND id_kelas=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid 
							 LEFT JOIN 
                            (SELECT id_siswa, nilai FROM pes_detail_nilai WHERE id_nilai=:id) n 
                            ON s.uuid=n.id_siswa
							");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPejabat()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT ketua, bahasa_1 FROM pes_pengurus_markazy LIMIT 1");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	############################# KATEGORI MATERI UBUDIYAH ##################################
	
	public function getKategoriUbudiyah()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM pes_kategori_materi_ubudiyah");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveKategoriUbudiyah($name){
		
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("INSERT INTO pes_kategori_materi_ubudiyah (kategori) VALUES(:name);");
		 $stmt->bindParam("name",$name);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailKategoriUbudiyah($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select * from pes_kategori_materi_ubudiyah where auto = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateKategoriUbudiyah($iditem,$name){
		
				
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE pes_kategori_materi_ubudiyah SET kategori = :name WHERE auto = :id;");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("id",$iditem);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	################################### MATERI UBUDIYAH ##################################
	public function getMateriUbudiyah()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, a.auto, a.materi, b.kategori
							FROM pes_materi_ubudiyah a JOIN pes_kategori_materi_ubudiyah b ON a.kategori=b.auto, (SELECT @rownum := 0) r ORDER BY b.kategori ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPlottingMateriUbudiyah()
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
		$tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, a.auto,m.materi, b.kategori, a.jenjang,
							(CASE WHEN a.tingkat='1' THEN 'Tingkat 1' WHEN a.tingkat='2' THEN 'Tingkat 2' WHEN a.tingkat='3' THEN 'Tingkat 3' END) AS tingkat
							FROM pes_plotting_materi_ubudiyah a JOIN pes_materi_ubudiyah m on a.materi=m.auto JOIN pes_kategori_materi_ubudiyah b ON m.kategori=b.auto, 
							(SELECT @rownum := 0) r WHERE a.id_tahun_akademik=:tapel ORDER BY a.tingkat ASC");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveMateriUbudiyah($name,$kategori){
		
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("INSERT INTO pes_materi_ubudiyah (materi,kategori) VALUES(:name,:kategori);");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("kategori",$kategori);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailMateriUbudiyah($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select auto as id_materi, materi, kategori from pes_materi_ubudiyah where auto = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateMateriUbudiyah($iditem,$name,$kategori){
		
				
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE pes_materi_ubudiyah SET materi = :name, kategori =:kategori WHERE auto = :id;");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("kategori",$kategori);
		 $stmt->bindParam("id",$iditem);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function deleteMateriUbudiyah($iditem){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("DELETE FROM pes_materi_ubudiyah WHERE auto = :id;");
		 $stmt->bindParam("id",$iditem);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	########################### PLOTTING MATERI UBUDIYAH ############################
	
	public function cekPlottingMateri($tingkat,$jenjang,$materi){
		$db = $this->dblocal;
		$array = $this->getTahunAkademik();
		$tapel = $array[1]['thn_ajaran_id'];
		try
		{
		 $stmt = $db->prepare("select auto as plotting from pes_plotting_materi_ubudiyah where jenjang = :jenjang AND tingkat=:tingkat AND id_tahun_akademik=:tapel AND materi=:materi ");
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->bindParam("jenjang",$jenjang);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("materi",$materi);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getMateriUbudiyahByCat($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT a.auto as id_materi, a.materi FROM pes_materi_ubudiyah a WHERE a.kategori=:id ORDER BY a.materi ASC");
	   $stmt->bindParam("id",$id);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function savePlottingMateriUbudiyah($name,$tingkat,$jenjang){
		
		$db = $this->dblocal;
		$array = $this->getTahunAkademik();
		$tapel = $array[1]['thn_ajaran_id'];
		try
		{
		 $stmt = $db->prepare("INSERT INTO pes_plotting_materi_ubudiyah (materi,tingkat,jenjang,id_tahun_akademik) VALUES(:name,:tingkat,:jenjang,:tapel);");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("jenjang",$jenjang);
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updatePlottingMateriUbudiyah($iditem,$name,$tingkat,$jenjang){
		
				
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("UPDATE pes_plotting_materi_ubudiyah SET materi = :name, tingkat =:tingkat, jenjang =:jenjang WHERE auto = :id;");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("tingkat",$tingkat);
		 $stmt->bindParam("jenjang",$jenjang);
		 $stmt->bindParam("id",$iditem);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailPlottingMateriUbudiyah($id){
	  $db = $this->dblocal;
	  try
	  {
		 $stmt = $db->prepare("select a.auto as id_plotting, a.materi as id_materi, m.kategori, a.jenjang, a.tingkat from pes_plotting_materi_ubudiyah a JOIN pes_materi_ubudiyah m ON a.materi=m.auto where a.auto = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	  }
	  catch(PDOException $ex)
	  {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	  }
	}
	
	public function deletePlottingMateriUbudiyah($iditem){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("DELETE FROM pes_plotting_materi_ubudiyah WHERE auto = :id;");
		 $stmt->bindParam("id",$iditem);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = 'Success';
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	########################### INPUT NILAI UBUDIYAH ############################
	
	public function getListSiswa()
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapel();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, uuid as nomor, nis, nama_lengkap FROM
							(SELECT s.uuid, j.nis, s.nama_lengkap FROM z_siswa s JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=s.uuid
							) data, (SELECT @rownum := 0) r ORDER BY nis ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function autoCompleteUbudiyah($term,$jenjang,$jenis,$tingkat,$asrama,$kamar)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  $query='';
	  if($kamar != ''){$query .=" AND k.id_kamar='".$kamar."'";}
	  else if($asrama != '' && $kamar == ''){$query .=" AND k.id_kamar IN(SELECT GROUP_CONCAT(auto SEPARATOR ', ') FROM pes_kamar WHERE asrama='".$asrama."')";}
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT uuid, nis, nama_lengkap FROM
							(SELECT s.uuid, j.nis, s.nama_lengkap FROM z_siswa s JOIN
							  (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenjang_pendidikan=:jenjang AND jenis=:jenis GROUP BY uuid) j
							  ON j.uuid=s.uuid
							  JOIN (SELECT id_siswa FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND tingkat=:tingkat GROUP BY id_siswa) t
							  ON s.uuid=t.id_siswa
							  JOIN pes_pembagian_kamar k ON s.uuid=k.id_siswa WHERE k.id_tahun_akademik=:tapel ".$query."
							) l WHERE nis LIKE :term or nama_lengkap LIKE :term ORDER BY nama_lengkap ASC");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->bindParam('jenjang', $jenjang);
	   $stmt->bindParam('jenis', $jenis);
	   $stmt->bindParam('tingkat', $tingkat);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getSearchSiswa($jenjang,$tingkat){
		$db = $this->dblocal;
		$tahun = $this->getTahunAkademik();
		$tapel = $tahun[1]['thn_ajaran_id'];
		// initilize all variable
		$params = $columns = $totalRecords = $data = array();

		$params = $_REQUEST;

		//define index of column
		$columns = array( 
			0 =>'urutan',
			1 =>'nis', 
			2 => 'nama',
		);

		$where = $sqlTot = $sqlRec = "";

		// check search value exist
		if( !empty($params['search']['value']) ) {   
			$where .=" WHERE ";
			$where .=" ( nis LIKE '%".$params['search']['value']."%' ";
			$where .=" OR nama_lengkap LIKE '%".$params['search']['value']."%' )";
		}

		// getting total number records without any search
		$sql = "SELECT * FROM (SELECT @rownum := @rownum + 1 AS urutan, uuid as nomor, nis, nama_lengkap FROM
				(SELECT s.uuid, j.nis, s.nama_lengkap FROM z_siswa s JOIN
				  (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenjang_pendidikan='".$jenjang."' GROUP BY uuid) j
				  ON j.uuid=s.uuid
				  JOIN (SELECT id_siswa FROM z_tingkat_siswa WHERE id_tahun_akademik='".$tapel."' AND tingkat='".$tingkat."' GROUP BY id_siswa) t
				  ON s.uuid=t.id_siswa
				) l, (SELECT @rownum := 0) r) data ";
		
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		
		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		$number = $db->prepare($sqlTot);
		$number->execute();
		$totalRecords = $number->rowCount();
		
		$array = $db->prepare($sqlRec);
		$array->execute();
		$queryRecords = $array->fetchAll(PDO::FETCH_ASSOC);
		
		//$data[] = $queryRecords;
		
		$json_data = array(
				"draw"            => intval( $params['draw'] ),   
				"recordsTotal"    => intval( $totalRecords ),  
				"recordsFiltered" => intval($totalRecords),
				"data"            => $queryRecords   // total data array
				);

		return $json_data;  // send data as json format
	}
	
	public function getDetailJenjangSiswa($idSiswa)
	{
	  $db = $this->dblocal;
	  $tahun = $this->getTahunAkademik();
	  $tapel = $tahun[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT s.uuid, j.nis, s.nama_lengkap,s.ayah,s.wali, 
							(CASE WHEN j.jenjang='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
							(CASE WHEN j.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis,
							(SELECT tingkat FROM z_tingkat_siswa WHERE id_siswa=s.uuid AND id_tahun_akademik=:tapel GROUP BY id_siswa) AS tingkat_kelas
							FROM z_siswa s JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis, jenis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND uuid=:id GROUP BY uuid) j
							ON j.uuid=s.uuid WHERE s.uuid=:id
							");
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->bindParam("id",$idSiswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailNilaiUbudiyah($jenjang,$tingkat,$idSiswa)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT k.kategori, m.auto, m.materi, n.date, n.auto as id_nilai, n.nilai, n.status, 
							(SELECT username FROM m_user WHERE id_user=n.penguji) AS penguji 
							FROM pes_plotting_materi_ubudiyah p JOIN pes_materi_ubudiyah m ON m.auto=p.materi JOIN pes_kategori_materi_ubudiyah k ON k.auto=m.kategori 
							LEFT JOIN (SELECT * FROM pes_nilai_ubudiyah WHERE id_siswa=:id AND id_tahun_akademik=:tapel) n ON n.materi=m.auto
							WHERE p.jenjang=:jenjang AND p.tingkat=:tingkat");
	   $stmt->bindParam('id',$idSiswa);
	   $stmt->bindParam('tapel',$tapel);
	   $stmt->bindParam('jenjang',$jenjang);
	   $stmt->bindParam('tingkat',$tingkat);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function cekNilaiUbudiyah($siswa,$materi){
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("SELECT auto as nilai FROM pes_nilai_ubudiyah WHERE id_siswa=:siswa AND materi=:materi AND id_tahun_akademik=:tapel");
		$stmt->bindParam("materi",$materi);
		$stmt->bindParam("siswa",$siswa);
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function simpanNilaiUbudiyah($siswa,$materi,$tanggal,$nilai,$status,$user){
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("INSERT INTO pes_nilai_ubudiyah(materi,nilai,date,status,penguji,id_siswa,id_tahun_akademik) 
							VALUES(:materi,:nilai,:tanggal,:status,:user,:siswa,:tapel)");
		$stmt->bindParam("materi",$materi);
		$stmt->bindParam("nilai",$nilai);
		$stmt->bindParam("tanggal",$tanggal);
		$stmt->bindParam("status",$status);
		$stmt->bindParam("user",$user);
		$stmt->bindParam("siswa",$siswa);
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateNilaiUbudiyah($idNilai,$tanggal,$nilai,$status,$user){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("UPDATE pes_nilai_ubudiyah SET nilai=:nilai, date=:tanggal, status=:status, penguji=:user 
							WHERE auto=:id");
		$stmt->bindParam("nilai",$nilai);
		$stmt->bindParam("tanggal",$tanggal);
		$stmt->bindParam("status",$status);
		$stmt->bindParam("user",$user);
		$stmt->bindParam("id",$idNilai);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success update!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getReportNilaiUbudiyah($jenjang,$jenis,$tingkat,$asrama,$kamar,$status)
	{
	  $db = $this->dblocal;
	  $query='';
	  if($kamar != ''){$query .=" AND k.id_kamar='".$kamar."'";}
	  else if($asrama != '' && $kamar == ''){$query .=" AND k.id_kamar IN(SELECT GROUP_CONCAT(auto SEPARATOR ', ') FROM pes_kamar WHERE asrama='".$asrama."')";}
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT uuid, nis, nama_lengkap FROM
							(SELECT s.uuid, j.nis, s.nama_lengkap FROM z_siswa s JOIN
							  (SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenjang_pendidikan=:jenjang AND jenis=:jenis GROUP BY uuid) j
							  ON j.uuid=s.uuid
							  JOIN (SELECT id_siswa FROM z_tingkat_siswa WHERE id_tahun_akademik=:tapel AND tingkat=:tingkat GROUP BY id_siswa) t
							  ON s.uuid=t.id_siswa
							  JOIN pes_pembagian_kamar k ON s.uuid=k.id_siswa WHERE k.id_tahun_akademik=:tapel ".$query."
							) l LEFT JOIN (xxx) ORDER BY nama_lengkap ASC");
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->bindParam('jenjang', $jenjang);
	   $stmt->bindParam('jenis', $jenis);
	   $stmt->bindParam('tingkat', $tingkat);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	########################### EKSTRAKURIKULER ############################
	public function getListEkstra()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.*,(SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=t.pengajar) AS trainer FROM z_ekstrakurikuler t, 
        (SELECT @rownum := 0) r ORDER BY ekstrakurikuler ASC");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveJenisEkstra($name,$pembina){
		
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("INSERT INTO z_ekstrakurikuler (ekstrakurikuler,pengajar) VALUES(:name,:pembina);");
		 $stmt->bindParam("name",$name);
		 $stmt->bindParam("pembina",$pembina);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = "Success";
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDetailEkstra($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.*, (SELECT nama_lengkap FROM z_pegawai WHERE id_pegawai=a.pengajar) AS pembina from z_ekstrakurikuler a where a.id_ekstrakurikuler = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateJenisEkstra($iditem,$name,$pembina)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_ekstrakurikuler SET ekstrakurikuler = :name, pengajar=:pembina WHERE id_ekstrakurikuler= :id;");
	   $stmt->bindParam("id",$iditem);
	   $stmt->bindParam("name",$name);
	   $stmt->bindParam("pembina",$pembina);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getNonPesertaEkstra($jenjang,$tingkat,$satuan,$jenis,$kelas,$term)
	{
	  if($term == '' || $term == NULL){
		$search = '';  
	  }else{
		$search = " AND d.nama_lengkap LIKE '%".$term."%'";
	  }
	  
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.* FROM (
								SELECT s.uuid, s.nama_lengkap, j.nis, 'nonsettle' AS tipe FROM tbl_santri s JOIN
								(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM tbl_jenjang_santri WHERE status_kesiswaan='siswa' AND jenjang_pendidikan=:jenjang AND tingkat_kelas=:tingkat GROUP BY uuid) j ON s.uuid=j.uuid
								UNION ALL
								SELECT s.uuid, s.nama_lengkap, j.nis, 'settle' AS tipe FROM z_siswa s JOIN 
								(SELECT  DISTINCT(uuid) AS uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenis=:jenis AND jenjang_pendidikan=:satuan AND tingkat_kelas=:tingkat GROUP BY uuid) j ON s.uuid=j.uuid
							) d
							LEFT OUTER JOIN (SELECT id_siswa FROM z_peserta_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:kelas) r ON r.id_siswa=d.uuid WHERE r.id_siswa IS NULL ".$search." ");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("tingkat",$tingkat);
	   $stmt->bindParam("satuan",$satuan);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPesertaEkstra($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM z_peserta_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid
							");
	   $stmt->bindParam("kelas",$kelas);
	   $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveCheckinEkstra($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("INSERT INTO z_peserta_ekstra(id_tahun_akademik,id_ekstra,id_siswa) VALUES(:tapel,:ekstra,:id_siswa)");
		$stmt->bindParam("ekstra",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deletePesertaEkstra($id_siswa,$kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_peserta_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:ekstra AND id_siswa=:id_siswa");
		$stmt->bindParam("ekstra",$kelas);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("id_siswa",$id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function autoCompleteEkstra($term,$kelas)
	{
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM z_peserta_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:kelas) r
							LEFT JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid WHERE d.nis LIKE :term or s.nama_lengkap LIKE :term ORDER BY s.nama_lengkap ASC
							");
	   $stmt->bindParam("term",$trm);
	   $stmt->bindParam('tapel', $tapel);
	   $stmt->bindParam('kelas', $kelas);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailNilaiEkstra($kelas)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT d.uuid, d.nis, s.nama_lengkap, jenjang, IFNULL(n.nilai,' ') AS nilai, IFNULL(n.keterangan,' ') AS keterangan FROM z_siswa s JOIN
							( SELECT * FROM
							(SELECT id_siswa FROM z_peserta_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:kelas) r
							JOIN
							(SELECT MAX(jenjang_pendidikan) AS jenjang, jenis, uuid, nis FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' GROUP BY uuid) j
							ON j.uuid=r.id_siswa
							) d ON d.uuid=s.uuid LEFT JOIN 
							(SELECT nilai, keterangan, id_siswa FROM z_nilai_ekstra WHERE id_tahun_akademik=:tapel AND id_ekstra=:kelas) n ON n.id_siswa=s.uuid
							ORDER BY s.nama_lengkap ASC");
	   $stmt->bindParam('kelas',$kelas);
	   $stmt->bindParam('tapel',$tapel);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteNilaiEkstra($id)
	{
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_nilai_ekstra WHERE id_ekstra=:id AND id_tahun_akademik=:tapel");
		$stmt->bindParam("id",$id);
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Delete!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveNilaiEkstra($data){
	  
	  $db = $this->dblocal;
	  $array = $this->getTahunAkademik();
	  $tapel = $array[1]['thn_ajaran_id'];
	  
	  $insert_values = array();
	  foreach($data as $k => $d){
		$insert_values[]='("'.$tapel.'","'.$d['ekstra'].'","'.$d['siswa'].'","'.$d['nilai'].'","'.$d['note'].'","'.$d['user'].'")';
	  }
	  
	  $sql = "INSERT INTO z_nilai_ekstra (id_tahun_akademik,id_ekstra,id_siswa,nilai,keterangan,input_by) VALUES ".implode(',', $insert_values);
	  try
	  {
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
}
	