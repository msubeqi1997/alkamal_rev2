 <?php
class model_psb extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
	
	/****************************** START SETTING TAHUN PELAJARAN ************/
	public function getTapel()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.* FROM z_tahun_ajaran t, 
        (SELECT @rownum := 0) r ORDER BY thn_ajaran_id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveTapel($item){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO z_tahun_ajaran(thn_ajaran) VALUES(UPPER(:thn_ajaran))");
		$stmt->bindParam("thn_ajaran",$item);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		$stat[2] =  $stmt->rowCount();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateTapel($id,$name)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_tahun_ajaran SET  thn_ajaran = UPPER(:name) WHERE thn_ajaran_id= :id;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("name",$name);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailTapel($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from z_tahun_ajaran a where a.thn_ajaran_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activatePSBTapel($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_tahun_ajaran SET psb = (case when thn_ajaran_id = :id then '1' when thn_ajaran_id != :id then '0' end);");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function activeTapelPSB()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT thn_ajaran_id, thn_ajaran FROM z_tahun_ajaran WHERE psb='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function activeTapelAkademik()
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT * FROM z_tahun_ajaran WHERE akademik='1'");
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	/****************** END SETTING TAHUN PELAJARAN ***************************/
	
	/****************************** START SETTING KELENGKAPAN ****************/
	
	public function getKelengkapan()
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.* FROM psb_kelengkapan t, 
        (SELECT @rownum := 0) r ORDER BY kelengkapan_id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getKelengkapanByJalur($jalur_id)
    {
      $db = $this->dblocal;
      try
      {
       $stmt = $db->prepare("SELECT t.* FROM psb_kelengkapan t WHERE FIND_IN_SET(t.kelengkapan_id,(SELECT kelengkapan_id FROM psb_jalur WHERE jalur_id=:id))");
       $stmt->bindParam("id",$jalur_id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveKelengkapan($name,$jenis,$aktif){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO psb_kelengkapan(kelengkapan,jenis_field,aktif) VALUES(UPPER(:name),:jenis,:aktif)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("jenis",$jenis);
		$stmt->bindParam("aktif",$aktif);
		$stmt->execute();
		$id = $db->lastInsertId();
		$nama_field = ''.$id.''.preg_replace('/\s+/', '', $name).'';
		$update =  $db->prepare("UPDATE psb_kelengkapan SET nama_field = :field WHERE kelengkapan_id=:id");
		$update->bindParam("id",$id);
		$update->bindParam("field",$nama_field);
		$update->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		$stat[2] =  $stmt->rowCount();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailKelengkapan($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from psb_kelengkapan a where a.kelengkapan_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activateKelengkapanPSB($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_kelengkapan SET aktif=case aktif when '1' then '0'
                             when '0' then '1' end WHERE kelengkapan_id=:id;");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updateKelengkapan($id,$name,$jenis,$aktif)
	{
	  $nama_field = ''.$id.''.preg_replace('/\s+/', '', $name).'';
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_kelengkapan SET kelengkapan = UPPER(:name), jenis_field=:jenis, nama_field=:field, aktif=:aktif WHERE kelengkapan_id= :id;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("name",$name);
	   $stmt->bindParam("jenis",$jenis);
	   $stmt->bindParam("field",$nama_field);
	   $stmt->bindParam("aktif",$aktif);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************** END SETTING KELENGKAPAN ***************************/
	
	
	/****************************** START SETTING REKENING BANK ****************/
	
	public function get_rekening_bank(){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select * from psb_rekening where id='1' ");
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function update_rekening_bank($norek,$bank,$cabang)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_rekening 
		SET  no_rekening = :norek,
		nama_rekening = :bank, 
		cabang = :cabang
		WHERE id= '1';");

	   $stmt->bindParam("norek",$norek);
	   $stmt->bindParam("bank",$bank);
	   $stmt->bindParam("cabang",$cabang);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************** END SETTING REKENING BANK ***************************/
	
	/****************************** START SETTING KEWAJIBAN ****************/
	
	public function getMasterKewajiban()
    {
      $db = $this->dblocal;
	  $array = $this->activeTapelPSB();
	  $tp = $array[1]['thn_ajaran_id'];
      try
      {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.*, 
	   (SELECT thn_ajaran FROM z_tahun_ajaran WHERE thn_ajaran_id =:tapel) AS tapel
	   FROM psb_kewajiban t, (SELECT @rownum := 0) r WHERE t.tapel=:tapel ORDER BY t.kewajiban, t.jenis_kewajiban_id ASC");
       $stmt->bindParam("tapel",$tp);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getJenisKewajiban()
    {
	  $array = $this->activeTapelPSB();
	  $tapel = $array[1]['thn_ajaran_id'];
      $db = $this->dblocal;
      try
      {
	   $stmt = $db->prepare("SELECT id AS id, name AS name FROM psb_jenis_kewajiban WHERE tapel=:tapel ORDER BY name ASC");
       $stmt->bindParam("tapel",$tapel);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveJenisKewajiban($name,$tapel){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO psb_jenis_kewajiban(name,tapel) VALUES(:name,:tapel)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		$stat[2] =  $stmt->rowCount();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailJenisKewajiban($item){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select * from psb_jenis_kewajiban where id=:id ");
		 $stmt->bindParam("id",$item);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateJenisKewajiban($iditem,$name)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_jenis_kewajiban SET  name =:name WHERE id =:id;");
	   $stmt->bindParam("name",$name);
	   $stmt->bindParam("id",$iditem);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function saveMasterKewajiban($name,$plot,$nominal,$aktif,$tapel){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO psb_kewajiban(kewajiban,jenis_kewajiban_id,nominal,aktif,tapel) VALUES(:name,:plot,:nominal,:aktif,:tapel)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("plot",$plot);
		$stmt->bindParam("nominal",$nominal);
		$stmt->bindParam("aktif",$aktif);
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		$stat[2] =  $stmt->rowCount();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getDetailKewajiban($item){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select * from psb_kewajiban where id=:id ");
		 $stmt->bindParam("id",$item);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateMasterKewajiban($iditem,$name,$plot,$nominal,$aktif)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_kewajiban SET  kewajiban =:name, jenis_kewajiban_id=:plot, nominal =:nominal, aktif =:aktif WHERE id =:id;");
	   $stmt->bindParam("name",$name);
	   $stmt->bindParam("plot",$plot);
	   $stmt->bindParam("nominal",$nominal);
	   $stmt->bindParam("aktif",$aktif);
	   $stmt->bindParam("id",$iditem);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function activateMasterKewajiban($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_kewajiban SET aktif=case aktif when '1' then '0'
                             when '0' then '1' end WHERE id =:id;");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	/****************** END SETTING KEWAJIBAN ***************************/
		
	/****************************** START SETTING JALUR PENDAFTARAN ****************/
	
	public function getJalurPendaftaran()
    {
     $db = $this->dblocal;
     $array = $this->activeTapelPSB();
	 $tp = $array[1]['thn_ajaran_id'];
	 try
     {
       $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan,t.*, 
	    (SELECT name FROM psb_jenis_kewajiban WHERE id = t.kewajiban_id) AS kewajiban,
	    (SELECT thn_ajaran FROM z_tahun_ajaran WHERE thn_ajaran_id =:tapel) AS tapel,
		(SELECT GROUP_CONCAT(k.kelengkapan SEPARATOR ',') FROM psb_kelengkapan k WHERE FIND_IN_SET(k.kelengkapan_id,t.kelengkapan_id) ) AS kelengkapan 
		FROM psb_jalur t ,(SELECT @rownum := 0) r WHERE t.thn_ajaran_id =:tapel ORDER BY t.tgl_mulai ASC");
       $stmt->bindParam("tapel",$tp);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function saveJalurPendaftaran($name,$mdk,$plot,$kelengkapan,$tapel,$first,$last,$aktif){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("INSERT INTO psb_jalur(jalur,jenis,thn_ajaran_id,tgl_mulai,tgl_sampai,kelengkapan_id,kewajiban_id,aktif) VALUES(:name,:jenis,:tapel,:first,:last,:kelengkapan,:kewajiban,:aktif)");
		$stmt->bindParam("name",$name);
		$stmt->bindParam("jenis",$mdk);
		$stmt->bindParam("tapel",$tapel);
		$stmt->bindParam("first",$first);
		$stmt->bindParam("last",$last);
		$stmt->bindParam("kelengkapan",$kelengkapan);
		$stmt->bindParam("kewajiban",$plot);
		$stmt->bindParam("aktif",$aktif);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		$stat[2] =  $stmt->rowCount();
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateJalurPendaftaran($iditem,$name,$mdk,$plot,$kelengkapan,$first,$last,$aktif)
	{
	  $db = $this->dblocal;
	  try
	  {
	    $stmt = $db->prepare("UPDATE psb_jalur SET  jalur =:name, jenis=:jenis, tgl_mulai =:first, tgl_sampai =:last,
							kelengkapan_id =:kelengkapan, kewajiban_id =:plot, aktif =:aktif WHERE jalur_id =:id;");
	    $stmt->bindParam("name",$name);
	    $stmt->bindParam("jenis",$mdk);
		$stmt->bindParam("first",$first);
		$stmt->bindParam("last",$last);
		$stmt->bindParam("kelengkapan",$kelengkapan);
		$stmt->bindParam("plot",$plot);
		$stmt->bindParam("aktif",$aktif);
	    $stmt->bindParam("id",$iditem);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getDetailJalurPendaftaran($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from psb_jalur a where a.jalur_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function activateJalurPendaftaran($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE psb_jalur SET aktif=case aktif when '1' then '0'
                             when '0' then '1' end WHERE jalur_id =:id;");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	/****************** END SETTING JALUR PENDAFTARAN ***************************/
	
	/****************************** START PROSES PENDAFTARAN ONLINE ****************/
	public function getTingkatPendidikan()
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM z_tingkat_pendidikan t ORDER BY t.id ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getTipeSeleksi($id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.* from psb_jalur a where a.jalur_id = :id ");
		 $stmt->bindParam("id",$id);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getJalurPendaftaranByDate()
    {
     $db = $this->dblocal;
     $array = $this->activeTapelPSB();
	 $tp = $array[1]['thn_ajaran_id'];
	 try
     {
       $stmt = $db->prepare("SELECT t.*
		FROM psb_jalur t WHERE t.thn_ajaran_id =:tapel AND aktif = '1' AND DATE(NOW()) BETWEEN t.tgl_mulai AND t.tgl_sampai ORDER BY t.tgl_mulai ASC");
       $stmt->bindParam("tapel",$tp);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getSatuanPendidikan($id)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM psb_tujuan_sekolah t WHERE t.satuan_pendidikan !=:id ORDER BY t.auto ASC");
       $stmt->bindParam("id",$id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getJenjangPendidikan($id)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM psb_tujuan_sekolah t WHERE t.kode_sekolah =:id");
       $stmt->bindParam("id",$id);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getBerkasBySiswa($uuid,$jalur_id){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("select a.*, b.html_value from psb_kelengkapan a LEFT JOIN 
							(SELECT kelengkapan, html_value FROM z_siswa_berkas WHERE uuid=:uuid) b ON a.kelengkapan_id=b.kelengkapan 
							WHERE FIND_IN_SET(a.kelengkapan_id,(SELECT kelengkapan_id FROM psb_jalur WHERE jalur_id=:id))");
		 $stmt->bindParam("id",$jalur_id);
		 $stmt->bindParam("uuid",$uuid);
		 $stmt->execute();
		 $stat[0] = true;
		 $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
		 return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function deleteBerkasSiswa($uuid,$id){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_siswa_berkas WHERE uuid=:uuid AND kelengkapan_id=:id");
		$stmt->bindParam("uuid",$uuid);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveBerkasSiswa($berkas){
	  
	  $keys = array();
	  foreach($berkas as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_siswa_berkas(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($berkas as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deleteRiwayatPendidikan($uuid){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_siswa_riwayat_pendidikan WHERE uuid=:uuid");
		$stmt->bindParam("uuid",$uuid);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function saveRiwayatPendidikan($berkas){
	  
	  $keys = array();
	  foreach($berkas as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_siswa_riwayat_pendidikan(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($berkas as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function savePendaftar($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_siswa(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function updateEmail($uuid,$email){
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("UPDATE z_siswa SET email=:email WHERE uuid=:uuid");
		$stmt->bindParam("uuid",$uuid);
		$stmt->bindParam("email",$email);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Update!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function inputjenjangpsb($tahunaktif,$uuid,$jenjang,$jenis,$sekolah,$tahun,$status,$jalur,$input_by){
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("call save_pendaftar( :id_tahun, :uuid, :jenjang, :jenis, :sekolah, :tahun, :status, :jalur, :input_by)");
		$stmt->bindParam("id_tahun",$tahunaktif);
		$stmt->bindParam("uuid",$uuid);
		$stmt->bindParam("jenjang",$jenjang);
		$stmt->bindParam("jenis",$jenis);
		$stmt->bindParam("sekolah",$sekolah);
		$stmt->bindParam("tahun",$tahun);
		$stmt->bindParam("status",$status);
		$stmt->bindParam("jalur",$jalur);
		$stmt->bindParam("input_by",$input_by);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success Save!";
		$stat[2] =  $stmt->fetchColumn(0);
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function getSequence($tahun){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT nomor_pendaftar(:tahun) AS nomor");
		 $stmt->bindParam('tahun', $tahun);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getPropinsi()
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.* FROM provinces t ORDER BY t.name ASC");
       $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getKabupaten($propinsi)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.id, t.name FROM regencies t WHERE t.province_id=:propinsi ORDER BY t.name ASC");
       $stmt->bindParam("propinsi",$propinsi);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getKecamatan($kabupaten)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.id, t.name FROM districts t WHERE t.regency_id=:kabupaten ORDER BY t.name ASC");
       $stmt->bindParam("kabupaten",$kabupaten);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function getDesa($kecamatan)
    {
     $db = $this->dblocal;
     try
     {
       $stmt = $db->prepare("SELECT t.id, t.name FROM villages t WHERE t.district_id=:kecamatan ORDER BY t.name ASC");
       $stmt->bindParam("kecamatan",$kecamatan);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	/****************** END PROSES PENDAFTARAN ONLINE ***************************/
	
	/****************************** START CETAK PENDAFTARAN ONLINE ****************/
	public function getSiswaPendaftar($token){
		
		$column_siswa=array('s.uuid','s.nama_lengkap','s.kelamin','s.tempat_lahir','s.tanggal_lahir','s.saudara','s.anak_ke','s.abk','s.alamat','s.email','s.no_hp','s.kode_pos','s.ayah','s.tanggal_lahir_ayah','s.tempat_lahir_ayah','s.alamat_ayah','s.hp_ayah','s.telp_ayah','s.pekerjaan_ayah','s.pendidikan_ayah',
							's.ibu','s.tanggal_lahir_ibu','s.tempat_lahir_ibu','s.hp_ibu','s.pekerjaan_ibu','s.pendidikan_ibu','s.wali','s.tanggal_lahir_wali','s.tempat_lahir_wali','s.telp_wali','s.pekerjaan_wali','s.pendidikan_wali','s.hubungan_wali','s.alamat_wali','s.photo_siswa','s.photo_wali');
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
		$tapel = $array[1]['thn_ajaran_id'];
		try
		{
		 $stmt = $db->prepare("SELECT ".implode(',',$column_siswa).",DATE(p.input_at) AS tertanggal,p.id as kode_pendaftaran,p.no_pendaftaran, p.tujuan_sekolah, p.jenjang_pendidikan, p.jenis,p.jalur,
							(SELECT thn_ajaran FROM z_tahun_ajaran WHERE thn_ajaran_id=(SELECT thn_ajaran_id FROM psb_jalur WHERE jalur_id=p.jalur)) AS tapel,
							(SELECT jalur FROM psb_jalur WHERE jalur_id=p.jalur) AS jalur_name,
							(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
							(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
							(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=p.tujuan_sekolah) AS tujuan,
							(SELECT SUM(nominal) AS nominal FROM psb_kewajiban WHERE jenis_kewajiban_id=(SELECT kewajiban_id FROM psb_jalur WHERE jalur_id=p.jalur)) AS nominal
							FROM z_siswa s JOIN z_pendaftaran p on s.uuid=p.uuid WHERE s.uuid=:token AND p.tahun_psb=:tapel");
		 $stmt->bindParam("token",$token);
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getRiwayatPendidikan($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.* FROM z_siswa_riwayat_pendidikan s WHERE s.uuid=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getNamaSiswa($nomor){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.nama_lengkap, s.kelamin, s.tempat_lahir, s.tanggal_lahir, s.no_hp, s.uuid FROM z_siswa s JOIN (SELECT MAX(jenjang_pendidikan), uuid FROM z_jenjang_siswa WHERE nis=:induk and jenjang_pendidikan < '3' GROUP BY uuid) j  ON s.uuid=j.uuid ");
		 $stmt->bindParam("induk",$nomor);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function cekEmail($email){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.uuid,s.email FROM z_siswa s WHERE s.email=:email");
		 $stmt->bindParam("email",$email);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	/****************** END CETAK PENDAFTARAN ONLINE ***************************/
	
	/****************************** START SELEKSI PENDAFTARAN ONLINE ****************/
	public function getAllPendaftar()
    {
     $db = $this->dblocal;
	 $array = $this->activeTapelPSB();
	  $tp = $array[1]['thn_ajaran_id'];
     try
     {
       $stmt = $db->prepare("SELECT s.uuid,s.nama_lengkap, p.no_pendaftaran,p.seleksi FROM z_siswa s JOIN z_pendaftaran p ON s.uuid=p.uuid JOIN psb_jalur j ON p.jalur=j.jalur_id
	   WHERE j.thn_ajaran_id=:tapel ORDER BY p.no_pendaftaran ASC");
       $stmt->bindParam("tapel",$tp);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function countAllPendaftar()
    {
     $db = $this->dblocal;
	 $array = $this->activeTapelPSB();
	  $tp = $array[1]['thn_ajaran_id'];
     try
     {
       $stmt = $db->prepare("SELECT s.uuid,s.id,s.nama_lengkap, p.no_pendaftaran FROM z_siswa s JOIN z_pendaftaran p ON s.uuid=p.uuid JOIN psb_jalur j ON p.jalur=j.jalur_id
	   WHERE j.thn_ajaran_id=:tapel ORDER BY p.no_pendaftaran ASC");
       $stmt->bindParam("tapel",$tp);
	   $stmt->execute();
       $stat[0] = true;
       $stat[1] = $stmt->rowCount();
       return $stat;
     }
     catch(PDOException $ex)
     {
       $stat[0] = false;
       $stat[1] = $ex->getMessage();
       return $stat;
     }
	}
	
	public function autoCompleteSearch($term)
	{
	  $array = $this->activeTapelPSB();
	  $tp = $array[1]['thn_ajaran_id'];
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT p.no_pendaftaran,a.nama_lengkap,a.uuid,
	   (SELECT jalur FROM psb_jalur WHERE jalur_id=p.jalur) AS jalur_name  FROM z_siswa a JOIN z_pendaftaran p ON a.uuid=p.uuid WHERE p.status='calon' AND 
	   FIND_IN_SET(p.jalur,(SELECT GROUP_CONCAT(jalur_id SEPARATOR ',') AS id_jalur FROM psb_jalur WHERE thn_ajaran_id=:tapel))
	   AND (p.no_pendaftaran like :term or a.nama_lengkap  like :term) order by p.no_pendaftaran asc");
	   $stmt->bindParam("tapel",$tp);
	   $stmt->bindParam("term",$trm);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getPMDKSiswa($token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT s.*,
							(SELECT pmdk FROM psb_pmdk WHERE id=s.pmdk) AS bidang 
							FROM z_berkas_pmdk s WHERE s.uuid=:token");
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getBerkasSiswa($token,$jalur){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT t.kelengkapan, t.kelengkapan_id,t.jenis_field,  
					(SELECT html_value FROM z_siswa_berkas WHERE uuid=:token AND kelengkapan=t.kelengkapan_id) AS berkas
					FROM psb_kelengkapan t WHERE FIND_IN_SET(t.kelengkapan_id,(SELECT kelengkapan_id FROM psb_jalur WHERE jalur_id=:jalur))");
		 $stmt->bindParam("token",$token);
		 $stmt->bindParam("jalur",$jalur);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateSeleksiSiswa($id,$stat,$user)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_pendaftaran SET seleksi = :stat, seleksi_by=:user WHERE id= :id;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("stat",$stat);
	   $stmt->bindParam("user",$user);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	/****************** END SELEKSI PENDAFTARAN ONLINE ***************************/
	
	/****************************** START DAFTAR ULANG PSB ***********************/
	public function autoCompleteSearchDaful($term)
	{
	  $array = $this->activeTapelPSB();
	  $tp = $array[1]['thn_ajaran_id'];
	  $trm = "%".$term."%";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("SELECT p.no_pendaftaran,a.nama_lengkap,a.uuid,p.jenjang_pendidikan,a.kelamin,(SELECT jalur FROM psb_jalur WHERE jalur_id=p.jalur) AS jalur_name  
	   FROM z_siswa a JOIN z_pendaftaran p ON a.uuid=p.uuid JOIN psb_jalur j ON p.jalur=j.jalur_id WHERE p.seleksi='lolos' AND 
	   thn_ajaran_id=:tapel AND (p.no_pendaftaran like :term or a.nama_lengkap  like :term) order by p.no_pendaftaran asc");
	   $stmt->bindParam("tapel",$tp);
	   $stmt->bindParam("term",$trm);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function checkSiswaAktif($id_siswa,$jenjang)
	{
	  $db = $this->dblocal;
	  $array = $this->activeTapelAkademik();
	  $tp = $array[1]['thn_ajaran_id'];
	  try
	  {
	   $stmt = $db->prepare("SELECT status_kesiswaan AS status FROM z_jenjang_siswa WHERE status_kesiswaan='siswa' AND jenjang_pendidikan =:jenjang AND uuid =:id GROUP BY uuid");
	   $stmt->bindParam("jenjang",$jenjang);
	   $stmt->bindParam("id",$id_siswa);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function getListKewajiban($jalur,$token){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT k.*, t.nominal as paid FROM psb_kewajiban k LEFT JOIN 
							(SELECT SUM(a.nominal) AS nominal, a.id_kewajiban  FROM t_payment_detail a 
							JOIN t_payment b ON a.payment_id=b.payment_id WHERE b.payment='daful' AND b.id_siswa=:token AND b.sts='1' GROUP BY a.id_kewajiban)t ON k.id=t.id_kewajiban
							WHERE k.jenis_kewajiban_id=(SELECT kewajiban_id FROM psb_jalur WHERE jalur_id=:jalur)");
		 $stmt->bindParam("jalur",$jalur);
		 $stmt->bindParam("token",$token);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function updateSiswaDaful($id,$stat,$daful)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_pendaftaran SET status = :stat, daful = :daful WHERE id= :id;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("stat",$stat);
	   $stmt->bindParam("daful",$daful);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function updateKelengkapanDaful($uuid,$id,$lengkap)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_siswa_berkas SET lengkap = :lengkap WHERE uuid= :uuid AND kelengkapan=:id;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("uuid",$uuid);
	   $stmt->bindParam("lengkap",$lengkap);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success Edit!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function insertJenjangSiswa($data){
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_jenjang_siswa(".implode(', ',$keys).") VALUES(".$par.")";
	  
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertTingkatSiswa($data){
	  
	  $db = $this->dblocal;
	  $array = $this->activeTapelPSB();
	  $tapel = $array[1]['thn_ajaran_id'];
	  
	  $keys = array();
	  foreach($data as $param => $val){
		$keys[] = $param;
	  }
	  $par = ":".implode(', :',$keys)."";
	  $sql = "INSERT INTO z_tingkat_siswa(".implode(', ',$keys).",id_tahun_akademik) VALUES(".$par.",:tapel)";
	  	  
	  try
	  {
		$stmt = $db->prepare($sql);
		foreach ($data as $key => &$val) {
			$stmt->bindParam($key, $val);
		}
		$stmt->bindParam('tapel', $tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function inputPaymentDaful($payment_id,$id_user,$id_siswa,$kode,$payment_date,$paid,$payment,$lunas)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("INSERT INTO t_payment(payment_id,id_user,id_siswa,id_pendaftaran,payment_date,paid,payment,lunas) VALUES(:payment_id,:id_user,:id_siswa,:kode,:payment_date,:paid,:payment,:lunas);");
	   $stmt->bindParam("payment_id",$payment_id);
	   $stmt->bindParam("id_user",$id_user);
	   $stmt->bindParam("id_siswa",$id_siswa);
	   $stmt->bindParam("kode",$kode);
	   $stmt->bindParam("payment_date",$payment_date);
	   $stmt->bindParam("paid",$paid);
	   $stmt->bindParam("payment",$payment);
	   $stmt->bindParam("lunas",$lunas);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function inputDetailPayment($array)
	{
	  $sql = array();
	  foreach($array as $k) {
		$sql[]="(".$k['payment_id'].",".$k['id_kewajiban'].",".$k['nominal'].")";
	  }
	  $query = "INSERT INTO t_payment_detail(payment_id,id_kewajiban,nominal) VALUES ".implode(',',$sql)."";
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare($query);
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success detail";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteJenjangSiswa($id)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("DELETE FROM z_jenjang_siswa WHERE id_pendaftaran=:id ");
	   $stmt->bindParam("id",$id);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success delete!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteTingkatSiswa($id_siswa){
	  $db = $this->dblocal;
	  $array = $this->activeTapelPSB();
	  $tapel = $array[1]['thn_ajaran_id'];
	  try
	  {
		$stmt = $db->prepare("DELETE FROM z_tingkat_siswa WHERE id_siswa=:siswa AND id_tahun_akademik=:tapel ");
		$stmt->bindParam('tapel', $tapel);
		$stmt->bindParam('siswa', $id_siswa);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "Success save!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function deletePaymentDaful($id,$kode)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE t_payment SET sts = '0' WHERE id_siswa= :id AND id_pendaftaran=:kode;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("kode",$kode);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success delete!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	
	public function deleteKelengkapanDaful($id,$kode)
	{
	  $db = $this->dblocal;
	  try
	  {
	   $stmt = $db->prepare("UPDATE z_siswa_berkas SET lengkap = '0' WHERE uuid= :id AND id_pendaftaran=:kode;");
	   $stmt->bindParam("id",$id);
	   $stmt->bindParam("kode",$kode);
	   
	   $stmt->execute();
	   $stat[0] = true;
	   $stat[1] = "Success delete!";
	   return $stat;
	 }
	 catch(PDOException $ex)
	 {
	   $stat[0] = false;
	   $stat[1] = $ex->getMessage();
	   return $stat;
	 }
	}
	/****************************** END DAFTAR ULANG PSB *************************/
	
	/****************************** START REPORT PSB ****************/
	public function getFilterSeleksi(array $var = array()){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tp = $array[1]['thn_ajaran_id'];
		
		$query_arr=array();
		foreach($var as $col => $val){
			$query_arr[] = "".$col."='".$val."'";
		}
		$query="";
		if(!empty($query_arr)){ $query = " WHERE ".implode(' AND ',$query_arr); }
		
		try
		{
		$sql = "SELECT id, no_pendaftaran, CONCAT(jenjang,' ',jenis_kelamin) AS jenjang, jenis, nama_lengkap, alamat, propinsi_name, kabupaten_name, sekolah_tujuan, seleksi, daful, admin, selektor  FROM 
			   (SELECT s.id, s.nama_lengkap, p.no_pendaftaran, s.alamat, p.seleksi, p.daful, p.input_at, 
				(SELECT username FROM m_user WHERE id_user=p.input_by) AS admin,
				(SELECT username FROM m_user WHERE id_user=p.seleksi_by) AS selektor,
				(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=p.tujuan_sekolah) AS sekolah_tujuan,
				(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
				(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
				(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin,
				(CASE WHEN p.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
				(CASE WHEN p.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis
				FROM z_siswa s JOIN z_pendaftaran p ON s.uuid=p.uuid
				JOIN psb_jalur j ON p.jalur=j.jalur_id WHERE j.thn_ajaran_id=:tapel) s   
				".$query." order by no_pendaftaran asc";
		
		 $stmt = $db->prepare($sql);
							  
		 $stmt->bindParam("tapel",$tp);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getFilterPendaftar(array $var = array()){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tp = $array[1]['thn_ajaran_id'];
		
		$column_siswa=array('s.uuid','s.nama_lengkap','s.kelamin','s.tempat_lahir','s.tanggal_lahir','s.saudara','s.anak_ke','s.abk','s.alamat','s.email','s.no_hp','s.kode_pos','s.ayah','s.tanggal_lahir_ayah','s.tempat_lahir_ayah','s.alamat_ayah','s.hp_ayah','s.telp_ayah','s.pekerjaan_ayah','s.pendidikan_ayah',
							's.ibu','s.tanggal_lahir_ibu','s.tempat_lahir_ibu','s.hp_ibu','s.pekerjaan_ibu','s.pendidikan_ibu','s.wali','s.tanggal_lahir_wali','s.tempat_lahir_wali','s.telp_wali','s.pekerjaan_wali','s.pendidikan_wali','s.hubungan_wali','s.alamat_wali','s.photo_siswa','s.photo_wali');
							
		$query_arr=array();
		foreach($var as $col => $val){
			$query_arr[] = "".$col."='".$val."'";
		}
		$query="";
		if(!empty($query_arr)){ $query = "WHERE ".implode(' AND ',$query_arr); }
		
		try
		{
		$sql = "SELECT * FROM 
			   (SELECT ".implode(',',$column_siswa).", p.no_pendaftaran, p.seleksi, p.daful, p.input_at, j.jalur,
			    (SELECT username FROM m_user WHERE id_user=p.input_by) AS admin,
			    (SELECT username FROM m_user WHERE id_user=p.seleksi_by) AS selektor,
				(SELECT nama_sekolah FROM psb_tujuan_sekolah WHERE kode_sekolah=p.tujuan_sekolah) AS sekolah_tujuan,
				(SELECT name FROM provinces WHERE id=s.propinsi) AS propinsi_name,
				(SELECT name FROM regencies WHERE id=s.kabupaten) AS kabupaten_name,
				(CASE WHEN s.kelamin='0' THEN 'PUTRI' ELSE 'PUTRA' END) AS jenis_kelamin,
				(CASE WHEN p.jenjang_pendidikan='2' THEN 'ULA' ELSE 'WUSTHO' END) AS jenjang,
				(CASE WHEN p.jenis='mdk' THEN 'MDK' ELSE 'Bukan MDK' END) AS jenis
				FROM z_siswa s JOIN z_pendaftaran p ON s.uuid=p.uuid
				JOIN psb_jalur j ON p.jalur=j.jalur_id WHERE j.thn_ajaran_id=:tapel) s   
				".$query." order by no_pendaftaran asc";
		
		 $stmt = $db->prepare($sql);
							  
		 $stmt->bindParam("tapel",$tp);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function deleteNilaiSeleksi($tapel){
	  $db = $this->dblocal;
	  try
	  {
		$stmt = $db->prepare("DELETE FROM psb_nilai_seleksi WHERE tapel=:tapel");
		$stmt->bindParam("tapel",$tapel);
		$stmt->execute();
		$stat[0] = true;
		$stat[1] = "deleted!";
		return $stat;
	  }
	  catch(PDOException $ex)
	  {
		$stat[0] = false;
		$stat[1] = $ex->getMessage();
		return $stat;
	  }
	}
	
	public function insertNilaiSeleksi(array $value = array()){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tapel = $array[1]['thn_ajaran_id'];
		
		$sql = "INSERT INTO psb_nilai_seleksi(siswa_id,tapel,nilai) VALUES ".implode(',', $value);
		
		try
		{
		 $stmt = $db->prepare($sql);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = 'Sukses';
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getNilaiSeleksi(){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tapel = $array[1]['thn_ajaran_id'];
		
		try
		{
		 $stmt = $db->prepare("SELECT @rownum := @rownum + 1 AS urutan, s.id, s.no_pendaftaran, s.nama_lengkap, s.alamat, s.sekolah_asal, n.nilai, j.jalur AS jalur_name FROM z_siswa s
							LEFT JOIN (SELECT siswa_id, nilai FROM psb_nilai_seleksi WHERE tapel=:tapel) n ON s.id=n.siswa_id 
							JOIN psb_jalur j ON s.jalur=j.jalur_id,(SELECT @rownum := 0) r ORDER BY nilai DESC");
		 $stmt->bindParam("tapel",$tapel);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getFilterPayment(array $var = array()){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tp = $array[1]['thn_ajaran_id'];
		
		$query_arr=array();
		foreach($var as $col => $val){
			$query_arr[] = "".$col."='".$val."'";
		}
		$query="";
		if(!empty($query_arr)){ $query = "AND ".implode(' AND ',$query_arr); }
		
		try
		{
		 $stmt = $db->prepare("SELECT p.no_pendaftaran, s.nama_lengkap, t.id_user, t.payment_date, t.paid, u.username
							   FROM z_siswa s JOIN z_pendaftaran p ON s.uuid=p.uuid JOIN psb_jalur j ON p.jalur=j.jalur_id 
							   JOIN t_payment t ON t.id_siswa=s.uuid JOIN m_user u ON u.id_user=t.id_user 
							   WHERE j.thn_ajaran_id=:tapel AND t.sts='1' ".$query."");
		 $stmt->bindParam("tapel",$tp);
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
	
	public function getDataServerside(){
		$db = $this->dblocal;
		$array = $this->activeTapelPSB();
	    $tapel = $array[1]['thn_ajaran_id'];
		// initilize all variable
		$params = $columns = $totalRecords = $data = array();

		$params = $_REQUEST;

		//define index of column
		$columns = array( 
			0 =>'urutan',
			1 =>'id', 
			2 => 'no_pendaftaran',
			3 => 'nama_lengkap',
			4 => 'alamat',
			5 => 'sekolah_asal',
			6 => 'nilai',
			7 => 'jalur_name'
		);

		$where = $sqlTot = $sqlRec = "";

		// check search value exist
		if( !empty($params['search']['value']) ) {   
			$where .=" WHERE ";
			$where .=" ( s.no_pendaftaran LIKE '%".$params['search']['value']."%' ";    
			$where .=" OR s.nama_lengkap LIKE '%".$params['search']['value']."%' ";
			$where .=" OR s.alamat LIKE '%".$params['search']['value']."%' ";
			$where .=" OR s.sekolah_asal LIKE '%".$params['search']['value']."%' ";
			$where .=" OR j.jalur LIKE '%".$params['search']['value']."%' )";
		}

		// getting total number records without any search
		$sql = "SELECT @rownum := @rownum + 1 AS urutan, s.id, s.no_pendaftaran, s.nama_lengkap, s.alamat, s.sekolah_asal, n.nilai, j.jalur AS jalur_name FROM z_siswa s
				LEFT JOIN (SELECT siswa_id, nilai FROM psb_nilai_seleksi WHERE tapel='".$tapel."') n ON s.id=n.siswa_id 
				JOIN psb_jalur j ON s.jalur=j.jalur_id,(SELECT @rownum := 0) r ";
		$sqlTot .= $sql;
		$sqlRec .= $sql;
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}


		$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
		
		$number = $db->prepare($sqlTot);
		$number->execute();
		$totalRecords = $number->rowCount();
		
		$array = $db->prepare($sqlRec);
		$array->execute();
		$queryRecords = $array->fetchAll(PDO::FETCH_ASSOC);
		
		//$data[] = $queryRecords;
		
		$json_data = array(
				"draw"            => intval( $params['draw'] ),   
				"recordsTotal"    => intval( $totalRecords ),  
				"recordsFiltered" => intval($totalRecords),
				"data"            => $queryRecords   // total data array
				);

		return $json_data;  // send data as json format
	}
	
	public function getPengurus(){
		$db = $this->dblocal;
		try
		{
		 $stmt = $db->prepare("SELECT * FROM pes_pengurus_markazy");
		 $stmt->execute();
		 $stat[0] = true;
	     $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
	     return $stat;
	   }
	   catch(PDOException $ex)
	   {
		 $stat[0] = false;
		 $stat[1] = $ex->getMessage();
		 return $stat;
	   }
	}
}