<?php
class utility extends dbconn {
	public function __construct()
	{
		$this->initDBO();
	}
 
 /******************************************************************************
    START OF MENU CODE
 *******************************************************************************/
    public function getTopMenu()
    {
    	$db = $this->dblocal;
    	try
    	{
    		$stmt = $db->prepare("select * from r_top_menu order by menu_order");
    		$stmt->execute();
    		$stat[0] = true;
    		$stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    		return $stat;
    	}
    	catch(PDOException $ex)
    	{
    		$stat[0] = false;
    		$stat[1] = $ex->getMessage();
    		return $stat;
    	}
    }
	
	public function getTopMenubyId($id)
    {
    	$db = $this->dblocal;
    	try
    	{
    		$stmt = $db->prepare("select * from r_top_menu WHERE id=:id");
			$stmt->bindParam("id",$id);
    		$stmt->execute();
    		$stat[0] = true;
    		$stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
    		return $stat;
    	}
    	catch(PDOException $ex)
    	{
    		$stat[0] = false;
    		$stat[1] = $ex->getMessage();
    		return $stat;
    	}
    }
	
	public function getMenu($menu_id='')
    {
    	//if($menu_id == ''){$query ='';}else{$query =" WHERE top_menu_id= '".$menu_id."'";}
		$db = $this->dblocal;
    	try
    	{
    		//$stmt = $db->prepare("select * from r_menu ".$query." order by menu_order");
    		$stmt = $db->prepare("select * from r_menu WHERE top_menu_id=:id order by menu_order");
			$stmt->bindParam("id",$menu_id);
    		$stmt->execute();
    		$stat[0] = true;
    		$stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    		return $stat;
    	}
    	catch(PDOException $ex)
    	{
    		$stat[0] = false;
    		$stat[1] = $ex->getMessage();
    		return $stat;
    	}
    }

    
    public function getSubMenu($id)
    {
    	$db = $this->dblocal;
    	try
    	{
    		$stmt = $db->prepare("select * from r_menu_sub where id_menu = :id order by sub_menu_order asc");
    		$stmt->bindParam("id",$id);
    		$stmt->execute();
    		$stat[0] = true;
    		$stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    		return $stat;
    	}
    	catch(PDOException $ex)
    	{
    		$stat[0] = false;
    		$stat[1] = $ex->getMessage();
    		return $stat;
    	}
    }

	

    /*********************query for system*********************/
    public function getLogin($user,$pass)
    {
     $db = $this->dblocal;
     try
     {
      $stmt = $db->prepare("select a.*,m.url,
        (select nama_sekolah from z_profil_sekolah where id_sekolah = 1) as nama_sekolah
        from m_user a left join r_menu_sub m on a.default_page=m.id_sub_menu
		where  upper(a.username)=upper(:user) and a.pass_user=md5(:id)");
      $stmt->bindParam("user",$user);
      $stmt->bindParam("id",$pass);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
      $stat[2] = $stmt->rowCount();
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      $stat[2] = 0;
      return $stat;
    }
  }

  public function getrefsytem()
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("select a.* from z_profil_sekolah a where id_sekolah = 1 ");
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      $stat[2] = 0;
      return $stat;
    }
  }
  
  public function getSubMenuById($menu)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("SELECT name_sub_menu FROM r_menu_sub WHERE id_sub_menu= :menu");

      $stmt->bindParam("menu",$menu);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->fetchColumn(0);
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }
  public function updateUniqLogin($id_user,$uniq_login)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("update m_user set uniq_login = :uniq_login where id_user = :id_user");

      $stmt->bindParam("id_user",$id_user);
      $stmt->bindParam("uniq_login",$uniq_login);

      $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses Ubah!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }

  /*********************query for master user*********************/
  public function getListUser()
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("select * from m_user where username<>'admin' order by username desc");
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }

  public function detailUser($id)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("select * from m_user where id_user=:id");
	   $stmt->bindParam("id",$id);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->fetch(PDO::FETCH_ASSOC);
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }
  
  public function saveUser($username,$pass_user,$t_menu,$h_menu,$page)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("insert into m_user(username,pass_user,level_access,h_menu,default_page)
        values(:name,MD5(:pass),:tmenu,:hmenu,:page)");

      $stmt->bindParam("name",$username);
      $stmt->bindParam("pass",$pass_user);
      $stmt->bindParam("tmenu",$t_menu);
      $stmt->bindParam("hmenu",$h_menu);
      $stmt->bindParam("page",$page);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses save!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }
  public function updateUser($id_user,$username,$t_menu,$h_menu,$page)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("update m_user set username = :name, level_access= :tmenu, h_menu = :hmenu, default_page=:page where id_user = :id");

      $stmt->bindParam("name",$username);
      $stmt->bindParam("id",$id_user);
	  $stmt->bindParam("tmenu",$t_menu);
      $stmt->bindParam("hmenu",$h_menu);
      $stmt->bindParam("page",$page);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses update!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }
  
  public function updatePhotoUser($id_user,$photo)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("update m_user set avatar = :photo where id_user = :id");

      $stmt->bindParam("photo",$photo);
      $stmt->bindParam("id",$id_user);
	  $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses update!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }
  
  public function deleteUser($id_user)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("delete from m_user  where id_user = :id");

      $stmt->bindParam("id",$id_user);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses update!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }

  public function checkPassword($id,$pass)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("select * from m_user where id_user = :id and pass_user = md5(:pass)");

      $stmt->bindParam("id",$id);
      $stmt->bindParam("pass",$pass);

      $stmt->execute();
      $stat[0] = true;
      $stat[1] = $stmt->rowCount();
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }

  public function resetPass($iduser,$pass)
  {
    $db = $this->dblocal;
    try
    {
      $stmt = $db->prepare("update m_user set pass_user = md5(:pass) where id_user=:id");

      $stmt->bindParam("id",$iduser);
      $stmt->bindParam("pass",$pass);
      $stmt->execute();
      $stat[0] = true;
      $stat[1] = "Sukses reset pass!";
      return $stat;
    }
    catch(PDOException $ex)
    {
      $stat[0] = false;
      $stat[1] = $ex->getMessage();
      return $stat;
    }
  }

	

  /******************************************************************************
    END OF MENU CODE
  *******************************************************************************/
  }

  ?>
