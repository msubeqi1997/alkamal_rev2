<?php
$tahun =  date("Y");
function emailRegistrasi($username,$password){
  $__base = 'https://psb.alkamalblitar.com';
  $output = '
  <div class="email-content" style="font-size: 14px;  margin: 0;  padding: 10px;  background-color: #154c28;">
  <div class="email-heading" style="background-color: #ffffff; margin-bottom: 5px;  padding: 0 10px 10px;text-align:center;">
  <img src="../../image/Logo-Al-kamal.png" alt="logo" class="logo" style="width: 200px;"/>
  </div>
  <div class="email-body" style="background-color: #ffffff; padding: 10px 10px 20px;  margin-bottom: 5px;">
  <h3>Thank you!</h3>
  <p>
  You have registered in <a href="'.$__base.'" title="My Buddy School" target="_blank">My Buddy School</a>.
  </p>
  <p>Please keep your log in information below:</p>
  <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
  <tr><td style="width:100px;">Username</td><td>: '.$username.'</td></tr>
  <tr><td style="width:100px;">Password</td><td>: '.$password.'</td></tr>
  </table>
  </div>
  <div class="email-footer" style="background-color: #ffffff;  padding: 10px;  line-height: 22px;">
  &copy; '.$tahun.' - Pondok Pesantren Terpadu Al-Kamal Blitar
  </div>
  </div>
  ';
  return $output;
}
function emailResPass($link){
  $__base = 'https://psb.alkamalblitar.com';
  $output = '
  <div class="email-content" style="font-size: 14px;  margin: 0;  padding: 10px;  background-color: #154c28;">
  <div class="email-heading" style="background-color: #ffffff; margin-bottom: 5px;  padding: 0 10px 10px;text-align:center;">
  <img src="../../image/Logo-Al-kamal.png" alt="logo" class="logo" style="width: 200px;"/>
  </div>
  <div class="email-body" style="background-color: #ffffff; padding: 10px 10px 20px;  margin-bottom: 5px;">
  <h3>Request Reset Password!</h3>
  <p>
  You have requested reset password.
  Please click or open this link below to reset your password.
  </p>
  <a class="tombol" href="'.$link.'" title="Reset Password"  target="_blank" style="padding: 5px 8px;background-color: #154c28;color:#ffffff; border-color: #f2691d; text-decoration: none;  border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;">'.$link.'</a>
  </div>
  <div class="email-footer" style="background-color: #ffffff;  padding: 10px;  line-height: 22px;">
  &copy; '.$tahun.' - Pondok Pesantren Terpadu Al-Kamal Blitar
  </div>
  </div>
  ';
  return $output;

}
function emailReqPrint($link){
  $__base = 'https://simpesma.alkamalblitar.com';
  $output = '
  <div class="email-content" style="font-size: 14px;  margin: 0;  padding: 10px;  background-color: #154c28;">
  <div class="email-heading" style="background-color: #ffffff; margin-bottom: 5px;  padding: 0 10px 10px;text-align:center;">
  <img src="'.$__base.'/image/Logo-Al-kamal.png" alt="logo" class="logo" style="width: 200px;"/>
  </div>
  <div class="email-body" style="background-color: #ffffff; padding: 10px 10px 20px;  margin-bottom: 5px;">
  <h3>Cetak data pendaftaran!</h3>
  <p>
  Silahkan klik atau buka tautan di bawah ini untuk mencetak data pendaftaran anda.
  </p>
  <a class="tombol" href="'.$link.'" title="Cetak Fromulir"  target="_blank" style="padding: 5px 8px;background-color: #154c28;color:#ffffff; border-color: #f2691d; text-decoration: none;  border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;">'.$link.'</a>
  </div>
  <div class="email-footer" style="background-color: #ffffff;  padding: 10px;  line-height: 22px;">
  &copy; '.date("Y").' - Pondok Pesantren Terpadu Al-Kamal Blitar
  </div>
  </div>
  ';
  return $output;

}
function emailResPassDone($username,$password){
  $__base = 'https://psb.alkamalblitar.com';
  $output = '
  <div class="email-content" style="font-size: 14px;  margin: 0;  padding: 10px;  background-color: #154c28;">
  <div class="email-heading" style="background-color: #ffffff; margin-bottom: 5px;  padding: 0 10px 10px;text-align:center;">
  <img src="../../image/Logo-Al-kamal.png" alt="logo" class="logo" style="width: 200px;"/>
  </div>
  <div class="email-body" style="background-color: #ffffff; padding: 10px 10px 20px;  margin-bottom: 5px;">
  <h3>Reset Password!</h3>
  <p>
  You have reset your <strong>password</strong>.
  </p>
  <p>Please keep your log in information below:</p>
  <table border="0" cellpadding="10" cellspacing="0" style="width:100%">
  <tr><td style="width:100px;">Username</td><td>: '.$username.'</td></tr>
  <tr><td style="width:100px;">Password</td><td>: '.$password.'</td></tr>
  </table>
  </div>
  <div class="email-footer" style="background-color: #ffffff;  padding: 10px;  line-height: 22px;">
  &copy; '.$tahun.' - Pondok Pesantren Terpadu Al-Kamal Blitar
  </div>
  </div>
  ';
  return $output;
}
function emailAkunAktif(){
  $__base = 'https://psb.alkamalblitar.com';
  $output = '
  <div class="email-content" style="font-size: 14px;  margin: 0;  padding: 10px;  background-color: #154c28;">
  <div class="email-heading" style="background-color: #ffffff; margin-bottom: 5px;  padding: 0 10px 10px;text-align:center;">
  <img src="../../image/Logo-Al-kamal.png" alt="logo" class="logo" style="width: 200px;"/>
  </div>
  <div class="email-body" style="background-color: #ffffff; padding: 10px 10px 20px;  margin-bottom: 5px;">
  <h3>Congratulation!</h3>
  <p>
  Your account has been <strong>activated</strong>. You can log in at <a href="'.$__base.'" title="My Buddy School" target="_blank">My Buddy School</a>
  </p>

    </div>
  <div class="email-footer" style="background-color: #ffffff;  padding: 10px;  line-height: 22px;">
  &copy; '.$tahun.' - Pondok Pesantren Terpadu Al-Kamal Blitar
  </div>
  </div>
  ';
  return $output;
}
