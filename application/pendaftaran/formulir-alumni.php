<?php 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");

$cp=$_GET['cp'];
$pos = new model_psb();
$array = $pos->getJalurPendaftaranByDate();
$jalur = $array[1];
$detail_jalur = $pos->getDetailJalurPendaftaran(base64_decode($cp));
$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];

$propinsi = $pos->getPropinsi();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">
  <!-- end no cache headers -->
  <title>Pendaftaran Online</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../dist/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../dist/ionic/css/ionicons.min.css"> 
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../dist/css/custom.css">
  <link rel="shortcut icon" href="../../dist/img/favicon.ico" />
  <link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-green-light layout-top-nav">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header skin-purple"> 
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="http://www.alkamalblitar.com" class="navbar-brand">
             <span class="logo-mini"> 
              <b>PONDOK PESANTREN TERPADU AL KAMAL BLITAR</b>
             </span>
			</a>
          </div>
        

		 <div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  
			</a> 
		   </li>
		  </ul>
		 </div><!--  /.<div class="navbar-custom-menu">-->
		</div><!-- ./container -->
	  </nav>
	</header>
  <?php if(!empty($jalur)){ ?>
	<!-- main content -->
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Formulir Pendaftaran MDK
		  </h1>
		</section>
	  
		<section class="content">
			<form method="post" id="target" action="formulir-online_process.php" enctype="multipart/form-data">
				<!-- Identitas siswa -->
				<div class="box box-success">
					<div class="box-header with-border">
					  <h3 class="box-title">Identitas santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
				  
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-8">
							<div class="form-group"> <label class="col-sm-3  control-label-l">Jalur pendaftaran</label>
								<div class="col-sm-7">
									<input type="text" id="jalur" name="jalur" class="form-control" value="<?php echo $detail_jalur[1]['jalur'];?>" disabled>
									<input type="hidden" id="jalur_daftar" name="txtjalur" class="" value="<?php echo $detail_jalur[1]['jalur_id'];?>">
									<input type="hidden" id="inputcrud" name="inputcrud" class="" value="E">
									<input type="hidden" id="id_siswa" name="id_siswa" class="" value="">
									<input type="hidden" name="method" class="" value="save_pendaftar_alumni">
										
								</div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Apakah MDK? </label>
								<div class="col-sm-9" id="tipe"> 
									<input type="text" class="form-control" name="mdk" id="mdk" value="MDK" disabled>
									<input type="hidden" id="jenis" name="jenis" value="mdk">
								</div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Sekolah tujuan <span class="text-danger">*</span></label>
							  <div class="col-sm-9" id="tujuan"> 
								<select class="form-control" id="sekolah" name="sekolah" required="required">
									<option value=""> Pilih sekolah </option>
									<?php
									$satpen = $pos->getSatuanPendidikan(2);
									foreach($satpen[1] as $row){
									  echo '<option value="'.$row['kode_sekolah'].'"> '.$row['nama_sekolah'].' </option>';
									}
									?>
								</select>
							  </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Jenjang pendidikan</label>
								<div class="col-sm-9" id="jenjang"> </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label">No Induk Santri<span class="text-danger">*</span></label>
								<div class="col-sm-9"><input type="text" class="form-control " id="no_induk" name="no_induk" value="" placeholder="Diisi jika alumni pesantren (MDK)"> </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label">Nama lengkap<span class="text-danger">*</span></label>
								<div class="col-sm-9"><input type="text" class="form-control " id="nama_lengkap" name="nama_lengkap" value="" placeholder="Isikan nama lengkap" required="" readonly="readonly"> </div>
							</div>
							<div class="form-group">
							  <label for="j-kelamin" class="control-label-l col-sm-3">Jenis kelamin<span class="text-danger"> *</span></label>
							  <div class="col-sm-9">
								<div class="radio">
								  <label><input type="radio" id="kelamin" name="kelamin" value="1" required disabled> Laki-laki</label>
								  <label><input type="radio" id="kelamin" name="kelamin" value="0" required disabled> Perempuan</label>
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label for="tempatlahir" class="control-label-l col-sm-3">Tempat & Tanggal lahir <span class="text-danger"> *</span></label>
							  <div class="col-sm-5">
								<input type="text" class="form-control letters" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" required readonly="readonly">
							  </div>
							  <div class="col-sm-4">
								<div class="input-group date" id="datepicker">
								  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir" id="tanggal_lahir" placeholder="dd-mm-yyyy"   readonly required>
								  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label for="email" class="control-label-l col-sm-3">Email</label>
							  <div class="col-sm-9">
								<input type="email" class="form-control" name="email" id="email" placeholder="Isi jika ganti email" >
							  </div>
							</div>
							<div class="form-group">
							  <label for="no_hp" class="control-label-l col-sm-3">Handphone yang aktif</label>
							  <div class="col-sm-9">
								<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)" readonly="readonly">
							  </div>
							</div>
						</div><!-- /.col -->
						
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				
					<div class="box-footer">
					  <span class="text-danger">*</span> ) harus diisi
					</div>
				  
				</div><!-- /identitas siswa-->
			
				<div class="box box-widget">
				  <div class="box-footer">
                    <button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Kirim & Cetak Formulir </button>
                    <span id="infoproses"></span>
				  </div><!-- /.box-footer -->
				</div><!-- /.box -->
			</form>
		</section>
	  </div>
	</div>
   <?php } else {?>
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Maaf, belum ada jadwal pendaftaran.
		  </h1>
		</section>
	  </div>
	</div>
   <?php }?>
  </div>
	
<div class="scroll-top-wrapper "><!-- back to top button-->
  <span class="scroll-top-inner">
    <i class="fa fa-2x fa-arrow-circle-up"></i>
  </span>
</div> <!-- end back to top button-->
 <footer class="main-footer"> 
 	<div class="pull-right hidden-xs">
 		Edu System <b>Version</b> 2.0 
 	</div>
 	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"></a>.</strong> All rights reserved.
 </footer>
</div><!-- ./wrapper -->
<div id="loadbargood" class="loadbargood hidden"></div>

<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
var popup = window.open("about:blank", "new_window_123", "height=150,width=150");
 setTimeout( function() {
    if(!popup || popup.outerHeight === 0) {
        alert("Untuk kelancaran pencetakan formulir pendaftaran matikan Pop up Blocker.");
    }else{
		popup.close();
	}	
}, 25);

$(document).ready(function() {
   
	jQuery.fn.getParent = function(num) {
		var last = this[0];
		for (var i = 0; i < num; i++) {
			last = last.parentNode;
		}
		return jQuery(last);
	};

});

function closepopup()
   {
      if(false == my_window.closed)
      {
         my_window.close ();
      }
      else
      {
         alert('Window already closed!');
      }
   }
   

		
	
    $(function () {
		
		$("div#tujuan").on("change", "select[name='sekolah']",function() {
			get_jenjang();
		});
		
		$("input[name='kelamin']").on("change", function() {
			get_jenjang();
		});
		
		$('#tanggal_lahir, #tanggal_lahir_ayah, #tanggal_lahir_ibu, #tanggal_lahir_wali').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  $("#btnsave").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	//searching nama by no induk
		$(document).on("blur","#no_induk",function(){
			$("#nama_lengkap").val('');
			var nomor = $(this).val();
			var value = {
				nomor:nomor,
				method : "get_name"
			};
			$.ajax(
			{
				url : "formulir-online_process.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.status == 'exist'){
						$("#nama_lengkap").val(data.nama);
						$("#kelamin").prop("readonly", true);
						$("#tempat_lahir").prop("readonly", true);
						$("#tanggal_lahir").prop("readonly", true);
						$("#no_hp").prop("readonly", true);
						$("#tempat_lahir").val(data.tempat_lahir);
						$("input[name='kelamin'][value='"+data.kelamin+"']").prop('checked', true);
						$("input[name='kelamin']").attr('disabled', true);
						$("#tanggal_lahir").val(data.tgl);
						$("#no_hp").val(data.no_hp);
						$("#id_siswa").val(data.id_siswa);
						get_jenjang();
					}else{
						bootbox.alert('No induk tidak terdaftar');
						$("#no_induk").val('');
						$("#nama_lengkap").val('');
						$("input[name='kelamin']").prop('checked', false);
						$("#tempat_lahir").val('');
						$("#tanggal_lahir").val('');
						$("#no_hp").val('');
						$("#id_siswa").val('');
						
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});

	$(document).ready(function(){
		$(".letters").keypress(function(event){
			var inputValue = event.which;
            // allow letters and whitespaces only.
            if(!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0) && inputValue != 8 ) { 
                event.preventDefault(); 
            }
            console.log(inputValue);
		});
	});

	function newitem(){
		
		$("input[name='kelamin']").prop('checked', false);
		$("#inputcrud").val('E');
		$("#nama_lengkap").val('');
		$("#tempat_lahir").val('');
		$("#tanggal_lahir").val('');
		$("#no_hp").val('');
		$("#id_siswa").val('');
	}
	
	function get_jenjang(){
		$('#jenjang').html('');
			
		var kelamin = $("input[name='kelamin']:checked").val();
		var sekolah = $("select[name='sekolah']").find('option:selected').val();
		var value = {
			kelamin: kelamin,
			sekolah: sekolah,
			method : "get_jenjang"
		};
		$.ajax(
		{
			url : "formulir-online_process.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var hasil = jQuery.parseJSON(data);
				$('#jenjang').append(hasil.jenjang);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		window.open('formulir-do-pdf.php?token='+respon.token+'&nomor='+respon.no_pendaftaran, '_blank');
		bootbox.alert("Berhasil. Data pendaftaran anda telah disimpan.");
		setTimeout(function(){
		  location.href='terima-kasih.php';
		},0);
	  }
	  else if(respon.status == 'EXIST'){
		bootbox.alert('Gagal. Email yang anda gunakan sudah terdaftar. Harap gunakan email pribadi anda.');
	  }
	  else{
		bootbox.alert(msg);
	  }
	};

	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	
    </script>
</body>
</html>
