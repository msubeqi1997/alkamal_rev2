<?php
//error_reporting(0);
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();

$idc = isset($_GET['token'])?$_GET['token']:'';
$no_pendaftaran = isset($_GET['nomor'])?$_GET['nomor']:'';
$pos = new model_psb();
$array = $pos->getSiswaPendaftar($idc);
$data = $array[1];
$tgl_ttd= tanggal_indo($data['tertanggal']);
$tgl_lahir_ayah = (empty($data['tanggal_lahir_ayah']) || $data['tanggal_lahir_ayah'] == '')?'':date('d-m-Y',strtotime($data['tanggal_lahir_ayah']));
$ttl_ayah = $data['tempat_lahir_ayah'].', '.$tgl_lahir_ayah;
$arr_pengurus = $pos->getPengurus();
$pengurus = $arr_pengurus[1];

$kelamin = array('0'=>'Perempuan','1'=>'Laki-laki');
$stopword = array('KOTA','KABUPATEN');
$walisantri = (!empty($data['wali']))?$data['wali']:$data['ayah'];
$pekerjaan = (!empty($data['wali']))?$data['pekerjaan_wali']:$data['pekerjaan_ayah'];
$rekening = $pos->get_rekening_bank();
$tingkatpend = $pos->getTingkatPendidikan();
$pendidikan = array();
$pendidikan['']='';
foreach($tingkatpend[1] as $row){
	$pendidikan[$row['id']]=$row['pendidikan'];
}

  $rwy_skl = $pos->getRiwayatPendidikan($idc);
  $sekolah = '';
  
	foreach($rwy_skl[1] as $skl){
	$sekolah .='<tr>
            <td class="label">Nama sekolah</td>
            <td>:</td>
            <td>'.$skl['penyelenggara'].'</td>
            <td>  </td>
            <td>  </td>
            <td>  </td>
            <td style="width:70px">Lulus tahun</td>
            <td>:</td>
            <td>'.$skl['tahun_selesai'].'</td>
          </tr>';
	}
  
  
$html = '<!DOCTYPE html>
  <html lang="id">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <div class="kertas">
      <div class="kop-surat">
        <img src="../../image/kop-alkamal.jpg" width="738" height="103" style="margin-top:-30px" alt="Kop surat">
      </div>
      <h1>FORMULIR ONLINE PENDAFTARAN SANTRI BARU</h1>
      <table class="formulir">
		<thead>

        </thead>
		<tbody>
		  <tr style="padding-top:10px;">
            <td class="label" style="width:145px;">No. pendaftaran</td>
            <td style="width:5px">:</td>
            <td colspan="7"> '.$data['no_pendaftaran'].'</td>
          </tr>
		  <tr style="background-color:#000;color:#fff;text-align:center;">
            <th colspan="11"><h2>IDENTITAS CALON SANTRI</h2></th>
          </tr>
		  <tr style="padding-top:10px;">
            <td class="label" style="width:145px;">Nama lengkap</td>
            <td style="width:5px">:</td>
            <td colspan="7"> '.$data['nama_lengkap'].'</td>
          </tr>
		  <tr>
            <td class="label">Tempat & tanggal lahir</td>
            <td>:</td>
            <td colspan="7"> '.$data['tempat_lahir'].', '.date('d-m-Y',strtotime($data['tanggal_lahir'])).'</td>
          </tr>
		  <tr>
            <td class="label">Jenis kelamin</td>
            <td>:</td>
            <td style="width:260px"> '.$kelamin[$data['kelamin']].'</td>
          </tr>
		  <tr>
            <td class="label">Tujuan sekolah formal</td>
            <td>:</td>
            <td> '.$data['tujuan'].'</td>
            
          </tr>
		  <tr>
            <td class="label">Alamat lengkap</td>
            <td>:</td>
            <td colspan="7"> '.$data['alamat'].' '.ucwords(strtolower($data['kabupaten_name'])).' '.ucwords(strtolower($data['propinsi_name'])).'</td>
          </tr>
		  <tr>
            <td class="label">Kode pos</td>
            <td>:</td>
            <td colspan="7"> '.$data['kode_pos'].'</td>
          </tr>
		  <tr><td style="margin-bottom:20px;">&nbsp;</td></tr>
		  <tr style="background-color:#000;color:#fff;text-align:center;">
            <th colspan="11"><h2>DATA SEKOLAH</h2></th>
          </tr>
		  '.$sekolah.'
		  <tr><td style="margin-bottom:20px;">&nbsp;</td></tr>
		  <tr style="background-color:#000;color:#fff;text-align:center;">
            <th colspan="11"><h2>DATA CALON ORANG TUA / WALI SANTRI</h2></th>
          </tr>
		  <tr>
		   <td colspan="11" style="font-weight:bold">I. DATA ORANG TUA</td>
		  </tr>
		  <tr>
            <td class="label">Nama ayah</td>
            <td>:</td>
            <td> '.$data['ayah'].'</td>
            <td>Nama ibu</td>
            <td>:</td>
            <td colspan="5"> '.$data['ibu'].'</td>
          </tr>
		  <tr>
            <td class="label">Pekerjaan</td>
            <td>:</td>
            <td> '.$data['pekerjaan_ayah'].'</td>
            <td>Pekerjaan</td>
            <td>:</td>
            <td colspan="5"> '.$data['pekerjaan_ibu'].'</td>
          </tr>
		  <tr>
            <td class="label">Pendidikan terakhir</td>
            <td>:</td>
            <td> '.$pendidikan[$data['pendidikan_ayah']].'</td>
            <td>Pendidikan terakhir</td>
            <td>:</td>
            <td colspan="5"> '.$pendidikan[$data['pendidikan_ibu']].'</td>
          </tr>
		  <tr>
            <td class="label">Telp/HP</td>
            <td>:</td>
            <td> '.$data['hp_ayah'].'</td>
            <td>Telp/HP</td>
            <td>:</td>
            <td colspan="5">'.$data['hp_ibu'].'</td>
          </tr>
		  <tr>
            <td class="label">Alamat lengkap</td>
            <td>:</td>
            <td colspan="7"> '.$data['alamat'].'</td>
          </tr>
		  <tr>
            <td class="label">Kode pos</td>
            <td>:</td>
            <td colspan="7"> '.$data['kode_pos'].'</td>
          </tr>
		  <tr>
		   <td colspan="11" style="font-weight:bold">&nbsp;</td>
		  </tr>
		  <tr>
		   <td colspan="11" style="font-weight:bold">II. DATA WALI</td>
		  </tr>
		  <tr>
            <td class="label">Nama wali</td>
            <td >:</td>
            <td colspan="7"> '.$data['wali'].'</td>
          </tr>
		  <tr>
            <td class="label">Alamat lengkap</td>
            <td >:</td>
            <td colspan="7"> '.$data['alamat_wali'].'</td>
          </tr>
		  <tr>
            <td class="label">Hubungan keluarga</td>
            <td >:</td>
            <td colspan="7"> '.$data['hubungan_wali'].'</td>
          </tr>
		  <tr>
            <td class="label">Telp/HP</td>
            <td >:</td>
            <td colspan="7"> '.$data['telp_wali'].'</td>
          </tr>
		  
		  <tr><td style="margin-bottom:20px;">&nbsp;</td></tr>

		</tbody>
	  </table>
	  <table>
		<tr>
          <td></td>
          <td style="text-align:left;"> '.str_replace($stopword,' ',$data['kabupaten_name']).', '.$tgl_ttd.'</td>
        </tr>
        <tr>
          <td style="width:458px; vertical-align:bottom;" rowspan="2">
			<img src="../../files/images_pendaftar/'.$data['photo_siswa'].'" style="height: 38mm; width: 29mm; margin-right:15px;" border="2">
			<img src="../../files/images_pendaftar/'.$data['photo_wali'].'" style="height: 38mm; width: 29mm" border="2">
		  </td>
          <td> Calon Santri PPTA </td>
        </tr>
		<tr>
          <td style="vertical-align:top;">( '.$data['nama_lengkap'].' )</td>
        </tr>
		<tr><td style="margin-bottom:12px;">&nbsp;</td></tr>
		<tr>
            <td colspan="2" class="rekening-judul">
              Pembayaran daftar ulang
            </td>
        </tr>
		<tr>
			<td class="rekening-detail">
			  Bank '.$rekening[1]['cabang'].' a.n '.$rekening[1]['nama_rekening'].' No. rekening '.$rekening[1]['no_rekening'].' 
			</td>
            <td class="rekening-detail">
              Nominal <br/>
              Rp. '.$data['nominal'].'
            </td>
        </tr>
	  </table>
	</div>

	<div class="kertas ganti-halaman">
      <div class="kop-surat">
        <img src="../../image/kop-alkamal.jpg" width="738" height="103" style="margin-top:-30px" alt="Kop surat">
      </div>
      <h1>SURAT PERNYATAAN ORANG TUA & WALISANTRI</h1>
      <table class="formulir">
        <thead>

        </thead>
        <tbody>
		  <tr>
            <td colspan="3">Yang bertandatangan di bawah ini: </td>
          </tr>
          <tr>
            <td class="" style="width:170px;">Nama Lengkap</td>
            <td style="width:2px;">:</td>
            <td>'.$walisantri.'</td>
          </tr>
          <tr>
            <td class="">Pekerjaan</td>
            <td>:</td>
            <td> '.$data['pekerjaan_ayah'].'</td>
          </tr>
          <tr>
            <td class="" >Orang Tua/Walisantri dari</td>
            <td>:</td>
            <td> '.$data['nama_lengkap'].'</td>
		  </tr>
          <tr>
            <td class="" >Alamat</td>
            <td>:</td>
            <td> '.$data['alamat'].' '.ucwords(strtolower($data['kabupaten_name'])).' '.ucwords(strtolower($data['propinsi_name'])).'</td>
		  </tr>
        </tbody>
      </table>
      <h1>MENYATAKAN</h1>
      <div class="pernyataan">
        <ol>
          <li>
            Menyerahkan sepenuhnya kepada bapak Pengasuh dan atau Staf Kepengurusan Pondok
            Pesantren Terpadu Al Kamal atas anak saya untuk dididik dan dibina sebagaimana
            semestinya.
          </li>
          <li>
            Siap menerima kebijakan Bapak Pengasuh dan atau Staf Kepengurusan PPTA yang
            berkaitan dengan anak saya selaku santri di Pondok Pesantren Terpadu Al Kamal
            Wonodadi Blitar.
          </li>
          <li>
            Siap menerima dan mematuhi peraturan /tata tertib yang berlaku bagi anak saya dan yang
            berkaitan dengan Orang Tua / Walisantri di Pondok Pesantren Terpadu Al Kamal Kunir
            Wonodadi Blitar.
          </li>
          <li>
            Siap dan ikhlas serta penuh tanggungjawab menerima sanksi yang diberikan kepada anak
            saya, apabila melanggar peraturan / tata tertib yang berlaku di Pondok Pesantren
            Terpadu Al Kamal Kunir Wonodadi Blitar.
          </li>
          <li>
            Jika anak saya mengundurkan diri, maka saya tidak akan menarik kembali sebagian atau
            seluruh biaya persyaratan pendaftaran yang telah disetorkan.
          </li>
        </ol>
      </div>
      <p>
        Demikian surat pernyataan ini saya tandatangani dengan sadar dan ikhlas tanpa adanya paksaan dari pihak
        manapun, demi kemanfa’atan dan kemaslahatan anak saya.
      </p>
      <div class="tgl-surat"></div>

      <table>
		<tr>
          <td></td>
          <td style="text-align:left;"> '.str_replace($stopword,' ',$data['kabupaten_name']).', '.$tgl_ttd.'</td>
        </tr>
        <tr>
          <td style="width:500px; vertical-align:bottom;" rowspan="3">
		  </td>
          <td style="padding-bottom:40px;"> Saya Yang Menyatakan </td>
        </tr>
		<tr>
		  <td style="font-size:8pt;font-style:italic;"><div class="materai">materai 6000<div></td>
		</tr>
		<tr>
		  <td>'.$walisantri.'</td>
        </tr>
	  </table>
        <br/>
		<div class="center">
          Mengetahui, <br/>
          Pengurus Pusat <br/>
          Pondok Pesantren Terpadu Al Kamal
        </div>

        <table class="paraf">
          <tr class="">
            <td style="text-align:center; "></td>
            <td style=""></td>
          </tr>
		  <tr class="tanda-tangan">
            <td style="width:50%; vertical-align: bottom; text-align:center;padding:-1px 0; ">
			  <img src="../../image/ketua-pondok.png" style="position: absolute;left: 108px;" height="96" width="120"><b> '.$pengurus['ketua'].' </b></td>
            <td style="text-align:center; vertical-align: bottom; padding:-1px 0;">
			  <img src="../../image/sekretaris.png" style="position: absolute;left: 478px;" height="96" width="120"><b>'.$pengurus['sekretaris'].'</b></td>
          </tr>
		  <tr>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">Ketua</td>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">Sekretaris</td>
          </tr>
        </table>
        <div class="center">
          Pengasuh <br/>
          Pondok Pesantren Terpadu Al Kamal
        </div>

        <table class="paraf">
          <tr class="tanda-tangan">
            <td style="width:50%; text-align:center; vertical-align: bottom;">
			  <img src="../../image/pengasuh-1.png" style="position: absolute;left: 128px;" height="76" width="90"><b>'.$pengurus['pengasuh_1'].'</b></td>
            <td style="text-align:center; vertical-align: bottom;">
			  <img src="../../image/pengasuh-2.png" style="position: absolute;left: 498px;" height="76" width="100"><b>'.$pengurus['pengasuh_2'].'</b></td>
          </tr>
		  <tr>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">&nbsp;</td>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">&nbsp;</td>
          </tr>
        </table>
    </div>
	
	<div class="kertas ganti-halaman">
      <div class="kop-surat">
        <img src="../../image/kop-alkamal.jpg" width="738" height="103" style="margin-top:-30px" alt="Kop surat">
      </div>
      <h1>SURAT PERNYATAAN SANTRI BARU</h1>
      <table class="formulir">
        <thead>

        </thead>
        <tbody>
		  <tr>
            <td colspan="3">Yang bertandatangan di bawah ini: </td>
          </tr>
          <tr>
            <td class="" style="width:170px;">Nama Lengkap</td>
            <td style="width:2px;">:</td>
            <td>'.$data['nama_lengkap'].'</td>
          </tr>
          <tr>
            <td class="">Tempat, Tanggal lahir</td>
            <td>:</td>
            <td> '.$data['tempat_lahir'].', '.date('d-m-Y',strtotime($data['tanggal_lahir'])).'</td>
          </tr>
          <tr>
            <td class="" >Jenis kelamin</td>
            <td>:</td>
            <td> '.$kelamin[$data['kelamin']].'</td>
		  </tr>
          <tr>
            <td class="" >Nama ortu/Walisantri</td>
            <td>:</td>
            <td> '.$data['ayah'].'/'.$data['wali'].'</td>
		  </tr>
		  <tr>
            <td class="" >Daftar sekolah (formal) di</td>
            <td>:</td>
            <td> '.$data['tujuan'].'</td>
		  </tr>
        </tbody>
      </table>
      <h1>MENYATAKAN</h1>
      <div class="pernyataan">
        <p style="font-size:13pt;">
          Siap mematuhi semua peraturan yang berlaku di Pondok Pesantren Terpadu Al Kamal Kunir Wonodadi Blitar, dan apabila saya melanggar peraturan tersebut, maka saya bersedia menerima sanksi yang telah ditentukan dengan penuh tanggungjawab dan hati yang ikhlas.
        </p>  
      </div>
      <p>
        Demikian surat pernyataan ini saya tandatangani dengan sadar dan ikhlas tanpa adanya paksaan dari pihak
        manapun.
      </p>
      <div class="tgl-surat"></div>

      <table>
		<tr>
          <td></td>
          <td style="text-align:left;"> '.str_replace($stopword,' ',$data['kabupaten_name']).', '.$tgl_ttd.'</td>
        </tr>
        <tr>
          <td style="width:500px; vertical-align:bottom;" rowspan="3">
		  </td>
          <td style="padding-bottom:40px;"> Saya Yang Menyatakan </td>
        </tr>
		<tr>
		  <td style="font-size:8pt;font-style:italic;"><br/></td>
		</tr>
		<tr>
		  <td>'.$data['nama_lengkap'].'</td>
        </tr>
	  </table>
        <br/>
		<div class="center">
          Mengetahui, <br/>
          Pengurus Pusat <br/>
          Pondok Pesantren Terpadu Al Kamal
        </div>

        <table class="paraf">
          <tr class="">
            <td style="text-align:center; "></td>
            <td style=""></td>
          </tr>
		  <tr class="tanda-tangan">
            <td style="width:50%; vertical-align: bottom; text-align:center;padding:-1px 0; ">
			  <img src="../../image/ketua-pondok.png" style="position: absolute;left: 108px;" height="96" width="120"><b> '.$pengurus['ketua'].' </b></td>
            <td style="text-align:center; vertical-align: bottom; padding:-1px 0;">
			  <img src="../../image/sekretaris.png" style="position: absolute;left: 478px;" height="96" width="120"><b>'.$pengurus['sekretaris'].'</b></td>
          </tr>
		  <tr>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">Ketua</td>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">Sekretaris</td>
          </tr>
        </table>
        <div class="center">
          Pengasuh <br/>
          Pondok Pesantren Terpadu Al Kamal
        </div>

        <table class="paraf">
          <tr class="tanda-tangan">
            <td style="width:50%; text-align:center; vertical-align: bottom;">
			  <img src="../../image/pengasuh-1.png" style="position: absolute;left: 128px;" height="76" width="90"><b>'.$pengurus['pengasuh_1'].'</b></td>
            <td style="text-align:center; vertical-align: bottom;">
			  <img src="../../image/pengasuh-2.png" style="position: absolute;left: 498px;" height="76" width="100"><b>'.$pengurus['pengasuh_2'].'</b></td>
          </tr>
		  <tr>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">&nbsp;</td>
            <td style="text-align:center;vertical-align:top; padding:-1px 0; margin-top:-3px;">&nbsp;</td>
          </tr>
        </table>
    </div>
	
    <style>
          @page {
            /* width = 21cm */
            size: 8.5in 12.99in;
            margin: 1cm 1cm 0.5cm;

          }
          @media print{
            table{
              width: 715px;
            }

          }
          .kertas.ganti-halaman{
            page-break-before: always;
          }
          table{
            width: 100%;
            border-spacing:0;
            border-collapse: collapse;
  		}
          table tr td{
            padding: 0;
            vertical-align: top;
            text-align: left;
  		  font-size:10pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  line-height: 1.6;
          }
          table tr th{
            padding: -11px 0;
            margin-bottom: 15px
            font-size: 12pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  font-weight:bold;
          }
          table.formulir tr td.label{
            font-weight: reguler;
          }
          table.formulir-slim tr td.label{
            width: 5px;
          }
          table tr td.has-child{
            padding: 0;
          }
          table tr td.has-child table tr td{
            padding: 2px 0;
          }
          table tr td.sub-label{
            width: 20px;
          }
          table tr td.label + td{
            width: 10px;
          }
          table.formulir{
            margin-bottom:47px;
          }
          h1{
            text-align: center;
            font-size: 12pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  font-weight:bold;
          }
  		h2{
            text-align: center;
            font-size: 12pt;
  		}
          table.paraf{

          }
          table.paraf tr td{
            text-align: center;
  		  width: 9cm;
          }
          .tanda-tangan td{
            height: 100px;
            vertical-align: bottom;
            text-align: center;
            padding: 0;
  		  font-family:"Times New Roman", Times, serif !important;
  		  font-size: 12pt;
          }
          .garis-bawah td{
            vertical-align: top;
            font-size: 11pt;
            padding: 0;
  		  width:50%;
  		  text-align:center;
          }
          .garis-bawah td .garis-ttd{
            display: inline-block;
            padding: 0 0;
            border-top: 1px solid #000;
            width: 150px;
  		  font-size: 9pt;
  		  font-style:italic;
          }
          .tgl-surat{
            text-align: right;
          }
          .materai{
            width: 45px;
            border:1px solid #454545;
            padding: 3px;
            font-size: 8pt;
            color: #454545;
            margin-left: -30px;
            margin-bottom: 5px;
          }

		  .bingkai{
            border:1px solid #a12d28;
            padding: 3px;
            font-size: 10pt;
            color: #454545;

          }
          .center{
            text-align: center;
          }
          .pernyataan{
            font-size: 12pt;
  		  font-style:italic;
  		  font-family:"Times New Roman", Times, serif !important;
          }
          .rekening-judul{
            padding: 0 5px;
            font-weight:bold;
            text-transform:uppercase;
            border:1px solid #000;
            background-color: #f1f1f1;

          }
          .rekening-detail{
            padding:0 5px;
            border:1px solid #000;

          }
          </style>

      </body>
    </html>
';

$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file=''.$data['nama_lengkap'].'.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}