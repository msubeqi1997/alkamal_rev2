<?php
error_reporting(0);
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
require_once ("../main/class_upload.php");
include 'mailer/PHPMailerAutoload.php';
include 'emailToSend.php';
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) || isset($_POST['method']))
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'select_jalur'){
		$jalur_id = $_POST['txtjalur'];
		$data = $pos->getDetailJalurPendaftaran($jalur_id);
		$jenis_seleksi = $data[1]['jenis'];
		$cp = base64_encode($jalur_id);
		if($jenis_seleksi == 'mdk'){
			$result['link'] = 'formulir-alumni.php?cp='.$cp;
		}else{
			$result['link'] = 'formulir-online.php?cp='.$cp;
		}
		echo json_encode($result);
	}
	
	if($method == 'get_form'){
		$jalur_id = $_POST['id_item'];
		
		$html_jenis = '';
		$html_sekolah = '';
		$data = $pos->getDetailJalurPendaftaran($jalur_id);
		$jenis_seleksi = $data[1]['jenis'];
		$html_sekolah .='<select class="form-control" id="sekolah" name="sekolah" required="required">
							<option value=""> Pilih sekolah </option>';
		
		if($jenis_seleksi == 'mdk'){
			$html_jenis .= '<input type="text" class="form-control" name="mdk" id="mdk" value="MDK" disabled>
							<input type="hidden" id="jenis" name="jenis" value="mdk">';
			$satpen = $pos->getSatuanPendidikan(2);
			foreach($satpen[1] as $row){
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'"> '.$row['nama_sekolah'].' </option>';
			}
			$result['mdk'] = 'ya';
		}else{
			$html_jenis .= '<input type="text" class="form-control" name="mdk" id="mdk" value="Bukan MDK" disabled>
							<input type="hidden" id="jenis" name="jenis" value="tidak">';
			$satpen = $pos->getSatuanPendidikan(1);
			foreach($satpen[1] as $row){
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'"> '.$row['nama_sekolah'].' </option>';
			}
			$result['mdk'] = 'tidak';
		}
		$html_sekolah .='</select>';
		
		$html_kel = '';
		$kelengkapan = $pos->getKelengkapanByJalur($jalur_id);
		$num=1;
		if(!empty($kelengkapan[1])){
		  foreach ($kelengkapan[1] as $res) {
			if($res['jenis_field'] == 'File'){$form ='<input type="file" name="'.$res['nama_field'].'" required />';}
			else{$form ='<input type="file" name="'.$res['nama_field'].'" accept="image/*"/>';}
			
			$html_kel .='<div class="form-group"> 
							<label class="col-sm-3  control-label">'.$num.'. '.$res['kelengkapan'].'</label>
							<div class="col-sm-9">
							'.$form.'	
							</div>
						</div>';$num++;
		  }
		}
		$result['kelengkapan'] = $html_kel;
		$result['jenis'] = $html_jenis;
		$result['tujuan'] = $html_sekolah;
		echo json_encode($result);
	}
	
	if($method == 'get_jenjang'){
		$sekolah = $_POST['sekolah'];
		$kelamin = $_POST['kelamin'];
		$html_jenjang = '';
		if($sekolah != '' && $kelamin != ''){
		  $array = $pos->getJenjangPendidikan($sekolah);
		  $satpen = $array[1]['satuan_pendidikan'];
		  $jenjang = $arrjenjang[$satpen]." ".$sex[$kelamin];
		  $html_jenjang .= '<input type="text" class="form-control" name="jenjang" id="jenjang" value="'.$jenjang.'" readonly>
							<input type="hidden" id="tingkat" name="tingkat" value="'.$satpen.'">';
		}
		$result['jenjang'] = $html_jenjang;
		echo json_encode($result);
	}
	
	if($method == 'get_kabupaten'){
		$propinsi_id = $_POST['propinsi_id'];
		$array = $pos->getKabupaten($propinsi_id);
		$result['data'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'get_name')
	{
		$nomor = $_POST['nomor'];
		if(!empty($nomor)){
			if(strlen(trim($nomor))<=0){
				$output['status']='null';
				$output['nama']=NULL;
			}else{
			$data=$pos->getNamaSiswa($nomor);
			if(!empty($data[1])){
				$output['status']='exist';
				$output['nama']=$data[1]['nama_lengkap'];
				$output['kelamin']=$data[1]['kelamin'];
				$output['tempat_lahir']=$data[1]['tempat_lahir'];
				$output['no_hp']=$data[1]['no_hp'];
				$output['id_siswa']=base64_encode(htmlentities(stripslashes($data[1]['uuid'])));
				$output['tgl']=display_to_report($data[1]['tanggal_lahir']);
				
			}else{
				$output['status']='null';
				$output['nama']=NULL;
			}
			}
		}else{
			$output['status']='null';
			$output['nama']=NULL;
		}
		echo json_encode($output);
	}
	
	if($method == 'save_pendaftar_alumni')
	{
		$tapelPSB = $pos->activeTapelPSB();
		$tahunaktif = $tapelPSB[1]['thn_ajaran_id'];
		$unique = getId();
		$uuid = '';
		$jalur = isset($_POST['txtjalur']) ? $_POST['txtjalur'] : '';
		$data['sekolah'] = isset($_POST['sekolah']) ? $_POST['sekolah'] : '';
		$jenis= isset($_POST['jenis']) ? $_POST['jenis'] : '';
		$data['input_by'] = $_SESSION['sess_id'];
		$status = 'calon';
		$crud = $_POST['inputcrud'];
		$tahun = substr($tapelPSB[1]['thn_ajaran'],0,4);
		$id_siswa = isset($_POST['id_siswa']) ? $_POST['id_siswa'] : '';;
		$email = isset($_POST['email']) ? $_POST['email'] : '';;
		
		$arrjenjang = $pos->getJenjangPendidikan($data['sekolah']);
		$jenjang = $arrjenjang[1]['satuan_pendidikan'];
		$uuid = base64_decode($id_siswa);
		
		if(!empty($email)){
			$edit_em=$pos->updateEmail($uuid,$email);
		}		
		
		$query = $pos->inputjenjangpsb($tahunaktif,$uuid,$jenjang,$jenis,$data['sekolah'],$tahun,$status,$jalur,$data['input_by']);
		
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
			$result['no_pendaftaran']=$query[2];
		}else{
			$result['status']='NO';
		}
		echo json_encode($result);
	}
	
	if($method == 'save_item')
	{
		//error_reporting(0);
	  $cek_email = $pos->cekEmail($_POST['email']);
	  if(!empty($cek_email[1]['email'])){
		$result['status']='EXIST';
	  }
	  else{
		$unique = getId();
		$uuid = '';
		$data['jalur'] = isset($_POST['txtjalur']) ? $_POST['txtjalur'] : '';
		$data['tingkat'] = isset($_POST['tingkat']) ? $_POST['tingkat'] : '';
		$data['nama_lengkap'] = isset($_POST['nama_lengkap']) ? $_POST['nama_lengkap'] : '';
		$data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
		$data['kelamin'] = isset($_POST['kelamin']) ? $_POST['kelamin'] : '';
		$data['tempat_lahir'] = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
		$data['tanggal_lahir'] = isset($_POST['tanggal_lahir']) ? display_to_sql($_POST['tanggal_lahir']) : '';
		$data['saudara'] = isset($_POST['saudara']) ? $_POST['saudara'] : '';
		$data['anak_ke'] = isset($_POST['anak_ke']) ? $_POST['anak_ke'] : '';
		$data['abk'] = isset($_POST['abk']) ? $_POST['abk'] : '';
		$data['propinsi'] = isset($_POST['propinsi']) ? $_POST['propinsi'] : '';
		$data['kabupaten'] = isset($_POST['kabupaten']) ? $_POST['kabupaten'] : '';
		$data['alamat'] = isset($_POST['alamat']) ? $_POST['alamat'] : '';
		$data['no_hp'] = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
		$data['kode_pos'] = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : '';
		
		$data['ayah'] = isset($_POST['nama_ayah']) ? $_POST['nama_ayah'] : '';
		//$data['tempat_lahir_ayah'] = isset($_POST['tempat_lahir_ayah']) ? $_POST['tempat_lahir_ayah'] : '';
		//$data['tanggal_lahir_ayah'] = isset($_POST['tanggal_lahir_ayah']) ? display_to_sql($_POST['tanggal_lahir_ayah']) : '';
		$data['pendidikan_ayah'] = isset($_POST['pendidikan_ayah']) ? $_POST['pendidikan_ayah'] : NULL;
		$data['pekerjaan_ayah'] = isset($_POST['pekerjaan_ayah']) ? $_POST['pekerjaan_ayah'] : '';
		$data['hp_ayah'] = isset($_POST['hp_ayah']) ? $_POST['hp_ayah'] : '';
		
		$data['ibu'] = isset($_POST['nama_ibu']) ? $_POST['nama_ibu'] : '';
		//$data['tempat_lahir_ibu'] = isset($_POST['tempat_lahir_ibu']) ? $_POST['tempat_lahir_ibu'] : '';
		//$data['tanggal_lahir_ibu'] = isset($_POST['tanggal_lahir_ibu']) ? display_to_sql($_POST['tanggal_lahir_ibu']) : '';
		$data['pendidikan_ibu'] = isset($_POST['pendidikan_ibu']) ? $_POST['pendidikan_ibu'] : NULL;
		$data['pekerjaan_ibu'] = isset($_POST['pekerjaan_ibu']) ? $_POST['pekerjaan_ibu'] : '';
		$data['hp_ibu'] = isset($_POST['hp_ibu']) ? $_POST['hp_ibu'] : '';
		
		$data['wali'] = isset($_POST['nama_wali']) ? $_POST['nama_wali'] : '';
		//$data['tempat_lahir_wali'] = isset($_POST['tempat_lahir_wali']) ? $_POST['tempat_lahir_wali'] : '';
		//$data['tanggal_lahir_wali'] = isset($_POST['tanggal_lahir_wali']) ? display_to_sql($_POST['tanggal_lahir_wali']) : '';
		$data['alamat_wali'] = isset($_POST['alamat_wali']) ? $_POST['alamat_wali'] : '';
		$data['pekerjaan_wali'] = isset($_POST['pekerjaan_wali']) ? $_POST['pekerjaan_wali'] : '';
		$data['telp_wali'] = isset($_POST['telp_wali']) ? $_POST['telp_wali'] : '';
		$data['hubungan_wali'] = isset($_POST['hubungan_wali']) ? $_POST['hubungan_wali'] : '';
		
		$data['input_at'] = date ('Y-m-d H:i:s');
		$data['status'] = 'calon';
		$crud = $_POST['inputcrud'];
		$tahun = date('Y');
		
		$folder_foto = '../../files/images_pendaftar/';
		$tmp_siswa = $_FILES['photo_siswa']['tmp_name'];
		$tipe_siswa = $_FILES['photo_siswa']['type'];
		$nama_siswa = $_FILES['photo_siswa']['name'];
		$ext_siswa = end((explode(".", $nama_siswa)));
		$nama_photo_siswa = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_siswa;
		
		$tmp_wali = $_FILES['photo_wali']['tmp_name'];
		$tipe_wali = $_FILES['photo_wali']['type'];
		$nama_wali = $_FILES['photo_wali']['name'];
		$ext_wali = end((explode(".", $nama_siswa)));
		$nama_photo_wali = 'wali_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_wali;
		
		
		if (!empty($tmp_siswa)) { //jika ada gambar
			UploadCompress($nama_photo_siswa, $folder_foto, $tmp_siswa);
			$data['photo_siswa']=$nama_photo_siswa;
		}
		if (!empty($tmp_wali)) { //jika ada gambar
			UploadCompress($nama_photo_wali, $folder_foto, $tmp_wali);
			$data['photo_wali']=$nama_photo_wali;
		}
		$uuid = $unique;
		$data['uuid'] = $uuid;
		
		$savesiswa = $pos->savePendaftar($data);
		
		$tapelPSB = $pos->activeTapelPSB();
		$tahunaktif = $tapelPSB[1]['thn_ajaran_id'];
		$tahun = substr($tapelPSB[1]['thn_ajaran'],0,4);
		$sekolah = isset($_POST['sekolah']) ? $_POST['sekolah'] : '';
		$arrjenjang = $pos->getJenjangPendidikan($sekolah);
		$jenjang = $arrjenjang[1]['satuan_pendidikan'];
		$jenis = isset($_POST['jenis']) ? $_POST['jenis'] : '';
		$input_by='0';
		
		if($savesiswa[0] == true){
			$query = $pos->inputjenjangpsb($tahunaktif,$uuid,$jenjang,$jenis,$sekolah,$tahun,$data['status'],$data['jalur'],$input_by);
		}
		
		if($query[0] == true){
			//riwayat pendidikan
			$lulus = $_POST['lulus'];
			foreach($_POST['riwayat'] as $key => $val){
				$riwayat['uuid'] = $uuid;
				$riwayat['penyelenggara'] = $val;
				$riwayat['tahun_selesai'] = $lulus[$key];
				$pos->saveRiwayatPendidikan($riwayat);
			}
			
			//upload file
			$file= $pos->getBerkasBySiswa($uuid,$data['jalur']); 
			if(!empty($file[1])){
			  foreach($file[1] as $upl){
				$berkas['uuid'] = $uuid;
				$berkas['id_pendaftaran'] = $query[2];
				$berkas['kelengkapan']=$upl['kelengkapan_id'];
				$berkas['tipe']=$upl['jenis_field'];
				$berkas['lengkap']='0';
				$berkas['html_value']='';

				$tmp_file = $_FILES[$upl['nama_field']]['tmp_name'];
				$tipe_file = $_FILES[$upl['nama_field']]['type'];
				$nama_file = $_FILES[$upl['nama_field']]['name'];
				$acak = rand(1, 99);
				$nama_file_unik = $upl['kelengkapan'].'-'.$uuid.'-'.$data['nama_lengkap'].'-'.$nama_file;

				$extension=strtolower(strrchr($nama_file , '.'));
				if (preg_match("/$extension/i", '.jpg.jpeg.gif.png')) {
					$folder = '../../files/images_pendaftar/';
				} else {
					$folder = '../../files/files_pendaftar/';
				}

				if (!empty($tmp_file)) { //jika ada gambar
					if (!empty($upl['html_value'])) {
					  unlink($folder.$nama_file_unik);
					}
					$pos->deleteBerkasSiswa($uuid,$upl['kelengkapan_id']);
					UploadCompress($nama_file_unik, $folder, $tmp_file);
					$berkas['html_value']=$nama_file_unik;
				}
				
				$pos->saveBerkasSiswa($berkas);
			  }
			}
		}
		
		$result['error'] = $query[1];
		$result['no_pendaftaran']=$query[2];
		$result['crud'] = $crud;
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
		}else{
			$result['status']='NO';
		}
	  }
	  echo json_encode($result);
	}
	
	if($method == 'cetak_ulang'){
		
		$email = $_POST['email'];
		
		$cek_email = $pos->cekEmail($email);
		//$output['uuid']=$cek_email[1]['uuid'];
		if(empty($cek_email[1]['uuid'])){
		  
		  $output['status']='NOT FOUND';
		  
		}else
		{
		  
		  $link = 'http://simpesma.alkamalblitar.com/application/pendaftaran/formulir-do-pdf.php?token='.$cek_email[1]['uuid'];
			  /*send to email*/
			  $euser = 'info@alkamalblitar.com';
			  $epass = 'alkamal_binfo';
			  $recipient = $cek_email[1]['email'];
			  $email_content = emailReqPrint($link);
			  $mail = new PHPMailer();

			  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

			  $mail->isSMTP();                                      // Set mailer to use SMTP
			  $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			  $mail->SMTPAuth = true;                               // Enable SMTP authentication
			  $mail->Username = $euser;                 // SMTP username
			  $mail->Password = $epass;                           // SMTP password
			  $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			  $mail->Port = 465;                                    // TCP port to connect to

			  $mail->From = $euser;
			  $mail->FromName = 'PPT Al-Kamal Blitar';
			  //$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
			  $mail->addAddress($recipient);               // Name is optional
			  //$mail->addReplyTo('info@example.com', 'Information');
			  //$mail->addCC('cc@example.com');
			  //$mail->addBCC('bcc@example.com');

			  //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			  //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
			  $mail->isHTML(true);                                  // Set email format to HTML

			  $mail->Subject = 'Cetak Data Pendaftaran';
			  $mail->Body = $email_content;
			  //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			  $mail->send();
			  $output['status']='OK';
		}
		$respon=json_encode($output);
		echo $respon;
	}
} else {
	exit('No direct access allowed.');
}