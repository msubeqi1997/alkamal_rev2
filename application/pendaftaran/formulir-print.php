<?php 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");

$pos = new model_psb();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">
  <!-- end no cache headers -->
  <title>Cetak Ulang Formulir</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../dist/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../dist/ionic/css/ionicons.min.css"> 
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../dist/css/custom.css">
  <link rel="shortcut icon" href="../../dist/img/favicon.ico" />
  <link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-green-light layout-top-nav">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header skin-green-light"> 
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="http://www.alkamal.com" class="navbar-brand">
             <span class="logo-mini"> 
              <b>AL KAMAL</b>
             </span>
			</a>
          </div>
        

		 <div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  
			</a> 
		   </li>
		  </ul>
		 </div><!--  /.<div class="navbar-custom-menu">-->
		</div><!-- ./container -->
	  </nav>
	</header>
  
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Cetak ulang formulir pendaftaran.
		  </h1>
		</section>
		<section class="content">
			<div class="col-sm-12">
			  <form class="form-horizontal" action="formulir-online_process.php" method="post" id="target">
				<div class="form-group">
				  <label for="email" class="control-label col-sm-2">Email<span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<div class="input-group">
					  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required="">
					  <input type="hidden" name="method" class="" value="cetak_ulang">
					  
					  <div class="input-group-btn">
						<button type="submit" class="btn btn-info"><i class="fa fa-print"></i> Cetak Data</button>

					  </div>
					</div>
					<p class="help-block">
					  Untuk menjaga kerahasiaan data anda, link cetak data akan dikirim ke email terdaftar yang anda masukkan.
					</p>
				  </div>
				</div>
			  </form>
			  <div class="text-center">
				<a href="pendaftaran.php" class="btn btn-default">Kembali</a>

			  </div>
			</div>
		</section>
	  </div>
	</div>

  </div>
	
<div class="scroll-top-wrapper "><!-- back to top button-->
  <span class="scroll-top-inner">
    <i class="fa fa-2x fa-arrow-circle-up"></i>
  </span>
</div> <!-- end back to top button-->
 <footer class="main-footer"> 
 	<div class="pull-right hidden-xs">
 		Edu System <b>Version</b> 2.0 
 	</div>
 	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"></a>.</strong> All rights reserved.
 </footer>
</div><!-- ./wrapper -->
<div id="loadbargood" class="loadbargood hidden"></div>

<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
	$(function () {
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  bootbox.confirm("Apakah email anda sudah benar?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
			  }else{
				// alert('canceled');
			  }
			});
			return false;
		});
			 
    });
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		bootbox.alert("Berhasil. Data pendaftaran anda telah ditemukan. Silahkan cek email anda.");
	  }
	  else if(respon.status == 'NOT FOUND'){
            bootbox.alert('Gagal. Email belum terdaftar.');
      }
	  else{
		bootbox.alert(msg);
	  }
	};
    </script>
</body>
</html>
