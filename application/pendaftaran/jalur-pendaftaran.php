<?php 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");

$pos = new model_psb();
$array = $pos->getJalurPendaftaranByDate();
$jalur = $array[1];

$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];

$propinsi = $pos->getPropinsi();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">
  <!-- end no cache headers -->
  <title>Pendaftaran Online</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../dist/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../dist/ionic/css/ionicons.min.css"> 
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../dist/css/custom.css">
  <link rel="shortcut icon" href="../../dist/img/favicon.ico" />
  <link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-green-light layout-top-nav">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header skin-purple"> 
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="http://www.alkamalblitar.com" class="navbar-brand">
             <span class="logo-mini"> 
              <b>PONDOK PESANTREN TERPADU AL KAMAL BLITAR</b>
             </span>
			</a>
          </div>
        

		 <div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  
			</a> 
		   </li>
		  </ul>
		 </div><!--  /.<div class="navbar-custom-menu">-->
		</div><!-- ./container -->
	  </nav>
	</header>
  <?php if(!empty($jalur)){ ?>
	<!-- main content -->
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Formulir Pendaftaran
		  </h1>
		</section>
	  
		<section class="content">
			<form method="post" id="target" action="formulir-online_process.php" enctype="multipart/form-data">
				<!-- Identitas siswa -->
				<div class="box box-success">
					<div class="box-header with-border">
					  <h3 class="box-title">Pilih jalur pendaftaran</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
				  
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-8">
							<div class="form-group"> <label class="col-sm-3  control-label-l">Jalur pendaftaran</label>
								<div class="col-sm-7">
									<select class="form-control" id="jalur_daftar" name="txtjalur" required>
										<option value=""> Pilih jalur </option>
										<?php
										foreach ($jalur as $opt) {
										echo "<option value='".$opt['jalur_id']."'> ".$opt['jalur']." </option>";
										}
										?>
									</select>
									<input type="hidden" name="method" class="" value="select_jalur">
										
								</div>
							</div>
							
						</div><!-- /.col -->
						
						
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				
					<div class="box-footer">
					  
					</div>
				  
				</div><!-- /identitas siswa-->
			
				
				<div class="box box-widget">
				  <div class="box-footer">
                    <button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Lanjutkan </button>
                    <span id="infoproses"></span>
				  </div><!-- /.box-footer -->
				</div><!-- /.box -->
			</form>
		</section>
	  </div>
	</div>
   <?php } else {?>
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Maaf, belum ada jadwal pendaftaran.
		  </h1>
		</section>
	  </div>
	</div>
   <?php }?>
  </div>
	
<div class="scroll-top-wrapper "><!-- back to top button-->
  <span class="scroll-top-inner">
    <i class="fa fa-2x fa-arrow-circle-up"></i>
  </span>
</div> <!-- end back to top button-->
 <footer class="main-footer"> 
 	<div class="pull-right hidden-xs">
 		Edu System <b>Version</b> 2.0 
 	</div>
 	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"></a>.</strong> All rights reserved.
 </footer>
</div><!-- ./wrapper -->
<div id="loadbargood" class="loadbargood hidden"></div>

<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
    $(function () {
		
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  $("#btnsave").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Anda akan melanjutkan pada form pengisian formulir?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
		location.href=respon.link;
	};

	
    </script>
</body>
</html>
