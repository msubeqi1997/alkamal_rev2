<?php 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");

$pos = new model_psb();
$array = $pos->getJalurPendaftaranByDate();
$jalur = $array[1];

$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];

$propinsi = $pos->getPropinsi();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">
  <!-- end no cache headers -->
  <title>Pendaftaran Online</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../dist/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../dist/ionic/css/ionicons.min.css"> 
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../dist/css/custom.css">
  <link rel="shortcut icon" href="../../dist/img/favicon.ico" />
  <link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-green-light layout-top-nav">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header skin-purple"> 
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="http://www.alkamalblitar.com" class="navbar-brand">
             <span class="logo-mini"> 
              <b>PONDOK PESANTREN TERPADU AL KAMAL BLITAR</b>
             </span>
			</a>
          </div>
        

		 <div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  
			</a> 
		   </li>
		  </ul>
		 </div><!--  /.<div class="navbar-custom-menu">-->
		</div><!-- ./container -->
	  </nav>
	</header>
  <?php if(!empty($jalur)){ ?>
	<!-- main content -->
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Formulir Pendaftaran
		  </h1>
		</section>
	  
		<section class="content">
			<form method="post" id="target" action="formulir-online_process.php" enctype="multipart/form-data">
				<!-- Identitas siswa -->
				<div class="box box-success">
					<div class="box-header with-border">
					  <h3 class="box-title">Identitas santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
				  
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-8">
							<div class="form-group"> <label class="col-sm-3  control-label-l">Jalur pendaftaran</label>
								<div class="col-sm-7">
									<select class="form-control" id="jalur_daftar" name="txtjalur" required>
										<option value=""> Pilih jalur </option>
										<?php
										foreach ($jalur as $opt) {
										echo "<option value='".$opt['jalur_id']."'> ".$opt['jalur']." </option>";
										}
										?>
									</select>
									<input type="hidden" id="pendaftaran" name="pendaftaran" value="">
									<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
									<input type="hidden" name="method" class="" value="save_item">
										
								</div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Apakah MDK? </label>
								<div class="col-sm-9" id="tipe"> <cite>Pilih jalur terlebih dahulu.</cite></div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Sekolah tujuan <span class="text-danger">*</span></label>
							  <div class="col-sm-9" id="tujuan"> 
								<cite>Pilih jalur terlebih dahulu.</cite>
							  </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label"> Jenjang pendidikan</label>
								<div class="col-sm-9" id="jenjang"> </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label">No Induk Santri<span class="text-danger">*</span></label>
								<div class="col-sm-9"><input type="text" class="form-control " id="no_induk" name="no_induk" value="" placeholder="Diisi jika alumni pesantren (MDK)"> </div>
							</div>
							<div class="form-group"> <label class="col-sm-3  control-label">Nama lengkap<span class="text-danger">*</span></label>
								<div class="col-sm-9"><input type="text" class="form-control " id="nama_lengkap" name="nama_lengkap" value="" placeholder="Isikan nama lengkap" required=""> </div>
							</div>
							<div class="form-group">
							  <label for="email" class="control-label-l col-sm-3">Email<span class="text-danger">*</span></label>
							  <div class="col-sm-9">
								<input type="email" class="form-control" name="email" id="email" placeholder="Email" required="">
							  </div>
							</div>
							<div class="form-group">
							  <label for="j-kelamin" class="control-label-l col-sm-3">Jenis kelamin<span class="text-danger"> *</span></label>
							  <div class="col-sm-9">
								<div class="radio">
								  <label><input type="radio" id="kelamin" name="kelamin" value="1" required> Laki-laki</label>
								  <label><input type="radio" id="kelamin" name="kelamin" value="0" required> Perempuan</label>
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label for="tempatlahir" class="control-label-l col-sm-3">Tempat & Tanggal lahir <span class="text-danger"> *</span></label>
							  <div class="col-sm-5">
								<input type="text" class="form-control letters" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" required>
							  </div>
							  <div class="col-sm-4">
								<div class="input-group date" id="datepicker">
								  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir" id="tanggal_lahir" placeholder="dd-mm-yyyy"   readonly required>
								  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label for="saudara" class="control-label-l col-sm-3">Jumlah saudara </label>
							  <div class="col-sm-3">
								<input type="text" class="form-control" name="saudara" id="saudara" placeholder="Saudara" onkeypress="return isNumber(event)">
							  </div>
							  <div class="col-sm-6">
								<label for="anakke" class="control-label-l col-sm-4">Anak ke </label>
								<div class="col-sm-3">
								  <input type="text" class="form-control" name="anak_ke" id="anak_ke" onkeypress="return isNumber(event)">
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label for="keb-khusus" class="control-label-l col-sm-3">Riwayat penyakit?</label>
							  <div class="col-sm-9">
								<input type="text" class="form-control" name="abk" id="abk" placeholder="Riwayat Penyakit">
								<span class="help-block">Kosongkan jika tidak memiliki riwayat penyakit</span>
							  </div>
							</div>
							<div class="form-group">
							  <label for="propinsi" class="control-label-l col-sm-3">Propinsi <span class="text-danger"> *</span></label>
							  <div class="col-sm-9">
								<select class="form-control" id="propinsi" name="propinsi" required>
									<option value=""> Pilih propinsi </option>
									<?php
									foreach ($propinsi[1] as $opt) {
									echo "<option value='".$opt['id']."'> ".$opt['name']." </option>";
									}
									?>
								</select>
							  </div>
							</div>
							<div class="form-group">
							  <label for="kabupaten" class="control-label-l col-sm-3">Kabupaten <span class="text-danger"> *</span></label>
							  <div class="col-sm-9">
								<select class="form-control" id="kabupaten" name="kabupaten" required>
									<option value=""> Pilih propinsi dahulu </option>
									
								</select>
							  </div>
							</div>
							<div class="form-group">
							  <label for="alamat" class="control-label-l col-sm-3">Alamat tempat tinggal <span class="text-danger"> *</span></label>
							  <div class="col-sm-9">
								<input type="text" class="form-control" name="alamat" id="alamat" placeholder="RT RW / Nama jalan nomor rumah, kecamatan, kelurahan" required>
							  </div>
							</div>
							<div class="form-group">
							  <label for="no_hp" class="control-label-l col-sm-3">Handphone yang aktif</label>
							  <div class="col-sm-9">
								<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)">
							  </div>
							</div>
							<div class="form-group">
							  <label for="kode_pos" class="control-label-l col-sm-3">Kode POS </label>
							  <div class="col-sm-9">
								<input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode POS" onkeypress="return isNumber(event)">
							  </div>
							</div>
						</div><!-- /.col -->
						<div class="col-md-4">
							<div class="hero-widget well well-md box-profile">
								<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_siswa" alt="foto santri 3x4">
								<label for="upload_siswa">Foto santri <small>(wajib upload foto)</small></label>
								<input type="file" name="photo_siswa" id="upload_siswa" accept="image/*" required />
							</div><!-- /.box -->
							<div class="hero-widget well well-md box-profile">
								<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_wali" alt="foto wali 3x4">
								<label for="upload_siswa">Foto wali santri<small>(wajib upload foto)</small></label>
								<input type="file" name="photo_wali" id="upload_wali" accept="image/*" required />
							</div><!-- /.box -->
						</div><!-- /.col -->
						
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				
					<div class="box-footer">
					  <span class="text-danger">*</span> ) harus diisi
					</div>
				  
				</div><!-- /identitas siswa-->
			
				<!-- orang tua / wali santri -->
				<div class="box box-default">
					<div class="box-header with-border">
					  <h3 class="box-title">Orang Tua / Wali Santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body">
					  <div class="row">
						<div class="col-md-6">
							<label><dt>1. Data ayah </dt></label>
							<div class="form-group">
							  <label for="nama_ayah">Nama <span class="text-danger"> *</span></label>
							  <input type="text" class="form-control" id="nama_ayah" name="nama_ayah" placeholder="Nama ayah" required>
							</div>
							<!--
							<div class="form-group">
								<div class="row">
								  <div class="col-sm-7">
									<label for="tempat_lahir_ayah" >Tempat lahir </label>
									<input type="text" class="form-control" name="tempat_lahir_ayah" id="tempat_lahir_ayah" placeholder="Tempat Lahir" required>
								  </div>
								  <div class="col-sm-5">
									<label for="tanggal_lahir_ayah" >Tanggal lahir </label>
									<div class="input-group date" id="datepicker">
									  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir_ayah" id="tanggal_lahir_ayah" placeholder="dd-mm-yyyy"   readonly required>
									  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								  </div>
								</div>
							</div>
							-->
							<div class="form-group">
							  <label for="pendidikan_ayah">Pendidikan</label>
							  <select class="form-control" id="pendidikan_ayah" name="pendidikan_ayah" >
								<option value=""> Tingkat pendidikan </option>
								<?php
								foreach ($pendidikan as $opt) {
								echo "<option value='".$opt['id']."'> ".$opt['pendidikan']." </option>";
								}
								?>
							  </select>
							</div>
							<div class="form-group">
							  <label for="pekerjaan_ayah">Pekerjaan </label>
							  <input type="text" class="form-control" id="pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Pekerjaan ayah">
							</div>
							<div class="form-group">
							  <label for="hp_ayah">Handphone yang aktif </label>
							  <input type="text" class="form-control" id="hp_ayah" name="hp_ayah" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)">
							</div>
							
						</div><!-- /.col -->
						<div class="col-md-6">
							<label><dt>2. Data ibu</dt></label>
							<div class="form-group">
							  <label for="nama_ibu">Nama <span class="text-danger"> *</span></label>
							  <input type="text" class="form-control" id="nama_ibu" name="nama_ibu" placeholder="Nama ibu" required>
							</div>
							<!--
							<div class="form-group">
								<div class="row">
								  <div class="col-sm-7">
									<label for="tempat_lahir_ibu" >Tempat lahir </label>
									<input type="text" class="form-control" name="tempat_lahir_ibu" id="tempat_lahir_ibu" placeholder="Tempat Lahir" required>
								  </div>
								  <div class="col-sm-5">
									<label for="tanggal_lahir_ibu" >Tanggal lahir </label>
									<div class="input-group date" id="datepicker">
									  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu" placeholder="dd-mm-yyyy"   readonly required>
									  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								  </div>
								</div>
							</div>
							-->
							<div class="form-group">
							  <label for="pendidikan_ibu">Pendidikan</label>
							  <select class="form-control" id="pendidikan_ibu" name="pendidikan_ibu" >
								<option value=""> Tingkat pendidikan </option>
								<?php
								foreach ($pendidikan as $opt) {
								echo "<option value='".$opt['id']."'> ".$opt['pendidikan']." </option>";
								}
								?>
							  </select>
							</div>
							<div class="form-group">
							  <label for="pekerjaan_ibu">Pekerjaan </label>
							  <input type="text" class="form-control" id="pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Pekerjaan ibu">
							</div>
							<div class="form-group">
							  <label for="hp_ibu">Handphone yang aktif </label>
							  <input type="text" class="form-control" id="hp_ibu" name="hp_ibu" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)">
							</div>
						</div><!-- /.col -->
						
						<div class="col-md-12">
							<label><b>3. Data wali </b>(diisi jika wali bukan orang tua kandung)</label>
							<div class="row">
							<div class="col-sm-6">
							
							<div class="form-group">
							  <label for="nama_wali">Nama wali</label>
							  <input type="text" class="form-control" id="nama_wali" name="nama_wali" placeholder="Nama wali">
							</div>
							<!--
							<div class="form-group">
								<div class="row">
								  <div class="col-sm-7">
									<label for="tempat_lahir_wali" >Tempat lahir </label>
									<input type="text" class="form-control" name="tempat_lahir_wali" id="tempat_lahir_wali" placeholder="Tempat Lahir" required>
								  </div>
								  <div class="col-sm-5">
									<label for="tanggal_lahir_wali" >Tanggal lahir </label>
									<div class="input-group date" id="datepicker">
									  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir_wali" id="tanggal_lahir_wali" placeholder="dd-mm-yyyy"   readonly required>
									  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								  </div>
								</div>
							</div>
							-->
							<div class="form-group">
							  <label for="alamat_wali">Alamat </label>
							  <input type="text" class="form-control" id="alamat_wali" name="alamat_wali" placeholder="Alamat wali">
							</div>
							
							</div>
							<div class="col-sm-6">
							
							<div class="form-group">
							  <label for="pekerjaan_wali">Pekerjaan </label>
							  <input type="text" class="form-control" id="pekerjaan_wali" name="pekerjaan_wali" placeholder="Pekerjaan wali">
							</div>
							<div class="form-group">
							  <label for="telp_wali">Nomor Handphone</label>
							  <input type="text" class="form-control" id="telp_wali" name="telp_wali" placeholder="Isikan nomor telepon rumah/Handphone" onkeypress="return isNumber(event)">
							</div>
							<div class="form-group">
							  <label for="hubungan_wali">Hubungan keluarga</label>
							  <input type="text" class="form-control" id="hubungan_wali" name="hubungan_wali" placeholder="Hubungan keluarga dengan siswa">
							</div>
							
							</div>
						</div><!-- /.col -->
						</div><!-- /.col -->
						
												
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
					
				</div><!-- /.box -->
				
				<!-- riwayat pendidikan -->
				<div class="box box-default">
					<div class="box-header with-border">
					  <h3 class="box-title">Riwayat pendidikan</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body" id="riwayat">
					
					  <div class="row fieldwrapper">
						<div class="col-md-6">
							<div class="form-group">
							  <label for="riwayat">Tingkat pendidikan</label>
							  <input type="text" class="form-control" id="riwayat" name="riwayat[]" placeholder="ex: SD/MI, Mts/SMP, Madin">
							</div>
						</div><!-- /.col -->
						<div class="col-md-3">
							<div class="form-group">
							  <label for="tahun-lulus">Lulus tahun</label>
							  <input type="text" class="form-control" id="tahun-lulus" name="lulus[]" placeholder="ex: 2019">
							</div>
						</div><!-- /.col -->
						<div class="col-md-1">
							<div class="form-group">
							  <label for="control">Control</label>
							  <input type="button" value="+" class="add_pend btn btn-success form-control col-md-4" id="add" />
							</div>
						</div><!-- /.col -->
					  </div><!-- /.row -->
					  
					</div><!-- /.box-body -->
					
				</div><!-- /.box -->
				
				<!-- Kelengkapan -->
				<div id="form-kelengkapan">
				<div class="box box-default">
					<div class="box-header with-border">
					  <h3 class="box-title">Kelengkapan Pendaftaran</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body">
					  <div class="row form-horizontal">
						<div class="col-md-8" id="list-kel">
						  							
						</div><!-- /.col -->
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				</div>
				</div><!-- /.box -->
				
				<div class="box box-widget">
				  <div class="box-footer">
                    <button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Kirim & Cetak Formulir </button>
                    <span id="infoproses"></span>
				  </div><!-- /.box-footer -->
				</div><!-- /.box -->
			</form>
		</section>
	  </div>
	</div>
   <?php } else {?>
	<div class="content-wrapper">
	  <div class="container">
		<section class="content-header">
		  <h1>
			Maaf, belum ada jadwal pendaftaran.
		  </h1>
		</section>
	  </div>
	</div>
   <?php }?>
  </div>
	
<div class="scroll-top-wrapper "><!-- back to top button-->
  <span class="scroll-top-inner">
    <i class="fa fa-2x fa-arrow-circle-up"></i>
  </span>
</div> <!-- end back to top button-->
 <footer class="main-footer"> 
 	<div class="pull-right hidden-xs">
 		Edu System <b>Version</b> 2.0 
 	</div>
 	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"></a>.</strong> All rights reserved.
 </footer>
</div><!-- ./wrapper -->
<div id="loadbargood" class="loadbargood hidden"></div>

<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
var popup = window.open("about:blank", "new_window_123", "height=150,width=150");
 setTimeout( function() {
    if(!popup || popup.outerHeight === 0) {
        alert("Untuk kelancaran pencetakan formulir pendaftaran matikan Pop up Blocker.");
    }else{
		popup.close();
	}	
}, 25);

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#riwayat"); //Fields wrapper
    var add_button      = $(".add_pend"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row fieldwrapper">'
						+'<div class="col-md-6">'
							+'<div class="form-group">'
							  +'<label for="riwayat">Tingkat pendidikan</label>'
							  +'<input type="text" class="form-control" id="riwayat" name="riwayat[]" placeholder="ex: SD/MI, Mts/SMP, Madin">'
							+'</div>'
						+'</div>'
						+'<div class="col-md-3">'
							+'<div class="form-group">'
							  +'<label for="tahun-lulus">Lulus tahun</label>'
							  +'<input type="text" class="form-control" id="tahun-lulus" name="lulus[]" placeholder="ex: 2019">'
							+'</div>'
						+'</div>'
						+'<div class="col-md-1">'
							+'<div class="form-group">'
							  +'<label for="control">&nbsp;</label>'
							  +'<input type="button" value="-" class="btn btn-danger form-control remove_pend" />'
							+'</div>'
						+'</div>'
					  +'</div>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_pend", function(e){ //user click on remove text
        e.preventDefault(); $(this).getParent(3).remove(); x--;
    })
	
	jQuery.fn.getParent = function(num) {
		var last = this[0];
		for (var i = 0; i < num; i++) {
			last = last.parentNode;
		}
		return jQuery(last);
	};

});

function closepopup()
   {
      if(false == my_window.closed)
      {
         my_window.close ();
      }
      else
      {
         alert('Window already closed!');
      }
   }
   

	var fileTag = document.getElementById("upload_siswa"),
	preview = document.getElementById("preview_siswa");

	fileTag.addEventListener("change", function() {
	  changeImage(this,preview);
	});
	
	var fileTagWali = document.getElementById("upload_wali"),
	previewWali = document.getElementById("preview_wali");

	fileTagWali.addEventListener("change", function() {
	  changeImage(this,previewWali);
	});
	function changeImage(input,output) {
	  var reader;
	  
	  if (input.files && input.files[0]) {
		if(input.files[0].size > 120000){
		  alert("File Terlalu besar. Maksimal 100 KB!");
		  input.value = "";
		}else{
		  reader = new FileReader();
		  reader.onload = function(e) {
			output.setAttribute('src', e.target.result);
		  }
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	}	
	
    $(function () {
		
		$("#form-kelengkapan").hide();
		$("#no_induk").hide();
		
		$('#jalur_daftar').on('change', function() {
			$('#tipe').html('');
			$('#tujuan').html('');
			$('#list-kel').html('');
			if(this.value == '' || this.value == null){$("#form-kelengkapan").hide();}	else{$("#form-kelengkapan").show();}
			var id_item = this.value;
			var value = {
				id_item: id_item,
				method : "get_form"
			};
			$.ajax(
			{
				url : "formulir-online_process.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					$('#list-kel').append(hasil.kelengkapan);
					
					if(id_item == '' || id_item == null){
						$('#tipe, #tujuan').append('<cite>Pilih jalur terlebih dahulu.</cite>'); 
					}else{ 
						$('#tipe').append(hasil.jenis);
						$('#tujuan').append(hasil.tujuan);
						if(hasil.mdk =='ya'){
							$("#no_induk").show();
							$("#nama_lengkap").val('');
							$("#nama_lengkap").prop("readonly", true);
						}else{
							$("#no_induk").hide();
							$("#nama_lengkap").prop("readonly", false);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
			get_jenjang();
		});
		
		$("div#tujuan").on("change", "select[name='sekolah']",function() {
			get_jenjang();
		});
		
		$("input[name='kelamin']").on("change", function() {
			get_jenjang();
		});
		
		$('#propinsi').on('change', function() {
			var value = {
				propinsi_id: this.value,
				method : "get_kabupaten"
			};
			$.ajax({
				url : "formulir-online_process.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil[1].length>0){
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih kabupaten </option>");
						$.each(hasil[1], function (key, val) {
						  $('#kabupaten').append('<option value="'+val.id+'">'+val.name+'</option>');
						})
					}else{
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih propinsi dahulu </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$('#tanggal_lahir, #tanggal_lahir_ayah, #tanggal_lahir_ibu, #tanggal_lahir_wali').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  $("#btnsave").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	//searching nama by no induk
	$(document).on("blur","#no_induk",function(){
		$("#nama_lengkap").val('');
		var nomor = $(this).val();
		var value = {
			nomor:nomor,
			method : "get_name"
		};
		$.ajax(
		{
			url : "formulir-online_process.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.status == 'exist'){
					$("#nama_lengkap").val(data.nama);
				}else{
					bootbox.alert('No induk tidak terdaftar');
					$("#no_induk").val('');
					$("#nama_lengkap").val('');
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				
			}
		});
	});

	$(document).ready(function(){
		$(".letters").keypress(function(event){
			var inputValue = event.which;
            // allow letters and whitespaces only.
            if(!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0) && inputValue != 8 ) { 
                event.preventDefault(); 
            }
            console.log(inputValue);
		});
	});

	function get_jenjang(){
		$('#jenjang').html('');
			
		var kelamin = $("input[name='kelamin']:checked").val();
		var sekolah = $("select[name='sekolah']").find('option:selected').val();
		var value = {
			kelamin: kelamin,
			sekolah: sekolah,
			method : "get_jenjang"
		};
		$.ajax(
		{
			url : "formulir-online_process.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var hasil = jQuery.parseJSON(data);
				$('#jenjang').append(hasil.jenjang);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		window.open('formulir-do-pdf.php?token='+respon.token, '_blank');
		bootbox.alert("Berhasil. Data pendaftaran anda telah disimpan.");
		setTimeout(function(){
		  location.href='terima-kasih.php';
		},0);
	  }
	  else if(respon.status == 'EXIST'){
		bootbox.alert('Gagal. Email yang anda gunakan sudah terdaftar. Harap gunakan email pribadi anda.');
	  }
	  else{
		bootbox.alert(msg);
	  }
	};

	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	
    </script>
</body>
</html>
