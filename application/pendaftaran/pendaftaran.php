<?php
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
$pos = new model_psb();
$pendaftar = $pos->countAllPendaftar();
$jumlah = $pendaftar[1];
$tahun = $pos->activeTapelPSB();
$tapel = $tahun[1]['thn_ajaran'];
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="no-cache">
  <meta http-equiv="Expires" content="-1">
  <meta http-equiv="Cache-Control" content="no-cache">
  <!-- end no cache headers -->
  <title>Pendaftaran Online</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../dist/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../dist/ionic/css/ionicons.min.css"> 
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../dist/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="../../dist/css/sweetalert.css">
  <link rel="stylesheet" href="../../dist/css/custom.css">
  <link rel="shortcut icon" href="../../dist/img/favicon.ico" />
  <link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-green-light layout-top-nav">
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header skin-green-light"> 
      <nav class="navbar navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="http://www.alkamal.com" class="navbar-brand">
             <span class="logo-mini"> 
              <b>AL KAMAL</b>
             </span>
			</a>
          </div>
       

		 <div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
		   <li class="dropdown user user-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  
			</a> 
		   </li>
		  </ul>
		 </div><!--  /.<div class="navbar-custom-menu">-->
		</div><!-- ./container -->
	  </nav>
	</header>

	<!-- main content -->
	<div class="content-wrapper">
	
		<section class="content-header  text-center">
		  <div class="logo">
			  <img src="../../image/Logo-Al-kamal.png" width="150px" alt="logo" />
		  </div>
		  <br/>
		  <br/>
		  <h1>
			SELAMAT DATANG!

		  </h1>
		  <h2>DI PENERIMAAN SANTRI BARU
			<br/>
			PONDOK PESANTREN TERPADU AL-KAMAL BLITAR
			<br/>
			TAHUN PELAJARAN  <?php echo $tapel;?>
		  </h2>
		  <h1>
			Jumlah pendaftar : <?php echo $jumlah;?> santri
		  </h1>
		  
		  <div class="pendaftar-tombol" style="padding-top:65px">
			<a href="http://alkamalblitar.com/tata-cara-pendaftaran-dan-panduan-pengisian-formulir-online/" class="btn btn-info btn-flat margin" target="_blank"><i class="fa fa-info-circle"></i> Tata Cara Pendaftaran</a>
			<a href="jalur-pendaftaran.php" class="btn bg-maroon btn-flat margin"><i class="fa fa-edit"></i> Daftar Sekarang</a>
			<a href="formulir-print.php" class="btn bg-purple btn-flat margin"><i class="fa fa-print"></i> Cetak Data</a>
		  </div>
		</section>
	  
	</div>
  </div>
	

<div class="scroll-top-wrapper "><!-- back to top button-->
  <span class="scroll-top-inner">
    <i class="fa fa-2x fa-arrow-circle-up"></i>
  </span>
</div> <!-- end back to top button-->
 <footer class="main-footer"> 
 	<div class="pull-right hidden-xs">
 		Edu System <b>Version</b> 2.0 
 	</div>
 	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"></a>.</strong> All rights reserved.
 </footer>
</div><!-- ./wrapper -->
<div id="loadbargood" class="loadbargood hidden"></div>

<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../../plugins/fastclick/fastclick.min.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="../../plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/myfunction.js" type="text/javascript"></script>
<script src="../../dist/js/sweetalert.min.js" type="text/javascript"></script>
<script src="../../dist/js/hotkey.js"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
var popup = window.open("about:blank", "new_window_123", "height=150,width=150");
 setTimeout( function() {
    if(!popup || popup.outerHeight === 0) {
        alert("Untuk kelancaran pencetakan formulir pendaftaran matikan Pop up Blocker.");
    }else{
		popup.close();
	}	
}, 25);
</script>
<style type="text/css">
            .content-wrapper {
                background:
				url('../../image/foto-pondok.png') 0 0 no-repeat;
                
            }
        </style>
</body>
</html>
