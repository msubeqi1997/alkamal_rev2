<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdaftarsantri'){
		$kelas = $_POST['kelas'];
		$tanggal = $_POST['tanggal'];
		$array = $pos->getAbsenSiswa($kelas,display_to_sql($tanggal));
		$kelas = $pos->getDetailKelas($kelas);
		$data = $array[1];
		
		$datax = array('data' => $data,'kelas' => $kelas[1]['kelas']);
		echo json_encode($datax);
	}
	
	if($method == 'save_absen_santri')
	{
		$kelas = $_POST['kelas'];
		$tanggal = $_POST['tanggal'];
		$siswa = isset($_POST['siswa'])?$_POST['siswa']:'';
		$absensi = isset($_POST['absensi'])?$_POST['absensi']:'';;
		$attributes = array('id_siswa', 'absen', 'tanggal', 'kelas');
		
		$data = array();
		if(!empty($siswa)){
		  $delete = $pos -> deleteAbsenSantri(display_to_sql($tanggal),$kelas);
		  foreach($siswa as $k => $v){
			$data[$k] = array();
			
			$data[$k]['siswa'] = $v;
			$data[$k]['tanggal'] = display_to_sql($tanggal);
			$data[$k]['absen'] = $absensi[$k];
			$data[$k]['kelas'] = $kelas;
			
		  }
		}
		
		$simpan = $pos -> saveAbsenSiswa($data);
		$result['result'] = $simpan[0];
		$result['msg'] = $simpan[1];
		echo json_encode($result);
	}
	
	if($method == 'getdaftartutor'){
		$tanggal = $_POST['tanggal'];
		$array = $pos->getAbsenTutor(display_to_sql($tanggal));
		$data = $array[1];
		
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_absen_tutor')
	{
		$tanggal = $_POST['tanggal'];
		$pegawai = isset($_POST['pegawai'])?$_POST['pegawai']:'';
		$absensi = isset($_POST['absensi'])?$_POST['absensi']:'';;
		
		$data = array();
		if(!empty($pegawai)){
		  $delete = $pos -> deleteAbsenTutor(display_to_sql($tanggal));
		  foreach($pegawai as $k => $v){
			$data[$k] = array();
			$data[$k]['pegawai'] = $v;
			$data[$k]['tanggal'] = display_to_sql($tanggal);
			$data[$k]['absen'] = $absensi[$k];
		  }
		}
		
		$simpan = $pos -> saveAbsenTutor($data);
		$result['result'] = $simpan[0];
		$result['msg'] = $simpan[1];
		echo json_encode($result);
	}
	
	if($method == 'reportabsensantri'){
		$kelas = $_POST['kelas'];
		$first = display_to_sql($_POST['firstdate']);
		$last = display_to_sql($_POST['lastdate']);
		$nama_kelas = $pos->getDetailKelas($kelas);
		$result = $pos->getReportAbsenSiswa($kelas,$first,$last);
		$array = $pos->generateDate($first,$last);
		$data = $array[1];
		$column = [];
		foreach($result[1] as $key=>$val){
			$id=base64_encode($val['uuid']).'|'.$val['nis'];
			$column[$id][$val['nama_lengkap']][$val['tanggal']] = $val['absen']; 
		}
		
		$datax = array('data' => $data,'kelas' => $nama_kelas[1]['kelas'],'absen' => $column);
		echo json_encode($datax);
	}
	
	if($method == 'reportabsentutor'){
		$first = display_to_sql($_POST['firstdate']);
		$last = display_to_sql($_POST['lastdate']);
		$array = $pos->generateDate($first,$last);
		$data = $array[1];
		$result = $pos->getReportAbsenTutor($first,$last);
		$column = [];
		foreach($result[1] as $key=>$val){
			$column[$val['nip']][$val['nama_lengkap']][$val['jadwal_hari']][$val['tanggal']] = $val['absen']; 
		}
		
		$datax = array('data' => $data,'absen' => $column);
		echo json_encode($datax);
	}
} else {
	exit('No direct access allowed.');
}