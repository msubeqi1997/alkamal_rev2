<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdaftarnilaisantri'){
		$kelas = $_POST['kelas'];
		$tanggal = $_POST['tanggal'];
		$subsubject = $_POST['subsubjek'];
		$jenis = $_POST['jenis'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$mapel = $pos->getDetailSubMapel($subsubject);
		$array = $pos->getSiswaRombel($kelas,$term);
		$kelas = $pos->getDetailKelas($kelas);
		$fetch_jenis = $pos->getDetailJenisNilai($jenis);
		$data = $array[1];
		
		$datax = array('data' => $data,'kelas' => $kelas[1]['kelas'],'sub_subjek' => $mapel[1]['mapel'],'jenis' => $fetch_jenis[1]['jenis_nilai']);
		echo json_encode($datax);
	}
	
	if($method == 'get_subjek')
	{
		$kelas=$_POST['kelas'];
		$data = $pos->getSubjekbyKelas($kelas);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'get_sub_subjek')
	{
		$subsubjek=$_POST['subsubjek'];
		$data = $pos->getSubSubjek($subsubjek);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'save_nilai_santri')
	{
		$kelas = $_POST['kelas'];
		$tanggal = display_to_sql($_POST['tanggal']);
		$subsubjek = $_POST['subsubjek'];
		$jenis = $_POST['jenis'];
		$nama = $_POST['nama'];
		$master_nilai = $_POST['idnilai'];
		$crud = $_POST['crud'];
		$siswa = isset($_POST['siswa'])?$_POST['siswa']:'';
		$nilai = isset($_POST['nilai'])?$_POST['nilai']:'';
		$user = $_SESSION['sess_id'];
		
		if($crud == 'N'){
			$data = array();
			if(!empty($siswa)){
			$cek_nilai = $pos -> cekNamaNilai($nama,$subsubjek,$jenis,$kelas);
				if($cek_nilai[1]['nilai'] == NULL){
				  $simpan = $pos -> simpanMasterNilai($nama,$subsubjek,$jenis,$kelas,$tanggal,$user);
				  if($simpan[0] == true){
					foreach($siswa as $k => $v){
					  $data[$k] = array();
					
					  $data[$k]['siswa'] = $v;
					  $data[$k]['mapel'] = $subsubjek;
					  $data[$k]['nilai'] = $nilai[$k];
					  $data[$k]['master'] = $simpan[1];
					
					}
					$array = $pos -> saveNilaiSiswa($data);
					$result['result'] = $array[0];
					$result['msg'] = $array[1];
				  }
				  else{
					$result['result'] = false;
					$result['msg'] = $simpan[1];
				  }
				  
				}else{
					$result['result'] = false;
					$result['msg'] = 'Nilai sudah ada';
				}
			}else
			{
				$result['result'] = false;
				$result['msg'] = 'Siswa kosong';
			}
		} else{
			$delete_nilai = $pos -> deleteDetailNilaiSiswa($master_nilai);
			$data = array();
			foreach($siswa as $k => $v){
			  $data[$k] = array();
			
			  $data[$k]['siswa'] = $v;
			  $data[$k]['mapel'] = $subsubjek;
			  $data[$k]['nilai'] = $nilai[$k];
			  $data[$k]['master'] = $master_nilai;
			
			}
			$array = $pos -> saveNilaiSiswa($data);
			$result['result'] = $array[0];
			$result['msg'] = $array[1];
		}
		echo json_encode($result);
	}
	
	if($method == 'getdaftartutor'){
		$tanggal = $_POST['tanggal'];
		$array = $pos->getAbsenTutor(display_to_sql($tanggal));
		$data = $array[1];
		
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'rekapnilaisantri'){
		$kelas = $_POST['kelas'];
		$nama_kelas = $pos->getDetailKelas($kelas);
		$result = $pos->getmapelrekap($kelas);
		
		$column = [];
		foreach($result[1] as $key=>$val){
			$column[$val['nama_mapel']][$val['sub_mapel']][$val['kode']][$val['id_master_nilai']] = $val['nama_nilai']; 
		}
		
		$datax = array('header' => $column,'kelas' => $nama_kelas[1]['kelas']);
		echo json_encode($datax);
	}
	
	if($method == 'rekapsubnilai'){
		$kelas = $_POST['kelas'];
		$subsubjek = $_POST['subsubjek'];
		$nama_kelas = $pos->getDetailKelas($kelas);
		$header = $pos->getheaderrekapsubmapel($kelas,$subsubjek);
		$result = $pos->getrekapsubmapel($kelas,$subsubjek);
		
		$column = [];
		foreach($header[1] as $key=>$val){
			$column[$val['mapel']][$val['jenis_nilai']][$val['id_master_nilai']] = $val['nama_nilai']; 
		}
		
		$nilai = [];
		foreach($result[1] as $key=>$val){
			$id=base64_encode($val['uuid']).'|'.$val['nis'];
			$nilai[$id][$val['nama_lengkap']][$val['id_master_nilai']] = $val['nilai']; 
		}
		
		$datax = array('header' => $column, 'nilai' => $nilai, 'kelas' => $nama_kelas[1]['kelas']);
		echo json_encode($datax);
	}
	
	if($method == 'raportsiswa'){
		$kelas = $_POST['kelas'];
		$id_siswa = base64_decode($_POST['siswa']);
		$nama_kelas = $pos->getDetailKelas($kelas);
		$akademik = $pos->activeTapel();
		$siswa = $pos->getDetailSantri($kelas,$id_siswa);
		$nilai = $pos->getNilaiRaportSiswa($kelas,$id_siswa);
		
		$datax = array('nilai' => $nilai[1], 'tahun' => $akademik[1]['thn_ajaran'],'semester' => $akademik[1]['semester'], 'kelas' => $nama_kelas[1]['kelas'],'nis' => $siswa[1]['nis'],'nama' => $siswa[1]['nama_lengkap']);
		echo json_encode($datax);
	}
	
	if($method == 'get_detail_nilai')
	{
		$kelas = $_POST['kelas'];
		$subsubjek = $_POST['subsubjek'];
		$jenis = $_POST['jenis'];
		$mapel = $pos->getDetailSubMapel($subsubjek);
		$detkelas = $pos->getDetailKelas($kelas);
		$fetch_jenis = $pos->getDetailJenisNilai($jenis);
		
		$datax = array('kelas' => $detkelas[1]['kelas'],'sub_subjek' => $mapel[1]['mapel'],'jenis' => $fetch_jenis[1]['jenis_nilai']);
		echo json_encode($datax);
	}
	
	if($method == 'get_list_nilai')
	{
		$kelas = $_POST['kelas'];
		$subsubjek = $_POST['subsubjek'];
		$jenis = $_POST['jenis'];
		$array = $pos->getListNilai($kelas,$subsubjek,$jenis);
		$list = $array[1];
		
		$data = [];
		$i=0;
		foreach ($list as $row) {
			
			$button = '<button  type="button" id_data="'.$row['id_master_nilai'].'"  title="Edit nilai" class="btn btn-sm btn-primary btnedit"  id="btnedit'.$row['id_master_nilai'].'"  ><i class="fa fa-edit"></i></button>
						<button  type="button" id_data="'.$row['id_master_nilai'].'"  title="Hapus nilai" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$row['id_master_nilai'].'"  ><i class="fa fa-trash"></i></button>';
					   
			$data[$i]['urutan'] =$i+1;
			$data[$i]['nama_nilai'] =$row['nama_nilai'];
			$data[$i]['tanggal'] =display_to_report($row['tanggal']);
			$data[$i]['edit'] =$button;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'delete_nilai')
	{
		$id_data=$_POST['id_data'];
		$data = $pos->deleteNilaiSiswa($id_data);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'getdetailedit'){
		$id_nilai = $_POST['nilai'];
		
		$array = $pos->getDetailNamaNilai($id_nilai);
		$detail = $array[1];
		$list = $pos->getNilaiSiswaEdit($detail['id_kelas'],$detail['id']);
		
		$datax = array('detail' => $detail, 'list' => $list[1]);
		echo json_encode($datax);
	}
} else {
	exit('No direct access allowed.');
}