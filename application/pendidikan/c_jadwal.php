<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	if($method == 'getjadwalkelas'){
		$kelas = $_POST['kelas'];
		
		$html ='';
		$hari = $pos->hariJadwal();
		foreach($hari[1] as $day){
		  $jam = $pos->jamJadwal($day['hari_ke']);
		  $html .='<div class="col-sm-2">
					<div class="alert alert-info text-center">
					  <strong>'.$day['hari'].'</strong>
					</div>';
		  foreach($jam[1] as $row){
			$jam = $pos->jadwalMapel($row['id'],$kelas);
			$tutor = array(); $subject = array();
			foreach($jam[1] as $v){
			  $tutor[] = $v['tutor'];
			  $subject[]=$v['subject'];
			}
			
			if(!empty($jam[1])){$edit='';}
			else{
			  $edit = '<a class="editjadwal" data-harike="'.$day['hari_ke'].'" data-hari="'.$day['hari'].'" data-kelas="'.$kelas.'" data-model="'.$row['id'].'">Edit</a> |'; 
			}
			
			$html .='<div class="panel panel-info">
					  <div class="panel-heading">
						<div class="row">
						  <div class="col-xs-3"><div class="badge bg-light-blue ">'.$row['jamke'].'</div></div>
						  <div class="col-xs-9 text-right"><small>'.$row['jam_mulai'].' - '.$row['jam_selesai'].'</small></div>
						</div>
					  </div>
					  <div class="panel-body">
						'.implode('',array_unique($subject)).'
						<div class="jampel-aksi">
						  '.$edit.'
						  <a href="#" class="deleteJadwal" data-kelas="'.$kelas.'" data-model="'.$row['id'].'">Hapus</a>
						</div>
						<div class="jampel-guru text-warning">
						'.implode('<br/>',$tutor).'
						</div>
					  </div>
					</div>';
		  }
		  $html .='</div>';
		}
		
		$result['html'] = $html;
		echo json_encode($result);
	}
	
	if($method == 'getdetailjadwal')
	{
		$harike = $_POST['harike'];
		$kelas = $_POST['kelas'];
		$model = $_POST['model'];
		$tutor = $pos->getTutor();
		$mapel = $pos->getMapel();
		$jam = $pos->jamKosong($harike,$kelas);
				
		$result['crud'] = 'N';
		$result['kelas'] = $kelas;
		$result['tutor'] = $tutor[1];
		$result['mapel'] = $mapel[1];
		$result['jam'] = $jam[1];
		
		echo json_encode($result);
	}
	
	if($method == 'save_jadwal')
	{
		$iditem = $_POST['id_item'];
		$kelas = $_POST['kelas'];
		$jam = $_POST['jam'];
		$mapel = $_POST['mapel'];
		$tutor = $_POST['tutor'];
		$crud=$_POST['crud'];
		$msg = true;
		$data = '';
		if($_POST['crud'] == 'N')
		{
		  foreach($jam as $k => $d){
			foreach($tutor as $t){
			  $cek = $pos->cekJadwal($kelas,$d,$t);
			  if(!empty($cek[1])){
			    $msg = false;
			  }else{
				$simpan = $pos->simpanJadwal($mapel,$kelas,$d,$t);
				$msg = $kelas.','.$d.','.$t;
				$data = $simpan[1];
			  }
			}
			
		  }
		}
		else
		{
			
		}
		
		$result['msg'] = $msg;
		$result['data'] = $data;
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'deletejadwal')
	{
		$kelas = $_POST['kelas'];
		$model = $_POST['model'];
		
		$delete = $pos->deleteJadwal($kelas,$model);
				
		$result['result'] = $delete[0];
		$result['msg'] = $delete[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}