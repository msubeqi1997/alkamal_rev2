<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	
	if($method == 'getdata'){
		$array = $pos->getKelas();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['id_kelas'].'"  title="Tombol edit kelas" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['id_kelas'].'"  ><i class="fa fa-edit"></i></button>';
			$print = '<button  type="submit" id_item="'.$key['id_kelas'].'"  title="Download anggota kamar" class="btn btn-sm btn-primary btndownload "  id="btndownload'.$key['id_kelas'].'"  ><i class="fa fa-download"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                          '.$print.'
						</div>
                       </td>';
			$data[$i]['satuan'] = ucwords($key['jenjang']);
			$data[$i]['button'] = $button;
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'get_pegawai')
	{
		$array = $pos->getPegawaiAktif();
		$html='';
		$result = array();
		if($array[0] == true)
		{
			foreach ($array[1] as $key) {
				
				$btn =  '<button  type="submit" pegawai_id="'.$key['id_pegawai'].'" pegawai="'.$key['nip'].' - '.$key['nama_lengkap'].'" title="Pilih wali" class="btn btn-success btn-sm insertpegawai"  id="btnmerk'.$key['id_pegawai'].'" ><i class="fa fa-arrow-circle-o-right"></i></button>';
				$html .= '	<tr>
				<td class="tdstrike">'.$key['urutan'].'</td>
				<td class="tdstrike">'.$key['nip'].' - '.$key['nama_lengkap'].'</td>
				<td class="tdstrike" style="min-width:80px">'.$btn.'</td>
				</tr>';
				$result['data'] = $html;
			}
			
		}
		echo json_encode($result);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$wali = $_POST['wali'];
		$satuan = $_POST['satuan'];
		$tingkat = $_POST['tingkat'];
		$order = $order_jenjang[$satuan];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveKelas($name,$wali,$satuan,$tingkat,$order);
		}
		else
		{
			$array = $pos->updateKelas($iditem,$name,$wali,$satuan,$tingkat,$order);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailKelas($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'get_kelas_rombel')
	{
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		$result = $pos->getKelasRombel($jenjang,$tingkat);
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'nonrombel'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$satuan;
		$jenis;
		if($jenjang == 'mdk'){
			$satuan = '3'; $jenis = 'mdk';
		}else if ($jenjang == 'wustho'){
			$satuan = '3'; $jenis = 'tidak';
		}else{ $satuan = '2'; $jenis = 'tidak';}
		$array = $pos->getNonRombel($jenjang,$tingkat,$satuan,$jenis,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Masukkan rombel" class="btn btn-sm btn-success checkin "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-right"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'rombel'){
		$kelas = $_POST['kelas'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$array = $pos->getSiswaRombel($kelas,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Keluarkan dari rombel" class="btn btn-sm btn-danger checkout "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-left"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	
	if($method == 'checkin'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->saveCheckin($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Kelas belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'checkout'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->deleteRombelSiswa($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Kelas belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'get_wali_kelas'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$array = $pos->getWaliKelas($jenjang,$tingkat,$tapel);
		$data = $array[1];
		$i=0;
		
		foreach ($data as $key) {
			
			$wali ='<input type="hidden" name="idkelas[]" value="'.$key['id_kelas'].'">
					<input type="hidden" name="txtidwali[]" id="txtidwali'.$key['id_kelas'].'" value="'.$key['id_guru'].'">
					<input type="text" name="wali[]" id="wali'.$key['id_kelas'].'" value="'.$key['wali'].'" size="40" disabled>';
			$search = '<button  type="button" id="btnopenpegawai'.$key['id_kelas'].'" kelas="'.$key['id_kelas'].'" title="pilih wali" class="btn btn-primary btnopenpegawai"  ><i class="fa fa-search"></i></button>';
			$data[$i]['wali'] = $wali;
			$data[$i]['search'] = $search;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'simpan_wali_kelas')
	{
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$value = array();
		$kelas = $_POST['idkelas'];
		$pegawai = $_POST['txtidwali'];
		foreach($kelas as $k => $v){
			$guru = (empty($pegawai[$k]))?'NULL':$pegawai[$k];
			$value[] ="(".$tapel.",".$guru.",".$v.")";
		}
		
		$delete = $pos->deleteWaliKelas($tapel);
		if($delete[0] == true){
			$query = $pos->insertWaliKelas($value);
			$result['result'] = $query[0];
			$result['msg'] = $query[1];
		}else{
			$result['result'] = false;
			$result['msg'] = $delete[1];
		}
		
		echo json_encode($result);
	}
	
	if($method == 'getjadwalkelas'){
		$kelas = $_POST['kelas'];
		
		$html ='';
		$hari = $pos->hariJadwal();
		foreach($hari[1] as $day){
		  $jam = $pos->jamJadwal($day['hari_ke']);
		  $html .='<div class="col-sm-2">
					<div class="alert alert-info text-center">
					  <strong>'.$day['hari'].'</strong>
					</div>';
		  foreach($jam[1] as $row){
			$jam = $pos->jadwalMapel($row['id'],$kelas);
			$tutor = array(); $subject = array();
			foreach($jam[1] as $v){
			  $tutor[] = $v['tutor'];
			  $subject[]=$v['subject'];
			}
			
			if(!empty($jam[1])){$edit='';}
			else{
			  $edit = '<a class="editjadwal" data-harike="'.$day['hari_ke'].'" data-hari="'.$day['hari'].'" data-kelas="'.$kelas.'" data-model="'.$row['id'].'">Edit</a> |'; 
			}
			
			$html .='<div class="panel panel-info">
					  <div class="panel-heading">
						<div class="row">
						  <div class="col-xs-3"><div class="badge bg-light-blue ">'.$row['jamke'].'</div></div>
						  <div class="col-xs-9 text-right"><small>'.$row['jam_mulai'].' - '.$row['jam_selesai'].'</small></div>
						</div>
					  </div>
					  <div class="panel-body">
						'.implode('',array_unique($subject)).'
						<div class="jampel-aksi">
						  '.$edit.'
						  <a href="#" class="deleteJadwal" data-kelas="'.$kelas.'" data-model="'.$row['id'].'">Hapus</a>
						</div>
						<div class="jampel-guru text-warning">
						'.implode('<br/>',$tutor).'
						</div>
					  </div>
					</div>';
		  }
		  $html .='</div>';
		}
		
		$result['html'] = $html;
		echo json_encode($result);
	}
	if($method == 'getdetailjadwal')
	{
		$harike = $_POST['harike'];
		$kelas = $_POST['kelas'];
		$model = $_POST['model'];
		$tutor = $pos->getTutor();
		$mapel = $pos->getMapel();
		$jam = $pos->jamKosong($harike,$kelas);
				
		$result['crud'] = 'N';
		$result['kelas'] = $kelas;
		$result['tutor'] = $tutor[1];
		$result['mapel'] = $mapel[1];
		$result['jam'] = $jam[1];
		
		echo json_encode($result);
	}
	if($method == 'save_jadwal')
	{
		$iditem = $_POST['id_item'];
		$kelas = $_POST['kelas'];
		$jam = $_POST['jam'];
		$mapel = $_POST['mapel'];
		$tutor = $_POST['tutor'];
		$crud=$_POST['crud'];
		$msg = true;
		$data = '';
		if($_POST['crud'] == 'N')
		{
		  foreach($jam as $k => $d){
			foreach($tutor as $t){
			  $cek = $pos->cekJadwal($kelas,$d,$t);
			  if(!empty($cek[1])){
			    $msg = false;
			  }else{
				$simpan = $pos->simpanJadwal($mapel,$kelas,$d,$t);
				$msg = $kelas.','.$d.','.$t;
				$data = $simpan[1];
			  }
			}
			
		  }
		}
		else
		{
			
		}
		
		$result['msg'] = $msg;
		$result['data'] = $data;
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'deletejadwal')
	{
		$kelas = $_POST['kelas'];
		$model = $_POST['model'];
		
		$delete = $pos->deleteJadwal($kelas,$model);
				
		$result['result'] = $delete[0];
		$result['msg'] = $delete[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}