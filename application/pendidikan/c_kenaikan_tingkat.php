<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	
	if($method == 'get_kelas_rombel')
	{
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		$result = $pos->getKelasRombel($jenjang,$tingkat);
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_kelas_tujuan')
	{
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		
		$result = $pos->getKelasRombel($jenjang,$tingkat);
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'kenaikan'){
		$kelas = $_POST['kelas'];
		$tingkat = $_POST['tingkat'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$array = $pos->getSiswaRombel($kelas,$term);
		$data = $array[1];
		$i=0;
		if($tingkat == '3'){
			$grade = '<select name="naik[]"><option value="lulus">Lulus</option><option value="tinggal">Tinggal</option></select>';
		}else{
			$grade = '<select name="naik[]"><option value="naik">Naik</option><option value="tinggal">Tinggal</option></select>';
		}
		foreach ($data as $key) {
			
			$naik = '<input type="hidden" name="idsiswa[]" value="'.base64_encode($key['uuid']).'"> '.$grade.'';
			
			$data[$i]['action'] = $naik;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'proses_kenaikan')
	{
		$jenjang = $_POST['txtjenjang'];
		$tingkat_awal = $_POST['txttingkat'];
		$kelas_awal = $_POST['txtkelas'];
		$kelas_tujuan = isset($_POST['acak'])?'NULL':$_POST['txtkelastujuan'];
		
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$lulus = array();
		$value = array();
		$siswa = $_POST['idsiswa'];
		$naik = $_POST['naik'];
		
		if($tingkat_awal == '3'){
		  foreach($siswa as $k => $v){
			if($naik[$k]=='lulus'){
			  $lulus[] = "(".$tapel.",".base64_decode($v).",".$jenjang.",".$tingkat_awal.",".$kelas_awal.")";  
			}elseif($naik[$k]=='tinggal'){
			  $tingkat_tujuan = $tingkat_awal;
			  $value[] ="(".$tapel.",".base64_decode($v).",".$kelas_awal.",".$tingkat_awal.",".$kelas_tujuan.",".$tingkat_tujuan.")";  
			}
		  }
		  
		  if(!empty($lulus)){
		    $delete = $pos->deleteTblLulus($tapel,$tingkat_awal,$kelas_awal);
			$query = $pos->insertSiswaLulus($value);
		  }
		  
		  if(!empty($value)){
			$delete = $pos->deleteTransisi($tapel,$tingkat_awal,$kelas_awal);
		    $query = $pos->insertTransisi($value);
		  }
		  
		}else{
		  foreach($siswa as $k => $v){
			$tingkat_tujuan = ($naik[$k]=='naik')?$tingkat_awal+1:$tingkat_awal;
			$value[] ="(".$tapel.",".base64_decode($v).",".$kelas_awal.",".$tingkat_awal.",".$kelas_tujuan.",".$tingkat_tujuan.")";
		  }
		
		  $delete = $pos->deleteTransisi($tapel,$tingkat_awal,$kelas_awal);
		  if($delete[0] == true){
			$query = $pos->insertTransisi($value);
			$result['result'] = $query[0];
			$result['msg'] = $query[1];
		  }else{
			$result['result'] = false;
			$result['msg'] = $delete[1];
		  }
		}
		
		echo json_encode($result);
	}
	
	if($method == 'siswa_transisi'){
		$kelas = $_POST['kelas'];
		$tingkat = $_POST['tingkat'];
		
		$array = $pos->getSiswaTransisi($tingkat,$kelas);
		$data = $array[1];
		$i=0;
		
		foreach ($data as $key) {
			$kelas_baru=(empty($key['kelas']))?'':'<button type="button" class="btn btn-flat bg-green btn-xs deletekelas" title="Hapus kelas tujuan" data="'.$key['id_transisi'].'" id="delete'.$key['id_transisi'].'"> &nbsp;&nbsp;'.$key['kelas'].' <i class="glyphicon glyphicon-remove"></i></button>';
			$data[$i]['urutan'] = $i+1;
			$data[$i]['kelas_baru'] = $kelas_baru;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'delete_tujuan'){
		$id_transisi = $_POST['id'];
		$array = $pos->deleteKelasTujuan($id_transisi);
		
		$result['result'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'proses_daful')
	{
		$jenjang = $_POST['txtjenjang'];
		$tingkat_awal = $_POST['txttingkat'];
		$kelas_awal = $_POST['txtkelas'];
		$kelas_tujuan = isset($_POST['acak'])?'NULL':$_POST['txtkelastujuan'];
		
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$value = array();
		$siswa = $_POST['idsiswa'];
		$naik = $_POST['naik'];
		foreach($siswa as $k => $v){
		  if($naik[$k]=='lulus'){
			  
		  }else{
			$tingkat_tujuan = ($naik[$k]=='naik')?$tingkat_awal+1:$tingkat_awal;
			$value[] ="(".$tapel.",".base64_decode($v).",".$kelas_awal.",".$tingkat_awal.",".$kelas_tujuan.",".$tingkat_tujuan.")";
		  }
		}
		
		//$delete = $pos->deleteTransisi($tapel,$tingkat_awal,$kelas_awal);
		if($delete[0] == true){
			$query = $pos->insertTransisi($value);
			$result['result'] = $query[0];
			$result['msg'] = $query[1];
		}else{
			$result['result'] = false;
			$result['msg'] = $delete[1];
		}
		
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}