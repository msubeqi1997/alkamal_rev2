<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_pend();
	$method=$_POST['method'];
	
	if($method == 'get_list_km'){
		$array = $pos->getKelompokMapel();
		$data = [];
		$i=0;
		foreach ($array[1] as $key) {
			$button = '<button  type="button" id_item="'.$key['id_kelompok_mapel'].'"  title="Tombol edit kitab" class="btn btn-sm btn-primary btnedit "><i class="fa fa-edit"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                        </div>
                       </td>';
			$data[$i]['urutan'] = $key['urut'];
			$data[$i]['nama'] = $key['kelompok_mapel'];
			$data[$i]['alias'] = $key['alias'];
			$data[$i]['order'] = $key['urutan'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$alias = $_POST['alias'];
		$urutan = $_POST['urutan'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJenisKM($name,$alias,$urutan);
		}
		else
		{
			$array = $pos->updateJenisKM($iditem,$name,$alias,$urutan);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_km')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailKM($id);
		$result['data'] = $array[1];
		$result['msg'] = $array[0];
		echo json_encode($result);
	}
	
	if($method == 'save_mapel')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$alias = $_POST['alias'];
		$standar = $_POST['standar'];
		$cat = $_POST['cat'];
		$jenjang = $_POST['jenjang'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveMapel($name,$alias,$standar,$cat,$jenjang );
		}
		else
		{
			$array = $pos->updateMapel($iditem,$name,$alias,$standar,$cat,$jenjang);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_list_mapel'){
		$array = $pos->getListMapel();
		$data = [];
		$i=0;
		foreach ($array[1] as $key) {
			$button = '<button  type="button" id_item="'.$key['id_mapel'].'"  title="Tombol edit bab" class="btn btn-sm btn-primary btnedit "><i class="fa fa-edit"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                        </div>
                       </td>';
			$data[$i]['urutan'] = $key['urut'];
			$data[$i]['bab'] = $key['mapel'];
			$data[$i]['alias'] = $key['alias'];
			$data[$i]['jenjang'] = $key['jenjang'];
			$data[$i]['standar'] = $key['capaian'];
			$data[$i]['kitab'] = $key['kitab'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'get_detail_mapel')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailMapel($id);
		$result['data'] = $array[1];
		$result['msg'] = $array[0];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}