<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
require_once ("../model/model_pend.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$posd = new model_pend();
	$method=$_POST['method'];
	
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdata'){
		$array = $posd->getDataSantri();
		
		$data = $array['data'];
		$i=0;
		foreach ($data as $row) {
			if($row['tipe']=='nonsettle'){
				$button = '<button  type="submit" id_siswa="'.$row['jid'].'"  title="Tombol edit santri" class="btn btn-sm btn-primary btnedit "  btn-data="edit" id="btnedit'.$row['jid'].'"  ><i class="fa fa-edit"></i></button>';
			}else{$button =' ';}
			$data[$i]['urutan'] = $row['urutan'];
			$data[$i]['nis'] = $row['nis'];
			$data[$i]['nama'] = $row['nama_lengkap'];
			$data[$i]['alamat'] = strtolower($row['alamat_lengkap']);
			$data[$i]['jenjang'] = ucfirst($row['jenjang_pendidikan']);
			$data[$i]['tingkat'] = "Tingkat ".$row['tingkat_kelas'];
			$data[$i]['button'] = $button;
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'get_kabupaten'){
		$propinsi_id = $_POST['propinsi_id'];
		$array = $pos->getKabupaten($propinsi_id);
		$result['data'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'save_item')
	{
		//error_reporting(0);
		$unique = getId();
		$uuid = '';
		$jid = isset($_POST['idsiswa']) ? $_POST['idsiswa'] : '';
		$jenjang['nis'] = isset($_POST['no_induk']) ? $_POST['no_induk'] : '';
		$jenjang['jenjang_pendidikan'] = isset($_POST['jenjang']) ? $_POST['jenjang'] : '';
		$jenjang['tingkat_kelas'] = isset($_POST['tingkat_kelas']) ? $_POST['tingkat_kelas'] : '';
		$jenjang['status_kesiswaan'] = isset($_POST['status_kesiswaan']) ? $_POST['status_kesiswaan'] : '';
		
		$data['nama_lengkap'] = isset($_POST['nama_lengkap']) ? $_POST['nama_lengkap'] : '';
		$data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
		$data['kelamin'] = isset($_POST['kelamin']) ? $_POST['kelamin'] : '';
		$data['tempat_lahir'] = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
		$data['tanggal_lahir'] = isset($_POST['tanggal_lahir']) ? display_to_sql($_POST['tanggal_lahir']) : '';
		$data['saudara'] = isset($_POST['saudara']) ? $_POST['saudara'] : '';
		$data['anak_ke'] = isset($_POST['anak_ke']) ? $_POST['anak_ke'] : '';
		$data['abk'] = isset($_POST['abk']) ? $_POST['abk'] : '';
		$data['propinsi'] = isset($_POST['propinsi']) ? $_POST['propinsi'] : '';
		$data['kabupaten'] = isset($_POST['kabupaten']) ? $_POST['kabupaten'] : '';
		$data['alamat'] = isset($_POST['alamat']) ? $_POST['alamat'] : '';
		$data['no_hp'] = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
		$data['kode_pos'] = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : '';
		
		$data['ayah'] = isset($_POST['nama_ayah']) ? $_POST['nama_ayah'] : '';
		$data['pendidikan_ayah'] = isset($_POST['pendidikan_ayah']) ? $_POST['pendidikan_ayah'] : NULL;
		$data['pekerjaan_ayah'] = isset($_POST['pekerjaan_ayah']) ? $_POST['pekerjaan_ayah'] : '';
		$data['hp_ayah'] = isset($_POST['hp_ayah']) ? $_POST['hp_ayah'] : '';
		
		$data['ibu'] = isset($_POST['nama_ibu']) ? $_POST['nama_ibu'] : '';
		$data['pendidikan_ibu'] = isset($_POST['pendidikan_ibu']) ? $_POST['pendidikan_ibu'] : NULL;
		$data['pekerjaan_ibu'] = isset($_POST['pekerjaan_ibu']) ? $_POST['pekerjaan_ibu'] : '';
		$data['hp_ibu'] = isset($_POST['hp_ibu']) ? $_POST['hp_ibu'] : '';
		
		$data['wali'] = isset($_POST['nama_wali']) ? $_POST['nama_wali'] : '';
		$data['alamat_wali'] = isset($_POST['alamat_wali']) ? $_POST['alamat_wali'] : '';
		$data['pekerjaan_wali'] = isset($_POST['pekerjaan_wali']) ? $_POST['pekerjaan_wali'] : '';
		$data['telp_wali'] = isset($_POST['telp_wali']) ? $_POST['telp_wali'] : '';
		$data['hubungan_wali'] = isset($_POST['hubungan_wali']) ? $_POST['hubungan_wali'] : '';
		
		$data['input_at'] = date ('Y-m-d H:i:s');
		
		$crud = $_POST['inputcrud'];
		$tahun = date('Y');
		
		$folder_foto = '../../files/images_pendaftar/';
		$tmp_siswa = $_FILES['photo_siswa']['tmp_name'];
		$tipe_siswa = $_FILES['photo_siswa']['type'];
		$nama_siswa = $_FILES['photo_siswa']['name'];
		$ext_siswa = pathinfo($nama_siswa, PATHINFO_EXTENSION);
		
		$tmp_wali = $_FILES['photo_wali']['tmp_name'];
		$tipe_wali = $_FILES['photo_wali']['type'];
		$nama_wali = $_FILES['photo_wali']['name'];
		$ext_wali = pathinfo($nama_wali, PATHINFO_EXTENSION);
		
		if($_POST['inputcrud'] == 'N')
		{
			$uuid = $unique;
			$nama_photo_siswa = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_siswa;
			$nama_photo_wali = 'wali_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_wali;
			
			if (!empty($tmp_siswa)) { //jika ada gambar
				UploadCompress($nama_photo_siswa, $folder_foto, $tmp_siswa);
				$data['photo_siswa']=$nama_photo_siswa;
			}
			if (!empty($tmp_wali)) { //jika ada gambar
				UploadCompress($nama_photo_wali, $folder_foto, $tmp_wali);
				$data['photo_wali']=$nama_photo_wali;
			}
			
			$data['uuid'] = $uuid;
			$jenjang['uuid']=$uuid;
			
			$query = $posd->saveSantri($data);
			$insert_jenjang = $posd->insertJenjangSantri($jenjang);
		}
		else
		{
			$kode = $posd->getKodeSantri($jid);
			$uuid = $kode[1]['uuid'];
			$nama_photo_siswa = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_siswa;
			$nama_photo_wali = 'wali_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_wali;
			
			if (!empty($tmp_siswa)) { //jika ada gambar
				unlink($folder_foto.$kode[1]['photo_siswa']);
				UploadCompress($nama_photo_siswa, $folder_foto, $tmp_siswa);
				$data['photo_siswa']=$nama_photo_siswa;
			}
			if (!empty($tmp_wali)) { //jika ada gambar
		    	unlink($folder_foto.$kode[1]['photo_wali']);
				UploadCompress($nama_photo_wali, $folder_foto, $tmp_wali);
				$data['photo_wali']=$nama_photo_wali;
			}
			
			unset($data['input_at']);
			$query = $posd->updateSantri($uuid,$data);
			$del_pendidikan = $posd->deleteRiwayatPendidikan($uuid);
			$update_jenjang = $posd->updateJenjangSantri($jid,$jenjang);
		}
		
		if($query[0] == true){
			//riwayat pendidikan
			
			$lulus = $_POST['lulus'];
			foreach($_POST['riwayat'] as $key => $val){
				$riwayat['uuid'] = $uuid;
				$riwayat['penyelenggara'] = $val;
				$riwayat['tahun_selesai'] = $lulus[$key];
				$posd->saveRiwayatPendidikan($riwayat);
			}
		}
		
		$result['error'] = $query[1];
		$result['crud'] = $crud;
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
		}else{
			$result['status']='NO';
		}
		
		echo json_encode($result);
	}
	
	if($method == 'select_siswa'){
		$idc = $_POST['siswa_id'];
		$array = $pos->getDetailSiswa($idc);
		$array['data'] = $array[1];
		$array['result'] = $array[0];
		echo json_encode($array);
	}
	
	if($method == 'mutasi_siswa'){
		$jid = $_POST['token'];
		$data['tanggal_keluar'] = display_to_sql($_POST['tanggal']);
		$data['alasan_keluar'] = $_POST['alasan'];
		$data['pegawai'] = $_SESSION['sess_id'];
		$data['status_kesiswaan'] = 'keluar';
		$array = $pos->saveMutasiSiswa($jid, $data);
		
			
		$array['status'] = $array[0];
		$array['msg'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'update_nis'){
		$target = basename($_FILES['upload_update']['name']) ;
		move_uploaded_file($_FILES['upload_update']['tmp_name'], $target);
		chmod($_FILES['upload_update']['name'],0777);
	}
} else {
	exit('No direct access allowed.');
}