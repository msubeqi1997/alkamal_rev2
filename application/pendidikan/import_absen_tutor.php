<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_pend();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$first = display_to_sql($_POST['firstdate']);
	$last = display_to_sql($_POST['lastdate']);
	$result = $pos->getReportAbsenTutor($first,$last);
	$array = $pos->generateDate($first,$last);
	$coldate = array_reverse($array[1]);
	$column = [];
	foreach($result[1] as $key=>$val){
		$column[$val['nip']][$val['nama_lengkap']][$val['jadwal_hari']][$val['tanggal']] = $val['absen']; 
	}
		
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'ABSENSI TUTOR')
	->setCellValue('A5', 'NO')
	->setCellValue('B5', 'NIP')
	->setCellValue('C5', 'NAMA')
	;
	
	$num =1;
	$rows = 6;
	$col = 'D';
	
	foreach($coldate as $row){
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', $row['date'])
		;
		$col++;
	}
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($col.'5', 'Total Masuk')
		;
		$col++;
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', 'Total Tidak Masuk')
		;
	foreach($column as $keypeg=>$valpeg){
	  $spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$rows, $num)
		->setCellValue('B'.$rows, $keypeg)
	  ;
	  foreach($valpeg as $keyname => $valname){
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('C'.$rows, $keyname)
		;
		
		$present='D';
		$present_values=array();
		foreach($coldate as $val){
		  	
		  if(array_key_exists($val['hari'],$valname)){				
			if( isset($valname[$val['hari']][$val['date']]) == true ) {
				$content = $valname[$val['hari']][$val['date']];
			}else{
				$content = 'A';
			}
		  }else{
			  $content = 'x';
		  }
			$present_values[]=$content;
			$spreadsheet->setActiveSheetIndex(0)
			  ->setCellValue($present.$rows, $content)
			;
			$present++;
		}
			$counts = array_count_values($present_values);

			$spreadsheet->setActiveSheetIndex(0)
			  ->setCellValue($present.$rows, $counts['H'])
			;
			$present++;
			$spreadsheet->setActiveSheetIndex(0)
			  ->setCellValue($present.$rows, $counts['A'])
			;
	  }
	  $num++;$rows++;
	}
	// Miscellaneous glyphs, UTF-8
	
	
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}