<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_pend.php");
include '../../plugins/excel/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Protection;

$method=$_POST['method'];
if($method == 'import'){
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
		
	$pos = new model_pend();
	
	
	$sex = array('putra' => '1','putri' => '0');
	$arrjenjang = array('ula' => '2', 'wustho' => '3');
	$kelamin = array('1' => 'Putra','0' => 'Putri');
	$jk = array('1' => 'Laki-laki','0' => 'Perempuan');
	$jenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	$pendidikan = array(''=>'','1'=>'SD/Sederajat','2'=>'SMP/Sederajat','3'=>'SMA/Sederajat','4'=>'Diploma','5'=>'S1','6'=>'S2','7'=>'S3');
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$array = $pos->getDataSantri();
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'NOMOR')
	->setCellValue('B1', 'NO INDUK')
	->setCellValue('C1', 'NAMA')
	->setCellValue('D1', 'ALAMAT')
	->setCellValue('E1', 'JENJANG')
	->setCellValue('F1', 'TINGKAT')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=2; 
	foreach($data as $row) {
	  $spreadsheet->setActiveSheetIndex(0)
	  ->setCellValue('A'.$i, $row['urutan'])
	  ->setCellValue('B'.$i, $row['nis'])
	  ->setCellValue('C'.$i, $row['nama_lengkap'])
	  ->setCellValue('D'.$i, $row['alamat_lengkap'])
	  ->setCellValue('E'.$i, $row['jenjang_pendidikan'])
	  ->setCellValue('F'.$i, $row['tingkat_kelas'])
	  ;
	$i++; $num++;
	}
	
	
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('data');
	$spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
	$spreadsheet->getActiveSheet()->getStyle('C1:AD'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
	$spreadsheet->getActiveSheet()->getStyle('B2:B'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
} else{
	echo "Not allowed direct access.";
}