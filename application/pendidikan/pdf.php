<?php
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/dompdf/autoload.inc.php';
require_once("../../plugins/I18N/Arabic.php");
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();
error_reporting(E_STRICT);
$Arabic = new I18N_Arabic('Numbers'); 
$html ='
  <!DOCTYPE html>
  <html lang="id">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  </head>
  <body>
	<div class="kertas">
	  ';

$html .='
		<div class="kop-surat">
			
		</div>
		
		<table class="tablebordered" style="margin-top:20px;">
		  <thead>
			<tr class="tableheader">
				<th style="width:110px; font-weight:bold;" >التوقيع من الوالد</th>
				<th style="width:210px; font-weight:bold;" colspan="5">النتيجة بالأرقام والأحرف</th>
				<th style="width:170px; font-weight:bold;" colspan="3">اسماء الكـــــتب</th>
				<th style="width:150px; font-weight:bold;" colspan="2">مواد الــدروس</th>
				<th style="width:55px; font-weight:bold;">النمرة</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
				<td style="text-align:right;"></td>
				<td style="text-align:right; width:65px;" colspan="4">٠١</td>
				<td style="text-align:center;">..</td>
				<td style="text-align:right;" colspan="3">مصحف القرءان العظيم .1</td>
				<td style="text-align:right;" colspan="2">ترتيل القرآن.1</td>
				<td style="text-align:center;">٠١</td>
			</tr>
			<tr>
				<td style="text-align:right;"></td>
				<td style="text-align:right; width:65px;" colspan="4">٠١</td>
				<td style="text-align:right;">..</td>
				<td style="text-align:right;" colspan="3">مصحف القرءان العظيم .1</td>
				<td style="text-align:right;" colspan="2">ترتيل القرآن.1</td>
				<td style="text-align:center;">٠١</td>
			</tr>
			<tr>
				<td style="text-align:center; vertical-align: middle;" colspan="2" rowspan="6" >مراتب الطلبة </td>
				<td style="width:5px;" rowspan="6" >&nbsp; </td>
				<td style="text-align:center; vertical-align: middle;" colspan="3" rowspan="2">عدم الحضور</td>
				<td style="border-bottom:0px;" colspan="6"> &nbsp;</td>
			</tr>
			<tr>
				<td style="border:0px; text-align:center; vertical-align: middle;" rowspan="4">..</td>
				<td style=" border:0px;width:5px; vertical-align: middle;" rowspan="4" > = </td>
				<td style="border:0px; text-align:center;" rowspan="2">..</td>
				<td style="border:0px; text-align:center;" rowspan="2">مجموعة النتائج </td>
				<td style="border:0px; width:5px; vertical-align: middle;" rowspan="4" > = </td>
				<td style="border-top:0px; border-left:0px; border-bottom:0px; text-align:center; vertical-align: middle;" rowspan="4">النتيجة</td>
			</tr>
			<tr>
				<td style="text-align:center;" rowspan="2">بلاإذن(a)</td>
				<td style="text-align:center;" rowspan="2">لإذن (i)</td>
				<td style="text-align:center;" rowspan="2">لمرض (s)</td>
				
			</tr>
			<tr>
				<td style="border-left:0px; border-right:0px; border-bottom:0px; text-align:center;" rowspan="2">١٠</td>
				<td style="border-left:0px; border-right:0px; border-bottom:0px; text-align:center;" rowspan="2">مدد الدروس</td>
				
			</tr>
			<tr>
				<td style="text-align:center;" rowspan="2">1</td>
				<td style="text-align:center;" rowspan="2">2</td>
				<td style="text-align:center;" rowspan="2">4</td>
				
			</tr>
			<tr>
				<td style="border-top:0px; "colspan="6"> </td>
			</tr>
			<tr>
				<td style="text-align:right; border-right:0px; border-bottom:0px;" colspan="7"> ١٥</td>
				<td style="border-left:0px; border-right:0px; border-bottom:0px;">:</td>
				<td style="text-align:right; border-left:0px; border-bottom:0px;" colspan="4"> النتــــــــــــــــــيجة المعـــــــادلة </td>
			</tr>
			<tr>
				<td style="text-align:right; border-right:0px; border-bottom:0px; border-top:0px;" colspan="7"> تنتقل الى الفصل 2 (الثان فى طبة الأولى)</td>
				<td style="border-left:0px; border-bottom:0px; border-top:0px;border-right:0px;">:</td>
				<td style="text-align:right; border-top:0px; border-left:0px; border-bottom:0px;" colspan="4"> كليمة البيان  من ادارة المدرسة </td>
			</tr>
			<tr>
				<td style="text-align:right; border-right:0px;border-top:0px;" colspan="7"> تعلم بالجد...!!!!</td>
				<td style="border-left:0px; border-top:0px; border-right:0px;">:</td>
				<td style="text-align:right; border-left:0px; border-top:0px;" colspan="4"> كليمة الإرشادات من ولي الفصل </td>
			</tr>
			<tr>
				<td style="border:0px; "colspan="12"> &nbsp;</td>
			</tr>
			<tr>
				<td style="border:0px; text-align:right;" colspan="12"> تقريرا ببليتار، 7 ربيع الأخير 1440 من هجرة الرسول</td>
			</tr><tr>
				<td style="border:0px; text-align:right;" colspan="12"> وفقا بـ 15 دسمبير 2018 المسيحي</td>
			</tr>
			<tr>
				<td style="border:0px; "colspan="12" rowspan="3"> &nbsp;</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td style="border:0px; text-align:right;"colspan="12"> رئيس المدرسة الدينية المعهد الإسلامى العصرى</td>
			</tr>
			<tr>
				<td style="border:0px; text-align:center;"colspan="10"> </td>
				<td style="border:0px; text-align:center;"colspan="2"> " الكمال "</td>
			</tr>
		  </tbody>
		</table>
		
	</div>

  <style>
  @page {
    /* width = 21cm */
	size: 8.5in 12.99in;
    margin: 0.5cm 1cm 0.5cm;

  }
  @media print{
    table{
      width: 19cm;
    }

  }
  .kertas{
    width: 100%;
    margin: 0 auto;
    font-size: 11pt;
  }
  
  .kertas.ganti-halaman{
	page-break-before: always;
  }
		  
  .kop-surat img{
    width:100%;
  }
  table{
    width: 100%;
  }
  
  .tablebordered {border-collapse:collapse; }
  .tablebordered th { color:#000; vertical-align:middle;}
  .tablebordered td, .tablebordered th { padding:3px;border:1px solid #000; }
  table tr td{
    padding: 1px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr th{
    padding: 3px 0;
    vertical-align: top;
    text-align: center;
  }
  table tr td.label{
    font-weight: bold;
    width: 30%;
  }
  table tr td.label + td{
    width: 1%;
  }
  table tr td.label + td + td{
    width: 69%;
  }
  table.formulir{
    margin-bottom: 20px;
  }
  .judul-surat{
    text-align: center;
    position: relative;
	margin-top: 10px;
  }
  .nomor-surat{
    margin:0;
  }
  .judul-surat h1{
    border-bottom: 1px solid #000;
    font-size: 12pt;
    display: inline;
    padding: 0 5px;
    margin-bottom:0;
    margin-top: 5px;
    text-transform: uppercase;
    letter-spacing: 2px;
    line-height: 11pt;
  }
  
  .pengembalian-surat{
    position: absolute;
	right: 0px;
    padding-top: -120px;
	border: 1px solid #dd4b39;
  }
  h3{
    text-align: center;
    font-size: 11pt;
    margin:0 0 3px;
    line-height: 11pt;
	font-weight:reguler;
  }
  .bold {
	 font-weight: bold; 
  }
  .center {
	text-align:center;
  }
  .blank{
    margin-top:40px;
  }
  .lm-20{
    margin-left: 20px;
  }
  .lwidth{
	width:140px;
  }
  .swidth{
	width:12px;
  }
  .tgl-surat{
    text-align: left;
    width:30%;
	padding-left:513px;
  }
  .tanda-tangan{
    margin-bottom: 10px;
  }

  .tanda-tangan tr td{
    vertical-align: bottom;
    text-align: center;
    padding: 0;
  }
  .tanda-tangan tr:first-child td{
    vertical-align: top;
    height:  62px;
  }
  .garis-nama{
    border-bottom: 1px solid #000;
  }
  .catatan{
    font-style: italic;
    margin-top: 10px;
    font-size: 10pt;
  }
  table.col-2 tr td{
    width: 50%;
  }
  table.col-4 tr td{
    width: 25%;
  }
  p{
    margin:5px 0;
    text-align: justify;
  }
  </style>

  </body>
  </html>
  ';
/*
$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file='raport_intensif.pdf';
$dompdf->stream($nama_file);

*/echo $html;

