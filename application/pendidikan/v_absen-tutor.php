<?php 
$titlepage="Absen Tutor";
$idsmenu=51; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_pend.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
?>
<section class="content-header">
  <h1>
	ABSEN PENGAJAR
	<small>Madrasah Diniyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="download_jadwal.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Tanggal absen</label>
					<div class="col-sm-3">
					  <div class="input-group">
						<input type="text" class="form-control" id="txttanggal"  name="txttanggal" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						<div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
						</div>
					  </div>
					</div>
					<button type="button" title="Search surat" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Absen Tutor</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row">
				<form id="inputAbsen">
				<input type="hidden" id="hiddentanggal">
				
				<div class="table-responsive">
				  <table id="absenTutor" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px">#</th>
						<th>No Induk </th>
						<th>Nama lengkap </th>
						<th>Absen</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Simpan absensi" class="btn btn-success " id="btnsaveitem" ><i class="fa fa-save"></i> Simpan</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
		$(function () {
			var lastDate = new Date();
			lastDate.setDate(lastDate.getDate());//any date you want
			
			$('#txttanggal').datepicker({
				format: 'dd-mm-yyyy',
			});
			$("#txttanggal").datepicker("setDate", lastDate);
			
			//Datemask dd/mm/yyyy
			$("#txttanggal").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$(document).on("click","#btnfilter",function(){
			var tanggal = $('#txttanggal').val();
			if( tanggal == null || tanggal == ''){
				$.notify({
					message: "Tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txttanggal").focus();
				return;
			}
			
			$('#absenTutor tbody').empty();
			var tbody = document.getElementById("absenTutor").tBodies[0];
			var value = {
				tanggal:tanggal,
				method : "getdaftartutor"
			};
			$.ajax(
			{
				url : "c_absen.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					$('#hiddentanggal').val(tanggal);
					var no = 0;
					
					$.each(hasil.data, function (key, val) {
					  if(val.absen == "H"){var hadir = 'selected';}
					  if(val.absen == "A"){var alpha = 'selected';}
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var nip = row.insertCell(1);
					  var nama = row.insertCell(2);
					  var absen = row.insertCell(3);
					  urut.innerHTML = no+1;	
					  nip.innerHTML = val.nip;	
					  nama.innerHTML = val.nama_lengkap;
					  absen.innerHTML = '<input type="hidden" name="pegawai[]" value="'+val.id_pegawai+'">'+
										'<select class="form-control" id="optabsen" name="optabsen[]" >'+
										'<option value="A" '+alpha+'>Tidak masuk</option>'+
										'<option value="H" '+hadir+'>Masuk</option>'+
										'</select>';
					  				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var tanggal = $('#hiddentanggal').val();
			var pegawai = [];
			$("input[name='pegawai[]']").each(function () {
			  pegawai.push($(this).val());
			});

			var absensi = [];
			$("select[name='optabsen[]']").each(function () {
			  absensi.push($(this).val());
			});
			
			var value = {
				tanggal: tanggal,
				pegawai: pegawai,
				absensi: absensi,
				method : "save_absen_tutor"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_absen.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					$("#btnfilter").trigger("click");
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	</script>
</body>
</html>
