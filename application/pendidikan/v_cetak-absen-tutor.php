<?php 
$titlepage="Laporan Absen Pengajar";
$idsmenu=72; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_pend.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
?>
<section class="content-header">
  <h1>
	LAPORAN ABSEN PENGAJAR
	<small>Madrasah Diniyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="download_jadwal.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Tanggal</label>
					<div class="col-sm-3">
					  <div class="input-group">
						<input type="text" class="form-control" id="firstdate"  name="firstdate" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						<div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
						</div>
					  </div>
					</div>
					<label class="pull-left control-label"> s.d </label>
					<div class="col-sm-3">
					  <div class="input-group">
						<input type="text" class="form-control" id="lastdate"  name="lastdate" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						<div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
						</div>
					  </div>
					</div>
					<button type="button" title="Search" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
				  </div>	
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Absen Tutor</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="import_absen_tutor.php" >
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row">
				<input type="hidden" name="firstdate" id="hiddenfirstdate">
				<input type="hidden" name="lastdate" id="hiddenlastdate">
				
				<div class="table-responsive">
				  <table id="absenTutor" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader" id="headAbsen">
						
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="submit" title="Download absensi" class="btn btn-success " id="btndownload" ><i class="fa fa-download"></i> Download</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
		$(function () {
			$('#firstdate, #lastdate').datepicker({
				format: 'dd-mm-yyyy',
			});
			
			//Datemask dd/mm/yyyy
			$("#firstdate, #lastdate").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$(document).on("click","#btnfilter",function(){
			var firstdate = $('#firstdate').val();
			var lastdate = $('#lastdate').val();
			if( firstdate == null || firstdate == ''){
				$.notify({
					message: "Tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#firstdate").focus();
				return;
			}
			
			if( lastdate == null || lastdate == ''){
				$.notify({
					message: "Tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#lastdate").focus();
				return;
			}
			
			$('#absenTutor tbody').empty();
			var tbody = document.getElementById("absenTutor").tBodies[0];
			th = document.createElement('th');
			var value = {
				firstdate: firstdate,
				lastdate: lastdate,
				method : "reportabsentutor"
			};
			$.ajax(
			{
				url : "c_absen.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					
					var hasil = jQuery.parseJSON(data);
					$('#hiddenfirstdate').val(firstdate);
					$('#hiddenlastdate').val(lastdate);
					var no = 0;
					var th = tbody.insertRow(no);
					var urut = th.insertCell(0);
					var nip = th.insertCell(1);
					var nama = th.insertCell(2);
					urut.innerHTML = 'NO';	
					nip.innerHTML = 'NIP';	
					nama.innerHTML = 'Nama';
					$.each(hasil.data, function (key, val) {
					  var coldate = th.insertCell(3);
					  coldate.innerHTML = '<th>'+val.date+'</th>';
					  no++;
					})
					var number = 1;
					$.each(hasil.absen, function (key, val) {
					  var tb = tbody.insertRow(number);
					  var asc = tb.insertCell(0);
					  var uuid = tb.insertCell(1);
					  var name = tb.insertCell(2);
					  
					  asc.innerHTML = number;	
					  uuid.innerHTML = key;
					  $.each(val, function (k, v) {
						name.innerHTML = k;
					    
						$.each(hasil.data, function (i, tgl) {
						  var present = tb.insertCell(3);
							if( tgl.hari in v ) {
							   if(v[tgl.hari][tgl.date] === undefined){
								present.innerHTML = '<td>A</td>';   
							   }else{
								present.innerHTML = '<td>'+v[tgl.hari][tgl.date]+'</td>';
							   }
							}else{
							  present.innerHTML = '<td>x</td>';
							}
						})
						
					  });
					  
					  number++;
					})
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var tanggal = $('#hiddentanggal').val();
			var siswa = [];
			$("input[name='siswa[]']").each(function () {
			  siswa.push($(this).val());
			});

			var absensi = [];
			$("select[name='optabsen[]']").each(function () {
			  absensi.push($(this).val());
			});
			
			var value = {
				tanggal: tanggal,
				siswa: siswa,
				absensi: absensi,
				method : "save_absen_santri"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_absen.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					$("#btnfilter").trigger("click");
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	</script>
</body>
</html>
