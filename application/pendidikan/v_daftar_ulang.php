<?php 
$titlepage="Daftar Ulang";
$idsmenu=41; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_pend.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
$tahun = $pos->activeTapel();
$tapel = $tahun[1]['thn_ajaran_id'];
$smt = $tahun[1]['semester'];

?>
<section class="content-header">
  <h1>
	DAFTAR ULANG 
	<small>Tahun Ajaran <?php echo $tahun[1]['thn_ajaran'];?></small>
  </h1>
</section>
<section class="content">
	<?php
	if($smt == 'genap'){ ?>
	  <div class="callout callout-warning">
		<h4>Maaf!</h4>
		<p><b>Proses Daftar Ulang</b> hanya dapat dilakukan pada semester ganjil.</p>
	  </div>
	<?php	
	}else{ ?>
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Data Santri</h3>
			</div>
			<!--./ box header-->
			<div class="box-body no-padding">
			  <form method="post" id="target" action="c_kenaikan_tingkat.php">
				<input type="hidden" name="method" value="proses_daful">
				<div class="row" style="padding-left:25px; padding-right:25px;">
					<div class="col-md-3">
					  <div class="form-group">
						<label>Jenjang pendidikan</label>
						<input type="hidden" id="tempjenjang" value="">
						<select class="form-control" id="txtjenjang" name="txtjenjang" style="width: 100%;">
						  <option value="">Semua</option>
						  <option value="ula">Ula</option>
						  <option value="wustho">Wustho</option>
						  <option value="mdk">MDK</option>
						</select>
					  </div><!-- /.form-group -->
					</div><!-- /.col -->
					<div class="col-md-3">
					  <div class="form-group"> 
						<label>Tingkat</label>
						<input type="hidden" id="temptingkat" value="">
						<select class="form-control" id="txttingkat" name="txttingkat" style="width: 100%;">
						  <option value="">Pilih tingkat</option>
						  <option value="1">Tingkat 1</option>
						  <option value="2">Tingkat 2</option>
						  <option value="3">Tingkat 3</option>
						</select>
					  </div>
					</div><!-- /.col -->
					<div class="col-md-3">
					  <div class="form-group">
						<label>Kelas</label>
						<input type="hidden" id="tempkelas" value="">
						<select class="form-control" id="txtkelas" name="txtkelas" style="width: 100%;">
						  <option value="">Pilih tingkat dahulu</option>
						  
						</select>
					  </div><!-- /.form-group -->
					</div><!-- /.col -->
					<div class="col-md-3">
					  <div class="form-group">
						<label style="width: 100%;"> &nbsp;&nbsp; </label>
						<button type="button" id="btnsearch" class="btn bg-blue btn-flat">
						  <i class="fa fa-search"></i> Cari
						</button>
					  </div><!-- /.form-group -->
					</div><!-- /.col -->
					
				</div>
				<div class="row">
				  <div class="col-md-9"> 
					<div class="table-responsive" style="height: 986px; overflow-y: scroll; padding-left:5px;">
					  <table id="rombel" class="table  table-bordered table-hover ">
						<thead>
						  <tr class="tableheader">
							<th style="width:45px">#</th>
							<th>No Induk </th>
							<th>Nama lengkap </th>
							<th>Kelas Baru</th>
						  </tr>
						</thead>
						<tbody></tbody>
					  </table>
					</div>
				  </div>
				  <div class="col-md-3 " style="padding-right:25px;">
					<button type="submit" id="btnproses" class="btn bg-green btn-flat margin">
					  <i class="fa fa-repeat"></i> Daftar Ulang
					</button>
					<span id="infoproses"></span>
					
					<dl>
					  <dt>Daftar ulang santri</dt>
						<dd>Daftar ulang santri hanya untuk santri lama pada tingkat baru.</dd>
					  <dt>Pencarian</dt>
						<dd>Untuk pemrosesan daftar ulang santri harus diproses dahulu pada kenaikan tingkat pada tahun yang lalu.</dd>
						<dd>Menampilkan santri melalui tombol pencarian dapat berdasarkan per kelas tujuan atau pada semua tingkat dengan pilihan kelas dikosongkan.</dd>
					  <dt>Pemrosesan</dt>
						<dd>Setelah data dianggap benar pemrosesan dilakukan dengan klik tombol <b>Daftar ulang</b>.</dd>
					</dl>
				  </div>
				</div>
			  </form>			
			</div>
		</div><!-- /.box -->
	<?php
	}
	?>	
</section><!-- /.content -->


	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
<script language="javascript">
	$(function () {
	$('#txtjenjang, #txttingkat').on('change', function() {
	  
	  var jenjang = $('#txtjenjang').val();
	  var tingkat = $('#txttingkat').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
		var value = {
			jenjang: jenjang,
			tingkat: tingkat,
			method : "get_kelas_rombel"
		};
		$.ajax({
			url : "c_kenaikan_tingkat.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtkelas').html("");
					$('#txtkelas').html("<option value=''> Pilih kelas </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtkelas').append('<option value="'+val.id_kelas+'">'+val.kelas+'</option>');
					})
				}else{
					$('#txtkelas').html("");
					$('#txtkelas').html("<option value=''> Kelas tidak ditemukan </option>");
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
		$("#rombel").DataTable().destroy();
		$("#rombel tbody").html('');
	});
	
});

$('#btnsearch').on('click', function() {
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	var kelas = $('#txtkelas').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
	  $('#tempjenjang').val(jenjang);
	  $('#temptingkat').val(tingkat);
	  $('#tempkelas').val(kelas);
	  rombel(kelas,tingkat);
});

//searching pegawai
$(document).on("click",".deletekelas",function(){
	var jenjang = $('#tempjenjang').val();
	var tingkat = $('#temptingkat').val();
	var kelas = $('#tempkelas').val();
	var id = $(this).attr("data");
	bootbox.confirm("Hapus kelas tujuan?",
	function (result) {
	  if (result == true) {
		var value = {
		id : id,
		method : "delete_tujuan"
		};
		$.ajax(
		{
			url : "c_kenaikan_tingkat.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.result == true){
				  $.notify(
					{ message: "Berhasil di hapus!"},
					{ type: 'success', delay: 3000,}
				  );
				  rombel(kelas,tingkat);
				}else{
				  $.notify(
					{ message: "Gagal di proses!"},
					{ type: 'warning', delay: 3000,}
				  );
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				
			}
		});
	  }else{
		
	  }
	});	
});
	
function rombel(kelas,tingkat){
	$("#rombel").DataTable().destroy();
	var value = {
		kelas:kelas,
		tingkat:tingkat,
		method : "siswa_transisi"
	};
	$('#rombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_kenaikan_tingkat.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		{ "data": "kelas_baru" },
		
		]
	});
} 

	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#target").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin melanjutkan proses?",
			function (result) {
			  if (result == true) {
				$("#btnproses").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. proses sudah selesai.");
		$("#rombel tbody").html('');
		$("#btnproses").prop('disabled', false);
	  }
	  else{
		bootbox.alert(msg);
		$("#btnproses").prop('disabled', false);
	  }
	}
</script>
</body>
</html>
