<?php 
$titlepage="Input Nilai Madrasah Diniyah";
$idsmenu=52; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_pend.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
$kelas = $pos->getKelas();
//$cat = $pos->getCatMapel();
$jenis = $pos->getJenisNilai();

?>
<section class="content-header">
  <h1>
	INPUT NILAI
	<small>Madrasah Diniyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Kelas</label>
					<div class="col-sm-3">
						<select class="form-control" id="kelas" name="kelas" >
						  <option value="">Pilih Kelas</option>
						  <?php 
							foreach($kelas[1] as $row){
							  echo "<option value='".$row['id_kelas']."'>".$row['kelas']."</option>";
							}
						  ?>
						</select>
					</div>
					<label class="col-sm-2  control-label">Tanggal</label>
					<div class="col-sm-3">
					  
					  <div class="input-group">
						<input type="text" class="form-control" id="txttanggal"  name="txttanggal" value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						<div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
						</div>
					  </div>
					</div>
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Bab Mapel</label>
					<div class="col-sm-3">
						<select class="form-control" id="subsubjek" name="subsubjek" >
						  <option value="">Pilih Bab</option>
						  
						</select>
					</div>
					<label class="col-sm-2  control-label">Jenis nilai</label>
					<div class="col-sm-3">
						<select class="form-control" id="jenis" name="jenis" >
						  <option value="">Pilih Jenis nilai</option>
						  <?php 
							foreach($jenis[1] as $row){
							  echo "<option value='".$row['id_jenis']."'>".$row['jenis_nilai']."</option>";
							}
						  ?>
						</select>
					</div>
					
				  </div>
				  <div class="form-group">
					
					<div class="col-sm-5 text-right">
					  &nbsp;&nbsp;
					  <button type="button" title="Search nilai" class="btn btn-primary " data-type="search" id="btnsearch" ><i class="fa fa-search"></i> Cari Nilai</button>
					  &nbsp;&nbsp;
					  <button type="button" title="Input nilai" class="btn btn-primary " data-type="input" id="btnfilter" ><i class="fa fa-edit"></i> Input Nilai</button>
					  
					</div>				
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Input Nilai Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  
			  <div class="row">
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Kelas</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detkelas"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Nama Nilai</b>
					</div>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="txtname"  name="txtname" value="">
					</div>
				  </div>
				</div>
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Jenis Nilai</b>
					</div>
					<div class="col-sm-8">
						<b >: </b> <span id="detjenis"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Bab Mapel</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detsubsubjek"></span>
					</div>
				  </div>
				  
				</div>
			  </div>
			  <div class="row">
				<form id="inputAbsen">
				<input type="hidden" id="hiddenkelas">
				<input type="hidden" id="hiddentanggal">
				<input type="hidden" id="hiddensubsubjek">
				<input type="hidden" id="hiddenjenisnilai">
				<input type="hidden" id="hiddennamanilai">
				<input type="hidden" id="crud">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px">#</th>
						<th>No Induk </th>
						<th>Nama lengkap </th>
						<th>Nilai</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Simpan absensi" class="btn btn-success " id="btnsaveitem" ><i class="fa fa-save"></i> Simpan</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<div id="modalnilai" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Nilai Santri</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6 form-horizontal">
						  <div class="form-group"> 
							<div class="col-sm-4 control-label">
								<b>Kelas</b>
							</div>
							<div class="col-sm-8">
								<b class="control-label">: </b> <span id="vkelas"></span>
							</div>
						  </div>
						  <div class="form-group"> 
							<div class="col-sm-4 control-label">
								<b>Bab pelajaran</b>
							</div>
							<div class="col-sm-8">
								<b>: </b> <span id="vsubsubjek"></span>
							</div>
						  </div>
						</div>
						<div class="col-md-6 form-horizontal">
						  <div class="form-group"> 
							<div class="col-sm-4 control-label">
								<b>Jenis Nilai</b>
							</div>
							<div class="col-sm-8">
								<b >: </b> <span id="vjenis"></span>
							</div>
						  </div>
						</div>
					</div><!-- /.row -->
					<div class="row">
						<div class="box-body table-responsive no-padding" >
							<table id="table_list_nilai" class="table  table-bordered table-hover table-striped" >
								<thead>
									<tr class="tableheader">
										<th style="width:30px">#</th>
										<th>Tanggal</th>
										<th>Nama Nilai</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div><!-- /.row -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
	
	function integer(){
		$(document).on("blur",".decimal",function(){
			var angka=parseFloat($(this).val());
			if(isNaN(angka))
			{
				angka=0;
			}
			$(this).val(angka);		
		});

		$(".decimal").focus(function(e){
			if(e.which === 9){
				return false;
			}
			$(this).select();
		});
	}
		$(function () {
			
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
	
			$('#subjek').on('change', function() {
				var value = {
					subsubjek: this.value,
					method : "get_sub_subjek"
				};
				$.ajax({
					url : "c_input_nilai.php",
					type: "POST",
					data : value,
					success: function(data, textStatus, jqXHR)
					{
						var respons = jQuery.parseJSON(data);
						if(respons.data.length>0){
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Pilih sub subjek </option>");
							$.each(respons.data, function (key, val) {
							  $('#subsubjek').append('<option value="'+val.id+'">'+val.sub_mapel+'</option>');
							})
						}else{
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Sub subjek tidak ditemukan </option>");
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			});
			
			$('#kelas').on('change', function() {
				var value = {
					kelas: this.value,
					method : "get_subjek"
				};
				$.ajax({
					url : "c_input_nilai.php",
					type: "POST",
					data : value,
					success: function(data, textStatus, jqXHR)
					{
						var respons = jQuery.parseJSON(data);
						if(respons.data.length>0){
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Pilih Bab Mapel </option>");
							$.each(respons.data, function (key, val) {
							  $('#subsubjek').append('<option value="'+val.id+'">'+val.subjek+'</option>');
							})
						}else{
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Bab mapel tidak ditemukan </option>");
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			});
	
			//decimal();
			integer();
			$('#txttanggal').datepicker({
				format: 'dd-mm-yyyy',
			});
			
			//Datemask dd/mm/yyyy
			$("#txttanggal").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$(document).on("click","#btnfilter, #btnsearch",function(){
			var kelas = $('#kelas').val();
			var tanggal = $('#txttanggal').val();
			var jenis = $('#jenis').val();
			var subsubjek = $('#subsubjek').val();
			var type = $(this).data('type');
			if( kelas == null || kelas == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Silahkan pilih kelas!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#kelas").focus();
				return;
			}
			if( jenis == null || jenis == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Pilih jenis nilai!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#jenis").focus();
				return;
			}
			
			if( subsubjek == null || subsubjek == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Pilih sub subjek!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#subsubjek").focus();
				return;
			}
			
			if(type == 'input'){
				inputNilai(kelas,subsubjek,jenis,tanggal);
			}else{
				searchNilai(kelas,subsubjek,jenis);
			}
		});
		
		function searchNilai(kelas,subsubjek,jenis){
			
			$("#modalnilai").modal("show");
			
			$('#table_list_nilai tbody').empty();
			
			var par = {
				kelas : kelas,
				subsubjek : subsubjek,
				jenis : jenis,
				method : "get_detail_nilai"
			};
			
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : par,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#vkelas').html(hasil.kelas);				
					$('#vjenis').html(hasil.jenis);
					$('#vsubsubjek').html(hasil.sub_subjek);
					
					tableListNilai(kelas,subsubjek,jenis);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function tableListNilai(kelas,subsubjek,jenis){
			var value = {
				kelas : kelas,
				subsubjek : subsubjek,
				jenis : jenis,
				method : "get_list_nilai"
			};
			
			$("#table_list_nilai").DataTable().destroy();
			
			$('#table_list_nilai').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": false,
				"ordering": false,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_input_nilai.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "tanggal" },
				{ "data": "nama_nilai" },
				{ "data": "edit" },
				]
			});
		}
		
		function inputNilai(kelas,subsubjek,jenis,tanggal){
			
			if( tanggal == null || tanggal == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Input nilai tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txttanggal").focus();
				return;
			}
			
			newitem();
			$('#nilaiSantri tbody').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			var value = {
				kelas: kelas,
				tanggal:tanggal,
				jenis:jenis,
				subsubjek:subsubjek,
				method : "getdaftarnilaisantri"
			};
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#hiddenkelas').val(kelas);
					$('#hiddentanggal').val(tanggal);
					$('#hiddensubsubjek').val(subsubjek);
					$('#hiddenjenisnilai').val(jenis);
					$('#crud').val('N');
					
					$('#detkelas').html(hasil.kelas);				
					$('#detjenis').html(hasil.jenis);
					$('#detsubsubjek').html(hasil.sub_subjek);
			
					var no = 0;
					$.each(hasil.data, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var nis = row.insertCell(1);
					  var nama = row.insertCell(2);
					  var absen = row.insertCell(3);
					  urut.innerHTML = no+1;	
					  nis.innerHTML = val.nis;	
					  nama.innerHTML = val.nama_lengkap;
					  absen.innerHTML = '<input type="hidden" name="siswa[]" value="'+val.uuid+'">'+
										'<input type="text" id="nilaisiswa'+no+'" class="form-control decimal" name="nilai[]" value="0">';
					  				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function newitem(){
			$('#hiddenkelas').val('');				
			$('#hiddenjenisnilai').val('');				
			$('#hiddentanggal').val('');				
			$('#hiddennamanilai').val('');				
			$('#txtname').val('');
			$("#txtname").removeAttr("readonly");
			$('#detkelas').html('');				
			$('#detjenis').html('');
			$('#detsubsubjek').html('');
			$('#crud').val('N');
		}
	
		$(document).on( "click","#btnsaveitem", function() {
			var kelas = $('#hiddenkelas').val();
			var tanggal = $('#hiddentanggal').val();
			var subsubjek = $('#hiddensubsubjek').val();
			var jenis = $('#hiddenjenisnilai').val();
			var nama = $('#txtname').val();
			var crud = $('#crud').val();
			var idnilai = $('#hiddennamanilai').val();
			
			if( nama == null || nama == ''){
				$.notify({
					message: "Nama harus diisi!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			
			var siswa = [];
			$("input[name='siswa[]']").each(function () {
			  siswa.push($(this).val());
			});

			var nilai = [];
			$("input[name='nilai[]']").each(function () {
			  nilai.push($(this).val());
			});
			
			var value = {
				kelas: kelas,
				tanggal: tanggal,
				siswa: siswa,
				nilai: nilai,
				nama:nama,
				subsubjek:subsubjek,
				jenis:jenis,
				crud:crud,
				idnilai:idnilai,
				method : "save_nilai_santri"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
					  $('#nilaiSantri tbody').empty();
					  $("#btnsaveitem").prop('disabled', false);
					  $("#infoproses").html("");
					  newitem();
					  $.notify({
							message: "Berhasil di simpan"
					  },{
							type: 'success',
							delay: 8000,
					  });
					}else{
					  $("#infoproses").html("");
					  $("#btnsaveitem").prop('disabled', false);
					  $.notify({
							message: data.msg
					  },{
							type: 'warning',
							delay: 8000,
					  });
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on("click",".btndelete",function(){
			
			var kelas = $('#valkelas').val();
			var subsubjek = $('#valsubsubjek').val();
			var jenis = $('#valjenisnilai').val();
			
			var id_data = $(this).attr('id_data');
			var value = {
				id_data : id_data,
				method : "delete_nilai"
			};
			swal({   
			title: "Hapus nilai",   
			text: "Apakah anda yakin akan menghapus nilai?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Hapus",   
			closeOnConfirm: true }, 
			function(){
			  $.ajax(
			  {
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
						$.notify('Proses hapus berhasil');
						tableListNilai(kelas,subsubjek,jenis);
					}else{
						$.notify({
							message: "Proses hapus gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});
				}
			  });
			});
		});
		
		$(document).on("click",".btnedit",function(){
			
			var id_data = $(this).attr('id_data');
			$("#modalnilai").modal("hide");
			newitem();
			$('#nilaiSantri tbody').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			var value = {
				nilai: id_data,
				method : "getdetailedit"
			};
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					var detail = hasil.detail;
					$('#crud').val('E');
					
					$('#detkelas').html(detail.kelas);				
					$('#detjenis').html(detail.jenis);
					$('#detsubsubjek').html(detail.subjek);
					$('#txtname').val(detail.nama_nilai);
					$('#hiddennamanilai').val(detail.id);
					$('#txtname').attr('readonly','readonly');
					
					$('#hiddenkelas').val();
					$('#hiddentanggal').val();
					$('#hiddensubjek').val();
					$('#hiddensubsubjek').val();
					$('#hiddenjenisnilai').val();
			
					var no = 0;
					$.each(hasil.list, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var nis = row.insertCell(1);
					  var nama = row.insertCell(2);
					  var absen = row.insertCell(3);
					  urut.innerHTML = no+1;	
					  nis.innerHTML = val.nis;	
					  nama.innerHTML = val.nama_lengkap;
					  absen.innerHTML = '<input type="hidden" name="siswa[]" value="'+val.uuid+'">'+
										'<input type="text" id="nilaisiswa'+no+'" class="form-control decimal" name="nilai[]" value="'+val.nilai+'">';
					  				  
					  no++;
					});
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
	</script>
</body>
</html>
