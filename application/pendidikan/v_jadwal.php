<?php 
$titlepage="Jadwal Pelajaran";
$idsmenu=49; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_pend.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
$kelas = $pos->getKelas();
?>
<section class="content-header">
  <h1>
	JADWAL PELAJARAN
	<small>Madrasah Diniyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="download_jadwal.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Kelas</label>
					<div class="col-sm-3">
						<select class="form-control" id="kelas" name="kelas" >
						  <option value="">Pilih Kelas</option>
						  <?php 
							foreach($kelas[1] as $row){
							  echo "<option value='".$row['id_kelas']."'>".$row['kelas']."</option>";
							}
						  ?>
						</select>
					</div>
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title">Jadwal</h3>
		  <div class="box-tools pull-right">
			<button type="button" name="excel" value="excel" id="btndownload" data-btn="download" class="btn btn-success"><i class="fa fa-download"></i> Download jadwal</button>
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row" id="jadwal">
				<!-- jadwal pelajaran -->
			  </div>
			</div>	
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<!-- Modal tambah jadwal -->
	<div id="modaleditjadwal" class="modal fade ">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title titleeditjadwal">Edit jadwal mengajar</h4>
				</div>
				<div class="modal-body">
				  <div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label namahari">Hari</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="hidden" id="txtkelas" name="txtkelas" class="">
								<select class="form-control" multiple="" id="optjam" name="optjam" >
									
								</select> 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Mata Pelajaran</label>
							<div class="col-sm-9">
								<select class="form-control" id="optmapel" name="optmapel" >
									
								</select>
							</div>
						</div>
						
						<div class="form-group"> <label class="col-sm-3  control-label">Pengajar</label>
							<div class="col-sm-9">
								<select class="form-control" multiple="" id="optpengajar" name="optpengajar" >
									
								</select>
							</div>
						</div>
						
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="button" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				  </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	<!-- End modal jam jadwal-->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script language="javascript">
		$(function () {
			$('#kelas').on('change', function() {
				var kelas = $(this).val();
				if( kelas == null || kelas == ''){
					$("#jadwal").html('');
					$.notify({
						message: "Silahkan pilih kelas!"
					},{
						type: 'warning',
						delay: 8000,
					});		
					$("#kelas").focus();
					return;
				}
				var value = {
					kelas: kelas,
					method : "getjadwalkelas"
				};
				$.ajax(
				{
					url : "c_kelas.php",
					type: "POST",
					data : value,
					success: function(data, textStatus, jqXHR)
					{
						var hasil = jQuery.parseJSON(data);
						$("#jadwal").html(hasil.html);
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			});
			
		});

		$(document).on("click",".editjadwal",function(){

			$("#modaleditjadwal").modal("show");
			$("#titleeditjadwal").html("Edit jadwal mengajar");
			var hari = $(this).data("hari");
			var harike = $(this).data("harike");
			var kelas = $(this).data("kelas");
			var model = $(this).data("model");
			$(".namahari").html(hari);
			$('#optmapel').html('');
			$('#optjam').html('');
			$('#optpengajar').html('');
			var value = {
				harike: harike,
				kelas:kelas,
				model:model,
				method : "getdetailjadwal"
			};
			$.ajax(
			{
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$("#inputcrud").val(hasil.crud);
					$("#txtkelas").val(hasil.kelas);
					$.each(hasil.jam, function (key, val) {
						$('#optjam').append('<option value="' + val.id + '" > Jam ke ' + val.jamke + ' : ' + val.jam_mulai + ' - '+ val.jam_selesai +'</option>');
					})
					$.each(hasil.mapel, function (key, val) {
						$('#optmapel').append('<option value="' + val.id + '" >' + val.nama_mapel + '</option>');
					})
					$.each(hasil.tutor, function (key, val) {
						$('#optpengajar').append('<option value="' + val.id_pegawai + '" >' + val.nip + ' - ' + val.nama_lengkap + '</option>');
					})
					$("#modalmasteritem").modal('show');
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
			
		});

		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var kelas = $("#txtkelas").val();
			var jam = $("#optjam").val();
			var mapel = $("#optmapel").val();
			var tutor = $("#optpengajar").val();
			
			var crud=$("#inputcrud").val();
			if( jam == null ){
				$.notify({
					message: "Jam kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optjam").focus();
				return;
			}
			if( tutor == null ){
				$.notify({
					message: "Pengajar kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optpengajar").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				kelas: kelas,
				jam: jam,
				mapel: mapel,
				tutor:tutor,
				crud: crud,
				method : "save_jadwal"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					if(data.msg == false){
					  $.notify('Tutor sudah ada jadwal!');
					}
					$("#kelas").trigger("change");
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on("click",".deleteJadwal",function(){

			var kelas = $(this).data("kelas");
			var model = $(this).data("model");
			var value = {
				kelas:kelas,
				model:model,
				method : "deletejadwal"
			};
			swal({   
			title: "Hapus jadwal",   
			text: "Apakah anda akan menghapus jadwal?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Hapus",   
			closeOnConfirm: true }, 
			function(){
			  $.ajax(
			  {
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
						$.notify('Proses hapus berhasil');
						$("#kelas").trigger("change");				
					}else{
						$.notify({
							message: "Gagal menghapus jadwal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});
				}
			  });
			});
		});
		
		$(document).on( "click","#btndownload", function() {
			var kelas = $('#kelas').val();
			
			if( kelas == null || kelas == ''){
				$.notify({
					message: "Kelas belum dipilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#kelas").focus();
				return;
			}
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "download_jadwal.php";

			var txtid = document.createElement("input");
			txtid.type = "hidden";
			txtid.name = "kelas";
			txtid.value = kelas;
			mapForm.appendChild(txtid);
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	</script>
</body>
</html>
