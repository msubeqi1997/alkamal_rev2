<?php 
$titlepage="Daftar Kelas";
$idsmenu=34; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content-header">
  <h1>
	KELAS
	<small>management kelas</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Kelas</button>
					
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					<button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
				<br>
			</div>
			<br>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Jenjang </th>
							<th>Tingkat </th>
							<th>Nama kelas </th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah kelas</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Nama Kelas</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="hidden" id="txtidwali" name="txtidwali" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Jenjang</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
									<option value=""> Pilih Jenjang </option>
									<option value="ula"> Ula</option>
									<option value="wustho"> Wustho</option>
									<option value="mdk"> MDK</option>
								</select>
							</div>
						</div>
						
						<div class="form-group"> <label class="col-sm-3  control-label">Tingkat</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="opttingkat" name="opttingkat" >
									<option value=""> Pilih Tingkat </option>
									<option value="1"> Tingkat 1</option>
									<option value="2"> Tingkat 2</option>
									<option value="3"> Tingkat 3</option>
								</select>
							</div>
						</div>
						<!--
						<div class="form-group"> <label class="col-sm-3  control-label">Wali kelas</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="txtwali" name="txtwali" disabled>
							</div>
							<div class="col-sm-2">
							  <button  type="submit" title="pilih wali" class="btn btn-primary btnopenpegawai"  ><i class="fa fa-search"></i></button>
							</div>
						</div>
						<div id="list-pembina">
							
						</div>
						-->
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>

	<!-- Modal searching pegawai -->
	<div id="modalpegawai" class="modal fade ">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title titlelistpegawai">Daftar wali</h4>
				</div>
				<div class="modal-body">
					<div class="box-body table-responsive no-padding">
						<table id="table_pegawai" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th style="width:150px">Nama Wali</th>
									<th style="width:50px">Pilih</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	<!-- End modal searching pegawai-->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script language="javascript">
		
		
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_kelas.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "satuan" },
				{ "data": "tingkat" },
				{ "data": "kelas" },
				{ "data": "action" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#txtidwali").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#txtwali").val("");
			$("#optjenjang").val("");
			$("#opttingkat").val("");
			$("#head-modal").html("Tambah Kelas");
			set_focus("#txtname");
			
		}
		
		//searching pegawai
		$(document).on("click",".btnopenpegawai",function(){

			$("#modalpegawai").modal("show");
			$("#titlelistpegawai").html("Daftar wali");
			var value = {
				method : "get_pegawai"
			};
			$.ajax(
			{
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#table_pegawai tbody").html(data.data);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});

		$(document).on("click",".insertpegawai",function(){
			$("#txtwali").val($(this).attr("pegawai"));
			$("#txtidwali").val($(this).attr("pegawai_id"));
			$("#modalpegawai").modal("hide");
		});

		$(document).on( "click",".btnedit", function() {
			newitem();
			$("#head-modal").html("Edit Kelas");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail"
			};
			$.ajax(
			{
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.msg;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.id_kelas);
					$("#txtidwali").val(data.wali_kelas);
					$("#txtname").val(data.kelas);
					$("#optjenjang option[value='"+data.jenjang+"']").prop('selected', true);
					$("#opttingkat option[value='"+data.tingkat+"']").prop('selected', true);
					$("#txtwali").val(data.nama_wali);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var wali = $("#txtidwali").val();
			var item_name = $("#txtname").val();
			var jenjang = $("#optjenjang").val();
			var tingkat = $("#opttingkat").val(); 
			var crud=$("#inputcrud").val();
			
			if(item_name == '' || item_name== null ){
				$.notify(
					{ message: "Nama kelas kosong!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#txtname").focus();
				return;
			}
			if(jenjang == '' || jenjang== null ){
				$.notify(
					{ message: "Jenjang belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#optjenjang").focus();
				return;
			}
			if(tingkat == '' || tingkat== null ){
				$.notify(
					{ message: "Tingkat belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#opttingkat").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				wali: wali,
				satuan:jenjang,
				tingkat:tingkat,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
							newitem();
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_rombel.php";

		var txtid = document.createElement("input");
		txtid.type = "text";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);

		mapForm.submit();
		
		
	});
	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_kelas.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	</script>
</body>
</html>
