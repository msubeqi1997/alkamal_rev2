<?php 
$titlepage="Kenaikan Tingkat";
$idsmenu=39; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_pend.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
$tahun = $pos->activeTapel();
$tapel = $tahun[1]['thn_ajaran_id'];
$smt = $tahun[1]['semester'];

?>
<section class="content-header">
  <h1>
	KENAIKAN TINGKAT
	<small>kenaikan tingkat</small>
  </h1>
</section>
<section class="content">
	<?php
	if($smt == 'ganjil'){ ?>
	  <div class="callout callout-warning">
		<h4>Maaf!</h4>
		<p><b>Kenaikan</b> hanya dapat dilakukan pada semester genap.</p>
	  </div>
	<?php	
	}else{ ?>
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Data Santri</h3>
			</div>
			<!--./ box header-->
			<div class="box-body no-padding">
			  <form method="post" id="target" action="c_kenaikan_tingkat.php">
				<input type="hidden" name="method" value="proses_kenaikan">
				<div class="row" style="padding-left:25px; padding-right:25px;">
					<div class="col-md-3">
					  <div class="form-group">
						<label>Jenjang pendidikan</label>
						<select class="form-control" id="txtjenjang" name="txtjenjang" style="width: 100%;">
						  <option value="">Semua</option>
						  <option value="ula">Ula</option>
						  <option value="wustho">Wustho</option>
						  <option value="mdk">MDK</option>
						</select>
					  </div><!-- /.form-group -->
					</div><!-- /.col -->
					<div class="col-md-3">
					  <div class="form-group"> 
						<label>Tingkat</label>
						<select class="form-control" id="txttingkat" name="txttingkat" style="width: 100%;">
						  <option value="">Pilih tingkat</option>
						  <option value="1">Tingkat 1</option>
						  <option value="2">Tingkat 2</option>
						  <option value="3">Tingkat 3</option>
						</select>
					  </div>
					</div><!-- /.col -->
					<div class="col-md-3">
					  <div class="form-group">
						<label>Kelas</label>
						<select class="form-control" id="txtkelas" name="txtkelas" style="width: 100%;">
						  <option value="">Pilih tingkat dahulu</option>
						  
						</select>
					  </div><!-- /.form-group -->
					</div><!-- /.col -->
				</div>
				<div class="row">
				  <div class="col-md-9"> 
					<div class="table-responsive" style="height: 986px; overflow-y: scroll; padding-left:5px;">
					  <table id="rombel" class="table  table-bordered table-hover ">
						<thead>
						  <tr class="tableheader">
							<th style="width:45px">#</th>
							<th>No Induk </th>
							<th>Nama lengkap </th>
							<th style="width:105px">Naik</th>
						  </tr>
						</thead>
						<tbody></tbody>
					  </table>
					</div>
				  </div>
				  <div class="col-md-3 " style="padding-right:25px;">
					<div class="form-group">
						<label>
						<input type="checkbox" name="acak" value="ya">
						 Rombak Kelas</label>
					</div><!-- /.form-group -->
					<div class="form-group">
						<label>Kelas Tujuan</label>
						<select class="form-control" id="txtkelastujuan" name="txtkelastujuan" style="width: 100%;">
						  <option value="">Pilih tingkat dahulu</option>
						  
						</select>
					</div><!-- /.form-group -->
					<button type="submit" id="btnproses" class="btn btn-app bg-green btn-flat margin">
					  <i class="glyphicon glyphicon-open"></i> Naik
					</button>
					<span id="infoproses"></span>
					<dl>
					  <dt>Rombak kelas</dt>
					  <dd>Jika pilihan rombak kelas di centang, kelas tujuan akan diabaikan.</dd>
					</dl>
				  </div>
				</div>
			  </form>			
			</div>
		</div><!-- /.box -->
	<?php
	}
	?>	
</section><!-- /.content -->


	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
<script language="javascript">
	$(function () {
	$('#txtjenjang, #txttingkat').on('change', function() {
	  
	  var jenjang = $('#txtjenjang').val();
	  var tingkat = $('#txttingkat').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
		var value = {
			jenjang: jenjang,
			tingkat: tingkat,
			method : "get_kelas_rombel"
		};
		$.ajax({
			url : "c_kenaikan_tingkat.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtkelas').html("");
					$('#txtkelas').html("<option value=''> Pilih kelas </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtkelas').append('<option value="'+val.id_kelas+'">'+val.kelas+'</option>');
					})
				}else{
					$('#txtkelas').html("");
					$('#txtkelas').html("<option value=''> Kelas tidak ditemukan </option>");
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
		kelas_tujuan(jenjang, tingkat);
		$("#rombel").DataTable().destroy();
		$("#rombel tbody").html('');
	});
	
	$('#txtkelas').on('change', function() {
	  var kelas = $(this).val();
	  var tingkat = $('#txttingkat').val();
	  
	  if(kelas == '' || kelas== null ){
		$.notify(
			{ message: "Kelas belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtkelas").focus();
		$("#rombel").DataTable().destroy();
		$("#rombel tbody").html('');
		return;
	  }
	  rombel(kelas,tingkat);
	});
	
});

function kelas_tujuan(jenjang, tingkat){
	
	if(tingkat == '3'){
		$('#txtkelastujuan').html("");
		$('#txtkelastujuan').html("<option value='lulus'> Lulus </option>");
	}else{
		var value = {
			jenjang:jenjang,
			tingkat:parseInt(tingkat)+1,
			method : "get_kelas_tujuan"
		};
		$.ajax({
			url : "c_kenaikan_tingkat.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtkelastujuan').html("");
					$('#txtkelastujuan').html("<option value=''> Pilih kelas </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtkelastujuan').append('<option value="'+val.id_kelas+'">'+val.kelas+'</option>');
					})
				}else{
					$('#txtkelastujuan').html("");
					$('#txtkelastujuan').html("<option value=''> Kelas tidak ditemukan </option>");
				}
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
}

function rombel(kelas,tingkat){
	$("#rombel").DataTable().destroy();
	var value = {
		kelas:kelas,
		tingkat:tingkat,
		method : "kenaikan"
	};
	$('#rombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_kenaikan_tingkat.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		{ "data": "action" },
		
		]
	});
} 

	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#target").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin melanjutkan proses?",
			function (result) {
			  if (result == true) {
				$("#btnproses").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. proses sudah selesai.");
		$("#rombel tbody").html('');
		$("#btnproses").prop('disabled', false);
	  }
	  else{
		bootbox.alert(msg);
		$("#btnproses").prop('disabled', false);
	  }
	}
</script>
</body>
</html>
