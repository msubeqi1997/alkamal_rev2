<?php 
$titlepage="Bab Kitab";
$idsmenu=44; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 
require_once ("../model/model_pend.php");
$pos = new model_pend();
$cat = $pos->getKelompokMapel();
?>
<section class="content-header">
  <h1>
	Bab Kitab 
	<small>Madrasah Diniyah</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Bab</button>
					<br>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Kitab </th>
							<th>Bab </th>
							<th>Alias</th>
							<th>Standar</th>
							<th>Jenjang</th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah bab</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Bab</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Alias</label>
							<div class="col-sm-9">
								<input type="text" class="form-control " id="txtalias" name="txtalias" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Standar</label>
							<div class="col-sm-9">
								<input type="text" class="form-control " id="txtstandar" name="txtstandar" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kitab</label>
							<div class="col-sm-9">
								<select class="form-control" id="optkategori" name="optkategori" >
									<option value=""> Pilih Kitab </option>
									<?php 
										foreach($cat[1] as $row){
										  echo "<option value='".$row['id_kelompok_mapel']."'>".$row['kelompok_mapel']."</option>";
										}
									  ?>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Jenjang</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
									<option value=""> Pilih Jenjang </option>
									<option value="ula"> Ula</option>
									<option value="wustho"> Wustho</option>
									<option value="mdk"> MDK</option>
								</select>
							</div>
						</div>
						
						
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>

	<!-- Modal searching pegawai -->
	<div id="modalpegawai" class="modal fade ">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title titlelistpegawai">Daftar wali</h4>
				</div>
				<div class="modal-body">
					<div class="box-body table-responsive no-padding">
						<table id="table_pegawai" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th style="width:150px">Nama Wali</th>
									<th style="width:50px">Pilih</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	<!-- End modal searching pegawai-->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script language="javascript">
		
		
		$(document).ready( function () 
		{
			var value = {
				method : "get_list_mapel"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_mapel.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "kitab" },
				{ "data": "bab" },
				{ "data": "alias" },
				{ "data": "standar" },
				{ "data": "jenjang" },
				{ "data": "action" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#txtalias").val("");
			$("#txtstandar").val("");
			$("#optjenjang").val("");
			$("#optkategori").val("");
			$("#head-modal").html("Tambah Bab Pelajaran");
			set_focus("#txtname");
			
		}
		
		$(document).on( "click",".btnedit", function() {
			newitem();
			$("#head-modal").html("Edit Bab Pelajaran");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail_mapel"
			};
			$.ajax(
			{
				url : "c_mapel.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.id_mapel);
					$("#txtalias").val(data.alias);
					$("#txtname").val(data.mapel);
					$("#optjenjang option[value='"+data.jenjang+"']").prop('selected', true);
					$("#optkategori option[value='"+data.id_kelompok_mapel+"']").prop('selected', true);
					$("#txtstandar").val(data.capaian);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var alias = $("#txtalias").val();
			var standar = $("#txtstandar").val();
			var jenjang = $("#optjenjang").val();
			var cat = $("#optkategori").val(); 
			var crud=$("#inputcrud").val();
			
			if(item_name == '' || item_name== null ){
				$.notify(
					{ message: "Nama bab harus diisi!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#txtname").focus();
				return;
			}
			if(alias == '' || alias== null ){
				$.notify(
					{ message: "Alias belum diisi!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#txtalias").focus();
				return;
			}
			if(jenjang == '' || jenjang== null ){
				$.notify(
					{ message: "Jenjang belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#optjenjang").focus();
				return;
			}
			
			if(cat == '' || cat== null ){
				$.notify(
					{ message: "Kitab belum diisi!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#optkategori").focus();
				return;
			}
			var value = {
				id_item: id_item,
				item_name: item_name,
				alias: alias,
				standar:standar,
				jenjang:jenjang,
				cat:cat,
				crud: crud,
				method : "save_mapel"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_mapel.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
							newitem();
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_rombel.php";

		var txtid = document.createElement("input");
		txtid.type = "text";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);

		mapForm.submit();
		
		
	});
	</script>
</body>
</html>
