<?php 
$titlepage="Wali Kelas";
$idsmenu=40; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_pend.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_pend();
$tahun = $pos->activeTapel();
$tapel = $tahun[1]['thn_ajaran'];
$smt = $tahun[1]['semester'];

?>
<section class="content-header">
  <h1>
	WALI KELAS
	<small><?php echo $tapel; ?></small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<form method="post" id="target" action="c_kelas.php">
		  <div class="box-body no-padding">
			<input type="hidden" name="method" value="simpan_wali_kelas">
			<div class="row" style="padding-left:25px; padding-right:25px;">
				<div class="col-md-3">
				  <div class="form-group">
					<label>Jenjang pendidikan</label>
					<select class="form-control" id="txtjenjang" name="txtjenjang" style="width: 100%;">
					  <option value="">Semua</option>
					  <option value="ula">Ula</option>
					  <option value="wustho">Wustho</option>
					  <option value="mdk">MDK</option>
					</select>
				  </div><!-- /.form-group -->
				</div><!-- /.col -->
				<div class="col-md-3">
				  <div class="form-group"> 
					<label>Tingkat</label>
					<select class="form-control" id="txttingkat" name="txttingkat" style="width: 100%;">
					  <option value="">Pilih tingkat</option>
					  <option value="1">Tingkat 1</option>
					  <option value="2">Tingkat 2</option>
					  <option value="3">Tingkat 3</option>
					</select>
				  </div>
				</div><!-- /.col -->
			</div>
			<div class="row">
			  <div class="col-md-12"> 
				<div class="table-responsive" style="min-height: 567px; padding-left:5px;">
				  <table id="rombel" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:65px">#</th>
						<th>Kelas </th>
						<th>Wali kelas </th>
						<th style="width:105px">Cari</th>
					  </tr>
					</thead>
					<tbody></tbody>
				  </table>
				</div>
			  </div>
			</div>
		  			
		  </div>
		  <div class="box-footer">
			<span id="infoproses"></span>
			<button type="submit" id="btnproses" class="btn btn-info pull-right">Simpan</button>
			
		  </div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->
</section><!-- /.content -->

	<!-- Modal searching pegawai -->
	<div id="modalpegawai" class="modal fade ">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title titlelistpegawai">Daftar pegawai</h4>
					<input type="hidden" value="" id="idkelaswali">
				</div>
				<div class="modal-body">
					<div class="box-body table-responsive no-padding">
						<table id="table_pegawai" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th style="width:150px">Nama Wali</th>
									<th style="width:50px">Pilih</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	<!-- End modal searching pegawai-->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
<script language="javascript">
$(function () {
	$("#btnproses").hide();
	
	$('#txtjenjang, #txttingkat').on('change', function() {
	  
	  var jenjang = $('#txtjenjang').val();
	  var tingkat = $('#txttingkat').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
		kelas(jenjang,tingkat);
		$("#rombel tbody").html('');
	});
	
	//searching pegawai
	$(document).on("click",".btnopenpegawai",function(){
		var kelas = $(this).attr("kelas");
		$("#modalpegawai").modal("show");
		$(".titlelistpegawai").html("");
		$(".titlelistpegawai").html("Daftar wali kelas");
		$("#idkelaswali").val(kelas);
		var value = {
			kelas : kelas,
			method : "get_pegawai"
		};
		$.ajax(
		{
			url : "c_kelas.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				$("#table_pegawai tbody").html(data.data);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				
			}
		});
	
		
	});
	
	$(document).on("click",".insertpegawai",function(){
		var kelas= $("#idkelaswali").val();
		$("#wali"+kelas).val($(this).attr("pegawai"));
		$("#txtidwali"+kelas).val($(this).attr("pegawai_id"));
		$("#modalpegawai").modal("hide");
	});
	
});

function kelas(jenjang,tingkat){
	$("#rombel").DataTable().destroy();
	var value = {
		jenjang:jenjang,
		tingkat:tingkat,
		method : "get_wali_kelas"
	};
	$('#rombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_kelas.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "kelas" },
		{ "data": "wali" },
		{ "data": "search" },
		
		]
	});
	
	$("#btnproses").show();
} 

	$(function () {
		var options = {
			success: suksesUpload
		}
		$("#target").submit(function( event ) {
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin melanjutkan proses?",
			function (result) {
			  if (result == true) {
				$("#btnproses").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesUpload(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. proses sudah selesai.");
		$("#btnproses").prop('disabled', false);
	  }
	  else{
		bootbox.alert(msg);
		$("#btnproses").prop('disabled', false);
	  }
	}
</script>
</body>
</html>
