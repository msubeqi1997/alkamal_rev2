<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_data();

$data = $pos->autoCompleteSearch($term);
foreach ($data[1] as $row) {
	$row['siswa_id']=htmlentities(stripslashes($row['uuid']));
	$row['nis']=htmlentities(stripslashes($row['nis']));
	$row['nama']=htmlentities(stripslashes($row['nama_lengkap']));
	$row['jenjang']=htmlentities(stripslashes($row['jenjang']));
	
	$row_set[] = $row;
}

echo json_encode($row_set);
?>