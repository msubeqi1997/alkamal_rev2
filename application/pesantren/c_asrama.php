<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	$tipe = array('1' => 'Putra','0' => 'Putri');
	if($method == 'getdata'){
		$array = $pos->getAsrama();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit firqoh" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			$pengurus = '<button  type="submit" id_item="'.$key['auto'].'"  title="Pengurus firqoh" class="btn btn-sm btn-primary btnpengurus "  ><i class="fa fa-users"></i> Pengurus</button>';
			$data[$i]['button'] = $button;
			$data[$i]['tipe_firqoh'] = $tipe[$key['tipe']];
			$data[$i]['pengurus'] = $pengurus;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$kode = $_POST['kode'];
		$tipe = $_POST['tipe'];
		$pembina = $_POST['pembina'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveAsrama($name,$kode,$tipe,$pembina);
		}
		else
		{
			$array = $pos->updateAsrama($iditem,$name,$kode,$tipe,$pembina);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_pembina')
	{
		$result = $pos->getPegawaiAktif();
		echo json_encode(array('data'=>$result));
	}
	
	if($method == 'get_detail')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailAsrama($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'save_pengurus')
	{
		$tapel = $pos->activeTapel();
		$crud = $_POST['inputcrud'];
		$iditem = $_POST['id_asrama'];
		$data_p['asrama'] = $_POST['asrama'];
		$data_p['id_tahun_ajaran'] = $tapel[1]['thn_ajaran_id'];
		$data_p['ketua'] = $_POST['ketua'];
		$data_p['sekretaris'] = $_POST['sekretaris'];
		$data_p['bendahara_1'] = $_POST['bendahara_1'];
		$data_p['bendahara_2'] = $_POST['bendahara_2'];
		$data_p['keamanan_ko'] = $_POST['keamanan_ko'];
		$data_p['keamanan_1'] = $_POST['keamanan_1'];
		$data_p['keamanan_2'] = $_POST['keamanan_2'];
		$data_p['keamanan_3'] = $_POST['keamanan_3'];
		$data_p['keamanan_4'] = $_POST['keamanan_4'];
		$data_p['bahasa_ko'] = $_POST['bahasa_ko'];
		$data_p['bahasa_1'] = $_POST['bahasa_1'];
		$data_p['bahasa_2'] = $_POST['bahasa_2'];
		$data_p['bahasa_3'] = $_POST['bahasa_3'];
		$data_p['bahasa_4'] = $_POST['bahasa_4'];
		$data_p['pendidikan_ko'] = $_POST['pendidikan_ko'];
		$data_p['pendidikan_1'] = $_POST['pendidikan_1'];
		$data_p['pendidikan_2'] = $_POST['pendidikan_2'];
		$data_p['pendidikan_3'] = $_POST['pendidikan_3'];
		$data_p['pendidikan_4'] = $_POST['pendidikan_4'];
		$data_p['orkesih_ko'] = $_POST['orkesih_ko'];
		$data_p['orkesih_1'] = $_POST['orkesih_1'];
		$data_p['orkesih_2'] = $_POST['orkesih_2'];
		$data_p['orkesih_3'] = $_POST['orkesih_3'];
		$data_p['orkesih_4'] = $_POST['orkesih_4'];
		$data_p['humas_ko'] = $_POST['humas_ko'];
		$data_p['humas_1'] = $_POST['humas_1'];
		$data_p['humas_2'] = $_POST['humas_2'];
		$data_p['humas_3'] = $_POST['humas_3'];
		$data_p['humas_4'] = $_POST['humas_4'];
		$data_p['mpk_ko'] = $_POST['mpk_ko'];
		$data_p['mpk_1'] = $_POST['mpk_1'];
		$data_p['mpk_2'] = $_POST['mpk_2']; 
		$data_p['mpk_3'] = $_POST['mpk_3'];
		$data_p['mpk_4'] = $_POST['mpk_4'];
	
		if($crud == 'N')
		{
			$array = $pos->savePengurusAsrama($data_p);
		}
		else
		{
			$array = $pos->updatePengurusAsrama($iditem,$data_p);
		}
		
		$result['result'] = $array[0];
		$result['data'] = $array[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}