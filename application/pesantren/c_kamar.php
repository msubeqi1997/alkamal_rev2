<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	
	function countPenghuni($id){
		$pos = new model_data();
		$array = $pos->countPenghuniKamar($id);
		if($array[0] == true){
			if($array[1]['terisi'] < $array[1]['kapasitas_kamar']){ 
				$data['msg'] = "ada";
			}
			else{ $data['msg'] = "penuh";}
			$data['terisi'] = $array[1]['terisi'];
			$data['kapasitas'] = $array[1]['kapasitas_kamar'];
		}else{
			$data['msg'] = "error";
		}
		return $data;
	}
	
	if($method == 'getdata'){
		$array = $pos->getKamar();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit kamar" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			$print = '<button  type="submit" id_item="'.$key['auto'].'"  title="Download anggota kamar" class="btn btn-sm btn-primary btndownload "  id="btndownload'.$key['auto'].'"  ><i class="fa fa-download"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                          '.$print.'
						</div>
                       </td>';
			$data[$i]['button'] = $button;
			$data[$i]['action'] = $action;
			$data[$i]['sisa'] = $key['kapasitas_kamar']-$key['terisi'];
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$kapasitas = $_POST['kapasitas'];
		$asrama = $_POST['firqoh'];
		$pembina = implode(',',$_POST['pembina']);
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveKamar($name,$kapasitas,$pembina,$asrama);
		}
		else
		{
			$array = $pos->updateKamar($iditem,$name,$kapasitas,$pembina,$asrama);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_firqoh')
	{
		$result = $pos->getAsrama();
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_detail')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailKamar($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'get_pembina')
	{
		$result = $pos->getPegawaiAktif();
		echo json_encode(array('data'=>$result));
	}
	
	if($method == 'get_asrama_bytipe')
	{
		$sex = array('putra' => '1','putri' => '0');
		$jenjang= explode(' ',$_POST['jenjang']);
		$tipe=$sex[$jenjang[1]];
		
		$result = $pos->getAsramabyTipe($tipe);
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_kamar_asrama')
	{
		$id = $_POST['asrama'];
		$result = $pos->getKamarbyAsrama($id);
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'count')
	{
		$id = $_POST['kamar'];
		$data = countPenghuni($id);
		if($data['msg'] != 'error'){
		    $result['terisi'] = $data['terisi'];
			$result['kapasitas'] = $data['kapasitas'];
		}else{ $result['msg'] != 'error';}
		echo json_encode($result);
	}
	
	if($method == 'group'){
		$id = $_POST['kamar'];
		$array = $pos->getPenghuniKamar($id);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Keluarkan penghuni kamar" class="btn btn-sm btn-danger checkout "  id="'.$key['uuid'].'"  ><i class="fa fa-arrow-left"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$data[$i]['mdk'] = ($key['jenis']=='mdk')?'MDK':'BUKAN';
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'ungroup'){
		$sex = array('putra' => '1','putri' => '0');
		$arrjenjang = array('ula' => '2', 'wustho' => '3');
	
		$jalur= explode(' ',$_POST['jenjang']);
		$kelamin=$sex[$jalur[1]];
		$jenjang=$arrjenjang[$jalur[0]];
		$tingkat = $_POST['tingkat'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
			
		$array = $pos->getNonRombel($kelamin,$jenjang,$tingkat,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Masukkan penghuni kamar" class="btn btn-sm btn-primary checkin "  id="'.$key['uuid'].'"  ><i class="fa fa-arrow-right"></i></button>';
			$data[$i]['urutan'] = $i+1;
			$data[$i]['mdk'] = ($key['jenis']=='mdk')?'MDK':'BUKAN';
			$data[$i]['button'] = $button;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'searching_siswa'){
		$sex = array('putra' => '1','putri' => '0');
		$arrjenjang = array('ula' => '2', 'wustho' => '3');
	
		$jalur= explode(' ',$_POST['jenjang']);
		$kelamin=$sex[$jalur[1]];
		$jenjang=$arrjenjang[$jalur[0]];
		$term = (!empty($_POST['term']))?$_POST['term']:' ';
		
		$array = $pos->getSearchUngroup($kelamin,$jenjang,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['id'].'"  title="Masukkan penghuni kamar" class="btn btn-sm btn-primary checkin "  id="'.$key['uuid'].'"  ><i class="fa fa-arrow-right"></i></button>';
			$data[$i]['urutan'] = $i+1;
			$data[$i]['mdk'] = ($key['jenis']=='mdk')?'MDK':'BUKAN';
			$data[$i]['button'] = $button;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'checkin'){
		$id_item = $_POST['id_item'];
		$kamar = $_POST['kamar'];
		
		$data = countPenghuni($kamar);
		if($data['msg'] != 'error'){
		  if($data['msg'] != 'penuh'){
			$array = $pos->saveCheckin($id_item,$kamar);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		  }else{ 
			$result['respons'] = false;
			$result['data'] = 'Kamar penuh';
		  }
		}
		
		echo json_encode($result);
	}
	
	if($method == 'checkout'){
		$id_item = $_POST['id_item'];
		$kamar = $_POST['kamar'];
		
		$array = $pos->saveCheckout($id_item,$kamar);
		$result['respons'] = $array[0];
		$result['data'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'get_info_kamar')
	{
		$id = base64_decode($_POST['id_item']);
		$terisi = countPenghuni($id);
		$array = $pos->getDetailKamar($id);
		$result['data'] = $array[0];
		$result['nama'] = $array[1]['nama_kamar'];
		$result['kapasitas'] = $array[1]['kapasitas_kamar'];
		$result['terisi'] = $terisi['terisi'];
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}