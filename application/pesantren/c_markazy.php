<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	
	
	if($method == 'save_pengurus')
	{
		$tapel = $pos->activeTapel();
		$crud = $_POST['inputcrud'];
		$iditem = $_POST['id'];
		$data_p['pengasuh_1'] = $_POST['pengasuh_1'];
		$data_p['pengasuh_2'] = $_POST['pengasuh_2'];
		$data_p['pengasuh_3'] = $_POST['pengasuh_3'];
		$data_p['pembina_1'] = $_POST['pembina_1'];
		$data_p['pembina_2'] = $_POST['pembina_2'];
		$data_p['pembina_3'] = $_POST['pembina_3'];
		$data_p['ketua'] = $_POST['ketua'];
		$data_p['wakil'] = $_POST['wakil'];
		$data_p['sekretaris'] = $_POST['sekretaris'];
		$data_p['bendahara'] = $_POST['bendahara'];
		$data_p['keamanan_1'] = $_POST['keamanan_1'];
		$data_p['keamanan_2'] = $_POST['keamanan_2'];
		$data_p['bahasa_1'] = $_POST['bahasa_1'];
		$data_p['bahasa_2'] = $_POST['bahasa_2'];
		$data_p['pendidikan_1'] = $_POST['pendidikan_1'];
		$data_p['pendidikan_2'] = $_POST['pendidikan_2'];
		$data_p['orkesih_1'] = $_POST['orkesih_1'];
		$data_p['orkesih_2'] = $_POST['orkesih_2'];
		$data_p['humas_1'] = $_POST['humas_1'];
		$data_p['humas_2'] = $_POST['humas_2'];
		$data_p['mpk_1'] = $_POST['mpk_1'];
		$data_p['mpk_2'] = $_POST['mpk_2']; 
		$data_p['sarpras_1'] = $_POST['sarpras_1'];
		$data_p['sarpras_2'] = $_POST['sarpras_2'];
		$data_p['pembangunan_1'] = $_POST['pembangunan_1'];
		$data_p['pembangunan_2'] = $_POST['pembangunan_2'];
		$data_p['it_1'] = $_POST['it_1'];
		$data_p['it_2'] = $_POST['it_2'];
		
		if($crud == 'N')
		{
			$array = $pos->savePengurusMarkazy($data_p);
		}
		else
		{
			$array = $pos->updatePengurusMarkazy($iditem,$data_p);
		}
		
		$result['result'] = $array[0];
		$result['data'] = $array[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}