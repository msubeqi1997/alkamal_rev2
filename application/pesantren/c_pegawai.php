<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	
	$sex = array('1' => 'Laki-laki','0' => 'Perempuan');
		
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdata'){
		$array = $pos->getDataPegawai();
		
		$data = $array['data'];
		$i=0;
		foreach ($data as $row) {
			$button = '<button  type="submit" id_pegawai="'.$row['id_pegawai'].'"  title="Tombol edit pegawai" class="btn btn-sm btn-primary btnedit "  btn-data="edit" id="btnedit'.$row['id_pegawai'].'"  ><i class="fa fa-edit"></i></button>';
			$data[$i]['urutan'] = $row['urutan'];
			$data[$i]['id_pegawai'] = $row['id_pegawai'];
			$data[$i]['nip'] = $row['nip'];
			$data[$i]['nama_lengkap'] = $row['nama_lengkap'];
			$data[$i]['alamat'] = $row['alamat'];
			$data[$i]['ttl'] = "".$row['tempat_lahir'].", ".display_to_report($row['tanggal_lahir'])."";
			$data[$i]['jenis'] = $row['jenis'];
			$data[$i]['aktif'] = ucwords($row['aktif']);
			$data[$i]['button'] = $button;
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'get_kabupaten'){
		$propinsi_id = $_POST['propinsi_id'];
		$array = $pos->getKabupaten($propinsi_id);
		$result['data'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'save_item')
	{
		//error_reporting(0);
		$unique = getId();
		$uuid = '';
		$jid = isset($_POST['idpegawai']) ? $_POST['idpegawai'] : '';
		
		
		$data['nip'] = isset($_POST['no_induk']) ? $_POST['no_induk'] : '';
		$data['nama_lengkap'] = isset($_POST['nama_lengkap']) ? $_POST['nama_lengkap'] : '';
		$data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
		$data['kelamin'] = isset($_POST['kelamin']) ? $_POST['kelamin'] : '';
		$data['tempat_lahir'] = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
		$data['tanggal_lahir'] = isset($_POST['tanggal_lahir']) ? display_to_sql($_POST['tanggal_lahir']) : '';
		$data['propinsi'] = isset($_POST['propinsi']) ? $_POST['propinsi'] : '';
		$data['kabupaten'] = isset($_POST['kabupaten']) ? $_POST['kabupaten'] : '';
		$data['alamat'] = isset($_POST['alamat']) ? $_POST['alamat'] : '';
		$data['no_hp'] = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
		$data['kode_pos'] = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : '';
		$data['pendidikan'] = isset($_POST['pendidikan']) ? $_POST['pendidikan'] : '';
		$data['jurusan'] = isset($_POST['jurusan']) ? $_POST['jurusan'] : '';
		$data['lulus'] = isset($_POST['lulus']) ? $_POST['lulus'] : '';
		$data['nikah'] = isset($_POST['nikah']) ? $_POST['nikah'] : '';
		$data['aktif'] = isset($_POST['status_kepegawaian']) ? $_POST['status_kepegawaian'] : '';
		
		$data['input_at'] = date ('Y-m-d H:i:s');
		$data['tanggal_edit'] = date ('Y-m-d H:i:s');
		
		$crud = $_POST['inputcrud'];
		
		$folder_foto = '../../files/images_pegawai/';
		$tmp_pegawai = $_FILES['photo_pegawai']['tmp_name'];
		$tipe_pegawai = $_FILES['photo_pegawai']['type'];
		$nama_pegawai = $_FILES['photo_pegawai']['name'];
		$extention = pathinfo($nama_pegawai, PATHINFO_EXTENSION);
		
		
		if($_POST['inputcrud'] == 'N')
		{
			$uuid = $unique;
			$nama_photo = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$extention;
			
			if (!empty($tmp_pegawai)) { //jika ada gambar
				UploadCompress($nama_photo, $folder_foto, $tmp_pegawai);
				$data['photo_pegawai']=$nama_photo;
			}
			
			$data['uuid'] = $uuid;
			unset($data['tanggal_edit']);
			$query = $pos->savePegawai($data);
		}
		else
		{
			$nama_photo = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$extention;
			$photo_pegawai = isset($_POST['photo_value']) ? $_POST['photo_value'] : '';
			
			if (!empty($tmp_pegawai)) { //jika ada gambar
				if($photo_pegawai !=''){
				  unlink($folder_foto.$photo_pegawai);
				}
				UploadCompress($nama_photo, $folder_foto, $tmp_pegawai);
				$data['photo_pegawai']=$nama_photo;
			}
			
			unset($data['input_at']);
			$query = $pos->updatePegawai($jid,$data);
		}
		
		
		
		
		$result['error'] = $query[1];
		$result['crud'] = $crud;
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
		}else{
			$result['status']='NO';
		}
		
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}