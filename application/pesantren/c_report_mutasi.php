<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	if($method == 'getdata'){
		
		$var = array();
		if($_POST['first'] != ''){$var['tanggal_keluar >']=display_to_sql($_POST['first']);}
		if($_POST['last']  != ''){$var['tanggal_keluar <']=display_to_sql($_POST['last']);}
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin']=strtolower($exp[1]);
									  $var['jenjang_pendidikan']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat_kelas']=$_POST['tingkat'];}
		
		$array = $pos->getFilterMutasi($var);
		
		$data = $array[1];
		$i=0;
		foreach ($data as $row) {
			$data[$i]['urutan'] = $i+1;
			$data[$i]['jenjang'] =$row['jenis_jenjang']." ".$row['jenis_kelamin'];
			$data[$i]['tingkat'] ="Tingkat ".$row['tingkat_kelas'];
			$data[$i]['jenis'] =$row['mdk'];
			$data[$i]['tanggal'] = display_to_report($row['tanggal_keluar']);
			$i++;
		}
		$datax = array('data' => $data);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	
	if($method == 'download'){
		
	}
	
} else {
	exit('No direct access allowed.');
}