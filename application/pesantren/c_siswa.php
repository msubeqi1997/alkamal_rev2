<?php
error_reporting(0);
session_start();
require_once ("../main/class_upload.php");
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=$_POST['method'];
	
	$sex = array('1' => 'Putra','0' => 'Putri');
	$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
	$mdk = array('tidak' => 'Bukan MDK', '' => 'MDK', '' => '');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdata'){
		$array = $pos->getDataSiswa();
		
		$data = $array['data'];
		$i=0;
		foreach ($data as $row) {
			$button = '<button  type="submit" id_siswa="'.$row['jid'].'"  title="Tombol edit santri" class="btn btn-sm btn-primary btnedit "  btn-data="edit" id="btnedit'.$row['jid'].'"  ><i class="fa fa-edit"></i></button>';
			$data[$i]['urutan'] = $row['urutan'];
			$data[$i]['id'] = $row['jid'];
			$data[$i]['nis'] = $row['nis'];
			$data[$i]['nama_lengkap'] = $row['nama_lengkap'];
			$data[$i]['address'] = "".$row['alamat']." ".ucwords(strtolower($row['kabupaten_name']))." ".ucwords(strtolower($row['propinsi_name']))."";;
			$data[$i]['tujuan'] = $row['tujuan'];
			$data[$i]['jenjang'] = $row['jenjang'];
			$data[$i]['jenis'] = $row['jenis'];
			$data[$i]['tingkat'] = $row['tingkat_kelas'];
			$data[$i]['button'] = $button;
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'get_form'){
		$mdk = $_POST['nilai'];
		
		$html_sekolah = '';
		$html_sekolah .='<select class="form-control" id="sekolah" name="sekolah" required>
							<option value=""> Pilih sekolah </option>';
		
		if($mdk == 'mdk'){
			$satpen = $pos->getSatuanPendidikan(2);
			foreach($satpen[1] as $row){
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'"> '.$row['nama_sekolah'].' </option>';
			}
		}else{
			$satpen = $pos->getSatuanPendidikan(1);
			foreach($satpen[1] as $row){
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'"> '.$row['nama_sekolah'].' </option>';
			}
		}
		$html_sekolah .='</select>';
		
		$result['tujuan'] = $html_sekolah;
		echo json_encode($result);
	}
	
	if($method == 'get_jenjang'){
		$sekolah = $_POST['sekolah'];
		$kelamin = $_POST['kelamin'];
		$html_jenjang = '';
		if($sekolah != '' && $kelamin != ''){
		  $array = $pos->getJenjangPendidikan($sekolah);
		  $satpen = $array[1]['satuan_pendidikan'];
		  $jenjang = $arrjenjang[$satpen]." ".$sex[$kelamin];
		  $html_jenjang .= '<input type="text" class="form-control" name="jenjang" id="jenjang" value="'.$jenjang.'" readonly>
							<input type="hidden" id="tingkat" name="tingkat" value="'.$satpen.'">';
		}
		$result['jenjang'] = $html_jenjang;
		echo json_encode($result);
	}
	
	if($method == 'get_kabupaten'){
		$propinsi_id = $_POST['propinsi_id'];
		$array = $pos->getKabupaten($propinsi_id);
		$result['data'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'get_name')
	{
		$nomor = $_POST['nomor'];
		$data=$pos->getNamaSiswa($nomor);
		if(!empty($data[1])){
			$output['status']='exist';
			$output['nama']=$data[1]['nama'];
		}else{
			$output['status']='null';
			$output['nama']=NULL;
		}
		echo json_encode($output);
	}
	
	if($method == 'save_item')
	{
		//error_reporting(0);
		$unique = getId();
		$uuid = '';
		$jid = isset($_POST['idsiswa']) ? $_POST['idsiswa'] : '';
		$jenjang['tujuan_sekolah'] = isset($_POST['sekolah']) ? $_POST['sekolah'] : '';
		$jenjang['nis'] = isset($_POST['no_induk']) ? $_POST['no_induk'] : '';
		$jenjang['jenis'] = isset($_POST['mdk']) ? $_POST['mdk'] : '';
		$jenjang['jenjang_pendidikan'] = isset($_POST['tingkat']) ? $_POST['tingkat'] : '';
		$jenjang['tingkat_kelas'] = isset($_POST['tingkat_kelas']) ? $_POST['tingkat_kelas'] : '';
		$jenjang['status_kesiswaan'] = isset($_POST['status_kesiswaan']) ? $_POST['status_kesiswaan'] : '';
		
		$data['nama_lengkap'] = isset($_POST['nama_lengkap']) ? $_POST['nama_lengkap'] : '';
		$data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
		$data['kelamin'] = isset($_POST['kelamin']) ? $_POST['kelamin'] : '';
		$data['tempat_lahir'] = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
		$data['tanggal_lahir'] = isset($_POST['tanggal_lahir']) ? display_to_sql($_POST['tanggal_lahir']) : '';
		$data['saudara'] = isset($_POST['saudara']) ? $_POST['saudara'] : '';
		$data['anak_ke'] = isset($_POST['anak_ke']) ? $_POST['anak_ke'] : '';
		$data['abk'] = isset($_POST['abk']) ? $_POST['abk'] : '';
		$data['propinsi'] = isset($_POST['propinsi']) ? $_POST['propinsi'] : '';
		$data['kabupaten'] = isset($_POST['kabupaten']) ? $_POST['kabupaten'] : '';
		$data['alamat'] = isset($_POST['alamat']) ? $_POST['alamat'] : '';
		$data['no_hp'] = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
		$data['kode_pos'] = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : '';
		
		$data['ayah'] = isset($_POST['nama_ayah']) ? $_POST['nama_ayah'] : '';
		$data['pendidikan_ayah'] = isset($_POST['pendidikan_ayah']) ? $_POST['pendidikan_ayah'] : NULL;
		$data['pekerjaan_ayah'] = isset($_POST['pekerjaan_ayah']) ? $_POST['pekerjaan_ayah'] : '';
		$data['hp_ayah'] = isset($_POST['hp_ayah']) ? $_POST['hp_ayah'] : '';
		
		$data['ibu'] = isset($_POST['nama_ibu']) ? $_POST['nama_ibu'] : '';
		$data['pendidikan_ibu'] = isset($_POST['pendidikan_ibu']) ? $_POST['pendidikan_ibu'] : NULL;
		$data['pekerjaan_ibu'] = isset($_POST['pekerjaan_ibu']) ? $_POST['pekerjaan_ibu'] : '';
		$data['hp_ibu'] = isset($_POST['hp_ibu']) ? $_POST['hp_ibu'] : '';
		
		$data['wali'] = isset($_POST['nama_wali']) ? $_POST['nama_wali'] : '';
		$data['alamat_wali'] = isset($_POST['alamat_wali']) ? $_POST['alamat_wali'] : '';
		$data['pekerjaan_wali'] = isset($_POST['pekerjaan_wali']) ? $_POST['pekerjaan_wali'] : '';
		$data['telp_wali'] = isset($_POST['telp_wali']) ? $_POST['telp_wali'] : '';
		$data['hubungan_wali'] = isset($_POST['hubungan_wali']) ? $_POST['hubungan_wali'] : '';
		
		$data['input_at'] = date ('Y-m-d H:i:s');
		
		$crud = $_POST['inputcrud'];
		$tahun = date('Y');
		
		$folder_foto = '../../files/images_pendaftar/';
		$tmp_siswa = $_FILES['photo_siswa']['tmp_name'];
		$tipe_siswa = $_FILES['photo_siswa']['type'];
		$nama_siswa = $_FILES['photo_siswa']['name'];
		$ext_siswa = pathinfo($nama_siswa, PATHINFO_EXTENSION);
		
		$tmp_wali = $_FILES['photo_wali']['tmp_name'];
		$tipe_wali = $_FILES['photo_wali']['type'];
		$nama_wali = $_FILES['photo_wali']['name'];
		$ext_wali = pathinfo($nama_wali, PATHINFO_EXTENSION);
		
		if($_POST['inputcrud'] == 'N')
		{
			$uuid = $unique;
			$nama_photo_siswa = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_siswa;
			$nama_photo_wali = 'wali_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_wali;
			
			if (!empty($tmp_siswa)) { //jika ada gambar
				UploadCompress($nama_photo_siswa, $folder_foto, $tmp_siswa);
				$data['photo_siswa']=$nama_photo_siswa;
			}
			if (!empty($tmp_wali)) { //jika ada gambar
				UploadCompress($nama_photo_wali, $folder_foto, $tmp_wali);
				$data['photo_wali']=$nama_photo_wali;
			}
			
			$data['uuid'] = $uuid;
			$jenjang['uuid']=$uuid;
			$tingkat['id_siswa']=$uuid;
			$tingkat['tingkat']=isset($_POST['tingkat_kelas']) ? $_POST['tingkat_kelas'] : '';
			
			$query = $pos->saveSiswa($data);
			$insert_jenjang = $pos->insertJenjangSiswa($jenjang);
			$tingkat = $pos->insertTingkatSiswa($tingkat);
		}
		else
		{
			$kode = $pos->getKodeSiswa($jid);
			$uuid = $kode[1]['uuid'];
			$nama_photo_siswa = 'pas_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_siswa;
			$nama_photo_wali = 'wali_'.$uuid.'_'.$data['nama_lengkap'].'.'.$ext_wali;
			
			if (!empty($tmp_siswa)) { //jika ada gambar
				unlink($folder_foto.$kode[1]['photo_siswa']);
				UploadCompress($nama_photo_siswa, $folder_foto, $tmp_siswa);
				$data['photo_siswa']=$nama_photo_siswa;
			}
			if (!empty($tmp_wali)) { //jika ada gambar
		    	unlink($folder_foto.$kode[1]['photo_wali']);
				UploadCompress($nama_photo_wali, $folder_foto, $tmp_wali);
				$data['photo_wali']=$nama_photo_wali;
			}
			
			unset($data['input_at']);
			$query = $pos->updateSiswa($uuid,$data);
			$del_pendidikan = $pos->deleteRiwayatPendidikan($uuid);
			$update_jenjang = $pos->updateJenjangSiswa($jid,$jenjang);
			$update_tingkat = $pos->updateTingkatSiswa($uuid,$jenjang['tingkat_kelas']);
		}
		
		if($query[0] == true){
			//riwayat pendidikan
			$lulus = $_POST['lulus'];
			foreach($_POST['riwayat'] as $key => $val){
				$riwayat['uuid'] = $uuid;
				$riwayat['penyelenggara'] = $val;
				$riwayat['tahun_selesai'] = $lulus[$key];
				$pos->saveRiwayatPendidikan($riwayat);
			}
		}
		
		$result['error'] = $query[1];
		$result['crud'] = $crud;
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
		}else{
			$result['status']='NO';
		}
		
		echo json_encode($result);
	}
	
	if($method == 'select_siswa'){
		$idc = $_POST['siswa_id'];
		$array = $pos->getSiswaPendaftar($idc);
		$array['data'] = $array[1];
		$array['result'] = $array[0];
		echo json_encode($array);
	}
	
	if($method == 'mutasi_siswa'){
		$jid = $_POST['token'];
		$data['tanggal_keluar'] = display_to_sql($_POST['tanggal']);
		$data['alasan_keluar'] = $_POST['alasan'];
		$data['pegawai'] = $_SESSION['sess_id'];
		$data['status_kesiswaan'] = 'keluar';
		$array = $pos->saveMutasiSiswa($jid, $data);
		
			
		$array['status'] = $array[0];
		$array['msg'] = $array[1];
		echo json_encode($array);
	}
	
	if($method == 'update_nis'){
		$target = basename($_FILES['upload_update']['name']) ;
		move_uploaded_file($_FILES['upload_update']['tmp_name'], $target);
		chmod($_FILES['upload_update']['name'],0777);
	}
	
	if($method == 'report_siswax'){
		$array = $pos->getReportSiswa();
		
		$data = $array['data'];
		$i=0;
		foreach ($data as $row) {
			$button = '<button  type="submit" id_siswa="'.$row['jid'].'"  title="Tombol edit santri" class="btn btn-sm btn-primary btnedit "  btn-data="edit" id="btnedit'.$row['jid'].'"  ><i class="fa fa-edit"></i></button>';
			$data[$i]['urutan'] = $row['urutan'];
			$data[$i]['id'] = $row['jid'];
			$data[$i]['nis'] = $row['nis'];
			$data[$i]['nama_lengkap'] = $row['nama_lengkap'];
			$data[$i]['address'] = "".$row['alamat']." ".ucwords(strtolower($row['kabupaten_name']))." ".ucwords(strtolower($row['propinsi_name']))."";;
			$data[$i]['tujuan'] = $row['tujuan'];
			$data[$i]['jenjang'] = $row['jenjang'];
			$data[$i]['jenis'] = $row['jenis'];
			$data[$i]['tingkat'] = $row['tingkat_kelas'];
			$data[$i]['button'] = $button;
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'report_siswa'){
		
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenjang_pendidikan = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		if($_POST['kamar']  != ''){$var['id_kamar = ']=$_POST['kamar'];}
		if($_POST['sekolah']  != ''){$var['tujuan_sekolah = ']=$_POST['sekolah'];}
		//if($_POST['search']  != ''){$var['(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")']='';									
		if($_POST['aktif']  != ''){$var['status = ']=$_POST['aktif'];}
		$search = $_POST['search'];
		
		$array = $pos->getReportSiswa($var,$search);
		
		$siswa = $array[1];
		$i=0;
		foreach ($siswa as $key) {
			$result[$i]['urutan'] = $key['urutan'];
			$result[$i]['nis'] = $key['nis'];
			$result[$i]['nama_lengkap'] = $key['nama_lengkap'];
			$result[$i]['alamat'] = ucwords(strtolower($key['alamat_lengkap']));
			$result[$i]['jenjang'] = strtoupper($key['jenjang']);
			$result[$i]['jenis'] = $mdk[$key['jenis']];
			$result[$i]['tingkat'] = "Tingkat ".$key['tingkat'];
			$result[$i]['asrama'] = $key['nama_asrama'];
			$result[$i]['kamar'] = $key['nama_kamar'];
			$result[$i]['sekolah'] = $key['sekolah_tujuan'];
			$i++;
		}
		$datax = array('data' => $result);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	
	if($method == 'pencarian_siswa'){
		
		$var = array();
		if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
									  $var['jenis_kelamin = ']=strtolower($exp[1]);
									  $var['jenjang_pendidikan = ']=strtolower($exp[0]);
									}
		if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
		if($_POST['tingkat']  != ''){$var['tingkat = ']=$_POST['tingkat'];}
		if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
		if($_POST['kamar']  != ''){$var['id_kamar = ']=$_POST['kamar'];}
		if($_POST['sekolah']  != ''){$var['id_sekolah = ']=$_POST['sekolah'];}
		//if($_POST['search']  != ''){$var['(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")']='';									
		//if($_POST['aktif']  != ''){$var['status = ']=$_POST['aktif'];}
		$search = $_POST['search'];
		$status = $_POST['aktif'];
		
		if ($status == 'siswa'){ 
		  $array = $pos->pencarianSiswa($var,$status,$search);
		}else{
		  $array = $pos->pencarianAlumni($_POST['jenjang'],$status,$search);
		}
		
		$siswa = $array[1];
		$i=0;
		foreach ($siswa as $key) {
			$alamat = "".$key['alamat']." ".$key['kabupaten_name']." ".$key['propinsi_name']."";
			$jenjang = $key['jenjang_pendidikan']." ".$key['jenis_kelamin'];
			$result[$i]['urutan'] = $key['urutan'];
			$result[$i]['nis'] = "<a class='product-title btn-nis' id-data=".base64_encode($key['uuid']).">".$key['nis']."</a>";
			$result[$i]['nama_lengkap'] = "<a class='product-title btn-nama' id-data=".base64_encode($key['uuid']).">".$key['nama_lengkap']."</a>";
			$result[$i]['alamat'] = ucwords(strtolower($alamat));
			$result[$i]['jenjang'] = strtoupper($jenjang);
			$result[$i]['jenis'] = $mdk[$key['jenis']];
			$result[$i]['tingkat'] = "Tingkat ".$key['tingkat'];
			$result[$i]['asrama'] = $key['nama_asrama'];
			$result[$i]['kamar'] = "<a class='product-title btn-kamar' id-kamar=".base64_encode($key['id_kamar']).">".$key['nama_kamar']."</a>";
			$result[$i]['sekolah'] = $key['sekolah_tujuan'];
			$i++;
		}
		$datax = array('data' => $result);
		//$datax = $array[2];
		echo json_encode($datax);
	}
	
} else {
	exit('No direct access allowed.');
}