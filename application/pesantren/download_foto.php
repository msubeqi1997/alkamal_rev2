<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
include '../../plugins/excel/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Protection;

$method=$_POST['method'];
if($method == 'import'){
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
		
	$pos = new model_data();
	
	
	$sex = array('putra' => '1','putri' => '0');
	$arrjenjang = array('ula' => '2', 'wustho' => '3');
	$kelamin = array('1' => 'Putra','0' => 'Putri');
	$jk = array('1' => 'Laki-laki','0' => 'Perempuan');
	$jenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	$pendidikan = array(''=>'','1'=>'SD/Sederajat','2'=>'SMP/Sederajat','3'=>'SMA/Sederajat','4'=>'Diploma','5'=>'S1','6'=>'S2','7'=>'S3');
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$var = array();
	if($_POST['jenjang'] != '')	{ $exp = explode(' ',$_POST['jenjang']); 
								  $var['jenis_kelamin = ']=strtolower($exp[1]);
								  $var['jenjang_pendidikan = ']=strtolower($exp[0]);
								}
	if($_POST['jenis']  != ''){$var['jenis = ']=$_POST['jenis'];}
	if($_POST['tingkat_kelas']  != ''){$var['tingkat = ']=$_POST['tingkat_kelas'];}
	if($_POST['asrama']  != ''){$var['id_asrama = ']=$_POST['asrama'];}
	if($_POST['kamar']  != ''){$var['id_kamar = ']=$_POST['kamar'];}
	if($_POST['sekolah_tujuan']  != ''){$var['tujuan_sekolah = ']=$_POST['sekolah_tujuan'];}
	//if($_POST['search']  != ''){$var['(nama_lengkap LIKE "%'.$_POST['search'].'%" OR nis LIKE "%'.$_POST['search'].'%")']='';									
	if($_POST['aktif']  != ''){$var['status = ']=$_POST['aktif'];}
	$search = $_POST['txtname'];
	$array = $pos->getReportSiswa($var,$search);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'ID')
	->setCellValue('B1', 'NIS')
	->setCellValue('C1', 'JENJANG')
	->setCellValue('D1', 'MDK')
	->setCellValue('E1', 'TINGKAT KELAS')
	->setCellValue('F1', 'SEKOLAH TUJUAN')
	->setCellValue('G1', 'NAMA LENGKAP')
	->setCellValue('H1', 'TEMPAT LAHIR')
	->setCellValue('I1', 'TANGGAL LAHIR')
	->setCellValue('J1', 'KELAMIN')
	->setCellValue('K1', 'ALAMAT')
	->setCellValue('L1', 'KABUPATEN')
	->setCellValue('M1', 'PROPINSI')
	->setCellValue('N1', 'JUMLAH SAUDARA')
	->setCellValue('O1', 'ANAK KE')
	->setCellValue('P1', 'RIWAYAT PENYAKIT')
	->setCellValue('Q1', 'NO HP')
	->setCellValue('R1', 'KODE POS')
	->setCellValue('S1', 'NAMA AYAH')
	->setCellValue('T1', 'PENDIDIKAN AYAH')
	->setCellValue('U1', 'PEKERJAAN AYAH')
	->setCellValue('V1', 'HP AYAH')
	->setCellValue('W1', 'NAMA IBU')
	->setCellValue('X1', 'PENDIDIKAN IBU')
	->setCellValue('Y1', 'PEKERJAAN IBU')
	->setCellValue('Z1', 'HP IBU')
	->setCellValue('AA1', 'NAMA WALI')
	->setCellValue('AB1', 'PEKERJAAN WALI')
	->setCellValue('AC1', 'HP WALI')
	->setCellValue('AD1', 'ALAMAT WALI')
	->setCellValue('AE1', 'HUBUNGAN KELUARGA')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=2; 
	foreach($data as $row) {
	  $spreadsheet->setActiveSheetIndex(0)
	  ->setCellValue('A'.$i, $row['urutan'])
	  ->setCellValue('B'.$i, $row['nis'])
	  ->setCellValue('C'.$i, $row['jenjang'])
	  ->setCellValue('D'.$i, $row['jenis'])
	  ->setCellValue('E'.$i, "Tingkat ".$row['tingkat'])
	  ->setCellValue('F'.$i, $row['sekolah_tujuan'])
	  ->setCellValue('G'.$i, $row['nama_lengkap'])
	  ->setCellValue('H'.$i, $row['tempat_lahir'])
	  ->setCellValue('I'.$i, display_to_report($row['tanggal_lahir']))
	  ->setCellValue('J'.$i, $jk[$row['kelamin']])
	  ->setCellValue('K'.$i, $row['alamat'])
	  ->setCellValue('L'.$i, $row['kabupaten_name'])
	  ->setCellValue('M'.$i, $row['propinsi_name'])
	  ->setCellValue('N'.$i, $row['saudara'])
	  ->setCellValue('O'.$i, $row['anak_ke'])
	  ->setCellValue('P'.$i, $row['abk'])
	  ->setCellValue('Q'.$i, $row['no_hp'])
	  ->setCellValue('R'.$i, $row['kode_pos'])
	  ->setCellValue('S'.$i, $row['ayah'])
	  ->setCellValue('T'.$i, $pendidikan[$row['pendidikan_ayah']])
	  ->setCellValue('U'.$i, $row['pekerjaan_ayah'])
	  ->setCellValue('V'.$i, $row['hp_ayah'])
	  ->setCellValue('W'.$i, $row['ibu'])
	  ->setCellValue('X'.$i, $pendidikan[$row['pendidikan_ibu']])
	  ->setCellValue('Y'.$i, $row['pekerjaan_ibu'])
	  ->setCellValue('Z'.$i, $row['hp_ibu'])
	  ->setCellValue('AA'.$i, $row['wali'])
	  ->setCellValue('AB'.$i, $row['pekerjaan_wali'])
	  ->setCellValue('AC'.$i, $row['telp_wali'])
	  ->setCellValue('AD'.$i, $row['alamat_wali'])
	  ->setCellValue('AE'.$i, $row['hubungan_wali'])
	  ;
	$i++; $num++;
	}
	
	
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('data');
	$spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
	$spreadsheet->getActiveSheet()->getStyle('C1:AD'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
	$spreadsheet->getActiveSheet()->getStyle('B2:B'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
} else{
	echo "Not allowed direct access.";
}