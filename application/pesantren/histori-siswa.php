<?php 
$titlepage="Histori Santri";
$idsmenu='75'; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";

 
?>
  <div class="content">
    <section class="content-header">
	  <h1>
		Data histori santri
	  </h1>
	  
	</section>
	<section class="content">
	<div class="row">
	  <div class="col-md-12">
		<div class="nav-tabs-custom">
		  <ul class="nav nav-tabs">
			<li class="active"><a href="#data-diri" data-toggle="tab">Data diri</a></li>
			<li><a href="#konseling" data-toggle="tab">Pelanggaran</a></li>
			<li><a href="#super" data-toggle="tab">Surat peringatan</a></li>
			<li><a href="#panggilan" data-toggle="tab">Panggilan orang tua</a></li>
			<li><a href="#skorsing" data-toggle="tab">Skorsing</a></li>
			<li><a href="#intensif" data-toggle="tab">Intensif bahasa</a></li>
			<li><a href="#ubudiyah" data-toggle="tab">Ubudiyah</a></li>
			<li><a href="#ekstra" data-toggle="tab">Ekstrakurikuler</a></li>
			<li><a href="#madin" data-toggle="tab">Madrasah diniyah</a></li>
		  </ul>
		  <div class="tab-content">
			<div class="tab-pane active" id="data-diri">
			  a
			</div>
			<div class="tab-pane " id="konseling">
				b
			</div>
			<div class="tab-pane" id="super">
				c
			</div>
			<div class="tab-pane" id="panggilan">
				d
			</div>
			<div class="tab-pane" id="skorsing">
				e
			</div>
			<div class="tab-pane" id="intensif">
				f
			</div>
			<div class="tab-pane" id="ubudiyah">
				g
			</div>
			<div class="tab-pane" id="ekstra">
				h
			</div>
			<div class="tab-pane" id="madin">
				i
			</div>
		  </div>
		</div>
	  </div>
	</div>
	</section><!-- /.content -->
	
  </div>
  
	<?php //include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			
		});
		
	
	</script>
</body>
</html>
