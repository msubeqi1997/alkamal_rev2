<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
    $pos = new model_data();
    
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$array = $pos->getPengurusMarkazy();
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'PENGURUS MARKAZY')
	->setCellValue('A5', 'NO')
    ->setCellValue('B5', 'PENGASUH 1')
    ->setCellValue('C5', 'PENGASUH 2')
    ->setCellValue('D5', 'PENGASUH 3')
    ->setCellValue('E5', 'PEMBINA 1')
    ->setCellValue('F5', 'PEMBINA 2')
    ->setCellValue('G5', 'PEMBINA 3')
    ->setCellValue('H5', 'KETUA')
    ->setCellValue('I5', 'WAKIL')
    ->setCellValue('J5', 'SEKRETARIS')
    ->setCellValue('K5', 'BENDAHARA')
    ->setCellValue('L5', 'KEAMANAN 1')
    ->setCellValue('M5', 'KEAMANAN 2')
    ->setCellValue('N5', 'BAHASA 1')
    ->setCellValue('O5', 'BAHASA 2')
    ->setCellValue('P5', 'PENDIDIKAN 1')
    ->setCellValue('Q5', 'PENDIDIKAN 2')
    ->setCellValue('R5', 'ORKESIH 1')
    ->setCellValue('S5', 'ORKESIH 2')
    ->setCellValue('T5', 'HUMAS 1')
    ->setCellValue('U5', 'HUMAS 2')
    ->setCellValue('V5', 'SARPRAS 1')
    ->setCellValue('W5', 'SARPRAS 2')
    ->setCellValue('X5', 'MPK 1')
    ->setCellValue('Y5', 'MPK 2')
    ->setCellValue('Z5', 'IT 1')
    ->setCellValue('AA5', 'IT 2')
    ->setCellValue('AB5', 'PEMBANGUNAN 1')
    ->setCellValue('AC5', 'PEMBANGUNAN 2')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=6;
		
	foreach($data as $row) {
		
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('A'.$i, $num)
          ->setCellValue('B'.$i, $row['pengasuh_1'])
          ->setCellValue('C'.$i, $row['pengasuh_2'])
          ->setCellValue('D'.$i, $row['pengasuh_3'])
          ->setCellValue('E'.$i, $row['pembina_1'])
          ->setCellValue('F'.$i, $row['pembina_2'])
          ->setCellValue('G'.$i, $row['pembina_3'])
          ->setCellValue('H'.$i, $row['ketua'])
          ->setCellValue('I'.$i, $row['wakil'])
          ->setCellValue('J'.$i, $row['sekretaris'])
          ->setCellValue('K'.$i, $row['bendahara'])
          ->setCellValue('L'.$i, $row['keamanan_1'])
          ->setCellValue('M'.$i, $row['keamanan_2'])
          ->setCellValue('N'.$i, $row['bahasa_1'])
          ->setCellValue('O'.$i, $row['bahasa_2'])
          ->setCellValue('P'.$i, $row['pendidikan_1'])
          ->setCellValue('Q'.$i, $row['pendidikan_2'])
          ->setCellValue('R'.$i, $row['orkesih_1'])
          ->setCellValue('S'.$i, $row['orkesih_2'])
          ->setCellValue('T'.$i, $row['humas_1'])
          ->setCellValue('U'.$i, $row['humas_2'])
          ->setCellValue('V'.$i, $row['sarpras_1'])
          ->setCellValue('W'.$i, $row['sarpras_2'])
          ->setCellValue('X'.$i, $row['mpk_1'])
          ->setCellValue('Y'.$i, $row['mpk_2'])
          ->setCellValue('Z'.$i, $row['it_1'])
          ->setCellValue('AA'.$i, $row['it_2'])
          ->setCellValue('AB'.$i, $row['pembangunan_1'])
          ->setCellValue('AC'.$i, $row['pembangunan_2'])        
		;
		$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	