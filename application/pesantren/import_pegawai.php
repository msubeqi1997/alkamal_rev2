<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;

		$method=$_POST['method'];
if($method == 'import'){
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	$agama = ['1'=> 'ISLAM'];
	$kelamin = array('1' => 'laki laki','0' => 'Perempuan');
	$pendidikan = array(''=>'','1'=>'SD/Sederajat','2'=>'SMP/Sederajat','3'=>'SMA/Sederajat','4'=>'Diploma','5'=>'S1','6'=>'S2','7'=>'S3');
	

	$pos = new model_data();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$array = $pos->getDataPegawaiImport();
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'NO')
	->setCellValue('B1', 'NIP')
	->setCellValue('C1', 'NAMA LENGKAP')
	->setCellValue('D1', 'JENIS KELAMIN')
	->setCellValue('E1', 'TEMPAT LAHIR')
	->setCellValue('F1', 'TANGGAL LAHIR')
	->setCellValue('G1', 'AGAMA')
	->setCellValue('H1', 'ALAMAT')
	->setCellValue('I1', 'KODE POS')
	->setCellValue('J1', 'NO HP')
	->setCellValue('K1', 'PENDIDIKAN')
	->setCellValue('L1', 'JURUSAN')
	->setCellValue('M1', 'LULUS')
	->setCellValue('N1', 'EMAIL')
	->setCellValue('O1', 'NIKAH')

	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=2;
		
	foreach($data as $row) {
		
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('A'.$i, $num)
		  ->setCellValue('B'.$i, $row['nip'])
		  ->setCellValue('C'.$i, $row['nama_lengkap'])
		  ->setCellValue('D'.$i, $kelamin[$row['kelamin']])
		  ->setCellValue('E'.$i, $row['tempat_lahir'])
		  ->setCellValue('F'.$i, display_to_report($row['tanggal_lahir']))
		  ->setCellValue('G'.$i, 'ISLAM')
		  ->setCellValue('H'.$i, $row['alamat'])
		  ->setCellValue('I'.$i, $row['kode_pos'])
		  ->setCellValue('J'.$i, $row['no_hp'])
		  ->setCellValue('K'.$i, $row['tingkat_pendidikan'])
		  ->setCellValue('L'.$i, $row['jurusan'])
		  ->setCellValue('M'.$i, $row['lulus'])
		  ->setCellValue('N'.$i, $row['email'])
		  ->setCellValue('O'.$i, $row['nikah'])


		;
		$i++; $num++;
	}
$spreadsheet->getActiveSheet()->setTitle('data pegawai');
$spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
$spreadsheet->getActiveSheet()->getStyle('C1:AD'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
$spreadsheet->getActiveSheet()->getStyle('B2:B'.$i)->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);
$spreadsheet->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
ob_end_clean();
$writer->save('php://output');
exit;
} else{
echo "Not allowed direct access.";
}