<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
    $pos = new model_data();
   
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$array = $pos->getPengurusAsrama($idc);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'PENGURUS FIRQOH')
	->setCellValue('A5', 'NO')
    ->setCellValue('B5', 'KETUA')
    ->setCellValue('C5', 'SEKRETARIS')
    ->setCellValue('D5', 'BENDAHARA 1')
    ->setCellValue('E5', 'BENDAHARA 2')
    ->setCellValue('F5', 'KEAMANAN KOORDINATOR')
    ->setCellValue('G5', 'KEAMANAN 1')
    ->setCellValue('H5', 'KEAMANAN 2')
    ->setCellValue('I5', 'KEAMANAN 3')
    ->setCellValue('J5', 'KEAMANAN 4')
    ->setCellValue('K5', 'BAHASA KOORDINATOR')
    ->setCellValue('L5', 'BAHASA 1')
    ->setCellValue('M5', 'BAHASA 2')
    ->setCellValue('N5', 'BAHASA 3')
    ->setCellValue('O5', 'BAHASA 4')
    ->setCellValue('P5', 'PENDIDIKAN KOORDINATOR')
    ->setCellValue('Q5', 'PENDIDIKAN 1')
    ->setCellValue('R5', 'PENDIDIKAN 2')
    ->setCellValue('S5', 'PENDIDIKAN 3')
    ->setCellValue('T5', 'PENDIDIKAN 4')
    ->setCellValue('U5', 'ORKESIH KOORDINATOR')
    ->setCellValue('V5', 'ORKESIH 1')
    ->setCellValue('W5', 'ORKESIH 2')
    ->setCellValue('X5', 'ORKESIH 3')
    ->setCellValue('Y5', 'ORKESIH 4')
    ->setCellValue('Z5', 'HUMAS KOORDINATOR')
    ->setCellValue('AA5', 'HUMAS 1')
    ->setCellValue('AB5', 'HUMAS 2')
    ->setCellValue('AC5', 'HUMAS 3')
    ->setCellValue('AD5', 'HUMAS 4')
    ->setCellValue('AE5', 'MPK KOORDINATOR')
    ->setCellValue('AF5', 'MPK 1')
    ->setCellValue('AG5', 'MPK 2')
    ->setCellValue('AH5', 'MPK 3')
    ->setCellValue('AI5', 'MPK 4')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=6;
		
	foreach($data as $row) {
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('A'.$i, $num)
          ->setCellValue('B'.$i, $row['ketua'])
          ->setCellValue('C'.$i, $row['sekretaris'])
          ->setCellValue('D'.$i, $row['bendahara_1'])
          ->setCellValue('E'.$i, $row['bendahara_2'])
          ->setCellValue('F'.$i, $row['keamanan_ko'])
          ->setCellValue('G'.$i, $row['keamanan_1'])
          ->setCellValue('H'.$i, $row['keamanan_2'])
          ->setCellValue('I'.$i, $row['keamanan_3'])
          ->setCellValue('J'.$i, $row['keamanan_4'])
          ->setCellValue('K'.$i, $row['bahasa_ko'])
          ->setCellValue('L'.$i, $row['bahasa_1'])
          ->setCellValue('M'.$i, $row['bahasa_2'])
          ->setCellValue('N'.$i, $row['bahasa_3'])
          ->setCellValue('O'.$i, $row['bahasa_4'])
          ->setCellValue('P'.$i, $row['pendidikan_ko'])
          ->setCellValue('Q'.$i, $row['pendidikan_1'])
          ->setCellValue('R'.$i, $row['pendidikan_2'])
          ->setCellValue('S'.$i, $row['pendidikan_3'])
          ->setCellValue('T'.$i, $row['pendidikan_4'])
          ->setCellValue('U'.$i, $row['orkesih_ko'])
          ->setCellValue('V'.$i, $row['orkesih_1'])
          ->setCellValue('W'.$i, $row['orkesih_2'])
          ->setCellValue('X'.$i, $row['orkesih_3'])
          ->setCellValue('Y'.$i, $row['orkesih_4'])
          ->setCellValue('Z'.$i, $row['humas_ko'])
          ->setCellValue('AA'.$i, $row['humas_1'])
          ->setCellValue('AB'.$i, $row['humas_2'])
          ->setCellValue('AC'.$i, $row['humas_3']) 
          ->setCellValue('AD'.$i, $row['humas_4']) 
          ->setCellValue('AE'.$i, $row['mpk_ko']) 
          ->setCellValue('AF'.$i, $row['mpk_1']) 
          ->setCellValue('AG'.$i, $row['mpk_2']) 
          ->setCellValue('AH'.$i, $row['mpk_3']) 
          ->setCellValue('AI'.$i, $row['mpk_4'])         
		;
		$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	