<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
include '../../plugins/excel/vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Reader\Csv;
	use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
	use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
	
$method=$_POST['method'];
if($method == 'upload_nis'){
	
	$pos = new model_data();
	$array = $pos->getJenjangSiswa();
	$data = $array[1];
	
	$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	$sheetname = 'data';
	if(isset($_FILES['file']['name']) && in_array($_FILES['file']['type'], $file_mimes)) {
	 
		$arr_file = explode('.', $_FILES['file']['name']);
		$extension = end($arr_file);
	 
		if('csv' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		} else {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		$reader->setLoadSheetsOnly($sheetname);
		
		$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
		 
		$sheetData = $spreadsheet->getActiveSheet()->toArray();
		$SheetDataKey = array();
		$createArray = array('ID', 'NIS');
		
		$arrayCount = count($sheetData);
		foreach($sheetData as $row) {
			foreach ($row as $key => $value) {
				if (in_array(trim($value), $createArray)) {
					$value = preg_replace('/\s+/', '', $value);
					$SheetDataKey[trim($value)] = $key;
				} 
			}
		}
		
		$id_siswa=array();
		for($i = 1; $i < $arrayCount-1; $i++){
			$nis = $SheetDataKey['NIS'];
			$id = $SheetDataKey['ID'];
			$id_siswa[$sheetData[$i][$id]] = $sheetData[$i][$nis];
			//echo "".$sheetData[$i][$id]." | ".$sheetData[$i][$nis]."</br>";
		}
		
		
		$query="INSERT INTO z_jenjang_siswa (id,nis,uuid,jenjang_pendidikan,jenis,tujuan_sekolah,tingkat_kelas,group_kelas,group_kamar,status_kesiswaan,tanggal_keluar,pegawai,alasan_keluar) VALUES";
		$values = array();
		
		foreach($data as $row){
			if(array_key_exists($row['id_siswa'], $id_siswa)){
				$values[]="(".$row['id'].",".var_export($id_siswa[$row['id_siswa']], true).",'".$row['uuid']."','".$row['jenjang_pendidikan']."','".$row['jenis']."','".$row['tujuan_sekolah']."','".$row['tingkat_kelas']."','".$row['group_kelas']."',
				'".$row['group_kamar']."','".$row['status_kesiswaan']."',".var_export($row['tanggal_keluar'], true).",".var_export($row['pegawai'], true).",".var_export($row['alasan_keluar'], true).")";
			}
		}
		
		$query .= implode(',',$values);
		$query .=" ON DUPLICATE KEY UPDATE nis=VALUES(nis)";
		/*
		$query .=" ON DUPLICATE KEY UPDATE
		nis=VALUES(nis), uuid=VALUES(uuid), jenjang_pendidikan=VALUES(jenjang_pendidikan), jenis=VALUES(jenis), tujuan_sekolah=VALUES(tujuan_sekolah), tingkat_kelas=VALUES(tingkat_kelas), group_kelas=VALUES(group_kelas), group_kamar=VALUES(group_kamar), status_kesiswaan=VALUES(status_kesiswaan)
		, tanggal_keluar=VALUES(tanggal_keluar), pegawai=VALUES(pegawai), alasan_keluar=VALUES(alasan_keluar);";
		*/
		$upd = $pos->updateNIS($query);
		$result['result'] = $upd[0];
		$result['error'] = $upd[1];
		echo json_encode($result);
	}
} else{
	echo "Not allowed direct access.";
}

