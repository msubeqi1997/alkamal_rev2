<?php 
$titlepage="Daftar Firqoh";
$idsmenu=22; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content-header">
  <h1>
	FIRQOH
	<small>management firqoh</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Daftar Firqoh</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Firqoh</button>
					<br><br>
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					  <button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Nama Firqoh </th>
							<th>Kode Firqoh </th>
							<th>Tipe </th>
							<th>Pembina Firqoh </th>
							<th>Jumlah Kamar </th>
							<th>Pengurus </th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah Firqoh</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Nama Firqoh</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kode Firqoh</label>
							<div class="col-sm-9">
								<input type="text" class="form-control " id="txtkode" name="txtkode" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Tipe</label>
							<div class="col-sm-9">
								<div class="radio">
									<label>
									  <input type="radio" name="tipe" id="putra" value="1">
									  Putra
									</label>
								</div>
								<div class="radio">
									<label>
									  <input type="radio" name="tipe" id="putri" value="0">
									  Putri
									</label>
								</div>
							</div>
						</div>
						<div class="form-group new"> <label class="col-sm-3  control-label">Pembina</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optpembina" name="optpembina" >
									<option value=""> Pilih Pembina </option>
									
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_asrama.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "nama_asrama" },
				{ "data": "kode_firqoh" },
				{ "data": "tipe_firqoh" },
				{ "data": "pembina" },
				{ "data": "jumlah_kamar" },
				{ "data": "pengurus" },
				{ "data": "button" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			pembina(crud,item);
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#txtkode").val("");
			$("input[name=tipe]").prop("checked",false);
			$("#optpembina").html("");
			$("#head-modal").html("Tambah Firqoh");
			set_focus("#txtname");
			
		}
		
		$(document).on( "click",".btnedit", function() {
			$("#head-modal").html("Edit Firqoh");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail"
			};
			$.ajax(
			{
				url : "c_asrama.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.msg;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.auto);
					$("#txtname").val(data.nama_asrama);
					$("#txtkode").val(data.kode_firqoh);
					$("input[name='tipe'][value='"+data.tipe+"']").prop('checked', true);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					
					pembina(crud,data.wali_asrama);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		function pembina(crud,wali){
			var selected = '';
			$('#optpembina').html('');
			var value = {
				method : "get_pembina"
			};
			$.ajax(
			{
				url : "c_asrama.php",
				type: "POST",
				data : value,
				dataType: 'JSON',
				success: function(respons, textStatus, jqXHR)
				{
				  if(crud=='N'){
					$('#optpembina').append('<option value="" > Pilih Pembina </option>');
					$.each(respons.data, function (key, val) {
						$('#optpembina').append('<option value="' + val.id_pegawai + '" >' + val.nama_lengkap + '</option>');
					})
				  }else{
					
					$('#optpembina').append('<option value="" > Pilih Pembina </option>');
					$.each(respons.data, function (key, val) {
						if(val.id_pegawai == wali){selected = 'selected';}else{selected = '';}
						$('#optpembina').append('<option value="' + val.id_pegawai + '" '+ selected +'>' + val.nama_lengkap + '</option>');
					})
				  }
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		$(document).on( "click",".btnpengurus", function() {
			var id = $(this).attr('id_item');
			
			var mapForm = document.createElement("form");
			mapForm.target = "";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "v_editpengurus.php";
			
			var txtid = document.createElement("input");
			txtid.type = "text";
			txtid.name = "id_asrama";
			txtid.value = id;
			mapForm.appendChild(txtid);
			
			document.body.appendChild(mapForm);

			mapForm.submit();
		});
	
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var kode = $("#txtkode").val();
			var tipe = document.querySelector('input[name="tipe"]:checked').value;
			var pembina = $("#optpembina").val(); 
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Nama Firqoh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			if(tipe == '' || tipe== null ){
				$.notify({
					message: "Tipe belum dipilih kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				kode: kode,
				tipe: tipe,
				pembina:pembina,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_asrama.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});

		$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_asrama.php";
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
		
	</script>
</body>
</html>
