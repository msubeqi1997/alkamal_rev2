<?php ob_start(); 
$titlepage="Data Pegawai";
$idsmenu=17; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];
$propinsi = $pos->getPropinsi();

if($_POST['method'] == 'E'){
	$idc = $_POST['idpegawai'];
	$array = $pos->getDetailPegawai($idc);
	
	$data = $array[1];
	if(empty($data)){
		header("Location:v_pegawai.php");
		die();
	}else{
		
	}
}
?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	PEGAWAI
	<small>edit data personal</small>
  </h1>
</section>
<section class="content">
	<form method="post" id="target" action="c_pegawai.php" enctype="multipart/form-data">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="<?php echo (isset($data))?'E':'N'; ?>">
	<input type="hidden" id="id" name="idpegawai" class="" value="<?php echo (isset($data))?$data['id_pegawai']:''; ?>">
	<input type="hidden" name="method" class="" value="save_item">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo (isset($data))?'Edit':'Tambah';?> Data Pegawai</h3>
		</div>
		<!--./ box header-->
		<div class="box-body form-horizontal">
		  <div class="row">
			<div class="col-md-8">
				<div class="form-group"> <label class="col-sm-3  control-label">No Induk Pegawai<span class="text-danger">*</span></label>
					<div class="col-sm-9"><input type="text" class="form-control " id="no_induk" name="no_induk" value="<?php if(isset($data)) echo $data['nip'];?>" placeholder=""> </div>
				</div>
				<div class="form-group"> <label class="col-sm-3  control-label">Nama lengkap<span class="text-danger">*</span></label>
					<div class="col-sm-9"><input type="text" class="form-control " id="nama_lengkap" name="nama_lengkap" value="<?php if(isset($data)) echo $data['nama_lengkap'];?>" placeholder="Isikan nama lengkap" required=""> </div>
				</div>
				<div class="form-group">
				  <label for="email" class="control-label col-sm-3">Email</label>
				  <div class="col-sm-9">
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php if(isset($data)) echo $data['email'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="j-kelamin" class="control-label col-sm-3">Jenis kelamin<span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<div class="radio">
					  <label><input type="radio" id="kelamin" name="kelamin" value="1" required <?php if(isset($data)) {echo ($data['kelamin']=='1')?'checked':'' ;}?>> Laki-laki</label>
					  <label><input type="radio" id="kelamin" name="kelamin" value="0" required <?php if(isset($data)) {echo ($data['kelamin']=='0')?'checked':'' ;}?>> Perempuan</label>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="tempatlahir" class="control-label col-sm-3">Tempat & Tanggal lahir <span class="text-danger"> *</span></label>
				  <div class="col-sm-5">
					<input type="text" class="form-control letters" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php if(isset($data)) echo $data['tempat_lahir'];?>" required>
				  </div>
				  <div class="col-sm-4">
					<div class="input-group date" id="datepicker">
					  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir" id="tanggal_lahir" placeholder="dd-mm-yyyy" value="<?php if(isset($data)) echo display_to_report($data['tanggal_lahir']);?>" readonly required>
					  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="propinsi" class="control-label col-sm-3">Propinsi <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<select class="form-control" id="propinsi" name="propinsi" required>
						<option value=""> Pilih propinsi </option>
						<?php
						foreach ($propinsi[1] as $opt) {
						$selected=(isset($data))?($data['propinsi']==$opt['id'])?'selected':'':'';
						echo "<option value='".$opt['id']."' ".$selected."> ".$opt['name']." </option>";
						}
						?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="kabupaten" class="control-label col-sm-3">Kabupaten <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<select class="form-control" id="kabupaten" name="kabupaten" required>
					<?php if(isset($data)){
						echo '<option value=""> Pilih kabupaten </option>';
						$regencies = $pos->getKabupaten($data['propinsi']);
						foreach($regencies[1] as $row){
							$selected=($data['kabupaten']==$row['id'])?'selected':'';
							echo '<option value="'.$row['id'].'" '.$selected.'> '.$row['name'].' </option>';
						}
					}else{ echo '<option value=""> Pilih propinsi dahulu </option>';}	?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="alamat" class="control-label col-sm-3">Alamat tempat tinggal <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="alamat" id="alamat" placeholder="RT RW / Nama jalan nomor rumah, kecamatan, kelurahan" value="<?php if(isset($data)) echo $data['alamat'];?>" required>
				  </div>
				</div>
				<div class="form-group">
				  <label for="no_hp" class="control-label col-sm-3">Handphone yang aktif</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['no_hp'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="kode_pos" class="control-label col-sm-3">Kode POS </label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode POS" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['kode_pos'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="saudara" class="control-label col-sm-3">Pendidikan terakhir </label>
				  <div class="col-sm-5">
					<select class="form-control" id="pendidikan" name="pendidikan" >
						<option value=""> Tingkat pendidikan </option>
						<?php
						foreach ($pendidikan as $opt) {
						$selected=(isset($data))?($data['pendidikan']==$opt['id'])?'selected':'':'';
						echo "<option value='".$opt['id']."' ".$selected."> ".$opt['pendidikan']." </option>";
						}
						?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="saudara" class="control-label col-sm-3">Jurusan </label>
				  <div class="col-sm-5">
					<input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Ex: Pendidikan Bahasa Indonesia" value="<?php if(isset($data)) echo $data['jurusan'];?>">
				  </div>
				  <div class="col-sm-4">
					<label for="anakke" class="control-label col-sm-4">Tahun </label>
					<div class="col-sm-8">
					  <input type="text" class="form-control" id="tahun-lulus" name="lulus" placeholder="ex: 2019" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['lulus'];?>">
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="nikah" class="control-label col-sm-3">Status Pernikahan</label>
				  <div class="col-sm-9">
					<div class="radio">
					  <label><input type="radio" id="nikah" name="nikah" value="belum" required <?php if(isset($data)) {echo ($data['nikah']=='belum')?'checked':'' ;}?>> Belum</label>
					  <label><input type="radio" id="nikah" name="nikah" value="sudah" required <?php if(isset($data)) {echo ($data['nikah']=='sudah')?'checked':'' ;}?>> Sudah</label>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="status" class="control-label col-sm-3">Status <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<div class="radio">
					  <label><input type="radio" id="status_kepegawaian" name="status_kepegawaian" value="aktif" required <?php if(isset($data)) {echo ($data['aktif']=='aktif')?'checked':'' ;} else{ echo 'checked';}?>> Aktif</label>
					  <?php if($_POST['method'] == 'E'){ ?>
					  <label><input type="radio" id="status_kepegawaian" name="status_kepegawaian" value="keluar" required <?php if(isset($data)) {echo ($data['aktif']=='keluar')?'checked':'' ;}?>> Keluar</label>
					  <label><input type="radio" id="status_kepegawaian" name="status_kepegawaian" value="pensiun" required <?php if(isset($data)) {echo ($data['aktif']=='pensiun')?'checked':'' ;}?>> Pensiun</label>
					  <?php } ?>
					</div>
				  </div>
				</div>
			</div><!-- /.col -->
			<div class="col-md-4">
				<div class="hero-widget well well-md box-profile">
					<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_pegawai" alt="foto 3x4" <?php if(isset($data)) echo 'src="../../files/images_pegawai/'.$data['photo_pegawai'].'"';?>>
					<label for="upload_siswa">Foto</label>
					<input type="file" name="photo_pegawai" id="upload_pegawai" accept="image/*" />
					<input type="hidden" name="photo_value"  value="<?php if(isset($data)) echo $data['photo_pegawai'];?>"/>
				</div><!-- /.box -->
			</div><!-- /.col -->
			
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer">
		  <span class="text-danger">*</span> ) harus diisi
		</div>
	</div><!-- /.box -->
	
	
	<div class="box box-widget">
	  <div class="box-footer">
		<button type="button" title="Back Button" class="btn btn-primary " onclick="goBack()" name=""><i class="fa fa-back"></i> Back</button>
		<button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Simpan </button>
		<span id="infoproses"></span>
	  </div><!-- /.box-footer -->
	</div><!-- /.box -->
	</form>			
</section><!-- /.content -->

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}


function bigimage(id){
	//var x = document.getElementsById().getAttribute("class");
	$('.imagepreview').attr('src', $(id).attr('src'));
	$('#imagemodal').modal('show');   
}

	var fileTag = document.getElementById("upload_pegawai"),
	preview = document.getElementById("preview_pegawai");

	fileTag.addEventListener("change", function() {
	  changeImage(this,preview);
	});
	
	function changeImage(input,output) {
	  var reader;

	  if (input.files && input.files[0]) {
		reader = new FileReader();

		reader.onload = function(e) {
		  output.setAttribute('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	  }
	}	
	
    $(function () {
		
		$('#propinsi').on('change', function() {
			var value = {
				propinsi_id: this.value,
				method : "get_kabupaten"
			};
			$.ajax({
				url : "c_pegawai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil[1].length>0){
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih kabupaten </option>");
						$.each(hasil[1], function (key, val) {
						  $('#kabupaten').append('<option value="'+val.id+'">'+val.name+'</option>');
						})
					}else{
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih propinsi dahulu </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$('#tanggal_lahir').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$("#btnsave").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	$(document).ready(function(){
		$(".letters").keypress(function(event){
			var inputValue = event.which;
            // allow letters and whitespaces only.
            if(!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0) && inputValue != 8 ) { 
                event.preventDefault(); 
            }
            console.log(inputValue);
		});
	});

	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		bootbox.alert("Berhasil. Data anda telah disimpan.");
		setTimeout(function(){
		  location.href='v_pegawai.php';
		},3000);
		
	  }
	  else if(respon.status == 'EXIST'){
		bootbox.alert('Gagal. Email yang anda gunakan sudah terdaftar. Harap gunakan email pribadi anda.');
	    $("#btnsave").prop('disabled', false);
	  }
	  else{
		bootbox.alert(msg);
		$("#btnsave").prop('disabled', false);
	  }
	};

	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	
</script>
</body>
</html>
