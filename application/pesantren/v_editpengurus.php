<?php 
$titlepage="Edit Pengurus Firqoh";
$idsmenu=22; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$idc = $_POST['id_asrama'];
$asrama = $pos->getDetailAsrama($idc);

$array = $pos->getPengurusAsrama($idc);
$data = $array[1];

?>
<section class="content-header">
  <h1>
   Pengurus <?php echo $asrama[1]['nama_asrama'];?>
	<small></small>
	<button type="download" title="Download" class="btn btn-success pull-right" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 
  </h1>
  
</section>

<section class="content">
  <form method="post" id="target" action="c_asrama.php" enctype="multipart/form-data">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="<?php echo (isset($data['auto']))?'E':'N'; ?>">
	<input type="hidden" id="id_asrama" name="id_asrama" class="" value="<?php echo (isset($data['auto']))?$data['auto']:''; ?>">
	<input type="hidden" id="id" name="asrama" class="" value="<?php echo (isset($idc))?$idc:''; ?>">
	<input type="hidden" name="method" class="" value="save_pengurus">
	<div class="row">
		<div class="col-md-4">
	
		</div>
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border text-center">
			  <h3 class="box-title ">Pengurus Harian</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Ketua</label>
					<input type="text" class="form-control" name="ketua" placeholder="Ketua" value="<?php echo (isset($data['ketua']))?$data['ketua']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Sekretaris</label>
					<input type="text" class="form-control" name="sekretaris" placeholder="Sekretaris" value="<?php echo (isset($data['sekretaris']))?$data['sekretaris']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Bendahara</label>
					<input type="text" class="form-control" name="bendahara_1" placeholder="Bendahara I" value="<?php echo (isset($data['bendahara_1']))?$data['bendahara_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bendahara_2" placeholder="Bendahara II" value="<?php echo (isset($data['bendahara_2']))?$data['bendahara_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  <h3 class="box-title">Keamanan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="keamanan_ko" placeholder="Koordinator" value="<?php echo (isset($data['keamanan_ko']))?$data['keamanan_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="keamanan_1" placeholder="Anggota I" value="<?php echo (isset($data['keamanan_1']))?$data['keamanan_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="keamanan_2" placeholder="Anggota II" value="<?php echo (isset($data['keamanan_2']))?$data['keamanan_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="keamanan_3" placeholder="Anggota III" value="<?php echo (isset($data['keamanan_3']))?$data['keamanan_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="keamanan_4" placeholder="Anggota IV" value="<?php echo (isset($data['keamanan_4']))?$data['keamanan_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Bahasa</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="bahasa_ko" placeholder="Koordinator" value="<?php echo (isset($data['bahasa_ko']))?$data['bahasa_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="bahasa_1" placeholder="Anggota I" value="<?php echo (isset($data['bahasa_1']))?$data['bahasa_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bahasa_2" placeholder="Anggota II" value="<?php echo (isset($data['bahasa_2']))?$data['bahasa_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bahasa_3" placeholder="Anggota III" value="<?php echo (isset($data['bahasa_3']))?$data['bahasa_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bahasa_4" placeholder="Anggota IV" value="<?php echo (isset($data['bahasa_4']))?$data['bahasa_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Pendidikan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="pendidikan_ko" placeholder="Koordinator" value="<?php echo (isset($data['pendidikan_ko']))?$data['pendidikan_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="pendidikan_1" placeholder="Anggota I" value="<?php echo (isset($data['pendidikan_1']))?$data['pendidikan_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pendidikan_2" placeholder="Anggota II" value="<?php echo (isset($data['pendidikan_2']))?$data['pendidikan_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pendidikan_3" placeholder="Anggota III" value="<?php echo (isset($data['pendidikan_3']))?$data['pendidikan_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pendidikan_4" placeholder="Anggota IV" value="<?php echo (isset($data['pendidikan_4']))?$data['pendidikan_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
	</div>

	<div class="row">
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Orkesih</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="orkesih_ko" placeholder="Koordinator" value="<?php echo (isset($data['orkesih_ko']))?$data['orkesih_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="orkesih_1" placeholder="Anggota I" value="<?php echo (isset($data['orkesih_1']))?$data['orkesih_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="orkesih_2" placeholder="Anggota II" value="<?php echo (isset($data['orkesih_2']))?$data['orkesih_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="orkesih_3" placeholder="Anggota III" value="<?php echo (isset($data['orkesih_3']))?$data['orkesih_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="orkesih_4" placeholder="Anggota IV" value="<?php echo (isset($data['orkesih_4']))?$data['orkesih_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Humas</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="humas_ko" placeholder="Koordinator" value="<?php echo (isset($data['humas_ko']))?$data['humas_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="humas_1" placeholder="Anggota I" value="<?php echo (isset($data['humas_1']))?$data['humas_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="humas_2" placeholder="Anggota II" value="<?php echo (isset($data['humas_2']))?$data['humas_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="humas_3" placeholder="Anggota III" value="<?php echo (isset($data['humas_3']))?$data['humas_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="humas_4" placeholder="Anggota IV" value="<?php echo (isset($data['humas_4']))?$data['humas_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">MPK</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Koordinator</label>
					<input type="text" class="form-control" name="mpk_ko" placeholder="Koordinator" value="<?php echo (isset($data['mpk_ko']))?$data['mpk_ko']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Anggota</label>
					<input type="text" class="form-control" name="mpk_1" placeholder="Anggota I" value="<?php echo (isset($data['mpk_1']))?$data['mpk_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="mpk_2" placeholder="Anggota II" value="<?php echo (isset($data['mpk_2']))?$data['mpk_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="mpk_3" placeholder="Anggota III" value="<?php echo (isset($data['mpk_3']))?$data['mpk_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="mpk_4" placeholder="Anggota IV" value="<?php echo (isset($data['mpk_4']))?$data['mpk_4']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
	</div>
	
	<div class="box box-widget">
	
	  <div class="box-footer">
		<button type="button" title="Back Button" class="btn btn-primary " onclick="goBack()" name=""><i class="fa fa-back"></i> Back</button>
		<button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Simpan </button>
					
		<span id="infoproses"></span>
	  </div><!-- /.box-footer -->
	</div><!-- /.box -->
	</form>			
</section>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script language="javascript">
	function goBack() {
		window.history.back();
	}

	$(function () {
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin data akan disimpan?",
			function (result) {
			  if (result == true) {
				$("#btnsave").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. Data anda telah disimpan.");
		$("#btnsave").prop('disabled', false);
		$("#inputcrud").val('E');
		$("#id_asrama").val(respon.data);
	  }
	 else{
		bootbox.alert(msg);
		$("#btnsave").prop('disabled', false);
	  }
	};

	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_pengurus.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
</script>
</body>
</html>
