<?php ob_start(); 
$titlepage="Data Santri";
$idsmenu=17; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$sex = array('1' => 'Putra','0' => 'Putri');
$arrjenjang = array('2' => 'Ula', '3' => 'Wustho');
function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}
	
$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];

$propinsi = $pos->getPropinsi();

if($_POST['method'] == 'E'){
	$idc = $_POST['idsiswa'];
	$array = $pos->getEditSiswa($idc);
	$berkas_siswa = $pos->getBerkasSiswa($array[1]['uuid']);
	
	$arr_berkas= array();
	foreach($berkas_siswa[1] as $bk){
		$arr_berkas[$bk['kelengkapan']]=array('tipe'=>$bk['tipe'],'value'=>$bk['html_value']);
	}
	
	$data = $array[1];
	if(empty($data)){
		header("Location:v_siswa.php");
		die();
	}else{
		$html_sekolah = '';
		$html_jenjang = '';
		
		$html_sekolah .='<select class="form-control" id="sekolah" name="sekolah" required>
							<option value=""> Pilih sekolah </option>';
		if($data['mdk'] == 'mdk'){
			$satpen = $pos->getSatuanPendidikan(2);
			foreach($satpen[1] as $row){
			  $selected = ($data['tujuan_sekolah'] == $row['kode_sekolah'])?'selected':'';
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'" '.$selected.'> '.$row['nama_sekolah'].' </option>';
			}
		}else{
			$satpen = $pos->getSatuanPendidikan(1);
			foreach($satpen[1] as $row){
			  $selected = ($data['tujuan_sekolah'] == $row['kode_sekolah'])?'selected':''; 
			  $html_sekolah .='<option value="'.$row['kode_sekolah'].'" '.$selected.'> '.$row['nama_sekolah'].' </option>';
			}
		}
		$html_sekolah .='</select>';
		
		$jenjang = $arrjenjang[$data['jenjang_pendidikan']]." ".$sex[$data['kelamin']];
		$html_jenjang .= '<input type="text" class="form-control" name="jenjang" id="jenjang" value="'.$jenjang.'" readonly>
						  <input type="hidden" id="tingkat" name="tingkat" value="'.$data['jenjang_pendidikan'].'">';
	}
}
?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	SANTRI
	<small>edit data personal</small>
  </h1>
</section>
<section class="content">
	<form method="post" id="target" action="c_siswa.php" enctype="multipart/form-data">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="<?php echo (isset($data))?'E':'N'; ?>">
	<input type="hidden" id="id" name="idsiswa" class="" value="<?php echo (isset($data))?$data['jid']:''; ?>">
	<input type="hidden" name="method" class="" value="save_item">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo (isset($data))?'Edit':'Tambah';?> Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body form-horizontal">
		  <div class="row">
			<div class="col-md-8">
				<div class="form-group"> <label class="col-sm-3  control-label"> Jalur Program? </label>
					<div class="col-sm-9" id="mdk"> 
					<div class="radio">
					  <label><input type="radio" id="mdk" name="mdk" value="mdk" required <?php if(isset($data)) {echo ($data['mdk']=='mdk')?'checked':'' ; }?>> MDK</label>
					  <label><input type="radio" id="mdk" name="mdk" value="tidak" required <?php if(isset($data)) {echo ($data['mdk']=='tidak')?'checked':'' ; }?>> Bukan MDK</label>
					</div>
					</div>
				</div>
				<div class="form-group"> <label class="col-sm-3  control-label"> Sekolah tujuan <span class="text-danger">*</span></label>
				  <div class="col-sm-9" id="tujuan">
					<?php if(isset($data)) {echo $html_sekolah;} else {?>
					<cite>Pilih jalur terlebih dahulu.</cite>
					<?php } ?>
				  </div>
				</div>
				<div class="form-group"> <label class="col-sm-3  control-label"> Jenjang pendidikan</label>
					<div class="col-sm-9" id="jenjang"> <?php if(isset($data)) {echo $html_jenjang;}?> </div>
				</div>
				<div class="form-group"> <label class="col-sm-3  control-label">No Induk Santri<span class="text-danger">*</span></label>
					<div class="col-sm-9"><input type="text" class="form-control " id="no_induk" name="no_induk" value="<?php if(isset($data)) echo $data['nis'];?>" placeholder=""> </div>
				</div>
				<div class="form-group"> <label class="col-sm-3  control-label">Nama lengkap<span class="text-danger">*</span></label>
					<div class="col-sm-9"><input type="text" class="form-control " id="nama_lengkap" name="nama_lengkap" value="<?php if(isset($data)) echo $data['nama_lengkap'];?>" placeholder="Isikan nama lengkap" required=""> </div>
				</div>
				<div class="form-group">
				  <label for="email" class="control-label col-sm-3">Email</label>
				  <div class="col-sm-9">
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php if(isset($data)) echo $data['email'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="j-kelamin" class="control-label col-sm-3">Jenis kelamin<span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<div class="radio">
					  <label><input type="radio" id="kelamin" name="kelamin" value="1" required <?php if(isset($data)) {echo ($data['kelamin']=='1')?'checked':'' ;}?>> Laki-laki</label>
					  <label><input type="radio" id="kelamin" name="kelamin" value="0" required <?php if(isset($data)) {echo ($data['kelamin']=='0')?'checked':'' ;}?>> Perempuan</label>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="tempatlahir" class="control-label col-sm-3">Tempat & Tanggal lahir <span class="text-danger"> *</span></label>
				  <div class="col-sm-5">
					<input type="text" class="form-control letters" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php if(isset($data)) echo $data['tempat_lahir'];?>" required>
				  </div>
				  <div class="col-sm-4">
					<div class="input-group date" id="datepicker">
					  <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="dd-mm-yyyy" value="<?php if(isset($data)) echo display_to_report($data['tanggal_lahir']);?>"  data-inputmask="'alias': 'dd-mm-yyyy'" data-mask required>
					  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="saudara" class="control-label col-sm-3">Jumlah saudara </label>
				  <div class="col-sm-3">
					<input type="text" class="form-control" name="saudara" id="saudara" placeholder="Saudara" value="<?php if(isset($data)) echo $data['saudara'];?>" onkeypress="return isNumber(event)">
				  </div>
				  <div class="col-sm-6">
					<label for="anakke" class="control-label col-sm-4">Anak ke </label>
					<div class="col-sm-3">
					  <input type="text" class="form-control" name="anak_ke" id="anak_ke" value="<?php if(isset($data)) echo $data['anak_ke'];?>" onkeypress="return isNumber(event)">
					</div>
				  </div>
				</div>
				<div class="form-group">
				  <label for="keb-khusus" class="control-label col-sm-3">Riwayat penyakit?</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="abk" id="abk" placeholder="Riwayat Penyakit" value="<?php if(isset($data)) echo $data['abk'];?>">
					<span class="help-block">Kosongkan jika tidak memiliki riwayat penyakit</span>
				  </div>
				</div>
				<div class="form-group">
				  <label for="propinsi" class="control-label col-sm-3">Propinsi <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<select class="form-control" id="propinsi" name="propinsi" required>
						<option value=""> Pilih propinsi </option>
						<?php
						foreach ($propinsi[1] as $opt) {
						$selected=(isset($data))?($data['propinsi']==$opt['id'])?'selected':'':'';
						echo "<option value='".$opt['id']."' ".$selected."> ".$opt['name']." </option>";
						}
						?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="kabupaten" class="control-label col-sm-3">Kabupaten <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<select class="form-control" id="kabupaten" name="kabupaten" required>
					<?php if(isset($data)){
						echo '<option value=""> Pilih kabupaten </option>';
						$regencies = $pos->getKabupaten($data['propinsi']);
						foreach($regencies[1] as $row){
							$selected=($data['kabupaten']==$row['id'])?'selected':'';
							echo '<option value="'.$row['id'].'" '.$selected.'> '.$row['name'].' </option>';
						}
					}else{ echo '<option value=""> Pilih propinsi dahulu </option>';}	?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="alamat" class="control-label col-sm-3">Alamat tempat tinggal <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="alamat" id="alamat" placeholder="RT RW / Nama jalan nomor rumah, kecamatan, kelurahan" value="<?php if(isset($data)) echo $data['alamat'];?>" required>
				  </div>
				</div>
				<div class="form-group">
				  <label for="no_hp" class="control-label col-sm-3">Handphone yang aktif</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['no_hp'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="kode_pos" class="control-label col-sm-3">Kode POS </label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode POS" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['kode_pos'];?>">
				  </div>
				</div>
				<div class="form-group">
				  <label for="tingkat_kelas" class="control-label col-sm-3">Kelas sekarang <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" required>
						<option value=""> Kelas sekarang </option>
						<?php
						for ($x = 1; $x <= 3; $x++) {
						$selected=(isset($data))?($data['tingkat_kelas']==$x)?'selected':'':'';
						echo "<option value='".$x."' ".$selected."> Kelas ".$x." </option>";
						}
						?>
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label for="status" class="control-label col-sm-3">Status <span class="text-danger"> *</span></label>
				  <div class="col-sm-9">
					<div class="radio">
					  <label><input type="radio" id="status_kesiswaan" name="status_kesiswaan" value="siswa" required <?php if(isset($data)) {echo ($data['status_kesiswaan']=='siswa')?'checked':'' ;} else{ echo 'checked';}?>> Aktif</label>
					  <?php if($_POST['method'] == 'E'){ ?>
					  <label><input type="radio" id="status_kesiswaan" name="status_kesiswaan" value="keluar" required <?php if(isset($data)) {echo ($data['status_kesiswaan']=='keluar')?'checked':'' ;}?>> Keluar</label>
					  <?php } ?>
					</div>
				  </div>
				</div>
			</div><!-- /.col -->
			<div class="col-md-4">
				<div class="hero-widget well well-md box-profile">
					<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_siswa" alt="foto santri 3x4" <?php if(isset($data)) echo 'src="../../files/images_pendaftar/'.$data['photo_siswa'].'"';?>>
					<label for="upload_siswa">Foto santri <small>(wajib upload foto)</small></label>
					<input type="file" name="photo_siswa" id="upload_siswa" accept="image/*" />
				</div><!-- /.box -->
				<div class="hero-widget well well-md box-profile">
					<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_wali" alt="foto wali 3x4" <?php if(isset($data)) echo 'src="../../files/images_pendaftar/'.$data['photo_wali'].'"';?>>
					<label for="upload_siswa">Foto wali santri<small>(wajib upload foto)</small></label>
					<input type="file" name="photo_wali" id="upload_wali" accept="image/*" />
				</div><!-- /.box -->
			</div><!-- /.col -->
			
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer">
		  <span class="text-danger">*</span> ) harus diisi
		</div>
	</div><!-- /.box -->
	
	<!-- orang tua / wali santri -->
	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Orang Tua / Wali Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<div class="box-body">
		  <div class="row">
			<div class="col-md-6">
				<label><dt>1. Data ayah </dt></label>
				<div class="form-group">
				  <label for="nama_ayah">Nama <span class="text-danger"> *</span></label>
				  <input type="text" class="form-control" id="nama_ayah" name="nama_ayah" placeholder="Nama ayah" value="<?php if(isset($data)) echo $data['ayah'];?>" required>
				</div>
				<div class="form-group">
				  <label for="pendidikan_ayah">Pendidikan</label>
				  <select class="form-control" id="pendidikan_ayah" name="pendidikan_ayah" >
					<option value=""> Tingkat pendidikan </option>
					<?php
					foreach ($pendidikan as $opt) {
					$selected=(isset($data))?($data['pendidikan_ayah']==$opt['id'])?'selected':'':'';
					echo "<option value='".$opt['id']."' ".$selected."> ".$opt['pendidikan']." </option>";
					}
					?>
				  </select>
				</div>
				<div class="form-group">
				  <label for="pekerjaan_ayah">Pekerjaan </label>
				  <input type="text" class="form-control" id="pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Pekerjaan ayah" value="<?php if(isset($data)) echo $data['pekerjaan_ayah'];?>">
				</div>
				<div class="form-group">
				  <label for="hp_ayah">Handphone yang aktif </label>
				  <input type="text" class="form-control" id="hp_ayah" name="hp_ayah" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['hp_ayah'];?>">
				</div>
				
			</div><!-- /.col -->
			<div class="col-md-6">
				<label><dt>2. Data ibu</dt></label>
				<div class="form-group">
				  <label for="nama_ibu">Nama <span class="text-danger"> *</span></label>
				  <input type="text" class="form-control" id="nama_ibu" name="nama_ibu" placeholder="Nama ibu" value="<?php if(isset($data)) echo $data['ibu'];?>" required>
				</div>
				<div class="form-group">
				  <label for="pendidikan_ibu">Pendidikan</label>
				  <select class="form-control" id="pendidikan_ibu" name="pendidikan_ibu" >
					<option value=""> Tingkat pendidikan </option>
					<?php
					foreach ($pendidikan as $opt) {
					$selected=(isset($data))?($data['pendidikan_ibu']==$opt['id'])?'selected':'':'';
					echo "<option value='".$opt['id']."' ".$selected."> ".$opt['pendidikan']." </option>";
					}
					?>
				  </select>
				</div>
				<div class="form-group">
				  <label for="pekerjaan_ibu">Pekerjaan </label>
				  <input type="text" class="form-control" id="pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Pekerjaan ibu" value="<?php if(isset($data)) echo $data['pekerjaan_ibu'];?>">
				</div>
				<div class="form-group">
				  <label for="hp_ibu">Handphone yang aktif </label>
				  <input type="text" class="form-control" id="hp_ibu" name="hp_ibu" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['hp_ibu'];?>">
				</div>
			</div><!-- /.col -->
			
			<div class="col-md-12">
				<label><b>3. Data wali </b>(diisi jika wali bukan orang tua kandung)</label>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="nama_wali">Nama wali</label>
						  <input type="text" class="form-control" id="nama_wali" name="nama_wali" placeholder="Nama wali" value="<?php if(isset($data)) echo $data['wali'];?>">
						</div>
						<div class="form-group">
						  <label for="alamat_wali">Alamat </label>
						  <input type="text" class="form-control" id="alamat_wali" name="alamat_wali" placeholder="Alamat wali" value="<?php if(isset($data)) echo $data['alamat_wali'];?>">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
						  <label for="pekerjaan_wali">Pekerjaan </label>
						  <input type="text" class="form-control" id="pekerjaan_wali" name="pekerjaan_wali" placeholder="Pekerjaan wali" value="<?php if(isset($data)) echo $data['pekerjaan_wali'];?>">
						</div>
						<div class="form-group">
						  <label for="telp_wali">Nomor Handphone</label>
						  <input type="text" class="form-control" id="telp_wali" name="telp_wali" placeholder="Isikan nomor telepon rumah/Handphone" onkeypress="return isNumber(event)" value="<?php if(isset($data)) echo $data['telp_wali'];?>">
						</div>
						<div class="form-group">
						  <label for="hubungan_wali">Hubungan keluarga</label>
						  <input type="text" class="form-control" id="hubungan_wali" name="hubungan_wali" placeholder="Hubungan keluarga dengan siswa" value="<?php if(isset($data)) echo $data['hubungan_wali'];?>">
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div><!-- /.box-body -->
	</div><!-- /.box orang tua/wali -->
	
	<!-- riwayat pendidikan -->
	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Riwayat pendidikan</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<div class="box-body" id="riwayat">
		<?php if(isset($data)){
			$riwayat = $pos->getRiwayatPendidikan($data['uuid']);
			$number = 1;
			if(!empty($riwayat[1])){
				foreach($riwayat[1] as $row){ ?>
				  <div class="row fieldwrapper">
					<div class="col-md-6">
						<div class="form-group">
						  <label for="riwayat">Tingkat pendidikan</label>
						  <input type="text" class="form-control" id="riwayat" name="riwayat[]" value="<?php echo $row['penyelenggara'];?>" placeholder="ex: SD/MI, Mts/SMP, Madin">
						</div>
					</div><!-- /.col -->
					<div class="col-md-3">
						<div class="form-group">
						  <label for="tahun-lulus">Lulus tahun</label>
						  <input type="text" class="form-control" id="tahun-lulus" name="lulus[]" value="<?php echo $row['tahun_selesai'];?>" placeholder="ex: 2019">
						</div>
					</div><!-- /.col -->
					<div class="col-md-1">
						<div class="form-group">
						  <label for="control">Control</label>
						  <?php echo ($number == 1)?'<input type="button" value="+" class="add_pend btn btn-success form-control col-md-4" id="add" />':'<input type="button" value="-" class="btn btn-danger form-control remove_pend" />';?>
						</div>
					</div><!-- /.col -->
				  </div><!-- /.row -->
				<?php
				$number++;
				}
			}else{ ?>
			  <div class="row fieldwrapper">
				<div class="col-md-6">
					<div class="form-group">
					  <label for="riwayat">Tingkat pendidikan</label>
					  <input type="text" class="form-control" id="riwayat" name="riwayat[]" placeholder="ex: SD/MI, Mts/SMP, Madin">
					</div>
				</div><!-- /.col -->
				<div class="col-md-3">
					<div class="form-group">
					  <label for="tahun-lulus">Lulus tahun</label>
					  <input type="text" class="form-control" id="tahun-lulus" name="lulus[]" placeholder="ex: 2019">
					</div>
				</div><!-- /.col -->
				<div class="col-md-1">
					<div class="form-group">
					  <label for="control">Control</label>
					  <input type="button" value="+" class="add_pend btn btn-success form-control col-md-4" id="add" />
					</div>
				</div><!-- /.col -->
			  </div><!-- /.row -->
			<?php 
			} 
		  }else{ ?>
		  <div class="row fieldwrapper">
			<div class="col-md-6">
				<div class="form-group">
				  <label for="riwayat">Tingkat pendidikan</label>
				  <input type="text" class="form-control" id="riwayat" name="riwayat[]" placeholder="ex: SD/MI, Mts/SMP, Madin">
				</div>
			</div><!-- /.col -->
			<div class="col-md-3">
				<div class="form-group">
				  <label for="tahun-lulus">Lulus tahun</label>
				  <input type="text" class="form-control" id="tahun-lulus" name="lulus[]" placeholder="ex: 2019">
				</div>
			</div><!-- /.col -->
			<div class="col-md-1">
				<div class="form-group">
				  <label for="control">Control</label>
				  <input type="button" value="+" class="add_pend btn btn-success form-control col-md-4" id="add" />
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  <?php } ?>
		</div><!-- /.box-body -->
		
	</div><!-- /.box riwayat pendidikan-->
	
	<!-- Kelengkapan
	<div id="form-kelengkapan">
	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Kelengkapan Berkas</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body">
		  <div class="row form-horizontal">
			<div class="col-md-8" id="list-kel">
				<?php
				/*
				$kelengkapan = $pos->getKelengkapan();
				$num=1;
				if(!empty($kelengkapan[1])){
				  foreach ($kelengkapan[1] as $res) {
					if($res['jenis_field'] == 'File'){$form ='<input type="file" name="'.$res['nama_field'].'" required />';}
					else{$form ='<input type="file" name="'.$res['nama_field'].'" accept="image/*"/>';}
					
					if(isset($data)){
					if($arr_berkas[$res['kelengkapan_id']]['value'] == ''){$berkas='';}
					else{ 
					  if($arr_berkas[$res['kelengkapan_id']]['tipe'] == 'Photo'){
						$berkas = '<a id="berkas'.$res['kelengkapan_id'].'" src="../../files/images_pendaftar/'.$arr_berkas[$res['kelengkapan_id']]['value'].'" onclick="return bigimage(berkas'.$res['kelengkapan_id'].');">[Click to view]</a>';
					  }else{
						$berkas = '<a id="berkas'.$res['kelengkapan_id'].'" src="../../files/files_pendaftar/'.$arr_berkas[$res['kelengkapan_id']]['value'].'" >[Click to download]</a>';  
					  }
					}
					}
					
					echo '<div class="form-group"> 
								<label class="col-sm-3  control-label">'.$num.'. '.$res['kelengkapan'].'</label>
								<div class="col-sm-5">
								'.$form.'	
								</div>
								<div class="col-sm-4">';
								if(isset($data)){ echo'<span>'.$berkas.'</span>';}
						  echo '</div>
							</div>';$num++;
				  }
				}*/
				?>
			</div>
		  </div>
		</div> 
	</div>
	</div><!-- /.box -->
	
	<div class="box box-widget">
	  <div class="box-footer">
		<button type="button" title="Back Button" class="btn btn-primary " onclick="goBack()" name=""><i class="fa fa-back"></i> Back</button>
		<button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Simpan </button>
		<span id="infoproses"></span>
	  </div><!-- /.box-footer -->
	</div><!-- /.box -->
	</form>			
</section><!-- /.content -->

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#riwayat"); //Fields wrapper
    var add_button      = $(".add_pend"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row fieldwrapper">'
						+'<div class="col-md-6">'
							+'<div class="form-group">'
							  +'<label for="riwayat">Tingkat pendidikan</label>'
							  +'<input type="text" class="form-control" id="riwayat" name="riwayat[]" placeholder="ex: SD/MI, Mts/SMP, Madin">'
							+'</div>'
						+'</div>'
						+'<div class="col-md-3">'
							+'<div class="form-group">'
							  +'<label for="tahun-lulus">Lulus tahun</label>'
							  +'<input type="text" class="form-control" id="tahun-lulus" name="lulus[]" placeholder="ex: 2019">'
							+'</div>'
						+'</div>'
						+'<div class="col-md-1">'
							+'<div class="form-group">'
							  +'<label for="control">&nbsp;</label>'
							  +'<input type="button" value="-" class="btn btn-danger form-control remove_pend" />'
							+'</div>'
						+'</div>'
					  +'</div>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_pend", function(e){ //user click on remove text
        e.preventDefault(); $(this).getParent(3).remove(); x--;
    })
	
	jQuery.fn.getParent = function(num) {
		var last = this[0];
		for (var i = 0; i < num; i++) {
			last = last.parentNode;
		}
		return jQuery(last);
	};

});

function bigimage(id){
	//var x = document.getElementsById().getAttribute("class");
	$('.imagepreview').attr('src', $(id).attr('src'));
	$('#imagemodal').modal('show');   
}

	var fileTag = document.getElementById("upload_siswa"),
	preview = document.getElementById("preview_siswa");

	fileTag.addEventListener("change", function() {
	  changeImage(this,preview);
	});
	
	var fileTagWali = document.getElementById("upload_wali"),
	previewWali = document.getElementById("preview_wali");

	fileTagWali.addEventListener("change", function() {
	  changeImage(this,previewWali);
	});
	function changeImage(input,output) {
	  var reader;

	  if (input.files && input.files[0]) {
		reader = new FileReader();

		reader.onload = function(e) {
		  output.setAttribute('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	  }
	}	
	
    $(function () {
		//Datemask dd/mm/yyyy
        $("#tanggal_lahir").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
		$("input[name='mdk']").on('change', function() {
			$('#tujuan').html('');
			var nilai = this.value;
			var value = {
				nilai: nilai,
				method : "get_form"
			};
			$.ajax(
			{
				url : "c_siswa.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(nilai == '' || nilai == null){
						$('#tipe, #tujuan').append('<cite>Pilih jalur terlebih dahulu.</cite>'); 
					}else{ 
						$('#tujuan').append(hasil.tujuan);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
			get_jenjang();
		});
		
		$("div#tujuan").on("change", "select[name='sekolah']",function() {
			get_jenjang();
		});
		
		$("input[name='kelamin']").on("change", function() {
			get_jenjang();
		});
		
		$('#propinsi').on('change', function() {
			var value = {
				propinsi_id: this.value,
				method : "get_kabupaten"
			};
			$.ajax({
				url : "c_siswa.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil[1].length>0){
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih kabupaten </option>");
						$.each(hasil[1], function (key, val) {
						  $('#kabupaten').append('<option value="'+val.id+'">'+val.name+'</option>');
						})
					}else{
						$('#kabupaten').html("");
						$('#kabupaten').html("<option value=''> Pilih propinsi dahulu </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$('#tanggal_lahir, #tanggal_lahir_ayah, #tanggal_lahir_ibu, #tanggal_lahir_wali').datepicker({
			format: 'dd-mm-yyyy',
		});
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$("#btnsave").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });
	
	//searching nama by no induk
	$(document).on("blur","#nis",function(){
		$("#nama_lengkap").val('');
		var nomor = $(this).val();
		var value = {
			nomor:nomor,
			method : "get_name"
		};
		$.ajax(
		{
			url : "c_siswa.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var data = jQuery.parseJSON(data);
				if(data.status == 'exist'){
					$("#nama_lengkap").val(data.nama);
				}else{
					bootbox.alert('No induk tidak terdaftar');
					$("#no_induk").val('');
					$("#nama_lengkap").val('');
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				
			}
		});
	});

	$(document).ready(function(){
		$(".letters").keypress(function(event){
			var inputValue = event.which;
            // allow letters and whitespaces only.
            if(!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0) && inputValue != 8 ) { 
                event.preventDefault(); 
            }
            console.log(inputValue);
		});
	});

	function get_jenjang(){
		$('#jenjang').html('');
			
		var kelamin = $("input[name='kelamin']:checked").val();
		var sekolah = $("select[name='sekolah']").find('option:selected').val();
		var value = {
			kelamin: kelamin,
			sekolah: sekolah,
			method : "get_jenjang"
		};
		$.ajax(
		{
			url : "c_siswa.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var hasil = jQuery.parseJSON(data);
				$('#jenjang').append(hasil.jenjang);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		bootbox.alert("Berhasil. Data anda telah disimpan.");
		setTimeout(function(){
		  location.href='v_siswa.php';
		},3000);
		
	  }
	  else if(respon.status == 'EXIST'){
		bootbox.alert('Gagal. Email yang anda gunakan sudah terdaftar. Harap gunakan email pribadi anda.');
	  }
	  else{
		bootbox.alert(msg);
	  }
	};

	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	
</script>
</body>
</html>
