<?php 
$titlepage="Daftar Kamar";
$idsmenu=23; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content-header">
  <h1>
	KAMAR
	<small>management kamar</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Daftar Kamar</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Kamar</button>
					<br><br>
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					  <button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Nama Firqoh </th>
							<th>Kamar </th>
							<th>Pembina </th>
							<th>Kapasitas </th>
							<th>Terisi </th>
							<th>Sisa </th>
							<th style="width:120px">Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah Kamar</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Nama Kamar</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kapasitas</label>
							<div class="col-sm-9">
								<input type="text" class="form-control " id="txtkapasitas" name="txtkapasitas" value="" > 
							</div>
						</div>
						
						<div class="form-group new"> <label class="col-sm-3  control-label">Firqoh</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optfirqoh" name="optfirqoh" >
									<option value=""> Pilih Firqoh </option>
									
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Pembina</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" id="firstpembina" name="pembina[]" value="" >
							</div>
							<div class="col-sm-2">
							  <input type="button" value="+" class="add_pem btn btn-success form-control col-md-4" id="add" />
							</div>
						</div>
						<div id="list-pembina">
							
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script src="../../plugins/select2/select2.full.min.js"></script>	
	<script language="javascript">
		$(document).ready(function() {
			var max_fields      = 10; //maximum input boxes allowed
			var wrapper         = $("#list-pembina"); //Fields wrapper
			var add_button      = $(".add_pem"); //Add button ID

			var x = 1; //initlal text box count
			$(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x < max_fields){ //max input box allowed
					x++; //text box increment
					$(wrapper).append('<div class="form-group"> <label class="col-sm-3"></label>'
								+'<div class="col-sm-7">'
									+'<input type="text" class="form-control" id="pembina" name="pembina[]" value="" >'
								+'</div>'
								+'<div class="col-sm-2">'
								  +'<input type="button" value="-" class="remove_pem btn btn-danger form-control col-md-4" />'
								+'</div>'
							+'</div>'); //add input box
				}
			});

			$(wrapper).on("click",".remove_pem", function(e){ //user click on remove text
				e.preventDefault(); $(this).getParent(2).remove(); x--;
			})
			
			jQuery.fn.getParent = function(num) {
				var last = this[0];
				for (var i = 0; i < num; i++) {
					last = last.parentNode;
				}
				return jQuery(last);
			};

		});
		
		$(document).ready( function () 
		{
			$(".select2").select2();
			money();
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_kamar.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "nama_asrama" },
				{ "data": "nama_kamar" },
				{ "data": "pembina" },
				{ "data": "kapasitas_kamar" },
				{ "data": "terisi" },
				{ "data": "sisa" },
				{ "data": "action" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			firqoh(crud,item);
			//pembina(crud,item);
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#txtkapasitas").val("");
			$("#firstpembina").val("");
			$("#optfirqoh").html("");
			$("#list-pembina").html("");
			$("#head-modal").html("Tambah Kamar");
			set_focus("#txtname");
			
		}
		
		$(document).on( "click",".btnedit", function() {
			newitem();
			$("#head-modal").html("Edit Kamar");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail"
			};
			$.ajax(
			{
				url : "c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.msg;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.auto);
					$("#txtname").val(data.nama_kamar);
					$("#txtkapasitas").val(data.kapasitas_kamar);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					var x = data.pembina.split(',');
					$.each(x, function(i, val){
					  if(i == 0){
						$("#firstpembina").val(val);	
					  }
					  else{ $('#list-pembina').append('<div class="form-group"> <label class="col-sm-3"></label>'
								+'<div class="col-sm-7">'
									+'<input type="text" class="form-control" id="pembina" name="pembina[]" value="'+val+'" >'
								+'</div>'
								+'<div class="col-sm-2">'
								  +'<input type="button" value="-" class="remove_pem btn btn-danger form-control col-md-4" />'
								+'</div>'
							+'</div>');
					  }
					});
					
					firqoh(crud,data.asrama);
					//pembina(crud,data.pembina);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		function firqoh(crud,firqoh){
			var selected = '';
			$('#optfirqoh').html('');
			var value = {
				method : "get_firqoh"
			};
			$.ajax(
			{
				url : "c_kamar.php",
				type: "POST",
				data : value,
				dataType: 'JSON',
				success: function(respons, textStatus, jqXHR)
				{
				  
				  if(crud=='N'){
					$('#optfirqoh').append('<option value="" > Pilih Firqoh </option>');
					$.each(respons.data, function (key, val) {
						$('#optfirqoh').append('<option value="' + val.auto + '" >' + val.nama_asrama + '</option>');
					})
				  }else{
					
					$('#optfirqoh').append('<option value="" > Pilih Firqoh </option>');
					$.each(respons.data, function (key, val) {
						if(val.auto == firqoh){selected = 'selected';}else{selected = '';}
						$('#optfirqoh').append('<option value="' + val.auto + '" '+ selected +'>' + val.nama_asrama + '</option>');
					})
				  }
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function pembina(crud,pembina){
			var selected = '';
			$('#optpembina').html('');
			var value = {
				method : "get_pembina"
			};
			$.ajax(
			{
				url : "c_kamar.php",
				type: "POST",
				data : value,
				dataType: 'JSON',
				success: function(respons, textStatus, jqXHR)
				{
				  $("#optpembina").val('').trigger("change");
				  if(crud=='N'){
					//$('#optpembina').select2();
					
					$('#optpembina').append('<option value="" > Pilih Pembina </option>');
					$.each(respons.data, function (key, val) {
						$('#optpembina').append('<option value="' + val.id_pegawai + '" >' + val.nama_lengkap + '</option>');
					})
				  }else{
					$('#optpembina').append('<option value="" > Pilih Pembina </option>');
					$.each(respons.data, function (key, val) {
						$('#optpembina').append('<option value="' + val.id_pegawai + '" >' + val.nama_lengkap + '</option>');
					})
					if (pembina == null) {}else{
					  $('#optpembina').select2({
						multiple:true,
					  });
					  var selectedValuesTest = pembina.split(',');
					  $("#optpembina").val(selectedValuesTest).trigger("change");
					}
				  }
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var kapasitas = $("#txtkapasitas").val();
			var firqoh = $("#optfirqoh").val(); 
			var pembina = [];
			$("input[name='pembina[]']").each(function () {
			  pembina.push( $(this).val() );
			}); 
			
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Nama kamar kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				kapasitas: kapasitas,
				firqoh:firqoh,
				pembina:pembina,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
							newitem();
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_penghuni.php";

		var txtid = document.createElement("input");
		txtid.type = "text";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);

		mapForm.submit();
		
		
	});
	
	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_kamar.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	</script>
</body>
</html>
