<?php ob_start(); 
$titlepage="Pembina Markazy";
$idsmenu=36; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$array = $pos->getPengurusMarkazy();
$data = $array[1];
?>

<section class="content-header">
  <h1>
	PENGURUS
	<small> markazy </small>
	<button type="download" title="Download" class="btn btn-success pull-right" id="btndownload" ><i class="fa fa-download"></i> Download data</button>
  </h1>
  
</section>
<section class="content">
  <form method="post" id="target" action="c_markazy.php" enctype="multipart/form-data">
	<input type="hidden" id="inputcrud" name="inputcrud" class="" value="<?php echo (isset($data['auto']))?'E':'N'; ?>">
	<input type="hidden" id="id" name="id" class="" value="<?php echo (isset($data['auto']))?$data['auto']:''; ?>">
	<input type="hidden" name="method" class="" value="save_pengurus">
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border text-center">
			  <h3 class="box-title ">DEWAN PENGASUH</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Pengasuh 1</label>
					<input type="text" class="form-control" name="pengasuh_1" placeholder="Pengasuh 1" value="<?php echo (isset($data['pengasuh_1']))?$data['pengasuh_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Pengasuh 2</label>
					<input type="text" class="form-control" name="pengasuh_2" placeholder="Pengasuh 2" value="<?php echo (isset($data['pengasuh_2']))?$data['pengasuh_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Pengasuh 3</label>
					<input type="text" class="form-control" name="pengasuh_3" placeholder="Pengasuh 3" value="<?php echo (isset($data['pengasuh_3']))?$data['pengasuh_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border text-center">
			  <h3 class="box-title ">DEWAN PEMBINA</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Pembina 1</label>
					<input type="text" class="form-control" name="pembina_1" placeholder="Pembina 1" value="<?php echo (isset($data['pembina_1']))?$data['pembina_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Pembina 2</label>
					<input type="text" class="form-control" name="pembina_2" placeholder="Pembina 2" value="<?php echo (isset($data['pembina_2']))?$data['pembina_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Pembina 3</label>
					<input type="text" class="form-control" name="pembina_3" placeholder="Pembina 3" value="<?php echo (isset($data['pembina_3']))?$data['pembina_3']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border text-center">
			  <h3 class="box-title ">ROIS AAM</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<label >Ketua</label>
					<input type="text" class="form-control" name="ketua" placeholder="Ketua" value="<?php echo (isset($data['ketua']))?$data['ketua']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Wakil</label>
					<input type="text" class="form-control" name="wakil" placeholder="Wakil" value="<?php echo (isset($data['wakil']))?$data['wakil']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Sekretaris</label>
					<input type="text" class="form-control" name="sekretaris" placeholder="Sekretaris" value="<?php echo (isset($data['sekretaris']))?$data['sekretaris']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<label >Bendahara</label>
					<input type="text" class="form-control" name="bendahara" placeholder="Bendahara" value="<?php echo (isset($data['bendahara']))?$data['bendahara']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  <h3 class="box-title">Keamanan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="keamanan_1" placeholder="Keamanan 1" value="<?php echo (isset($data['keamanan_1']))?$data['keamanan_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="keamanan_2" placeholder="Keamanan 2" value="<?php echo (isset($data['keamanan_2']))?$data['keamanan_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Kebahasaan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bahasa_1" placeholder="Kebahasaan 1" value="<?php echo (isset($data['bahasa_1']))?$data['bahasa_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="bahasa_2" placeholder="Kebahasaan 2" value="<?php echo (isset($data['bahasa_2']))?$data['bahasa_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Pendidikan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pendidikan_1" placeholder="Pendidikan 1" value="<?php echo (isset($data['pendidikan_1']))?$data['pendidikan_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pendidikan_2" placeholder="Pendidikan 2" value="<?php echo (isset($data['pendidikan_2']))?$data['pendidikan_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
	</div>

	<div class="row">
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Orkesih</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="orkesih_1" placeholder="Orkesih 1" value="<?php echo (isset($data['orkesih_1']))?$data['orkesih_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="orkesih_2" placeholder="Orkedih 2" value="<?php echo (isset($data['orkesih_2']))?$data['orkesih_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Humas</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="humas_1" placeholder="Humas 1" value="<?php echo (isset($data['humas_1']))?$data['humas_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="humas_2" placeholder="Humas 2" value="<?php echo (isset($data['humas_2']))?$data['humas_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Mading/Perpustakaan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="mpk_1" placeholder="Mading 1" value="<?php echo (isset($data['mpk_1']))?$data['mpk_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="mpk_2" placeholder="Mading 2" value="<?php echo (isset($data['mpk_2']))?$data['mpk_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
	</div>
	
	<div class="row">
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Sarana Prasarana</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="sarpras_1" placeholder="Sarana Prasarana 1" value="<?php echo (isset($data['sarpras_1']))?$data['sarpras_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="sarpras_2" placeholder="Sarana Prasarana 2" value="<?php echo (isset($data['sarpras_2']))?$data['sarpras_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">Pembangunan</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pembangunan_1" placeholder="Pembangunan 1" value="<?php echo (isset($data['pembangunan_1']))?$data['pembangunan_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="pembangunan_2" placeholder="Pembangunan 2" value="<?php echo (isset($data['pembangunan_2']))?$data['pembangunan_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
		<div class="col-md-4">
		  <div class="box box-solid">
			<div class="box-header with-border">
			  
			  <h3 class="box-title">IT dan Web</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div id="form">
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="it_1" placeholder="IT 1" value="<?php echo (isset($data['it_1']))?$data['it_1']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="text" class="form-control" name="it_2" placeholder="IT 2" value="<?php echo (isset($data['it_2']))?$data['it_2']:''; ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				  </div>
				</div>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- ./col -->
	</div>
	
	<div class="box box-widget">
	  <div class="box-footer">
		<button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Simpan </button>
		<span id="infoproses"></span>
	  </div><!-- /.box-footer -->
	</div><!-- /.box -->
	</form>			
</section>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script language="javascript">
	function goBack() {
		window.history.back();
	}

	$(function () {
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin data akan disimpan?",
			function (result) {
			  if (result == true) {
				$("#btnsave").prop('disabled', true);
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#infoproses").html("");
			  }
			});
			return false;
		});
			 
    });

	
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Berhasil. Data anda telah disimpan.");
		$("#btnsave").prop('disabled', false);
		$("#inputcrud").val('E');
		$("#id_asrama").val(respon.data);
	  }
	 else{
		bootbox.alert(msg);
		$("#btnsave").prop('disabled', false);
	  }
	};

	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_markazy.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
</script>
</body>
</html>
