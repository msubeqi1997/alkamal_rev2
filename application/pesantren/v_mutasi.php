<?php ob_start(); 
$titlepage="Mutasi Santri";
$idsmenu=21; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$sex = array('1' => 'Laki-laki','0' => 'Perempuan');

function display_to_report($date){
	return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
}

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	MUTASI SANTRI
	<small>proses mutasi</small>
  </h1>
</section>
<section class="content">
	
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Cari Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body form-horizontal">
		  <div class="row">
			<div class="col-md-12">
				<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
	</div><!-- /.box -->	
	  
	<form method="post" id="target" action="c_siswa.php">
	<input type="hidden" id="txtidsiswa" name="idsiswa" class="" value="">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body ">
		  <div class="row">
			<div class="col-md-8 form-horizontal">
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nomor Induk</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnis"></span>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-4">
						<b>Nama lengkap</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtnama"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Jenis kelamin</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtkelamin"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Tempat & Tanggal lahir</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtttl"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Alamat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtalamat"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Jenjang</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtjenjang"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>MDK</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtmdk"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Firqoh</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="txtfirqoh"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<b>Tanggal keluar</b><span class="text-danger"> *</span>
					</div>
					<div class="col-sm-4">
					  <div class="input-group date" id="datepicker">
						<input type="text" class="form-control txtperiode tgl" name="tanggal_keluar" id="tanggal_keluar" placeholder="dd-mm-yyyy" value="" readonly required>
					    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					</div>
				</div>
			</div><!-- /.col -->
			
			<div class="col-md-4">
				<div class="hero-widget well well-md box-profile">
					<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_siswa" src="">
				</div><!-- /.box -->
			</div><!-- /.col -->
			
			<div class="col-md-12">
				<div class="form-group">
				  <label>Alasan mutasi</label>
				  <textarea class="form-control" rows="3" id="txtalasan" name="alasan" placeholder="..."></textarea>
				</div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		</div>
		
		<div class="box-footer">
		  <button type="button" id="btndelete" data-btn="batal" class="btn btn-danger padding pull-right"> <i class="fa fa-close"></i> Mutasi </button>
		</div>
	</div><!-- /.box -->
	
	</form>			
</section><!-- /.content -->

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}
	$(document).ready( function () 
	{
		
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_search.php", { term: $('#txtsearchitem').val() }, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dl><dt>"+item.nis + " - "+item.nama+"</dt>"+item.jenjang+"</dl>"  )
			.appendTo( ul );
		};
	});
	
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
	
	var kelamin = {'0': 'Perempuan', '1': 'Laki-laki'};
	var mdk = {'mdk': 'MDK', 'tidak': 'Bukan MDK', '':''};
	function sql_to_report(tgl){
		var thn = tgl.substr(0, 4);
		var bln = tgl.substr(5, 2);
		var dy = tgl.substr(8, 2);
		var date = dy+'-'+bln+'-'+thn;
		return date;
	}
	
	function temptabel(id){
		var value = {
			siswa_id: id,
			method : "select_siswa"
		};
		$.ajax(
		{
			url : "c_siswa.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var hasil = jQuery.parseJSON(data);
				var data = hasil[1];
				
				$('#txtidsiswa').val(data.jid);				
				$('#txtnis').html(data.nis);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtkelamin').html(kelamin[data.kelamin]);					
				$('#txtttl').html(''+data.tempat_lahir+', '+sql_to_report(data.tanggal_lahir)+'');						
				$('#txtalamat').html(data.alamat);
				$('#txtjenjang').html(data.jenjang);				
				$('#txtmdk').html(mdk[data.jenis]);				
				$('#txtfirqoh').html(data.nama_asrama);				
				
				var url = '../../files/images_pendaftar/'+data.photo_siswa;
				bigimage('preview_siswa',url);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$('#txtidsiswa').val('');				
		$('#txtnis').html('');				
		$('#txtjenjang').html('');			
		$('#txtmdk').html('');			
		$('#txtfirqoh').html('');			
		$('#txtnama').html('');
		$('#txtkelamin').html('');
		$('#txtttl').html('');						
		$('#txtalamat').html('');
		$('#tanggal_keluar').val('');
		$('#txtalasan').val('');
		$('#preview_siswa').attr('src', '');
	}
    $(function () {
		
		$('#tanggal_keluar').datepicker({
			format: 'dd-mm-yyyy',
		});
		
			 
    });
	
	$(document).on( "click","#btndelete", function() {
		var token = $("#txtidsiswa").val();
		var tanggal = $("#tanggal_keluar").val();
		var alasan = $("#txtalasan").val();
		
		if(token == '' || token== null){
			$.notify({
				message: "Santri belum dipilih"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsearchitem");
			return;
		}
		
		if(tanggal == '' || tanggal== null){
			$.notify({
				message: "Tanggal belum diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#tanggal_keluar");
			return;
		}
		
		if(alasan== '' || alasan== null){
			$.notify({
				message: "Alasan belum diisi"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtalasan");
			return;
		}
		swal({   
		title: "Mutasi santri",   
		text: "Apakah anda yakin melanjutkan proses mutasi?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			tanggal:tanggal,
			alasan:alasan,
			method : "mutasi_siswa"
		  };
		  $("#btndelete").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_siswa.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btndelete").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.status == true){
					$.notify('Mutasi berhasil');
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Mutasi gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btndelete").prop('disabled', false);
			}
		  });
		});
	});
	
</script>
</body>
</html>
