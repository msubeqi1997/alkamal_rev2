<?php 
$titlepage="Data Pegawai";
$idsmenu=18; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; ?>

<section class="content-header">
  <h1>
	DAFTAR PEGAWAI
	<small>data master</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Pegawai</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" btn-data="new" name=""><i class="fa fa-plus"></i> Tambah pegawai</button>
					<br><br>
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					  <button type="submit" title="Upload" class="btn btn-primary bg-navy" id="btnupload" ><i class="fa fa-upload"></i> Upload</button>
					  <button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th >No Induk</th>
							<th >Nama Lengkap </th>
							<th >Jenis Kelamin </th>
							<th >TTL </th>
							<th width="30%">Alamat </th>
							<th> Status</th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->


<div id="modalupload" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Update NIS</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" action="update_induk.php" enctype="multipart/form-data">
					<div class="form-group">
						<label for="upload">Upload File excel : </label>
						<input type="file" id="file" name="file">
						<input type="hidden" value="upload_nis" name="method">
					</div>
					<div class="form-group">
						<button type="submit" title="Upload" class="btn btn-primary" id="btnupload" ><i class="fa fa-upload"></i> Upload data</button> 				
					</div>
				  </form>
				  <div id="result">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>

<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script language="javascript">
	$(document).ready( function () 
	{
		tabel();
	});
	
	function tabel(){
		var value = {
			method : "getdata"
		};
		$('#table_item').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax": {
				"url": "c_pegawai.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nip" },
			{ "data": "nama_lengkap" },
			{ "data": "jenis" },
			{ "data": "ttl" },
			{ "data": "alamat" },
			{ "data": "aktif" },
			{ "data": "button" },
			]
		});
		$("#table_item_filter").addClass("pull-right");
	}
	
	$(document).on( "click","#btnadd, .btnedit", function() {
		var opt = $(this).attr('btn-data');
		
		var mapForm = document.createElement("form");
			mapForm.target = "";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "v_editpegawai.php";
			
		if(opt == 'new'){
			
			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "N";
			mapForm.appendChild(method);
		}else{
			
			var idpegawai = $(this).attr('id_pegawai');
			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "E";
			mapForm.appendChild(method);
			
			var txtid = document.createElement("input");
			txtid.type = "text";
			txtid.name = "idpegawai";
			txtid.value = idpegawai;
			mapForm.appendChild(txtid);
		}
		document.body.appendChild(mapForm);

		mapForm.submit();
		
	});
	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_pegawai.php";
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_pegawai.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
	$(document).on("click","#btnupload",function(){
	  $("#modalupload").modal("show");
	  
	});
	
</script>
</body>
</html>
