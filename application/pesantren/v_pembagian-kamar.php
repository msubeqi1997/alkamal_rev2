<?php ob_start(); 
$titlepage="Pembagian Kamar Santri";
$idsmenu=24; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content-header">
  <h1>
	PEMBAGIAN KAMAR
	
  </h1>
</section>
<section class="content">
  <div class="row">
	<div class="col-md-6">
	  <div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body no-padding">
		  <div class="row" style="padding-left:25px; padding-right:25px;">
			<div class="col-md-6">
			  <div class="form-group">
				<label>Jenjang pendidikan</label>
				<select class="form-control" id="txtjenjang" name="txtjenjang" style="width: 100%;">
				  <option value="">Semua</option>
				  <option value="ula putra">Ula Putra</option>
				  <option value="ula putri">Ula Putri</option>
				  <option value="wustho putra">Wustho Putra</option>
				  <option value="wustho putri">Wustho Putri</option>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-6">
			  <div class="form-group"> 
				<label>Tingkat</label>
				<select class="form-control" id="txttingkat" name="txttingkat" style="width: 100%;">
				  <option value="">Pilih tingkat</option>
				  <option value="1">Tingkat 1</option>
				  <option value="2">Tingkat 2</option>
				  <option value="3">Tingkat 3</option>
				</select>
			  </div>
			  </div><!-- /.col -->
			<div class="col-md-10">
			  <div class="form-group"> 
				<input type="text" class="form-control" name="txtsearch" id="txtsearch"  placeholder="Cari No induk atau nama" value="" >
			  </div>
			</div><!-- /.col -->
			<div class="col-md-2">
			  <div class="form-group"> 
				<button  type="button"  title="Cari kelas" class="form-control btn btn-block btn-flat btn-primary "  id="searchrombel"  > Cari</button>
			  </div>
			</div><!-- /.col -->
		  </div>
		  
			<div class="table-responsive" style="height: 1136px; overflow-y: scroll; padding-left:5px;">
			  <table id="nonrombel" class="table  table-bordered table-hover ">
				<thead>
				  <tr class="tableheader">
					<th style="width:45px">#</th>
					<th>No Induk </th>
					<th>Nama lengkap </th>
					<th>MDK </th>
					<th >Proses [<label id="labelTerisi">0</label>/<label id="labelKapasitas">0</label>]</th>
				  </tr>
				</thead>
				<tbody></tbody>
			  </table>
			</div>
		  
		</div>
	  </div><!-- /.box -->
	</div><!-- /.col-6 -->
	
	<div class="col-md-6">
	  <div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Santri per kamar</h3>
		</div>
		<!--./ box header-->
		<div class="box-body no-padding">
		  <div class="row" style="padding-left:25px; padding-right:25px;">
		  	<div class="col-md-6">
			  <div class="form-group">
				<label>Firqoh</label>
				<select class="form-control" id="txtasrama" name="txtasrama" style="width: 100%;">
				  <option value="">Pilih Jenjang dahulu</option>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-6">
			  <div class="form-group">
				<label>Kamar</label>
				<select class="form-control" id="txtkamar" name="txtkamar" style="width: 100%;">
				  <option value="">Pilih Firqoh dahulu</option>	  
				</select>
			  </div><!-- /.form-group -->
			  </div><!-- /.col -->
			<div class="col-md-10">
			  <div class="form-group"> 
				<input type="text" class="form-control" name="txtsearch" id="txtsearch"  placeholder="Cari No induk atau nama" value="" >
			  </div>
			</div><!-- /.col -->
			<div class="col-md-2">
			  <div class="form-group"> 
				<button  type="button"  title="Cari kelas" class="form-control btn btn-block btn-flat btn-primary "  id="searchnon"  > Cari</button>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  
			<div class="table-responsive" style="height: 1136px; overflow-y: scroll; padding-left:5px;">
			  <table id="rombel" class="table  table-bordered table-hover ">
				<thead>
				  <tr class="tableheader">
					<th style="width:50px">Keluarkan </th>
					<th style="width:50px">#</th>
					<th>No Induk </th>
					<th>Nama lengkap </th>
					<th>MDK </th>
					<th>Tingkat </th>
				  </tr>
				</thead>
				<tbody></tbody>
			  </table>
			</div>
		  
		</div>
	  </div><!-- /.box -->
	</div><!-- /.col-6 -->
  </div><!-- /.box -->
				
</section><!-- /.content -->
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}

//searching nama
$(document).on("blur","#xx",function(){
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	var term = $(this).val();
	
	rombel(jenjang,tingkat,term); 
});

$(function () {
	$('#searchnon').on('click', function() {
	  var jenjang = $('#txtjenjang').val();
	  var tingkat = $('#txttingkat').val();
	  var term = $('#txtsearch').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
		var value = {
			jenjang: jenjang,
			tingkat: tingkat,
			term: term,
			method : "get_asrama_bytipe"
		};
		$.ajax({
			url : "c_kamar.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtasrama').html("");
					$('#txtasrama').html("<option value=''> Pilih firqoh </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtasrama').append('<option value="'+val.auto+'">'+val.nama_asrama+'</option>');
					})
				}else{
					$('#txtasrama').html("");
					$('#txtasrama').html("<option value=''> Firqoh tidak ditemukan </option>");
				}
				nonrombel(jenjang,tingkat,term);
				$("#txtasrama").trigger("click");
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	});
	
	$('#txtasrama').on('change', function() {
		var value = {
			asrama: this.value,
			method : "get_kamar_asrama"
		};
		$.ajax({
			url : "c_kamar.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				reset_rombel();
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtkamar').html("");
					$('#txtkamar').html("<option value=''> Pilih kamar </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
					})
				}else{
					$('#txtkamar').html("");
					$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	});
	
	$('#txtkamar').on('change', function() {
		$('#labelKapasitas').html('0');$('#labelTerisi').html('0');
		var kamar = $(this).val();
		if(kamar == '' || kamar== null ){
		  reset_rombel();
		}else{
		  kuota(kamar);
		  rombel(kamar);
		}
	});
	
});

$(document).on( "click",".checkin", function() {
	var id_item = $(this).attr("id_item");
	var idkamar = $("#txtkamar").val();
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	
	if(idkamar == '' || idkamar== null ){
		$.notify({
			message: "Kamar belum dipilih!"
		},{
			type: 'warning',
			delay: 8000,
		});		
		$("#txtkamar").focus();
		return;
	}
	var value = {
		id_item: id_item,
		kamar: idkamar,
		method : "checkin"
	};
	$.ajax(
	{
		url : "c_kamar.php",
		type: "POST",
		data : value,
		success: function(result, textStatus, jqXHR)
		{
			var data = jQuery.parseJSON(result);
			if(data.respons == true){
			  $("#nonrombel").DataTable().destroy();
			  nonrombel(jenjang,tingkat);
			  $("#rombel").DataTable().destroy();
			  rombel(idkamar);
			}else{
				$.notify({
					message: data.data
				},{
					type: 'warning',
					delay: 8000,
				});
			}
			kuota(idkamar);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
});

$(document).on( "click",".checkout", function() {
	var id_item = $(this).attr("id_item");
	var idkamar = $("#txtkamar").val(); 
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	
	var value = {
		id_item: id_item,
		kamar: idkamar,
		method : "checkout"
	};
	$.ajax(
	{
		url : "c_kamar.php",
		type: "POST",
		data : value,
		success: function(result, textStatus, jqXHR)
		{
			var data = jQuery.parseJSON(result);
			if(data.respons == true){
			  $("#nonrombel").DataTable().destroy();
			  nonrombel(jenjang,tingkat);
			  $("#rombel").DataTable().destroy();
			  rombel(idkamar);
			}else{
				$.notify({
					message: data.data
				},{
					type: 'warning',
					delay: 8000,
				});
			}
			kuota(idkamar);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
});

//searching nama
$(document).on("blur","#txtsearch",function(){
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	var term = $(this).val();
	
	nonrombel(jenjang,tingkat,term); 
});

function reset_rombel(){
	$('#labelKapasitas').html('0');$('#labelTerisi').html('0');
	$("#rombel").DataTable().destroy();
	$("#rombel tbody").html('');
}

function rombel(kamar){
	$("#rombel").DataTable().destroy();
	var value = {
		kamar:kamar,
		method : "group"
	};
	$('#rombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_kamar.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "button" },
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		{ "data": "mdk" },
		]
	});
} 
	
function nonrombel(jenjang,tingkat,term = ''){
	$("#nonrombel").DataTable().destroy();
	var value = {
		jenjang:jenjang,
		tingkat:tingkat,
		term:term,
		method : "ungroup"
	};
	$('#nonrombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_kamar.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		{ "data": "mdk" },
		{ "data": "button" },
		]
	});
}

function kuota(id){
	var param = {
		kamar:id,
		method : "count"
	};
	$.ajax(
	{
		url : "c_kamar.php",
		type: "POST",
		data : param,
		success: function(result, textStatus, jqXHR)
		{
			var data = jQuery.parseJSON(result);
			if(data.msg == 'error')
			{
				$('#labelKapasitas').html('0');$('#labelTerisi').html('0');
			}else{
				$('#labelKapasitas').html(data.kapasitas);$('#labelTerisi').html(data.terisi);
			}
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
}	
</script>
</body>
</html>
