<?php 
$titlepage="Laporan Mutasi Santri";
$idsmenu=25; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_psb();

?>
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
  <div class="content">
  
    <section class="content-header">
	  <h1>
		DAFTAR SANTRI MUTASI
	  </h1>
	  
	</section>
	<section class="content">
	  <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Filter</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
			<form method="post" id="target" class="form-horizontal" target="_blank" action="import_mutasi.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="excel">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Awal</label>
					<div class="col-sm-3">
					  <input type="text" class="form-control txtperiode tgl" name="first" id="txtfirstperiod"  value=""  data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> 	
					</div>
					<label class="col-sm-2  control-label" >Sampai</label>
					<div class="col-sm-3">
					  <input type="text" class="form-control txtperiode tgl" name="last" id="txtlastperiod"  value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
					</div>
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Jenjang</label>
					<div class="col-sm-3">
						<select class="form-control" id="jenjang" name="jenjang" >
						  <option value="">Semua</option>
						  <option value="ula putra">Ula Putra</option>
						  <option value="ula putri">Ula Putri</option>
						  <option value="wustho putra">Wustho Putra</option>
						  <option value="wustho putri">Wustho Putri</option>
						</select>
					</div>
					<label class="col-sm-2  control-label" >MDK/Bukan</label>
					<div class="col-sm-3">
						<select class="form-control" id="jenis" name="jenis" >
						  <option value="">Semua</option>
						  <option value="tidak">Bukan MDK</option>
						  <option value="mdk">MDK</option>
						</select>
					</div>
				  </div>				
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Tingkat</label>
					<div class="col-sm-3">
						<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
						  <option value="">Semua</option>
						  <option value="1">Tingkat 1</option>
						  <option value="2">Tingkat 2</option>
						  <option value="3">Tingkat 3</option>
						</select>
					</div>
				  </div>
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			<div class="box-footer">
			  <div class="pull-right">
				<button type="button" id="btnsearch" data-btn="search" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				<button type="submit" id="btndownloadx" data-btn="download" class="btn btn-success"><i class="fa fa-download"></i> Download excel</button>
				
				<span id="infoproses"></span>
			  </div>
			</div>
			</form>
			
          </div><!-- /.box -->
		  
		  <div class="box box-info">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Report mutasi</h3>
				</div> <!-- end of box-header -->
				<div class="box-body table-responsive">
					<table id="table_item" class="table  table-bordered table-hover ">
					  <thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th width="15%">No Induk </th>
							<th >Nama Santri </th>
							<th>Jenjang </th>
							<th>MDK </th>
							<th>Tingkat </th>
							<th>Status </th>
							<th>Alasan </th>
						</tr>
					  </thead>
					  <tbody></tbody>
					</table>
				</div> <!-- end of box-body -->
			</div>
		</div><!-- end of div box info -->
	</section><!-- /.content -->

  </div>
    <?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
				
		$(document).ready( function () 
		{		
			$("#txtfirstperiod,#txtlastperiod").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
			$('#txtfirstperiod,#txtlastperiod').datepicker({
				format: 'dd-mm-yyyy',
			});
		});
	function tabel(){
		var first =  $('#txtfirstperiod').val();
		var last = $('#txtlastperiod').val();
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		
		var value = {
			first: first,
			last: last,
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			method : "getdata"
		};
		$('#table_item').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"pageLength": 50,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_report_mutasi.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "jenjang" },
			{ "data": "jenis" },
			{ "data": "tingkat" },
			{ "data": "tanggal" },
			{ "data": "alasan_keluar" },
			]
		});
		$("#table_item_filter").addClass("pull-right");
	}
	
	$(document).on( "click","#btndownload, #btnsearch", function() {
		var opt = $(this).attr('data-btn');
		var first =  $('#txtfirstperiod').val();
		var last = $('#txtlastperiod').val();
		if(opt == 'search'){
			$("#table_item").DataTable().destroy();
			tabel();
		}else{
		    var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "import_mutasi.php";

			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "excel";
			mapForm.appendChild(method);
			
			var txtfirst = document.createElement("input");
			txtfirst.type = "text";
			txtfirst.name = "first";
			txtfirst.value = first;
			mapForm.appendChild(txtfirst);
			
			var txtlast = document.createElement("input");
			txtlast.type = "text";
			txtlast.name = "last";
			txtlast.value = last;
			mapForm.appendChild(txtlast);

			document.body.appendChild(mapForm);
			mapForm.submit();
			
		}
	});
	</script>
</body>
</html>
