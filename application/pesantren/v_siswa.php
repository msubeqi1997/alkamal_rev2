<?php 
$titlepage="Data Santri";
$idsmenu=17; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; ?>

<section class="content-header">
  <h1>
	DAFTAR SANTRI
	<small>data master</small>
  </h1>
</section>
		
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" btn-data="new" name=""><i class="fa fa-plus"></i> Tambah santri</button>
					<br><br>
				</div>
				<div class="col-md-6">
					<div class="pull-right">
					  <button type="submit" title="Update NIS" class="btn btn-primary bg-navy" id="btnupload" ><i class="fa fa-upload"></i> Update NIS</button>
					  <button type="submit" title="Download" class="btn btn-success" id="btndownload" ><i class="fa fa-download"></i> Download data</button> 				
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th >No Induk</th>
							<th style="width:22%">Nama Lengkap </th>
							<th style="width:30%">Alamat </th>
							<th class="select-filter">Jenjang </th>
							<th class="select-filter">MDK </th>
							<th class="select-filter">Tingkat </th>
							<th>Sekolah </th>
							<th style="width:60px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th class="select-filter"></th>
						<th class="select-filter"></th>
						<th></th>
						<th></th>
					</tfoot>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

	<div id="modalupload" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Update NIS</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
				  <form method="post" id="target" action="update_induk.php" enctype="multipart/form-data">
					<div class="form-group">
						<label for="upload">Upload File excel : </label>
						<input type="file" id="file" name="file">
						<input type="hidden" value="upload_nis" name="method">
					</div>
					<div class="form-group">
						<button type="submit" title="Upload" class="btn btn-primary" id="btnupload" ><i class="fa fa-upload"></i> Upload data</button> 				
					</div>
				  </form>
				  <div id="result">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script language="javascript">
	$(document).ready( function () 
	{
		var myArray = ['jenis'];

		var value = {
			method : "getdata"
		};
		$('#table_item').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax": {
				"url": "c_siswa.php",
				"type": "POST",
				"data":value,
			},
			initComplete: function () {
				this.api().columns('.select-filter').every( function () {
					var column = this;
					var select = $('<select class="form-control input-sm"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
							column.search( this.value ).draw();
						} );
					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );
				} );
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "address" },
			{ "data": "jenjang" },
			{ "data": "jenis" },
			{ "data": "tingkat" },
			{ "data": "tujuan" },
			{ "data": "button" },
			]
		});
		$("#table_item_filter").addClass("pull-right");
		
	
	});
	
	
	
	$(document).on( "click","#btnadd, .btnedit", function() {
		var opt = $(this).attr('btn-data');
		
		if(opt == 'new'){
			var mapForm = document.createElement("form");
			mapForm.target = "";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "v_editsiswa.php";
			
			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "N";
			mapForm.appendChild(method);
		}else{
			var mapForm = document.createElement("form");
			mapForm.target = "";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "v_editsiswa.php";
			
			var idsiswa = $(this).attr('id_siswa');
			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "E";
			mapForm.appendChild(method);
			
			var txtid = document.createElement("input");
			txtid.type = "text";
			txtid.name = "idsiswa";
			txtid.value = idsiswa;
			mapForm.appendChild(txtid);
		}
		document.body.appendChild(mapForm);

		mapForm.submit();
		
	});
	$(document).on( "click","#btndownload", function() {
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_siswa.php";

		var method = document.createElement("input");
		method.type = "text";
		method.name = "method";
		method.value = "import";
		mapForm.appendChild(method);
		
		document.body.appendChild(mapForm);
		mapForm.submit();
	});
	
	$(document).on("click","#btnupload",function(){
	  $("#modalupload").modal("show");
	  
	});
	
	$(function () {
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  
		  $("#btnupload").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin akan mengupdate data massal?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#btnupload").prop('disabled', false);
				$("#infoproses").html("");
				
			  }else{
				$("#btnupload").prop('disabled', false);
				$("#infoproses").html("");
			  }
			  
			  
			});
			return false;
		});
	});
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == true) {
		bootbox.alert("Update NIS telah berhasil.");
		$("#file").val("");
		$("#modalupload").modal("hide");
	  }
	  else{
		bootbox.alert(msg);
	  }
	};
</script>
</body>
</html>
