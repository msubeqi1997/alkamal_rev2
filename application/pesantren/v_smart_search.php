<?php 
$titlepage="Smart Search";
$idsmenu=75; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_data();
$asrama = $pos->getAsrama();
$satpen = $pos->getSatuanPendidikan(1);
?>

<section class="content-header">
  <h1>
	SMART SEARCH
	<small>report</small>
  </h1>
</section>
		
<section class="content">
	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="import_report_siswa.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Jenjang</label>
					<div class="col-sm-3">
						<select class="form-control" id="jenjang" name="jenjang" >
						  <option value="">Semua</option>
						  <option value="ula putra">Ula Putra</option>
						  <option value="ula putri">Ula Putri</option>
						  <option value="wustho putra">Wustho Putra</option>
						  <option value="wustho putri">Wustho Putri</option>
						</select>
					</div>
					<label class="col-sm-2  control-label" >MDK/Bukan</label>
					<div class="col-sm-3">
						<select class="form-control" id="jenis" name="jenis" >
						  <option value="">Semua</option>
						  <option value="tidak">Bukan MDK</option>
						  <option value="mdk">MDK</option>
						</select>
					</div>
				  </div>				
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Tingkat</label>
					<div class="col-sm-3">
						<select class="form-control" id="tingkat_kelas" name="tingkat_kelas" >
						  <option value="">Semua</option>
						  <option value="1">Tingkat 1</option>
						  <option value="2">Tingkat 2</option>
						  <option value="3">Tingkat 3</option>
						</select>
					</div>
					<label class="col-sm-2  control-label" >Firqoh</label>
					<div class="col-sm-3">
						<select class="form-control" id="asrama" name="asrama" >
						  <option value="">Semua</option>
						  <?php
						  foreach ($asrama[1] as $opt) {
							echo "<option value='".$opt['auto']."' ".$selected."> ".$opt['nama_asrama']." </option>";
						  }
						?>
						</select>
					</div>
					
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Sekolah tujuan</label>
					<div class="col-sm-3">
						<select class="form-control" id="sekolah_tujuan" name="sekolah_tujuan" >
						  <option value="">Semua</option>
						  <?php
						  foreach ($satpen[1] as $opt) {
							echo "<option value='".$opt['kode_sekolah']."'> ".$opt['nama_sekolah']." </option>";
						  }
						  ?>
						</select>
					</div>
					<label class="col-sm-2  control-label" >Kamar</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtkamar" name="kamar" >
						  <option value="">Semua</option>
						</select>
					</div>
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Status</label>
					<div class="col-sm-3">
						<select class="form-control" id="aktif" name="aktif" >
						  <option value="siswa">Aktif</option>
						  <option value="keluar">Keluar</option>
						</select>
					</div>
					<label class="col-sm-2  control-label" >Cari Nama/NIS</label>
					<div class="col-sm-4"><input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="Cara berdasar Nama / NIS"> </div>
				  </div>
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			<div class="box-footer">
			  <div class="pull-right">
				<button type="button" id="btnsearch" data-btn="search" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				<!--<button type="submit" name="excel" value="excel" id="btndownloadx" data-btn="download" class="btn btn-success"><i class="fa fa-download"></i> Download excel</button>-->
				<span id="infoproses"></span>
			  </div>
			</div>
		</form>
	</div><!-- /.box -->
		  
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th >No Induk</th>
							<th >Nama Lengkap </th>
							<th style="width:22%;">Alamat </th>
							<th class="select-filter">Jenjang </th>
							<th class="select-filter">MDK </th>
							<th class="select-filter">Tingkat </th>
							<th class="select-filter">Firqoh </th>
							<th class="select-filter">Kamar </th>
							<th>Sekolah </th>
						</tr>
					</thead>
					<tbody></tbody>
					
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalinfokamar" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Data Kamar</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Nama Kamar</label>
							<div class="col-sm-9" id="namakamar">
								
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kapasitas</label>
							<div class="col-sm-9" id="kapasitaskamar">
								 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Terisi</label>
							<div class="col-sm-9" id="kamarterisi">
								 
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
		</div>
			<!--modal-content-->
	</div>
	<!--modal-dialog modal-lg-->
</div>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script language="javascript">
	$(document).ready( function () 
	{
		$(document).on( "click",".btn-kamar", function() {
			$("#modalinfokamar").modal('show');
			var id_item = $(this).attr("id-kamar");
			var value = {
				id_item: id_item,
				method : "get_info_kamar"
			};
			$.ajax(
			{
				url : "c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$("#namakamar").html(hasil.nama);
					$("#kapasitaskamar").html(hasil.kapasitas);
					$("#kamarterisi").html(hasil.terisi);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btn-nama", function() {
			var id_item = $(this).attr("id-data");
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "info-siswa.php";

			var data = document.createElement("input");
			data.type = "hidden";
			data.name = "id";
			data.value = id_item;
			mapForm.appendChild(data);
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
		
		$(document).on( "click",".btn-nis", function() {
			var id_item = $(this).attr("id-data");
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "histori-siswa.php";

			var data = document.createElement("input");
			data.type = "hidden";
			data.name = "id";
			data.value = id_item;
			mapForm.appendChild(data);
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	
	});
	
	$('#asrama').on('change', function() {
		var value = {
			asrama: this.value,
			method : "get_kamar_asrama"
		};
		$.ajax({
			url : "c_kamar.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var respons = jQuery.parseJSON(data);
				if(respons.data.length>0){
					$('#txtkamar').html("");
					$('#txtkamar').html("<option value=''> Pilih kamar </option>");
					$.each(respons.data, function (key, val) {
					  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
					})
				}else{
					$('#txtkamar').html("");
					$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	});
	$(document).on( "click","#btnsearch", function() {
		$("#table_item").DataTable().destroy();
		data();
	});
	function data(){
		var jenjang = $('#jenjang').val();
		var jenis = $('#jenis').val();
		var tingkat = $('#tingkat_kelas').val();
		var asrama = $('#asrama').val();
		var kamar = $('#txtkamar').val();
		var sekolah = $('#sekolah_tujuan').val();
		var search = $('#txtname').val();
		var aktif = $('#aktif').val();
		var value = {
			method : "pencarian_siswa",
			jenjang : jenjang,
			jenis : jenis,
			tingkat : tingkat,
			asrama : asrama,
			kamar : kamar,
			sekolah : sekolah,
			search : search,
			aktif : aktif
		};
		$('#table_item').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"pageLength": 50,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_siswa.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "nis" },
			{ "data": "nama_lengkap" },
			{ "data": "alamat" },
			{ "data": "jenjang" },
			{ "data": "jenis" },
			{ "data": "tingkat" },
			{ "data": "asrama" },
			{ "data": "kamar" },
			{ "data": "sekolah" }
			]
		});
		$("#table_item_filter").addClass("pull-right");
	}
			
	
</script>
</body>
</html>
