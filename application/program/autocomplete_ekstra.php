<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_program();
$kelas = $_GET['kelas'];
$data = $pos->autoCompleteEkstra($term,$kelas);

foreach ($data[1] as $row) {
	$rows['siswa_id']=base64_encode(htmlentities(stripslashes($row['uuid'])));
	$rows['nis']=htmlentities(stripslashes($row['nis']));
	$rows['nama']=htmlentities(stripslashes($row['nama_lengkap']));
	
	$row_set[] = $rows;
}

echo json_encode($row_set);
?>