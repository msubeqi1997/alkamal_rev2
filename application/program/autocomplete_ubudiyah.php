<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");

$jenjang='';$jenis='';
$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_program();
$pend = $_GET['jenjang'];
$tingkat = $_GET['tingkat'];
$asrama = $_GET['asrama'];
$kamar = $_GET['kamar'];
if($pend == 'ula'){$jenis='tidak';$jenjang='2';}
else if($pend == 'wustho'){$jenis='tidak';$jenjang='3';}
else if($pend == 'mdk'){$jenis='mdk';$jenjang='3';}
$data = $pos->autoCompleteUbudiyah($term,$jenjang,$jenis,$tingkat,$asrama,$kamar);

foreach ($data[1] as $row) {
	$rows['siswa_id']=base64_encode(htmlentities(stripslashes($row['uuid'])));
	$rows['nis']=htmlentities(stripslashes($row['nis']));
	$rows['nama']=htmlentities(stripslashes($row['nama_lengkap']));
	
	$row_set[] = $rows;
}

echo json_encode($row_set);
?>