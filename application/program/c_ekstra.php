<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_program();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'get_list_ekstra'){
		
		$array = $pos->getListEkstra();
		
		$data = [];
		$i=0;
		foreach ($array[1] as $row) {
			$button = '<button  type="submit" id_item="'.$row['id_ekstrakurikuler'].'"  title="Tombol edit ekstrakurikuler" class="btn btn-sm btn-primary btnedit "><i class="fa fa-edit"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                        </div>
                       </td>';
			$data[$i]['urutan'] =$row['urutan'];
			$data[$i]['nama'] =$row['ekstrakurikuler'];
			$data[$i]['pembina'] =$row['trainer'];
			$data[$i]['action'] =$action;
			$i++;
		}
		
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$pembina = $_POST['pembina'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJenisEkstra($name,$pembina);
		}
		else
		{
			$array = $pos->updateJenisEkstra($iditem,$name,$pembina);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_ekstra')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailEkstra($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'nonrombel'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		$kelas = $_POST['kelas'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$satuan;
		$jenis;
		if($jenjang == 'mdk'){
			$satuan = '3'; $jenis = 'mdk';
		}else if ($jenjang == 'wustho'){
			$satuan = '3'; $jenis = 'tidak';
		}else{ $satuan = '2'; $jenis = 'tidak';}
		$array = $pos->getNonPesertaEkstra($jenjang,$tingkat,$satuan,$jenis,$kelas,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Masukkan rombel" class="btn btn-sm btn-success checkin "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-right"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'rombel'){
		$kelas = $_POST['kelas'];
		$array = $pos->getPesertaEkstra($kelas);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Keluarkan dari ekstra" class="btn btn-sm btn-danger checkout "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-left"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'checkin'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->saveCheckinEkstra($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Ekstrakurikuler belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'checkout'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->deletePesertaEkstra($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Kelas belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'getdaftarnilaisantri'){
		$kelas = $_POST['kelas'];
		
		$tapel = $pos->getTahunAkademik();
		$ekstra = $pos->getDetailEkstra($kelas);
		$nilai = $pos->getDetailNilaiEkstra($kelas);
		
		$datax = array('data' => $nilai[1], 'tapel' => $tapel[1]['thn_ajaran'],'ekstra' => $ekstra[1]['ekstrakurikuler']);
		echo json_encode($datax);
	}
	
	if($method == 'save_nilai_ekstra')
	{
		$kelas = $_POST['kelas'];
		$crud = $_POST['crud'];
		$siswa = isset($_POST['siswa'])?$_POST['siswa']:'';
		$nilai = isset($_POST['nilai'])?$_POST['nilai']:'';
		$note = isset($_POST['note'])?$_POST['note']:'';
		$user = $_SESSION['sess_id'];
		
		$delete_nilai = $pos -> deleteNilaiEkstra($kelas);
		$data = array();
		foreach($siswa as $k => $v){
		  $data[$k] = array();
		
		  $data[$k]['siswa'] = $v;
		  $data[$k]['ekstra'] = $kelas;
		  $data[$k]['nilai'] = $nilai[$k];
		  $data[$k]['note'] = $note[$k];
		  $data[$k]['user'] = $user;
		
		}
		$array = $pos -> saveNilaiEkstra($data);
		$result['result'] = $array[0];
		$result['msg'] = $array[1];
		
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}