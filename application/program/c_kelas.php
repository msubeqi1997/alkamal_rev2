<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_program();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	
	if($method == 'getdata'){
		$array = $pos->getKelas();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['id_kelas'].'"  title="Tombol edit kelas" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['id_kelas'].'"  ><i class="fa fa-edit"></i></button>';
			$print = '<button  type="submit" id_item="'.$key['id_kelas'].'"  title="Download rombongan" class="btn btn-sm btn-primary btndownload "  id="btndownload'.$key['id_kelas'].'"  ><i class="fa fa-download"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                          '.$print.'
						</div>
                       </td>';
			$data[$i]['button'] = $button;
			$data[$i]['tingkat_kelas'] = "Tingkat ".$key['tingkat'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$tingkat = $_POST['tingkat'];
		$ruang = $_POST['ruang'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveKelas($name,$tingkat,$ruang);
		}
		else
		{
			$array = $pos->updateKelas($iditem,$name,$tingkat,$ruang);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailKelas($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'nonrombel'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		if($_POST['term'] == NULL){
			$term = '';
		}else{
			$term = $_POST['term'];
		}
		$satuan;
		$jenis;
		if($jenjang == 'mdk'){
			$satuan = '3'; $jenis = 'mdk';
		}else if ($jenjang == 'wustho'){
			$satuan = '3'; $jenis = 'tidak';
		}else{ $satuan = '2'; $jenis = 'tidak';}
		$array = $pos->getNonRombel($jenjang,$tingkat,$satuan,$jenis,$term);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Masukkan rombel" class="btn btn-sm btn-success checkin "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-right"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'rombel'){
		$kelas = $_POST['kelas'];
		$array = $pos->getSiswaRombel($kelas);
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['uuid'].'"  title="Keluarkan dari rombel" class="btn btn-sm btn-danger checkout "  id="btn_'.$key['uuid'].'"  ><i class="fa fa-arrow-left"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	
	if($method == 'checkin'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->saveCheckin($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Kelas belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'checkout'){
		$id_siswa = $_POST['id_siswa'];
		$kelas = $_POST['kelas'];
		
		if($kelas != ''){
			$array = $pos->deleteRombelSiswa($id_siswa,$kelas);
			$result['respons'] = $array[0];
			$result['data'] = $array[1];
		}else{ 
			$result['respons'] = false;
			$result['data'] = 'Kelas belum dipilih';
		}
		echo json_encode($result);
	}
	
	if($method == 'get_wali_kelas'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$array = $pos->getWaliKelas($jenjang,$tingkat,$tapel);
		$data = $array[1];
		$i=0;
		
		foreach ($data as $key) {
			
			$wali ='<input type="hidden" name="idkelas[]" value="'.$key['id_kelas'].'">
					<input type="hidden" name="txtidwali[]" id="txtidwali'.$key['id_kelas'].'" value="'.$key['id_guru'].'">
					<input type="text" name="wali[]" id="wali'.$key['id_kelas'].'" value="'.$key['wali'].'" size="40" disabled>';
			$search = '<button  type="button" id="btnopenpegawai'.$key['id_kelas'].'" kelas="'.$key['id_kelas'].'" title="pilih wali" class="btn btn-primary btnopenpegawai"  ><i class="fa fa-search"></i></button>';
			$data[$i]['wali'] = $wali;
			$data[$i]['search'] = $search;
			$data[$i]['urutan'] = $i+1;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'simpan_wali_kelas')
	{
		$array = $pos->activeTapel();
		$tapel = $array[1]['thn_ajaran_id'];
		
		$value = array();
		$kelas = $_POST['idkelas'];
		$pegawai = $_POST['txtidwali'];
		foreach($kelas as $k => $v){
			$guru = (empty($pegawai[$k]))?'NULL':$pegawai[$k];
			$value[] ="(".$tapel.",".$guru.",".$v.")";
		}
		
		$delete = $pos->deleteWaliKelas($tapel);
		if($delete[0] == true){
			$query = $pos->insertWaliKelas($value);
			$result['result'] = $query[0];
			$result['msg'] = $query[1];
		}else{
			$result['result'] = false;
			$result['msg'] = $delete[1];
		}
		
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}