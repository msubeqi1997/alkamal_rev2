<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_program();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	
	if($method == 'getdatakategori'){
		$array = $pos->getKategoriUbudiyah();
		$i=0;
		$data=array();
		foreach ($array[1] as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit kategori" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.'
                        </div>
                       </td>';
			$data[$i]['urutan'] = $i+1;
			$data[$i]['button'] = $button;
			$data[$i]['kategori'] = $key['kategori'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	
	if($method == 'save_kategori')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveKategoriUbudiyah($name);
		}
		else
		{
			$array = $pos->updateKategoriUbudiyah($iditem,$name);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_kategori')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailKategoriUbudiyah($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'getdatamateri'){
		$array = $pos->getMateriUbudiyah();
		$i=0;
		$data=array();
		foreach ($array[1] as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit materi" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			$delete = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol delete materi" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$key['auto'].'"  ><i class="fa fa-trash"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.' 
                          '.$delete.' 
                        </div>
                       </td>';
			$data[$i]['urutan'] = $key['urutan'];
			$data[$i]['kategori'] = $key['kategori'];
			$data[$i]['materi'] = $key['materi'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'getdataplotting'){
		$array = $pos->getPlottingMateriUbudiyah();
		$i=0;
		$data=array();
		foreach ($array[1] as $key) {
			$button = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol edit materi" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['auto'].'"  ><i class="fa fa-edit"></i></button>';
			$delete = '<button  type="submit" id_item="'.$key['auto'].'"  title="Tombol delete materi" class="btn btn-sm btn-danger btndelete "  id="btndelete'.$key['auto'].'"  ><i class="fa fa-trash"></i></button>';
			$action = '<td>
                        <div class="btn-group">
                          '.$button.' 
                          '.$delete.' 
                        </div>
                       </td>';
			$data[$i]['urutan'] = $key['urutan'];
			$data[$i]['jenjang'] = ucfirst($key['jenjang']);
			$data[$i]['tingkat'] = $key['tingkat'];
			$data[$i]['kategori'] = $key['kategori'];
			$data[$i]['materi'] = $key['materi'];
			$data[$i]['action'] = $action;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_materi')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$kategori = $_POST['kategori'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveMateriUbudiyah($name,$kategori);
		}
		else
		{
			$array = $pos->updateMateriUbudiyah($iditem,$name,$kategori);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'save_plotting_materi')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$tingkat = $_POST['tingkat'];
		$jenjang = $_POST['jenjang'];
		$crud=$_POST['crud'];
		$cek = $pos->cekPlottingMateri($tingkat,$jenjang,$name);
		
		if($_POST['crud'] == 'N')
		{
		  if($cek[1]['plotting']==NULL){
			$array = $pos->savePlottingMateriUbudiyah($name,$tingkat,$jenjang);
		  }else
		  {
			$array[0]=false;
			$array[1]='Data sudah ada';
		  }
		}
		else
		{
			$array = $pos->updatePlottingMateriUbudiyah($iditem,$name,$tingkat,$jenjang);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_plotting')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailPlottingMateriUbudiyah($id);
		$cat = $array[1]['kategori'];
		$materi = $pos->getMateriUbudiyahByCat($cat);
		$result['data'] = $array[0];
		$result['materi'] = $materi[1];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_materi')
	{
		$id = $_POST['id_item'];
		$array = $pos->getDetailMateriUbudiyah($id);
		$result['data'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	if($method == 'delete_materi_ubudiyah')
	{
		$id = $_POST['id_item'];
		$array = $pos->deleteMateriUbudiyah($id);
		$result['result'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'delete_materi_ubudiyah')
	{
		$id = $_POST['id_item'];
		$array = $pos->deletePlottingMateriUbudiyah($id);
		$result['result'] = $array[0];
		$result['msg'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'get_materi_ubudiyah')
	{
		$id = $_POST['cat'];
		$array = $pos->getMateriUbudiyahByCat($id);
		$result['msg'] = $array[0];
		$result['data'] = $array[1];
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}