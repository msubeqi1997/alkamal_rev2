<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_program();
	$method=$_POST['method'];
	
	$order_jenjang = array('ula' => '1','wustho' => '2','mdk' => '3');
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'get_list_siswa'){
		$jenjang = $order_jenjang[$_POST['jenjang']];
		$tingkat = $_POST['tingkat'];
		$array = $pos->getSearchSiswa($jenjang,$tingkat);
		
		$data = [];
		$i=0;
		foreach ($array['data'] as $row) {
			
			$data[$i]['urutan'] =$row['urutan'];
			$data[$i]['nis'] =$row['nis'];
			$data[$i]['nama'] =$row['nama_lengkap'];
			$data[$i]['idSiswa'] =base64_encode(htmlentities(stripslashes($row['nomor'])));
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	if($method == 'getdaftarnilaisantri'){
		$jenjang = $_POST['jenjang'];
		$tingkat = $_POST['tingkat'];
		$idSiswa = base64_decode($_POST['idSiswa']);
		
		$detail = $pos->getDetailJenjangSiswa($idSiswa);
		$nilai = $pos->getDetailNilaiUbudiyah($jenjang,$tingkat,$idSiswa);
		$detail = $detail[1];
		$jenjang = ($detail['jenis']=='MDK')?'MDK':$detail['jenis'];
		$tingkat = "Tingkat ".$detail['tingkat_kelas'];
		
		$data = [];
		$i=0;
		foreach ($nilai[1] as $row) {
			
			$button = '<button  type="button" id_data="'.$row['auto'].'"  title="Edit nilai" class="btn btn-sm btn-primary btnedit"  id="btnedit'.$row['auto'].'"  ><i class="fa fa-save"></i> Simpan</button>';
			$stat = ($row['status'] == 'mengulang')?'selected':'';		   
			$date = (!empty($row['date']))?display_to_report($row['date']):'';		   
			$data[$i]['kategori'] =$row['kategori'];
			$data[$i]['materi'] =$row['materi'];
			$data[$i]['nilai'] ='<input type="text" class="form-group decimal" name="nilai[]" id="nilai'.$row['auto'].'" value='.$row['nilai'].'>';
			$data[$i]['status'] = '<select name="status[]" id="status'.$row['auto'].'" class="form-control input-sm">
									<option value="lulus"> Lulus </option>
									<option value="mengulang" '.$stat.'> Mengulang </option>
								  </select>';
			$data[$i]['tanggal'] ='<div class="input-group">
									<input type="text" class="form-control txttanggal" id="tanggal'.$row['auto'].'"  name="txttanggal[]" value="'.$date.'" data-inputmask="\'alias\': \'dd-mm-yyyy\'" data-mask>
									<div class="input-group-addon">
									  <i class="fa fa-calendar"></i>
									</div>
								  </div>';
			$data[$i]['button'] =$button;
			$i++;
		}
		$datax = array('data' => $data, 'nis' => $detail['nis'],'nama' => $detail['nama_lengkap'],'tingkat' => $tingkat,'jenjang' =>$jenjang);
		echo json_encode($datax);
	}
	
	if($method == 'save_nilai_santri')
	{
		$tanggal = display_to_sql($_POST['tanggal']);
		$materi = $_POST['materi'];
		$nilai = $_POST['nilai'];
		$siswa = base64_decode($_POST['siswa']);
		$status = $_POST['status'];
		$user = $_SESSION['sess_id'];
		
		$cek_nilai = $pos -> cekNilaiUbudiyah($siswa,$materi);
		if($cek_nilai[1]['nilai'] == NULL){
		  $simpan = $pos -> simpanNilaiUbudiyah($siswa,$materi,$tanggal,$nilai,$status,$user);
		  $result['result'] = $simpan[0];
		  $result['msg'] = $simpan[1];
		}else{
		  $update = $pos -> updateNilaiUbudiyah($cek_nilai[1]['nilai'],$tanggal,$nilai,$status,$user);
		  $result['result'] = $update[0];
		  $result['msg'] = $update[1];
		}
		
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}