<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_program();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$kelas = $_POST['kelas'];
	$nama_kelas = $pos->getDetailKelas($kelas);
	$hari = $pos->hariJadwal();
		
		
		
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'JADWAL KELAS '.$nama_kelas[1]['kelas'])
	;
	$span_hari='B';
	foreach($hari[1] as $day){
	  $rows='7';
	  $spreadsheet->setActiveSheetIndex(0)
		->setCellValue(''.$span_hari.'5', $day['hari'])
	  ;
	  
	  
	  $jam = $pos->jamJadwal($day['hari_ke']);
	  foreach($jam[1] as $row){
		$jam = $pos->jadwalMapel($row['id'],$kelas);
		$tutor = array(); $subject = array();
		foreach($jam[1] as $v){
		  $tutor[] = $v['tutor'];
		  $subject[]=$v['subject'];
		}
		$rows_subject=$rows+1;
		$rows_tutor=$rows+2;
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($span_hari.$rows, ''.$row['jam_mulai'].' - '.$row['jam_selesai'].'')
		  ->setCellValue($span_hari.$rows_subject, implode('',array_unique($subject)))
		  ->setCellValue($span_hari.$rows_tutor, implode(', ',$tutor))
	    ;
		$rows=+5;
		
		
	  }
	  $span_hari++;
	}
	
	
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('jadwal');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}