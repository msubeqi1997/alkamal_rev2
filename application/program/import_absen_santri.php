<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_program();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$kelas = $_POST['kelas'];
	$first = display_to_sql($_POST['firstdate']);
	$last = display_to_sql($_POST['lastdate']);
	$nama_kelas = $pos->getDetailKelas($kelas);
	$result = $pos->getReportAbsenSiswa($kelas,$first,$last);
	$array = $pos->generateDate($first,$last);
	$coldate = array_reverse($array[1]);
	$column = [];
	foreach($result[1] as $key=>$val){
		$id=base64_encode($val['uuid']).'|'.$val['nis'];
		$column[$id][$val['nama_lengkap']][$val['tanggal']] = $val['absen']; 
	}
		
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'KELAS :'.$nama_kelas[1]['kelas'])
	->setCellValue('A5', 'NO')
	->setCellValue('B5', 'NIS')
	->setCellValue('C5', 'NAMA')
	;
	
	$num =1;
	$rows = 6;
	$col = 'D';
	
	foreach($coldate as $row){
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', $row['date'])
		;
		$col++;
	}
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($col.'5', 'Total Masuk')
		;
		$col++;
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', 'Total Alpa')
		;
		$col++;
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', 'Total Ijin')
		;
		$col++;
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($col.'5', 'Total Sakit')
		;
	
	foreach($column as $keysis=>$valsis){
	  $nis = explode('|',$keysis);
	  $spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$rows, $num)
		->setCellValue('B'.$rows, $nis[1])
	  ;
	  
	  foreach($valsis as $keyname => $valname){
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('C'.$rows, $keyname)
		;
		$present_values=array();
		$present='D';
		foreach($coldate as $val){
			if( array_key_exists($val['date'],$valname) ) {
				$content = $valname[$val['date']];
			}else{
				$content = 'x';
			}
			$present_values[]=$content;
			$spreadsheet->setActiveSheetIndex(0)
			  ->setCellValue($present.$rows, $content)
			;
			$present++;
		}
		$counts = array_count_values($present_values);

		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($present.$rows, isset($counts['H'])? $counts['H']:'0')
		;
		$present++;
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($present.$rows, isset($counts['A'])? $counts['A']:'0')
		;
		$present++;
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($present.$rows, isset($counts['I'])? $counts['I']:'0')
		;
		$present++;
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue($present.$rows, isset($counts['S'])? $counts['S']:'0')
		;
	  }
		
	  $num++;$rows++;
	}
	// Miscellaneous glyphs, UTF-8
	
	
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}