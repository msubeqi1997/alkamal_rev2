<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_program();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	$kelas = $_POST['kelas'];
	$subjek = $_POST['subjek'];
	$subsubjek = $_POST['subsubjek'];
	$nama_kelas = $pos->getDetailKelas($kelas);
	$sub_mapel = $pos->getdetailbysubsubjek($subsubjek);
	$header = $pos->getheaderrekapsubmapel($kelas,$subjek,$subsubjek);
	$result = $pos->getrekapsubmapel($kelas,$subjek,$subsubjek);
	
	$column = [];
	foreach($header[1] as $key=>$val){
		$column[$val['nama_mapel']][$val['sub_mapel']][$val['jenis_nilai']][$val['id_master_nilai']] = $val['nama_nilai']; 
	}
	
	$nilai = [];
	foreach($result[1] as $key=>$val){
		$id=base64_encode($val['uuid']).'|'.$val['nis'];
		$nilai[$id][$val['nama_lengkap']][$val['id_master_nilai']] = $val['nilai']; 
	}
	
	$spreadsheet->getActiveSheet()->mergeCells("A5:A8");
	$spreadsheet->getActiveSheet()->mergeCells("B5:B8");
	$spreadsheet->getActiveSheet()->mergeCells("C5:C8");
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'REKAP NILAI KELAS '.$nama_kelas[1]['kelas'])
	->setCellValue('A2', 'SUBJEK :'.$sub_mapel[1]['nama_mapel'])
	->setCellValue('A3', 'SUB SUBJEK :'.$sub_mapel[1]['sub_mapel'])
	->setCellValue('A5', 'NO')
	->setCellValue('B5', 'NIS')
	->setCellValue('C5', 'NAMA')
	;
	
	$master = array();
	foreach($column as $keymapel => $valmapel){
	  $span_mapel='D';
	  
		foreach($valmapel as $keysub => $valsub){
		  $span_sub='D';
		  $span_jenis='D';
			foreach($valsub as $keyjenis => $valjenis){
			  $count_jenis = count($valjenis);
			  $nextspan = $span_jenis;
			  for($i = 1; $i < $count_jenis; $i++) {
				$nextspan++;
			  }
			  $spreadsheet->getActiveSheet()->mergeCells("".$span_jenis."7:".$nextspan."7");
			  $spreadsheet->setActiveSheetIndex(0)
				->setCellValue(''.$span_jenis.'7', $keyjenis)
			  ;
			  
			  $span_nilai=$span_sub;
			  $urut=1;
				foreach($valjenis as $keynilai => $valnilai){
					
					$master[]=$keynilai;	
					
					  $spreadsheet->setActiveSheetIndex(0)
						->setCellValue(''.$span_nilai.'8', $urut)
					  ;
					$span_mapel++;$span_sub++;$span_jenis++;$urut++;$span_nilai++;
				}
			  
			  
			}
			
		  $spreadsheet->getActiveSheet()->mergeCells("D6:".$span_sub."6");
		  $spreadsheet->setActiveSheetIndex(0)
			->setCellValue('D6', $keysub)
		  ;	
		}
	  $spreadsheet->getActiveSheet()->mergeCells("D5:".$span_mapel."5");
	  $spreadsheet->setActiveSheetIndex(0)
		->setCellValue('D5', $keymapel)
	  ;
	}
	
	$master_nilai=array_reverse($master);
	$num=1;
	$rows =9;
	foreach($nilai as $keysis=>$valsis){
	  $nis = explode('|',$keysis);
	  $spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$rows, $num)
		->setCellValue('B'.$rows, $nis[1])
	  ;
	  foreach($valsis as $keyname => $valname){
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('C'.$rows, $keyname)
		;
		
		$present='D';
		foreach($master as $val){
		  	
		  if(array_key_exists($val,$valname)){				
			  $content = $valname[$val];
		  }else{
			  $content = 'x';
		  }
			
			$spreadsheet->setActiveSheetIndex(0)
			  ->setCellValue($present.$rows, $content)
			;
			$present++;
		}
	  }
	  $num++;$rows++;
	}
	// Miscellaneous glyphs, UTF-8
	
	
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}