<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
		
	$pos = new model_program();
	
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');
	
	$jenjang='';$jenis='';
	$pend = $_POST['jenjang'];
	$tingkat = $_POST['tingkat'];
	$asrama = $_POST['asrama'];
	$kamar = $_POST['kamar'];
	$status = $_POST['status'];
	if($pend == 'ula'){$jenis='tidak';$jenjang='2';}
	else if($pend == 'wustho'){$jenis='tidak';$jenjang='3';}
	else if($pend == 'mdk'){$jenis='mdk';$jenjang='3';}
	$data = $pos->getReportNilaiUbudiyah($jenjang,$jenis,$tingkat,$asrama,$kamar,$status);
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'REKAP NILAI UBUDIYAH ')
	->setCellValue('A5', 'NO')
	->setCellValue('B5', 'NIS')
	->setCellValue('C5', 'NAMA')
	->setCellValue('D5', 'KATEGORI')
	->setCellValue('E5', 'MATERI')
	->setCellValue('F5', 'TANGGAL UJIAN')
	->setCellValue('G5', 'NILAI')
	->setCellValue('H5', 'STATUS')
	->setCellValue('I5', 'PENGUJI')
	;
		
	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=6;
		
	foreach($data as $row) {
		
		$spreadsheet->setActiveSheetIndex(0)
		  ->setCellValue('A'.$i, $num)
		  ->setCellValue('B'.$i, $row['jenis'])
		  ->setCellValue('C'.$i, $row['bobot'])
		  ->setCellValue('D'.$i, $row['poin'])
		;
		$i++; $num++;
	}
	
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}