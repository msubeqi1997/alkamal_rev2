<?php
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');
require_once ("../model/dbconn.php");
require_once ("../model/model_program.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();

$pos = new model_program();
$valjenjang = $_POST['jenjang'];
$valtingkat = $_POST['tingkat'];
$id_siswa = base64_decode($_POST['siswa']);
$detail = $pos->getDetailJenjangSiswa($id_siswa);
$akademik = $pos->getTahunAkademik();
$siswa = $detail[1];
$jenjang = ucfirst($valjenjang);
$jenis = ($siswa['jenis']=='MDK')?'MDK':$siswa['jenis'];
$tingkat = "Tingkat ".$siswa['tingkat_kelas'];
$nilai = $pos->getDetailNilaiUbudiyah($valjenjang,$valtingkat,$id_siswa);
$pejabat = $pos->getPejabat();
$riwayat = array();
$urutan =1;
foreach($nilai[1] as $row){
	$riwayat[] ='<tr>
				  <td class="center">'.$urutan.'</td>
				  <td>'.$row['kategori'].'</td>
				  <td>'.$row['materi'].'</td>
				  <td class="center">'.$row['nilai'].'</td>
				  <td class="center">'.grade($row['nilai']).'</td>
				</tr>';
	$urutan++;
}
$now = (new \DateTime())->format('Y-m-d');
if($siswa[1]['wali']==''){ $wali_santri = $siswa[1]['ayah'];} else{ $wali_santri = $siswa[1]['wali']; }	
$html ='
  <!DOCTYPE html>
  <html lang="id">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  </head>
  <body>
	<div class="kertas">
	  ';

$html .='
		<div class="kop-surat">
			<img src="../../image/header_raport_intensive.png" alt="Kop surat">
		</div>
		<table class="regular blank">
		  <tbody>
			  <tr>
			    <td class="lwidth">Nama</td>
			    <td class="swidth">:</td>
				<td> '.$siswa['nama_lengkap'].'</td>
				<td class="lwidth">Nomor Induk</td>
			    <td class="swidth">:</td>
				<td> '.$siswa['nis'].'</td>
			  </tr>
			  <tr>
				<td class="">Jenjang</td>
				<td>:</td>
				<td> '.$jenjang.'</td>
				<td class="lwidth">Tahun Akademik</td>
			    <td class="swidth">:</td>
				<td> '.$akademik[1]['thn_ajaran'].'</td>
			  </tr>
			  <tr>
			    <td class="">Tingkat</td>
				<td>:</td>
				<td> '.$tingkat.'</td>
				<td class="lwidth">Semester</td>
			    <td class="swidth">:</td>
				<td> '.ucfirst($akademik[1]['semester']).' </td>
			  </tr>
		  </tbody>
		</table>
		
		<table class="tablebordered" style="margin-top:20px;">
		  <thead>
			<tr class="tableheader">
				<th style="width:30px; background-color:#e8e8e8;">NO</th>
				<th style="width:125px; background-color:#e8e8e8;">KATEGORI MATERI</th>
				<th style="width:140px; background-color:#e8e8e8;">MATERI</th>
				<th style="width:69px; background-color:#e8e8e8;">NILAI</th>
				<th style="width:69px; background-color:#e8e8e8;">GRADE</th>
			</tr>
		  </thead>
		  <tbody>
			'.implode($riwayat).'
		  </tbody>
		</table>
		<table class=" col-2 blank">
		  
		</table>
		  <table class="tablebordered" style="margin-top:20px; width: 55%;">
			<tr>
			  <td class="center">A</td>
			  <td>Istimewa</td>
			  <td class="center">86 - 100</td>
			</tr>
			<tr>
			  <td class="center">B</td>
			  <td>Memuaskan</td>
			  <td class="center">71 - 85</td>
			</tr>
			<tr>
			  <td class="center">C</td>
			  <td>Baik</td>
			  <td class="center">56 - 70</td>
			</tr>
			<tr>
			  <td class="center">D</td>
			  <td>Kurang baik</td>
			  <td class="center">1 - 55</td>
			</tr>
		  </table>
		<div class="tgl-surat">Dikeluarkan di &nbsp;&nbsp;&nbsp;: Blitar</div>
		<div class="tgl-surat">Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '.tanggal_indo($now).'</div>
		<table class="tanda-tangan col-2 blank">
		  <tr>
			  <td>Koordinator Tarbiyah</td>
			  <td>Orang tua/Wali</td>
		  </tr>
		  <tr>
			  <td class="bold" >M. Zunaidi Abas Bahria, SHI</td>
			  <td class="bold">'.ucfirst(strtolower($wali_santri)).'</td>
		  </tr>
		</table>
		<table class="tanda-tangan col-2 blank">
		  <tr>
			<td>Rois Markazy</td>
		  </tr>
		  <tr>
			<td class="bold">'.$pejabat[1]['ketua'].'</td>
		  </tr>
		  
		</table>
	</div>

  <style>
  @page {
    /* width = 21cm */
	size: 8.5in 12.99in;
    margin: 0.5cm 1cm 0.5cm;

  }
  @media print{
    table{
      width: 19cm;
    }

  }
  .kertas{
    width: 100%;
    margin: 0 auto;
    font-size: 11pt;
  }
  
  .kertas.ganti-halaman{
	page-break-before: always;
  }
		  
  .kop-surat img{
    width:100%;
  }
  table{
    width: 100%;
  }
  
  .tablebordered {border-collapse:collapse; }
  .tablebordered th { color:#000; vertical-align:middle;}
  .tablebordered td, .tablebordered th { padding:3px;border:1px solid #000; }
  table tr td{
    padding: 1px 0;
    vertical-align: top;
    text-align: left;
  }
  table tr th{
    padding: 3px 0;
    vertical-align: top;
    text-align: center;
  }
  table tr td.label{
    font-weight: bold;
    width: 30%;
  }
  table tr td.label + td{
    width: 1%;
  }
  table tr td.label + td + td{
    width: 69%;
  }
  table.formulir{
    margin-bottom: 20px;
  }
  .judul-surat{
    text-align: center;
    position: relative;
	margin-top: 10px;
  }
  .nomor-surat{
    margin:0;
  }
  .judul-surat h1{
    border-bottom: 1px solid #000;
    font-size: 12pt;
    display: inline;
    padding: 0 5px;
    margin-bottom:0;
    margin-top: 5px;
    text-transform: uppercase;
    letter-spacing: 2px;
    line-height: 11pt;
  }
  
  .pengembalian-surat{
    position: absolute;
	right: 0px;
    padding-top: -120px;
	border: 1px solid #dd4b39;
  }
  h3{
    text-align: center;
    font-size: 11pt;
    margin:0 0 3px;
    line-height: 11pt;
	font-weight:reguler;
  }
  .bold {
	 font-weight: bold; 
  }
  .center {
	text-align:center;
  }
  .blank{
    margin-top:40px;
  }
  .lm-20{
    margin-left: 20px;
  }
  .lwidth{
	width:140px;
  }
  .swidth{
	width:12px;
  }
  .tgl-surat{
    text-align: left;
    width:30%;
	padding-left:513px;
  }
  .tanda-tangan{
    margin-bottom: 10px;
  }

  .tanda-tangan tr td{
    vertical-align: bottom;
    text-align: center;
    padding: 0;
  }
  .tanda-tangan tr:first-child td{
    vertical-align: top;
    height:  62px;
  }
  .garis-nama{
    border-bottom: 1px solid #000;
  }
  .catatan{
    font-style: italic;
    margin-top: 10px;
    font-size: 10pt;
  }
  table.col-2 tr td{
    width: 50%;
  }
  table.col-4 tr td{
    width: 25%;
  }
  p{
    margin:5px 0;
    text-align: justify;
  }
  </style>

  </body>
  </html>
  ';
 
$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file='raport_ubudiyah.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

function daftar_hari($hari){	
	$daftar_hari = array(
	 'Sunday' => 'Ahad',
	 'Monday' => 'Senin',
	 'Tuesday' => 'Selasa',
	 'Wednesday' => 'Rabu',
	 'Thursday' => 'Kamis',
	 'Friday' => 'Jumat',
	 'Saturday' => 'Sabtu'
	);
	return $daftar_hari[$hari];
}

function grade($score) {
    if (!$score) {
        return ' ';
    } elseif ($score <= 55) {
        return 'D';
    } elseif ($score <= 70) {
        return 'C';
    } elseif ($score <= 85) {
        return 'B';
    } elseif ($score <= 100) {
        return 'A';
    }
}

