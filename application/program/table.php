<?php
$titlepage="Input Nilai Program Intensif";
$idsmenu=52; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 
?>


        <div class="row">
            <div class="col-md-5">
			<p><b>Details:</b> <span id="example-console">N/A</span></p>
                <table id="example" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Kode Obat</th>
                                    <th>Nama Obat</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                    <tr class="pilih" data-kodeobat="5">
                                        <td>1</td>
                                        <td>Dramamin</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="2">
                                        <td>2</td>
                                        <td>Ultraflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="3">
                                        <td>3</td>
                                        <td>Antangin</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="4">
                                        <td>4</td>
                                        <td>Sanaflu</td>
                                    </tr>
                                    <tr class="pilih" data-kodeobat="5">
                                        <td>5</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="6">
                                        <td>6</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="7">
                                        <td>7</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="8">
                                        <td>8</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="9">
                                        <td>9</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="10">
                                        <td>10</td>
                                        <td>Sanaflu</td>
                                    </tr>
									<tr class="pilih" data-kodeobat="11">
                                        <td>11</td>
                                        <td>Sanaflu</td>
                                    </tr>
                            </tbody>
                        </table>
            </div>
        </div>
        
		
		<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.1.1/css/keyTable.dataTables.min.css">
		<script src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
		<script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
        <script language="javascript">
		
//          
			
			
//            tabel lookup obat
            $(document).ready(function (){
				var div = document.getElementById('example-console');
				var table = $('#example').DataTable({
					keys: {
					   keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ],
					//   focus: ':eq(0)'
					},
					"paging": false,
				"lengthChange": false,
				"searching": true,
				"ordering": false,
				"info": false,
				"responsive": true,
				"autoWidth": false,
					
				});
				$('#example_filter input', table.table().container()).focus();
				$('#example_filter input').unbind();
				$("#example_filter input").keyup( function (e) {
				if (e.keyCode == 13) {
					table.search( this.value ).draw();
				}else if(e.keyCode == 40) {
					$('#example_filter input', table.table().container()).blur();
					table.cell( ':eq(0)' ).focus();
				}
				} );

				// Handle event when cell gains focus
				$('#example').on('key-focus.dt', function(e, datatable, cell){
					// Select highlighted row
					$(table.row(cell.index().row).node()).addClass('selected');
				});

				// Handle event when cell looses focus
				$('#example').on('key-blur.dt', function(e, datatable, cell){
					// Deselect highlighted row
					$(table.row(cell.index().row).node()).removeClass('selected');
				});
					
				// Handle key event that hasn't been handled by KeyTable
				$('#example').on('key.dt', function(e, datatable, key, cell, originalEvent, indexes ){
					// If ENTER key is pressed
					if(key === 13){
						div.innerHTML += ' Hello World<br />';
						/*var data = table.row(cell.index().row).data()[1];
						var node = table.row( cell.index().row ). nodes()[0];
						var bankid = $(node).attr("data-kodeobat");
						// FOR DEMONSTRATION ONLY
						//$("#example-console").html($('.pilih').eq(cell.index().row).attr('data-kodeobat'));
						$("#example-console").html(table.row(cell.index().row).data()[1]);*/
					}
				});
			});
			
			function dummy() {
                var kode_obat = document.getElementById("kode_obat").value;
                alert('kode obat ' + kode_obat + ' berhasil tersimpan');
            }
		
        </script>
    
