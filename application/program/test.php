<?php
$titlepage="Input Nilai Program Intensif";
$idsmenu=52; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
require_once("../../plugins/I18N/Arabic.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 
 
	error_reporting(E_STRICT);
	$Arabic = new I18N_Arabic('Numbers'); 
	$Arabic->setFeminine(2);
    $Arabic->setFormat(2);

    $integer = 'Muhamad Nur Rafli';
	
    $text = $Arabic->en2ar($integer);
?>


        <div class="row">
            <div class="col-md-5">
                <h2>modal lookup - harviacode.com</h2>
                <?php
				echo $text;
				?>
            </div>
        </div>
        
		
		<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.1.1/css/keyTable.dataTables.min.css">
		<script src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
		<script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
        <script language="javascript">
		
//            jika dipilih, kode obat akan masuk ke input dan modal di tutup
            $(document).on( "click",".pilih", function() {
                document.getElementById("kode_obat").value = $(this).attr('data-kodeobat');
                $('#myModal').modal('hide');
            });
			
			$(document).on( "click",".mdlookup", function() {
               lookup();
            });
//            tabel lookup obat
            function lookup (){
				var div = document.getElementById('console');
				$('#modalsearch').modal({
					focus: this,
					show: true
				  });
					
				var table = $('#example').DataTable({
					keys: {
					   keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ],
					//   focus: ':eq(0)'
					}
				});
				$('div.dataTables_filter input', table.table().container()).focus();
				
				// Handle event when cell gains focus
				$('#example').on('key-focus.dt', function(e, datatable, cell){
					// Select highlighted row
					$(table.row(cell.index().row).node()).addClass('selected');
				});

				// Handle event when cell looses focus
				$('#example').on('key-blur.dt', function(e, datatable, cell){
					// Deselect highlighted row
					$(table.row(cell.index().row).node()).removeClass('selected');
				});
					
				// Handle key event that hasn't been handled by KeyTable
				table.one('key.dt', function(e, datatable, key, cell, originalEvent){
					// If ENTER key is pressed
					if(key === 13){
						div.innerHTML += ' Hello World<br />';
						
						/*var data = table.row(cell.index().row).data();
						//$("#example-console").html($('.pilih').eq(cell.index().row).attr('data-kodeobat'));
						setTimeout( function() {
							
							editor
								.one( 'close', function () {
									table.keys.enable();
								} )
								.inline( cell.node() );
						}, 100 );
				 
						table.keys.disable();*/
					}
					//$("#example").DataTable().destroy();
					table.destroy();
					$("#example").data('event');
					$("#modalsearch").modal("hide");
				});
			};
			
			$(document).ready(function (){
				
				
			});
			function dummy() {
                var kode_obat = document.getElementById("kode_obat").value;
                alert('kode obat ' + kode_obat + ' berhasil tersimpan');
            }
		
        </script>
    
