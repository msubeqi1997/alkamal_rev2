<?php 
$titlepage="Rekap Raport Santri";
$idsmenu=56; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
?>
<section class="content-header">
  <h1>
	RAPORT SANTRI
	<small>Program Bahasa Intensif</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Kelas</label>
					<div class="col-sm-3">
						<select class="form-control" id="kelas" name="kelas" >
						  <option value="">Pilih Kelas</option>
						  <?php 
							foreach($kelas[1] as $row){
							  echo "<option value='".$row['id_kelas']."'>".$row['kelas']."</option>";
							}
						  ?>
						</select>
					</div>
					
				  </div>
				  <div class="form-group"> 
					
					
				  </div>
				  <div class="form-group">
					<label class="col-sm-2  control-label">Santri</label>
					<div class="col-sm-5">
						<input type="hidden" id="id_siswa">
						<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nis...">
					</div>
					<div class="col-sm-2 ">
					  <button type="button" title="Search nilai" class="btn btn-primary pull-right" id="btnfilter" ><i class="fa fa-search"></i> Search</button>
					</div>				
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Raport</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<form method="post" id="target" target="_blank" action="raport_pdf.php" >
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row">
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Name</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detname"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Class</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detclass"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Registered Number</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detreg"></span>
					</div>
				  </div>
				</div>
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Academic Year</b>
					</div>
					<div class="col-sm-8">
						<b >: </b> <span id="detyear"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Semester</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detsemester"></span>
					</div>
				  </div>
				  
				</div>
			  </div>
			  <div class="row">
				
				<input type="hidden" name="kelas" id="hiddenkelas">
				<input type="hidden" name="siswa" id="hiddensiswa">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader" id="headNilai">
						
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Download raport" class="btn btn-success " id="btndownload" ><i class="fa fa-download"></i> Download</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
		$(function () {
			$('#kelas').on('change', function() {
				$('#id_siswa').val('');
				$('#txtsearchitem').val('');
			});
			
			$( "#txtsearchitem" ).autocomplete({
				
				search  : function(){$(this).addClass('working');},
				open    : function(){$(this).removeClass('working');},
				source: function(request, response) {
					$.getJSON("autocomplete_search.php", { term: $('#txtsearchitem').val(), kelas: $('#kelas').val()}, 
						response); },
				minLength:1,
				select:function(event, ui){
						event.preventDefault();
						$('#txtsearchitem').val(ui.item.nis+" - "+ui.item.nama);
						temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
				return $( "<li>" )
				.append( "<dt>"+item.nis + " - "+item.nama+"</dt>"  )
				.appendTo( ul );
			};
		});
		
		function temptabel(id){
			$('#id_siswa').val(id);
		}
		
		$(document).on("click","#btnfilter",function(){
			var kelas = $('#kelas').val();
			var siswa = $('#id_siswa').val();
			if( siswa == null || siswa == ''){
				$.notify({
					message: "Santri belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtsearchitem").focus();
				return;
			}
			
			$('#nilaiSantri tbody').empty();
			$('#nilaiSantri > thead > tr').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			
			var value = {
				kelas: kelas,
				siswa:siswa,
				method : "raportsiswa"
			};
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					
					var hasil = jQuery.parseJSON(data);
					$('#hiddenkelas').val(kelas);
					$('#hiddensiswa').val(siswa);
					$('#detname').html(hasil.nama);
					$('#detreg').html(hasil.nis);
					$('#detclass').html(hasil.kelas);
					$('#detyear').html(hasil.tahun);
					$('#detsemester').html(hasil.semester);
					var no = 3;
					var master = [];
										
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" >NO</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" >SKILL</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" >GRADE</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" >SCORE</th>');
					
					var number = 0;
					$.each(hasil.nilai, function (key, val) {
						var tr = tbody.insertRow(number);
						var nomor = tr.insertCell(0);
						var sub_mapel = tr.insertCell(1);
						var grade = tr.insertCell(2);
						var nilai = tr.insertCell(3);
						
						nomor.innerHTML = number+1;	
						sub_mapel.innerHTML = val.sub_mapel;	
						grade.innerHTML = ' ';	
						nilai.innerHTML =val.rata;
						number++;
					});
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btndownload", function() {
			var kelas = $('#hiddenkelas').val();
			var siswa = $('#hiddensiswa').val();
			
			if( siswa == null || siswa == ''){
				$.notify({
					message: "Santri belum dipilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#kelas").focus();
				return;
			}
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "raport_pdf.php";

			var txtkelas = document.createElement("input");
			txtkelas.type = "hidden";
			txtkelas.name = "kelas";
			txtkelas.value = kelas;
			mapForm.appendChild(txtkelas);
			
			var txtsiswa = document.createElement("input");
			txtsiswa.type = "hidden";
			txtsiswa.name = "siswa";
			txtsiswa.value = siswa;
			mapForm.appendChild(txtsiswa);
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	</script>
</body>
</html>
