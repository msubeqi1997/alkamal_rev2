<?php 
$titlepage="Rekap Nilai Santri";
$idsmenu=55; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
?>
<section class="content-header">
  <h1>
	REKAP NILAI SANTRI
	<small>Program Bahasa Intensif</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="download_jadwal.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Kelas</label>
					<div class="col-sm-3">
						<select class="form-control" id="kelas" name="kelas" >
						  <option value="">Pilih Kelas</option>
						  <?php 
							foreach($kelas[1] as $row){
							  echo "<option value='".$row['id_kelas']."'>".$row['kelas']."</option>";
							}
						  ?>
						</select>
					</div>
					<label class="col-sm-2  control-label">Pilih Subjek</label>
					<div class="col-sm-3">
						<select class="form-control" id="subjek" name="subjek" >
						  <option value="">Pilih Subjek</option>
						  <option value="1">Pilih Subjek</option>
						  
						</select>
					</div>
				  </div>
				  <div class="form-group"> 
					
					
				  </div>
				  <div class="form-group">
					<label class="col-sm-2  control-label">Sub subjek</label>
					<div class="col-sm-3">
						<select class="form-control" id="subsubjek" name="subsubjek" >
						  <option value="">Pilih sub nilai</option>
						  
						</select>
					</div>
					<div class="col-sm-5 ">
					  <button type="button" title="Search nilai" class="btn btn-primary pull-right" id="btnfilter" ><i class="fa fa-search"></i> Search</button>
					</div>				
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Rekap Nilai</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="import_rekap_nilai.php" >
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row">
				
				<input type="hidden" name="kelas" id="hiddenkelas">
				<input type="hidden" name="subjek" id="hiddensubjek">
				<input type="hidden" name="subsubjek" id="hiddensubsubjek">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader" id="headNilai">
						
					  </tr>
					  <tr></tr>
					  <tr></tr>
					  <tr></tr>
					  
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="submit" title="Download absensi" class="btn btn-success " id="btndownload" ><i class="fa fa-download"></i> Download</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
		$(function () {
			$('#firstdate, #lastdate').datepicker({
				format: 'dd-mm-yyyy',
			});
			
			//Datemask dd/mm/yyyy
			$("#firstdate, #lastdate").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
			
			$('#kelas').on('change', function() {
				var value = {
					kelas: this.value,
					method : "get_subjek"
				};
				$.ajax({
					url : "c_input_nilai.php",
					type: "POST",
					data : value,
					success: function(data, textStatus, jqXHR)
					{
						var respons = jQuery.parseJSON(data);
						if(respons.data.length>0){
							$('#subjek').html("");
							$('#subjek').html("<option value=''> Pilih subjek </option>");
							$.each(respons.data, function (key, val) {
							  $('#subjek').append('<option value="'+val.id+'">'+val.subjek+'</option>');
							})
						}else{
							$('#subjek').html("");
							$('#subjek').html("<option value=''> Subjek tidak ditemukan </option>");
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			});
			
			$('#subjek').on('change', function() {
				var value = {
					subsubjek: this.value,
					method : "get_sub_subjek"
				};
				$.ajax({
					url : "c_input_nilai.php",
					type: "POST",
					data : value,
					success: function(data, textStatus, jqXHR)
					{
						var respons = jQuery.parseJSON(data);
						if(respons.data.length>0){
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Pilih sub subjek </option>");
							$.each(respons.data, function (key, val) {
							  $('#subsubjek').append('<option value="'+val.id+'">'+val.sub_mapel+'</option>');
							})
						}else{
							$('#subsubjek').html("");
							$('#subsubjek').html("<option value=''> Sub subjek tidak ditemukan </option>");
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
					}
				});
			});
		});
		
		$(document).on("click","#btnfilter",function(){
			var kelas = $('#kelas').val();
			var subjek = $('#subjek').val();
			var subsubjek = $('#subsubjek').val();
			if( kelas == null || kelas == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Silahkan pilih kelas!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#kelas").focus();
				return;
			}
			if( subjek == null || subjek == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Pilih subjek!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#subjek").focus();
				return;
			}
			if( subsubjek == null || subsubjek == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Pilih sub subjek!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#subsubjek").focus();
				return;
			}
			$('#nilaiSantri tbody').empty();
			$('#nilaiSantri > thead > tr').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			
			var value = {
				kelas: kelas,
				subjek:subjek,
				subsubjek:subsubjek,
				method : "rekapsubnilai"
			};
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					
					var hasil = jQuery.parseJSON(data);
					$('.titleAbsen').html('Kelas '+hasil.kelas);
					$('#hiddenkelas').val(kelas);
					$('#hiddensubjek').val(subjek);
					$('#hiddensubsubjek').val(subsubjek);
					var no = 3;
					var master = [];
										
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" rowspan="4">NO</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" rowspan="4">NIS</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" rowspan="4">Nama</th>');
					$.each(hasil.header, function (key, val) {
					  var mapel_span = 0;
					  
					  $.each(val, function (sk, sv) {
						
						var sub_span = 0;
						var nilai_span;
						
						$.each(sv, function (kn, vn) {
						  var sub_rowspan = '';
						  sub_rowspan = Object.keys(vn).length;
						  if(sub_rowspan < 1){sub_rowspan='rowspan="2"';}
						  nilai_span = Object.keys(vn).length;
						  $('#nilaiSantri > thead > tr').eq(2).append('<th class="text-center" '+sub_rowspan+' colspan="'+nilai_span+'">'+kn+'</th>');
						  var urut = 1;
							
							$.each(vn, function (jk, jn) {
							   master.push(jk);
							  if(jk == ''){
								$('#nilaiSantri > thead > tr').eq(3).append('');  
							  }else{
								$('#nilaiSantri > thead > tr').eq(3).append('<th class="text-center">'+urut+'</th>');
							  }
							  urut++;
							  sub_span++;
							  mapel_span++;
							});
							
						});
						
						$('#nilaiSantri > thead > tr').eq(1).append('<th class="text-center" colspan="'+sub_span+'">'+sk+'</th>');			  
						
					  });
					  
					  $('#nilaiSantri > thead > tr').eq(0).append('<th class="text-center" colspan="'+mapel_span+'">'+key+'</th>');
					  no++;
					})
					
					var master_nilai = reverseArray(master);
					
					var number = 0;
					$.each(hasil.nilai, function (knis, vnis) {
						var tr = tbody.insertRow(number);
						var nomor = tr.insertCell(0);
						var nis = tr.insertCell(1);
						var name = tr.insertCell(2);
						var induk = knis.split("|")[1];
						nomor.innerHTML = number+1;	
						nis.innerHTML = induk;
						$.each(vnis, function (kname, vname) {
							name.innerHTML = kname;
							
							$.each(master_nilai, function (i, valid) {
							  var tdnilai = tr.insertCell(3);
							  if(vname[valid] == null){
								tdnilai.innerHTML = '<td> </td>';
							  }else{
								tdnilai.innerHTML = '<td>'+vname[valid]+'</td>';  
							  }
							});
						});
						number++;
					});
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		function reverseArray(arr) {
		  var newArray = [];
		  for (var i = arr.length - 1; i >= 0; i--) {
			newArray.push(arr[i]);
		  }
		  return newArray;
		}
	</script>
</body>
</html>
