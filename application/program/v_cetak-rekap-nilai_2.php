<?php 
$titlepage="Rekap Nilai Santri";
$idsmenu=55; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
?>
<section class="content-header">
  <h1>
	REKAP NILAI SANTRI
	<small>Program Bahasa Intensif</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form method="post" id="target" class="form-horizontal" target="_blank" action="download_jadwal.php" >
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="method" value="import">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Kelas</label>
					<div class="col-sm-3">
						<select class="form-control" id="kelas" name="kelas" >
						  <option value="">Pilih Kelas</option>
						  <?php 
							foreach($kelas[1] as $row){
							  echo "<option value='".$row['id_kelas']."'>".$row['kelas']."</option>";
							}
						  ?>
						</select>
					</div>
					<button type="button" title="Search" class="btn btn-primary " id="btnfilter" ><i class="fa fa-refresh"></i> Search</button>
				  </div>
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Rekap Nilai</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  <div class="row">
				<form id="inputAbsen">
				<input type="hidden" id="hiddenkelas">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader" id="headNilai">
						
					  </tr>
					  <tr></tr>
					  <tr></tr>
					  <tr></tr>
					  
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Simpan absensi" class="btn btn-success " id="btnsaveitem" ><i class="fa fa-save"></i> Simpan</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
		$(function () {
			$('#firstdate, #lastdate').datepicker({
				format: 'dd-mm-yyyy',
			});
			
			//Datemask dd/mm/yyyy
			$("#firstdate, #lastdate").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$(document).on("click","#btnfilter",function(){
			var kelas = $('#kelas').val();
			if( kelas == null || kelas == ''){
				$("#jadwal").html('');
				$.notify({
					message: "Silahkan pilih kelas!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#kelas").focus();
				return;
			}
			
			$('#nilaiSantri tbody').empty();
			$('#nilaiSantri > thead > tr').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			
			var value = {
				kelas: kelas,
				method : "rekapnilaisantri"
			};
			$.ajax(
			{
				url : "c_input_nilai.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					
					var hasil = jQuery.parseJSON(data);
					$('.titleAbsen').html('Kelas '+hasil.kelas);
					$('#hiddenkelas').val(kelas);
					var no = 3;
					
					$('#nilaiSantri > thead > tr').eq(0).append('<th rowspan="4">NO</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th rowspan="4">NIS</th>');
					$('#nilaiSantri > thead > tr').eq(0).append('<th rowspan="4">Nama</th>');
					$.each(hasil.header, function (key, val) {
					  var mapel_span = 0;
					  
					  $.each(val, function (sk, sv) {
						
						var sub_span = 0;
						var nilai_span;
						$.each(sv, function (kn, vn) {
						  
						  nilai_span = Object.keys(vn).length;
						  $('#nilaiSantri > thead > tr').eq(2).append('<th colspan="'+nilai_span+'">'+kn+'</th>');
						  var urut = 1;
							
							$.each(vn, function (jk, jn) {
							  if(jk == ''){
								$('#nilaiSantri > thead > tr').eq(3).append('<th></th>');  
							  }else{
								$('#nilaiSantri > thead > tr').eq(3).append('<th>'+urut+'</th>');
							  }
							  urut++;
							  sub_span++;
							  mapel_span++;
							});
							
						});
						
						$('#nilaiSantri > thead > tr').eq(1).append('<th colspan="'+sub_span+'">'+sk+'</th>');			  
						
					  });
					  
					  $('#nilaiSantri > thead > tr').eq(0).append('<th colspan="'+mapel_span+'">'+key+'</th>');
					  no++;
					})
					
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		
	</script>
</body>
</html>
