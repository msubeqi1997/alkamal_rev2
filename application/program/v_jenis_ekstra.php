<?php 
$titlepage="Jenis Ekstra";
$idsmenu=62; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content-header">
  <h1>
	JENIS
	<small>Ekstrakurikuler</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Jenis</button>
					<br>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Ekstrakurikuler </th>
							<th>Pengajar </th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah Ekstrakurikuler</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Nama Ekstrakurikuler</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" > 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Pembina</label>
							<div class="col-sm-7">
							  <input type="text" id="txtpengajar" name="txtpengajar" class="form-control" readonly>
							  <input type="hidden" id="txtpembina" name="txtpembina">
							</div>
							<div class="col-sm-2"> <button type="button" title="Cari Pengajar" class="btn btn-primary " id="btncaripengajar" name=""><i class="fa fa-search"></i></button></div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<!-- Modal searching pegawai -->
	<div id="modalpegawai" class="modal fade ">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title titlelistpegawai">Daftar pegawai</h4>
					<input type="hidden" value="" id="idkelaswali">
				</div>
				<div class="modal-body">
					<div class="box-body table-responsive no-padding">
						<table id="table_pegawai" class="table  table-bordered table-hover table-striped">
							<thead>
								<tr class="tableheader">
									<th style="width:30px">#</th>
									<th style="width:150px">Nama Wali</th>
									<th style="width:50px">Pilih</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	<!-- End modal searching pegawai-->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script language="javascript">
		
		
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "get_list_ekstra"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_ekstra.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "nama" },
				{ "data": "pembina" },
				{ "data": "action" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		//searching pegawai
		$(document).on("click","#btncaripengajar",function(){
			$("#modalpegawai").modal("show");
			$(".titlelistpegawai").html("");
			$(".titlelistpegawai").html("Daftar pegawai");
			var value = {
				method : "get_pegawai"
			};
			$.ajax(
			{
				url : "../pendidikan/c_kelas.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					$("#table_pegawai tbody").html(data.data);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		
			
		});
		
		$(document).on("click",".insertpegawai",function(){
			var kelas= $("#idkelaswali").val();
			$("#txtpengajar").val($(this).attr("pegawai"));
			$("#txtpembina").val($(this).attr("pegawai_id"));
			$("#modalpegawai").modal("hide");
		});
	
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#txtpengajar").val("");
			$("#txtpembina").val("");
			$("#head-modal").html("Tambah Ekstrakurikuler");
			set_focus("#txtname");
			
		}
		
		$(document).on( "click",".btnedit", function() {
			newitem();
			$("#head-modal").html("Edit Ekstrakurikuler");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail_ekstra"
			};
			$.ajax(
			{
				url : "c_ekstra.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.msg;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.id_ekstrakurikuler);
					$("#txtname").val(data.ekstrakurikuler);
					$("#txtpembina").val(data.pengajar);
					$("#txtpengajar").val(data.pembina);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var pembina = $("#txtpembina").val();
			var crud=$("#inputcrud").val();
			
			if(item_name == '' || item_name== null ){
				$.notify(
					{ message: "Nama ekstra tidak boleh kosong!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#txtname").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				pembina : pembina,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_ekstra.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
							newitem();
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	$(document).on( "click",".btndownload", function() {
		var id_item = $(this).attr('id_item');
		
		var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "import_rombel.php";

		var txtid = document.createElement("input");
		txtid.type = "hidden";
		txtid.name = "id_item";
		txtid.value = id_item;
		mapForm.appendChild(txtid);
		
		document.body.appendChild(mapForm);

		mapForm.submit();
		
		
	});
	</script>
</body>
</html>
