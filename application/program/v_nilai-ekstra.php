<?php 
$titlepage="Input nilai ekstra";
$idsmenu=64; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getListEkstra();

?>
<section class="content-header">
  <h1>
	INPUT NILAI
	<small>Ekstrakurikuler</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form id="target" class="form-horizontal" action="javascript:void(0);">
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="siswaid" id="siswaid" value="">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Ekstrakurikuler</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtkelas" name="txtkelas" style="width: 100%;">
						  <option>Pilih Ekstrakurikuler</option>
						  <?php
						  foreach($kelas[1] as $opt){
							echo '<option value="'.$opt['id_ekstrakurikuler'].'">'.$opt['ekstrakurikuler'].'</option>';
						  }
						  ?>
						</select>
					</div>
					<div class="col-sm-2">
					  <button  type="button"  title="Cari santri" class=" btn btn-block btn-flat btn-primary"  id="btnsearch"  ><span class="fa fa-search"></span> Cari</button>				
					</div>
				  </div>
				 
				  			
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Input Nilai Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  
			  <div class="row">
			  
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Ekstrakurikuler</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detkelas"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tapel</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettapel"></span>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="row">
				<form id="inputNilai">
				<input type="hidden" id="kodekelas">
				<input type="hidden" id="crud">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px">#</th>
						<th>Nis </th>
						<th>Nama </th>
						<th>Nilai</th>
						<th>Keterangan</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Simpan absensi" class="btn btn-success " id="btnsaveitem" ><i class="fa fa-save"></i> Simpan</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	   
	<script language="javascript">
		$(function () {
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
	
			//decimal();
			integer();
		});
		
		$(document).on('focus',".txttanggal", function(){
			$(this).datepicker({
				format: 'dd-mm-yyyy'
			});
			$(this).inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		
		function integer(){
			$(document).on("blur",".decimal",function(){
				var angka=parseFloat($(this).val());
				if(isNaN(angka))
				{
					angka=0;
				}
				$(this).val(angka);		
			});

			$(".decimal").focus(function(e){
				if(e.which === 9){
					return false;
				}
				$(this).select();
			});
		}
		
		$(document).on("click","#btnsearch",function(){
			var kelas = $('#txtkelas').val();
			if( kelas == null || kelas == ''){
				$.notify({
					message: "Kelas belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtkelas").focus();
				return;
			}
			
			inputNilai(kelas);
		});
		
		function inputNilai(kelas){
			
			$('#nilaiSantri tbody').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			var value = {
				kelas : kelas,
				method : "getdaftarnilaisantri"
			};
			$.ajax(
			{
				url : "c_ekstra.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#kodekelas').val(kelas);
					$('#crud').val('N');
					
					$('#detkelas').html(hasil.ekstra);				
					$('#dettapel').html(hasil.tapel);
					
					var no = 0;
					$.each(hasil.data, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var nis = row.insertCell(1);
					  var nama = row.insertCell(2);
					  var nilai = row.insertCell(3);
					  var ket = row.insertCell(4);
					  urut.innerHTML = no+1;	
					  nis.innerHTML = val.nis;	
					  nama.innerHTML = val.nama_lengkap;
					  nilai.innerHTML = '<input type="hidden" name="siswa[]" value="'+val.uuid+'">'+
										'<input type="text" id="nilaisiswa'+no+'" class="form-control decimal" name="nilai[]" value="'+val.nilai+'">';
					  ket.innerHTML = '<input type="text" id="ketsiswa'+no+'" class="form-control" name="keterangan[]" value="'+val.keterangan+'">';				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function newitem(){
			$('#detkelas').html('');				
			$('#dettapel').html('');				
			$('#kodekelas').val('');				
			$('#crud').val('N');
		}
	
		$(document).on( "click","#btnsaveitem", function() {
			var kelas = $('#kodekelas').val();
			var crud = $('#crud').val();
			
			var siswa = [];
			$("input[name='siswa[]']").each(function () {
			  siswa.push($(this).val());
			});

			var nilai = [];
			$("input[name='nilai[]']").each(function () {
			  nilai.push($(this).val());
			});
			
			var note = [];
			$("input[name='keterangan[]']").each(function () {
			  note.push($(this).val());
			});
			
			var value = {
				kelas: kelas,
				siswa: siswa,
				nilai: nilai,
				note:note,
				crud: crud,
				method : "save_nilai_ekstra"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_ekstra.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
					  $('#nilaiSantri tbody').empty();
					  $("#btnsaveitem").prop('disabled', false);
					  $("#infoproses").html("");
					  newitem();
					  $.notify({
							message: "Berhasil di simpan"
					  },{
							type: 'success',
							delay: 8000,
					  });
					}else{
					  $("#infoproses").html("");
					  $("#btnsaveitem").prop('disabled', false);
					  $.notify({
							message: data.msg
					  },{
							type: 'warning',
							delay: 8000,
					  });
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		
	</script>
</body>
</html>
