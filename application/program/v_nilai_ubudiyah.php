<?php 
$titlepage="Ubudiyah";
$idsmenu=60; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_data.php");
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
$jenis = $pos->getJenisNilai();
$conn = new model_data();
$asrama = $conn->getAsrama();
?>
<section class="content-header">
  <h1>
	INPUT NILAI
	<small>Program Ubudiyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		
			<div class="box-body">
			  <div class="row">
				<div class="col-md-10">
				  <form id="target" class="form-horizontal" action="javascript:void(0);">
				  <input type="hidden" name="siswaid" id="siswaid" value="">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Firqoh</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtasrama" name="txtasrama" style="width: 100%;">
						  <option value="">Pilih Firqoh</option>
						  <?php
						  foreach($asrama[1] as $opt){
							echo '<option value="'.$opt['auto'].'">'.$opt['nama_asrama'].'</option>';
						  }
						  ?>
						</select>
					</div>
					<label class="col-sm-2  control-label">Pilih kamar</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtkamar" name="txtkamar" style="width: 100%;">
						  <option value="">Pilih Firqoh dahulu</option>
						  
						</select>
					</div>
					
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Jenjang</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
							<option value=""> Pilih Jenjang </option>
							<option value="ula"> Ula</option>
							<option value="wustho"> Wustho</option>
							<option value="MDK"> MDK</option>
						</select>
					</div>
					<label class="col-sm-2  control-label">Jenis nilai</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="opttingkat" name="opttingkat" >
							<option value=""> Pilih Tingkat </option>
							<option value="1"> Tingkat 1</option>
							<option value="2"> Tingkat 2</option>
							<option value="3"> Tingkat 3</option>
						</select>
					</div>
					
				  </div>
				  <div class="form-group">
					<label class="col-sm-2  control-label">Santri</label>
					<div class="col-sm-5">
						<input type="hidden" id="kodesiswa"  name="kodesiswa" value="" >
						<input type="text" class="form-control" id="txtsiswa"  name="txtsiswa" value="" data-target="modalsearch">
					</div>
					<div class="col-sm-2">
					  <button  type="button"  title="Cari santri" class=" btn btn-block btn-flat btn-primary"  id="btnsearch"  ><span class="fa fa-search"></span> Cari</button>				
					</div>				
				  </div>
				  </form>
				</div>
				<div class="col-md-2">
				   <div class="form-group">
                      <label for="optstatus">Status</label>
                      <select class="form-control" id="optstatus" name="optstatus" style="width: 100%;">
						  <option value="">Pilih Status</option>
						  <option value="lulus">Lulus</option>
						  <option value="mengulang">Mengulang</option>
					  </select>
                   </div>
				   <div class="form-group">
                      <button type="button" title="Doanload report" class="btn btn-success " id="btndownload" ><i class="fa fa-download"></i> Download</button>
                   </div>
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Input Nilai Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  
			  <div class="row">
			  
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>No Induk</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detinduk"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Nama</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detnama"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tahun Akademik</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettapel"></span>
					</div>
				  </div>
				</div>
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Jenjang</b>
					</div>
					<div class="col-sm-8">
						<b >: </b> <span id="detjenjang"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettingkat"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Semester</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detsemester"></span>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="row">
				<form id="inputNilai">
				<input type="hidden" id="hiddenid">
				<input type="hidden" id="crud">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px">#</th>
						<th>Kategori </th>
						<th>Materi </th>
						<th>Tanggal Ujian</th>
						<th>Nilai</th>
						<th>Status</th>
						<th>Penguji</th>
						<th>Update</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	   
	<script language="javascript">
		$(function () {
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
	
			//decimal();
			integer();
		});
		
		$(document).on('focus',".txttanggal", function(){
			$(this).datepicker({
				format: 'dd-mm-yyyy'
			});
			$(this).inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$('#txtasrama').on('change', function() {
			$('#kodesiswa').val('');
			$('#txtsiswa').val('');
			var value = {
				asrama: this.value,
				method : "get_kamar_asrama"
			};
			$.ajax({
				url : "../pesantren/c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var respons = jQuery.parseJSON(data);
					if(respons.data.length>0){
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Pilih kamar </option>");
						$.each(respons.data, function (key, val) {
						  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
						})
					}else{
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		function integer(){
			$(document).on("blur",".decimal",function(){
				var angka=parseFloat($(this).val());
				var ids = $(this).attr('ids');
				if(isNaN(angka))
				{
					angka=0;
				}
				$(this).val(angka);
				if(angka <= 55){
					$("#status"+ids+" option[value='mengulang']").prop('selected', true);
				}else if(angka <= 100){
					$("#status"+ids+" option[value='lulus']").prop('selected', true);
				}
			});

			$(".decimal").focus(function(e){
				if(e.which === 9){
					return false;
				}
				$(this).select();
			});
		}
		
		$(function () {
			
			$( "#txtsiswa" ).autocomplete({
				
				search  : function(){$(this).addClass('working');},
				open    : function(){$(this).removeClass('working');},
				source: function(request, response) {
					$.getJSON("autocomplete_ubudiyah.php", { term: $('#txtsiswa').val(), jenjang: $('#optjenjang').val(), tingkat: $('#opttingkat').val(), asrama:$('#txtasrama').val(),kamar:$('#txtkamar').val()}, 
						response); },
				minLength:1,
				select:function(event, ui){
						event.preventDefault();
						$('#txtsiswa').val(ui.item.nis+" - "+ui.item.nama);
						$('#kodesiswa').val(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
				return $( "<li>" )
				.append( "<dt>"+item.nis + " - "+item.nama+"</dt>"  )
				.appendTo( ul );
			};
		});
		
		$(document).on("click","#btnsearch",function(){
			var jenjang = $('#optjenjang').val();
			var tingkat = $('#opttingkat').val();
			var siswa = $('#kodesiswa').val();
			if( jenjang == null || jenjang == ''){
				$.notify({
					message: "Jenjang belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optjenjang").focus();
				return;
			}
			if( tingkat == null || tingkat == ''){
				$.notify({
					message: "Tingkat belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#opttingkat").focus();
				return;
			}
			if( siswa == null || siswa == ''){
				$.notify({
					message: "Santri tidak ada!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtsiswa").focus();
				return;
			}
			inputNilai(jenjang,tingkat,siswa);
		});
		
		function inputNilai(jenjang,tingkat,idSiswa){
			
			$('#nilaiSantri tbody').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			var value = {
				idSiswa: idSiswa,
				jenjang: jenjang,
				tingkat: tingkat,
				method : "getdaftarnilaisantri"
			};
			$.ajax(
			{
				url : "c_nilai_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#hiddensiswa').val(idSiswa);
					$('#crud').val('N');
					
					$('#detinduk').html(hasil.nis);				
					$('#detnama').html(hasil.nama);
					$('#detjenjang').html(hasil.jenjang);
					$('#dettingkat').html(hasil.tingkat);
					$('#dettapel').html(hasil.tapel);
					$('#detsemester').html(hasil.semester);
					
					var no = 0;
					$.each(hasil.data, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var kategori = row.insertCell(1);
					  var materi = row.insertCell(2);
					  var tanggal = row.insertCell(3);
					  var nilai = row.insertCell(4);
					  var status = row.insertCell(5);
					  var penguji = row.insertCell(6);
					  var button = row.insertCell(7);
					  
					  urut.innerHTML = no+1;	
					  kategori.innerHTML = val.kategori;	
					  materi.innerHTML = val.materi;
					  tanggal.innerHTML = val.tanggal;
					  nilai.innerHTML = val.nilai;
					  status.innerHTML = val.status;
					  penguji.innerHTML = val.penguji;
					  button.innerHTML = val.button;
					  				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function newitem(){
			$('#txtasrama').val('');				
			$('#txtkamar').val('');	
			$('#optjenjang').val('');				
			$('#opttingkat').val('');				
			$('#txtsiswa').val('');				
			$('#kodesiswa').val('');				
			$('#crud').val('N');
		}
	
		$(document).on( "click",".btnedit", function() {
			var id_data = $(this).attr('id_data');
			var status = $('#status'+id_data).val();
			var nilai = $('#nilai'+id_data).val();
			var tanggal = $('#tanggal'+id_data).val();
			var siswa = $('#kodesiswa').val();
			if( tanggal == null || tanggal == ''){
				$.notify({
					message: "Tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$('#tanggal'+id_data).focus();
				return;
			}
			if( nilai == null || nilai == ''){
				$.notify({
					message: "Nilai tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$('#nilai'+id_data).focus();
				return;
			}			
			var value = {
				materi: id_data,
				tanggal: tanggal,
				siswa: siswa,
				nilai: nilai,
				status:status,
				method : "save_nilai_santri"
			};
			
			$.ajax(
			{
				url : "c_nilai_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
					  
					  $.notify({
							message: "Berhasil di simpan"
					  },{
							type: 'success',
							delay: 8000,
					  });
					}else{
					  $.notify({
							message: data.msg
					  },{
							type: 'warning',
							delay: 8000,
					  });
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on( "click","#btndownload", function() {
			var jenjang = $('#optjenjang').val();
			var tingkat = $('#opttingkat').val();
			var asrama = $('#txtasrama').val();
			var kamar = $('#txtkamar').val();
			var status = $('#optstatus').val();
			
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "import_ubudiyah.php";

			var fjenjang = document.createElement("input");
			fjenjang.type = "hidden";
			fjenjang.name = "jenjang";
			fjenjang.value = jenjang;
			mapForm.appendChild(fjenjang);
			
			var ftingkat = document.createElement("input");
			ftingkat.type = "hidden";
			ftingkat.name = "tingkat";
			ftingkat.value = tingkat;
			mapForm.appendChild(ftingkat);
			
			var fasrama = document.createElement("input");
			fasrama.type = "hidden";
			fasrama.name = "asrama";
			fasrama.value = asrama;
			mapForm.appendChild(fasrama);
				
			var fkamar = document.createElement("input");
			fkamar.type = "hidden";
			fkamar.name = "kamar";
			fkamar.value = kamar;
			mapForm.appendChild(fkamar);
			
			var fstatus = document.createElement("input");
			fstatus.type = "hidden";
			fstatus.name = "status";
			fstatus.value = status;
			mapForm.appendChild(fstatus);
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	</script>
</body>
</html>
