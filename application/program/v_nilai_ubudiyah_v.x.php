<?php 
$titlepage="Ubudiyah";
$idsmenu=60; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
$jenis = $pos->getJenisNilai();

?>
<section class="content-header">
  <h1>
	INPUT NILAI
	<small>Program Ubudiyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form id="target" class="form-horizontal" action="javascript:void(0);">
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="siswaid" id="siswaid" value="">
				  
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Jenjang</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
							<option value=""> Pilih Jenjang </option>
							<option value="ula"> Ula</option>
							<option value="wustho"> Wustho</option>
							<option value="MDK"> MDK</option>
						</select>
					</div>
					<label class="col-sm-2  control-label">Jenis nilai</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="opttingkat" name="opttingkat" >
							<option value=""> Pilih Tingkat </option>
							<option value="1"> Tingkat 1</option>
							<option value="2"> Tingkat 2</option>
							<option value="3"> Tingkat 3</option>
						</select>
					</div>
					
				  </div>
				  <div class="form-group">
					<label class="col-sm-2  control-label">Santri</label>
					<div class="col-sm-5">
						<input type="hidden" id="kodesiswa"  name="kodesiswa" value="" >
						<input type="text" class="form-control" id="txtsiswa"  name="txtsiswa" value="" data-target="modalsearch">
					</div>
					<div class="col-sm-2">
					  <button  type="button"  title="Cari santri" class=" btn btn-block btn-flat btn-primary"  id="searchnon"  ><span class="fa fa-search"></span> Cari</button>				
					</div>				
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Input Nilai Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  
			  <div class="row">
			  
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>No Induk</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detinduk"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Nama</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detnama"></span>
					</div>
				  </div>
				</div>
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Jenjang</b>
					</div>
					<div class="col-sm-8">
						<b >: </b> <span id="detjenjang"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettingkat"></span>
					</div>
				  </div>
				  
				</div>
			  </div>
			  <div class="row">
				<form id="inputNilai">
				<input type="hidden" id="hiddenid">
				<input type="hidden" id="crud">
				<div class="table-responsive">
				  <table id="nilaiSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px">#</th>
						<th>Kategori </th>
						<th>Materi </th>
						<th>Tanggal Ujian</th>
						<th>Nilai</th>
						<th>Status</th>
						<th>Update</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Simpan absensi" class="btn btn-success " id="btnsaveitem" ><i class="fa fa-save"></i> Simpan</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<div id="modalsearch" class="modal fade ">
		<div class="modal-dialog modal-md" style="width:940px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Cari Santri</h4>
				</div>
				<!--modal header-->
				<div class="modal-body">
					
					<div class="row">
						<div class="table-responsive no-padding" >
							<table id="table_list_siswa" class="table  table-bordered table-hover table-striped" >
								<thead>
									<tr class="tableheader">
										<th style="width:30px">#</th>
										<th>NIS</th>
										<th>Nama</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div><!-- /.row -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.1.1/css/keyTable.dataTables.min.css">
	<script src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
	<script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
       
	<script language="javascript">
		$(function () {
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
	
			//decimal();
			integer();
		});
		
		$(document).on('focus',".txttanggal", function(){
			$(this).datepicker({
				format: 'dd-mm-yyyy'
			});
			$(this).inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
			
		function integer(){
			$(document).on("blur",".decimal",function(){
				var angka=parseFloat($(this).val());
				if(isNaN(angka))
				{
					angka=0;
				}
				$(this).val(angka);		
			});

			$(".decimal").focus(function(e){
				if(e.which === 9){
					return false;
				}
				$(this).select();
			});
		}
			
		$(document).on( "keydown","#txtsiswa", function(a) {
		  
		  if(a.which == 13){
			
			var jenjang = $('#optjenjang').val();
			var tingkat = $('#opttingkat').val();
			var siswa = $('#txtsiswa').val();
			if( jenjang == null || jenjang == ''){
				$.notify({
					message: "Pilih jenjang dahulu!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optjenjang").focus();
				return;
			}
			if( tingkat == null || tingkat == ''){
				$.notify({
					message: "Pilih tingkat dahulu!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#opttingkat").focus();
				return;
			}
			$('#modalsearch').modal({
				focus: this,
				show: true
			  });
			
			$("#table_list_siswa").DataTable().destroy();
			var searching = setTimeout(function () { $('#table_list_siswa_filter input').focus(); 
				$('#table_list_siswa_filter input').unbind();
				$("#table_list_siswa_filter input").keyup( function (e) {
					if (e.keyCode == 13) {
						table.search( this.value ).draw();
					}else if(e.keyCode == 40) {
						$('#table_list_siswa_filter input', table.table().container()).blur();
						table.cell( ':eq(0)' ).focus();
					}
				} );
			}, 500);
			var value = {
				jenjang : jenjang,
				tingkat : tingkat,
				siswa : siswa,
				method : "get_list_siswa"
			};
			
			var table = $('#table_list_siswa').DataTable({
				"bProcessing": true,
				"serverSide": true,
				keys: {
				   keys: [ 13 /* ENTER */, 38 /* UP */, 40 /* DOWN */ ],
				//   focus: ':eq(0)'
				},
				"ajax": {
					"url": "c_nilai_ubudiyah.php",
					"type": "POST",
					"data":value,
				},
				'createdRow': function( row, data, dataIndex ) {
					$(row).attr('data-siswa', data.idSiswa);
					$(row).attr('class', 'pilih');
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "nis" },
				{ "data": "nama" },
				]
			});
			
			$('#table_list_siswa').on('key-focus.dt', function(e, datatable, cell){
				$(table.row(cell.index().row).node()).addClass('selected');
			});

			$('#table_list_siswa').on('key-blur.dt', function(e, datatable, cell){
				$(table.row(cell.index().row).node()).removeClass('selected');
			});
				
			$('#table_list_siswa').one('key.dt', function(e, datatable, key, cell, originalEvent, indexes){
				if(key === 13){
					var node = table.row( cell.index().row ). nodes()[0];
					var bankid = $(node).attr("data-siswa");
					$("#kodesiswa").val(bankid);
					var nis = $(node).children('td').slice(1, 2).html();
					var nama = $(node).children('td').slice(2).html();
					$("#txtsiswa").val(nis+' - '+nama);
					$("#table_list_siswa").DataTable().destroy();
					$("#modalsearch").modal("hide");
					
					inputNilai(jenjang,tingkat,bankid);
					setTimeout(function () { $('#txtsiswa').focus();}, 200);
				}
			});
		  }
		});
		
		function inputNilai(jenjang,tingkat,idSiswa){
			
			$('#nilaiSantri tbody').empty();
			var tbody = document.getElementById("nilaiSantri").tBodies[0];
			var value = {
				idSiswa: idSiswa,
				jenjang: jenjang,
				tingkat: tingkat,
				method : "getdaftarnilaisantri"
			};
			$.ajax(
			{
				url : "c_nilai_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#hiddensiswa').val(idSiswa);
					$('#crud').val('N');
					
					$('#detinduk').html(hasil.nis);				
					$('#detnama').html(hasil.nama);
					$('#detjenjang').html(hasil.jenjang);
					$('#dettingkat').html(hasil.tingkat);
					
					var no = 0;
					$.each(hasil.data, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var kategori = row.insertCell(1);
					  var materi = row.insertCell(2);
					  var tanggal = row.insertCell(3);
					  var nilai = row.insertCell(4);
					  var status = row.insertCell(5);
					  var button = row.insertCell(6);
					  
					  urut.innerHTML = no+1;	
					  kategori.innerHTML = val.kategori;	
					  materi.innerHTML = val.materi;
					  tanggal.innerHTML = val.tanggal;
					  nilai.innerHTML = val.nilai;
					  status.innerHTML = val.status;
					  button.innerHTML = val.button;
					  				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function newitem(){
			$('#optjenjang').val('');				
			$('#opttingkat').val('');				
			$('#txtsiswa').val('');				
			$('#kodesiswa').val('');				
			$('#crud').val('N');
		}
	
		$(document).on( "click",".btnedit", function() {
			var id_data = $(this).attr('id_data');
			var status = $('#status'+id_data).val();
			var nilai = $('#nilai'+id_data).val();
			var tanggal = $('#tanggal'+id_data).val();
			var siswa = $('#kodesiswa').val();
			if( tanggal == null || tanggal == ''){
				$.notify({
					message: "Tanggal tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$('#tanggal'+id_data).focus();
				return;
			}
			if( nilai == null || nilai == ''){
				$.notify({
					message: "Nilai tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$('#nilai'+id_data).focus();
				return;
			}			
			var value = {
				materi: id_data,
				tanggal: tanggal,
				siswa: siswa,
				nilai: nilai,
				status:status,
				method : "save_nilai_santri"
			};
			
			$.ajax(
			{
				url : "c_nilai_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
					  
					  $.notify({
							message: "Berhasil di simpan"
					  },{
							type: 'success',
							delay: 8000,
					  });
					}else{
					  $.notify({
							message: data.msg
					  },{
							type: 'warning',
							delay: 8000,
					  });
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		
	</script>
</body>
</html>
