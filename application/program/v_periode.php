<?php 
$titlepage="Periode Program Intensif";
$idsmenu=57; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_program.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; ?>

<section class="content-header">
  <h1>
	PERIODE
	<small>Program Bahasa Intensif</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Aktivasi</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Tapel</button>
					<br>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Periode</th>
							<th>Semester</th>
							<th style="width:240px">Aktif</th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Tambah periode</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Periode</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="ex: 2017/2018"> 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Semester</label>
							<div class="col-sm-9">
								<select class="form-control" id="semester" name="semester" >
								  <option value="">Semester</option>
								  <option value="Ganjil">Ganjil</option>
								  <option value="Genap">Genap</option>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_periode.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "thn_ajaran" },
				{ "data": "semester" },
				{ "data": "active" },
				{ "data": "button" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			$("#modalmasteritem").modal('show');
			newitem();
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#semester").val("");
			set_focus("#txtname");
		}
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var semester = $("#semester").val();
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Periode tidak boleh kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			
			if(semester == '' || semester== null ){
				$.notify({
					message: "Semester harus dipilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#semester").focus();
				return;
			}

			var value = {
				id_item: id_item,
				item_name: item_name,
				semester: semester,
				crud:crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_periode.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.ceksat == 0){
						$.notify(data.error);
					}else{
						if(data.crud == 'N'){
							if(data.result == 1){
								$.notify('Proses simpan berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								newitem();				
							}else{
								$.notify({
									message: "Proses simpan gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});
								set_focus("#txtname");
							}
						}else if(data.crud == 'E'){
							if(data.result == 1){
								$.notify('Proses update berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								$("#modalmasteritem").modal("hide");
							}else{
								$.notify({
									message: "Proses update gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});					
								set_focus("#txtname");
							}
						}else{
							$.notify({
								message: "Invalid request"
							},{
								type: 'danger',
								delay: 8000,
							});	
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on( "click",".btnedit", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "get_detail_item"
			};
			$.ajax(
			{
				url : "c_periode.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.thn_ajaran_id);
					$("#txtname").val($.trim(data.thn_ajaran));
					$("#semester option[value='"+data.semester+"']").prop('selected', true);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btnactive", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "activate"
			};
			$.ajax(
			{
				url : "c_periode.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil.result == true){
						$.notify('Proses aktifasi berhasil');
						var table = $('#table_item').DataTable(); 
						table.ajax.reload( null, false );
					}else{
						$.notify({
							message: "Proses aktifasi gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
	</script>
</body>
</html>
