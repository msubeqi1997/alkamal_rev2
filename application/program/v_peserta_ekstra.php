<?php ob_start(); 
$titlepage="Peserta ekstrakurikuler";
$idsmenu=63; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
require_once("../model/model_program.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getListEkstra();
?>
<section class="content-header">
  <h1>
	PESERTA
	<small>Ekstrakurikuler</small>
  </h1>
</section>
<section class="content">
  <div class="row">
	<div class="col-md-6">
	  <div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Data Santri</h3>
		</div>
		<!--./ box header-->
		<div class="box-body no-padding">
		  <div class="row" style="padding-left:25px; padding-right:25px;">
			<div class="col-md-6">
			  <div class="form-group">
				<label>Jenjang pendidikan</label>
				<select class="form-control" id="txtjenjang" name="txtjenjang" style="width: 100%;">
				  <option value="">Semua</option>
				  <option value="ula">Ula</option>
				  <option value="wustho">Wustho</option>
				  <option value="mdk">MDK</option>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-6">
			  <div class="form-group"> 
				<label>Tingkat</label>
				<select class="form-control" id="txttingkat" name="txttingkat" style="width: 100%;">
				  <option value="">Pilih tingkat</option>
				  <option value="1">Tingkat 1</option>
				  <option value="2">Tingkat 2</option>
				  <option value="3">Tingkat 3</option>
				</select>
			  </div>
			</div><!-- /.col -->
			<div class="col-md-10">
			  <div class="form-group"> 
				<input type="text" class="form-control" name="txtsearch" id="txtsearch"  placeholder="Cari No induk atau nama" value="" >
			  </div>
			</div><!-- /.col -->
			<div class="col-md-2">
			  <div class="form-group"> 
				<button  type="button"  title="Cari santri" class="form-control btn btn-block btn-flat btn-primary"  id="searchnon"  > Cari</button>
			  </div>
			</div><!-- /.col -->
		  </div>
		  
			<div class="table-responsive" style="height: 1136px; overflow-y: scroll; padding-left:5px;">
			  <table id="nonrombel" class="table  table-bordered table-hover ">
				<thead>
				  <tr class="tableheader">
					<th style="width:45px">#</th>
					<th>No Induk </th>
					<th>Nama lengkap </th>
					<th style="width:85px">Proses</th>
				  </tr>
				</thead>
				<tbody></tbody>
			  </table>
			</div>
		  
		</div>
	  </div><!-- /.box -->
	</div><!-- /.col-6 -->
	
	<div class="col-md-6">
	  <div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Ekstrakurikuler</h3>
		</div>
		<!--./ box header-->
		<div class="box-body no-padding">
		  <div class="row" style="padding-left:25px; padding-right:25px;">
		  	<div class="col-md-6">
			  <div class="form-group">
				<label>Ekstrakurikuler</label>
				<select class="form-control" id="txtkelas" name="txtkelas" style="width: 100%;">
				  <option value="">Pilih ekstrakurikuler</option>
				  <?php 
					foreach($kelas[1] as $row){
					  echo "<option value='".$row['id_ekstrakurikuler']."'>".$row['ekstrakurikuler']."</option>";
					}
				  ?>
				</select>
			  </div><!-- /.form-group -->
			</div><!-- /.col -->
			<div class="col-md-2">
			  <div class="form-group">
				<label>&nbsp;</label>
				<button  type="button"  title="Cari santri" class="form-control btn btn-block btn-flat btn-primary"  id="searchrombel"  > Cari</button>
			  </div>
			</div><!-- /.col -->
		  </div><!-- /.row -->
		  
			<div class="table-responsive" style="height: 1136px; overflow-y: scroll; padding-left:5px;">
			  <table id="rombel" class="table  table-bordered table-hover ">
				<thead>
				  <tr class="tableheader">
					<th style="width:85px">Keluarkan </th>
					<th style="width:50px">#</th>
					<th>No Induk </th>
					<th>Nama lengkap </th>
				  </tr>
				</thead>
				<tbody></tbody>
			  </table>
			</div>
		  
		</div>
	  </div><!-- /.box -->
	</div><!-- /.col-6 -->
  </div><!-- /.box -->
				
</section><!-- /.content -->
  
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../dist/js/bootbox.min.js" type="text/javascript"></script>
<script src="../../dist/js/jquery.form.js" type="text/javascript"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
function goBack() {
    window.history.back();
}

//searching nama
$(document).on("blur","#xx",function(){
	var jenjang = $('#txtjenjang').val();
	var tingkat = $('#txttingkat').val();
	var term = $(this).val();
	
	nonrombel(jenjang,tingkat,term); 
});

$(function () {
	$('#searchnon').on('click', function() {
	  var jenjang = $('#txtjenjang').val();
	  var tingkat = $('#txttingkat').val();
	  var kelas = $('#txtkelas').val();
	  var term = $('#txtsearch').val();
	  if(jenjang == '' || jenjang== null ){
		$.notify(
			{ message: "Jenjang belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtjenjang").focus();
		return;
	  }
	  if(tingkat == '' || tingkat== null ){
		$.notify(
			{ message: "Tingkat belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txttingkat").focus();
		return;
	  }
	  if(kelas == '' || kelas== null ){
		$.notify(
			{ message: "Ekstrakurikuler belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtkelas").focus();
		return;
	  }
	  nonrombel(jenjang,tingkat,kelas,term); 
	});
	
	$('#searchrombel').on('click', function() {
	  var kelas = $('#txtkelas').val();
	  
	  if(kelas == '' || kelas== null ){
		$.notify(
			{ message: "Ekstrakurikuler belum dipilih!"},
			{ type: 'warning', delay: 8000,}
		);		
		$("#txtkelas").focus();
		return;
	  }
	  rombel(kelas);
	});
	
});

$(document).on( "click",".checkin", function() {
	var id_siswa = $(this).attr("id_item");
	var kelas = $("#txtkelas").val(); 
	var term = $('#txtsearch').val();
	if(kelas == '' || kelas== null ){
		$.notify({
			message: "Ekstrakurikuler belum dipilih!"
		},{
			type: 'warning',
			delay: 8000,
		});		
		$("#txtkelas").focus();
		return;
	}
	var value = {
		id_siswa: id_siswa,
		kelas: kelas,
		method : "checkin"
	};
	$.ajax(
	{
		url : "c_ekstra.php",
		type: "POST",
		data : value,
		success: function(result, textStatus, jqXHR)
		{
			var data = jQuery.parseJSON(result);
			if(data.respons == true){
			  var jenjang = $('#txtjenjang').val();
			  var tingkat = $('#txttingkat').val();
			  if(jenjang == '' || tingkat == ''){
				$('#nonrombel').html('');
			  }else{
				nonrombel(jenjang,tingkat,kelas,term);
			  }
			  rombel(kelas);
			}else{
				$.notify({
					message: data.data
				},{
					type: 'warning',
					delay: 8000,
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
});

$(document).on( "click",".checkout", function() {
	var id_siswa = $(this).attr("id_item");
	var kelas = $("#txtkelas").val(); 
	var term = $('#txtsearch').val();
	var value = {
		id_siswa: id_siswa,
		kelas: kelas,
		method : "checkout"
	};
	$.ajax(
	{
		url : "c_ekstra.php",
		type: "POST",
		data : value,
		success: function(result, textStatus, jqXHR)
		{
			var data = jQuery.parseJSON(result);
			if(data.respons == true){
			  var jenjang = $('#txtjenjang').val();
			  var tingkat = $('#txttingkat').val();
			  if(jenjang == '' || tingkat == ''){
				$("#nonrombel").DataTable().destroy();
			  }else{
				nonrombel(jenjang,tingkat,kelas,term);
			  }
			  rombel(kelas);
			}else{
				$.notify({
					message: data.data
				},{
					type: 'warning',
					delay: 8000,
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
});
		
function rombel(kelas){
	$("#rombel").DataTable().destroy();
	var value = {
		kelas:kelas,
		method : "rombel"
	};
	$('#rombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_ekstra.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "button" },
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		
		]
	});
} 
	
function nonrombel(jenjang,tingkat,kelas,term = ''){
	 $("#nonrombel").DataTable().destroy();
	var value = {
		jenjang:jenjang,
		tingkat:tingkat,
		kelas:kelas,
		term:term,
		method : "nonrombel"
	};
	$('#nonrombel').DataTable({
		"paging": false,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_ekstra.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "nis" },
		{ "data": "nama_lengkap" },
		{ "data": "button" },
		]
	});
}    
	
</script>
</body>
</html>
