<?php 
$titlepage="Ubudiyah";
$idsmenu=59; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 
require_once ("../model/model_program.php");
$pos = new model_program();
$cat = $pos->getKategoriUbudiyah();
?>
<section class="content-header">
  <h1>
	MATERI
	<small>Program Ubudiyah</small>
  </h1>
</section>
<section class="content">
	<div class="box box-success">
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Materi</button>
					<br>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Jenjang </th>
							<th>Tingkat kelas </th>
							<th>Kategori </th>
							<th>Materi </th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title" id="head-modal">Tambah materi</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Jenjang</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
									<option value=""> Pilih Jenjang </option>
									<option value="ula"> Ula</option>
									<option value="wustho"> Wustho</option>
									<option value="mdk"> Mdk</option>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Tingkat</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="opttingkat" name="opttingkat" >
									<option value=""> Pilih Tingkat </option>
									<option value="1"> Tingkat 1</option>
									<option value="2"> Tingkat 2</option>
									<option value="3"> Tingkat 3</option>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kategori</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optkategori" name="optkategori" >
									<option value=""> Pilih Kategori </option>
									<?php 
										foreach($cat[1] as $row){
										  echo "<option value='".$row['auto']."'>".$row['kategori']."</option>";
										}
									  ?>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Materi</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optmateri" name="optmateri" >
									<option value=""> Pilih Materi </option>
									
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script language="javascript">
		
		
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "getdataplotting"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_materi_ubudiyah.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "jenjang" },
				{ "data": "tingkat" },
				{ "data": "kategori" },
				{ "data": "materi" },
				{ "data": "action" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			var crud = 'N';
			var item = '';
			$("#modalmasteritem").modal('show');
			newitem();
			
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#optmateri").val("");
			$("#optkategori").val("");
			$("#optjenjang").val("");
			$("#opttingkat").val("");
			$("#head-modal").html("Tambah Materi");
			set_focus("#txtname");
			
		}
		
		$('#optkategori').on('change', function() {
			var value = {
				cat: this.value,
				method : "get_materi_ubudiyah"
			};
			$.ajax({
				url : "c_materi_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var respons = jQuery.parseJSON(data);
					if(respons.data.length>0){
						$('#optmateri').html("");
						$('#optmateri').html("<option value=''> Pilih materi </option>");
						$.each(respons.data, function (key, val) {
						  $('#optmateri').append('<option value="'+val.id_materi+'">'+val.materi+'</option>');
						})
					}else{
						$('#optmateri').html("");
						$('#optmateri').html("<option value=''> Materi tidak ditemukan </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btnedit", function() {
			newitem();
			$("#head-modal").html("Edit Materi");
			var id_item = $(this).attr("id_item");
			var crud = 'E';
			var value = {
				id_item: id_item,
				method : "get_detail_plotting"
			};
			$.ajax(
			{
				url : "c_materi_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#modalmasteritem").modal('show');
					var hasil = jQuery.parseJSON(data);
					data = hasil.msg;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.id_plotting);
					$("#optjenjang option[value='"+data.jenjang+"']").prop('selected', true);
					$("#opttingkat option[value='"+data.tingkat+"']").prop('selected', true);
					$("#optkategori option[value='"+data.kategori+"']").prop('selected', true);
					$('#optmateri').html("");
					$.each(hasil.materi, function (key, val) {
					  $('#optmateri').append('<option value="'+val.id_materi+'">'+val.materi+'</option>');
					})
					$("#optmateri option[value='"+data.id_materi+"']").prop('selected', true);
					set_focus("#optjenjang");
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btndelete", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "delete_plotting_materi"
			};
			swal({   
			title: "Hapus plotting materi ubudiyah",   
			text: "Apakah anda yakin melanjutkan proses?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Hapus",   
			closeOnConfirm: true }, 
			function(){
			  $.ajax(
			  {
				url : "c_materi_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
						$.notify('Proses hapus berhasil');
						var table = $('#table_item').DataTable(); 
						table.ajax.reload( null, false );
						newitem();				
					}else{
						$.notify({
							message: "Proses hapus gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			  });
			});
		});
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#optmateri").val();
			var jenjang = $("#optjenjang").val(); 
			var tingkat = $("#opttingkat").val(); 
			var crud=$("#inputcrud").val();
			
			if(item_name == '' || item_name== null ){
				$.notify(
					{ message: "Materi belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#txtname").focus();
				return;
			}
			
			if(jenjang == '' || jenjang== null ){
				$.notify(
					{ message: "Jenjang belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#opttingkat").focus();
				return;
			} 
			
			if(tingkat == '' || tingkat== null ){
				$.notify(
					{ message: "Tingkat belum dipilih!"},
					{ type: 'warning', delay: 8000,}
				);		
				$("#opttingkat").focus();
				return;
			}
			var value = {
				id_item: id_item,
				item_name: item_name,
				tingkat:tingkat,
				jenjang:jenjang,
				crud: crud,
				method : "save_plotting_materi"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_materi_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.crud == 'N'){
						if(data.result == true){
							$.notify('Proses simpan berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							newitem();				
						}else{
							$.notify({
								message: "Proses simpan gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});
							set_focus("#txtname");
						}
					}else if(data.crud == 'E'){
						if(data.result == true){
							$.notify('Proses update berhasil');
							var table = $('#table_item').DataTable(); 
							table.ajax.reload( null, false );
							$("#modalmasteritem").modal("hide");
							newitem();
						}else{
							$.notify({
								message: "Proses update gagal, error :"+data.error
							},{
								type: 'danger',
								delay: 8000,
							});					
							set_focus("#txtname");
						}
					}else{
						$.notify({
							message: "Invalid request"
						},{
							type: 'danger',
							delay: 8000,
						});	
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
	
	</script>
</body>
</html>
