<?php 
$titlepage="Ubudiyah";
$idsmenu=61; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
require_once("../model/model_data.php");
require_once("../model/model_program.php");
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_program();
$kelas = $pos->getKelas();
$jenis = $pos->getJenisNilai();
$conn = new model_data();
$asrama = $conn->getAsrama();
?>
<section class="content-header">
  <h1>
	CETAK RAPORT
	<small>Program Ubudiyah</small>
  </h1>
</section>
<section class="content">

	<div class="box box-default">
		<div class="box-header with-border">
		  <h3 class="box-title">Filter</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<form id="target" class="form-horizontal" action="javascript:void(0);">
			<div class="box-body">
			  <div class="row">
				<div class="col-md-11">
				  <input type="hidden" name="siswaid" id="siswaid" value="">
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Firqoh</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtasrama" name="txtasrama" style="width: 100%;">
						  <option value="">Pilih Firqoh</option>
						  <?php
						  foreach($asrama[1] as $opt){
							echo '<option value="'.$opt['auto'].'">'.$opt['nama_asrama'].'</option>';
						  }
						  ?>
						</select>
					</div>
					<label class="col-sm-2  control-label">Pilih kamar</label>
					<div class="col-sm-3">
						<select class="form-control" id="txtkamar" name="txtkamar" style="width: 100%;">
						  <option value="">Pilih Firqoh dahulu</option>
						  
						</select>
					</div>
					
				  </div>
				  <div class="form-group"> 
					<label class="col-sm-2  control-label">Pilih Jenjang</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="optjenjang" name="optjenjang" >
							<option value=""> Pilih Jenjang </option>
							<option value="ula"> Ula</option>
							<option value="wustho"> Wustho</option>
							<option value="MDK"> MDK</option>
						</select>
					</div>
					<label class="col-sm-2  control-label">Jenis nilai</label>
					<div class="col-sm-3">
						<select class="form-control autocomplete" id="opttingkat" name="opttingkat" >
							<option value=""> Pilih Tingkat </option>
							<option value="1"> Tingkat 1</option>
							<option value="2"> Tingkat 2</option>
							<option value="3"> Tingkat 3</option>
						</select>
					</div>
					
				  </div>
				  <div class="form-group">
					<label class="col-sm-2  control-label">Santri</label>
					<div class="col-sm-5">
						<input type="hidden" id="kodesiswa"  name="kodesiswa" value="" >
						<input type="text" class="form-control" id="txtsiswa"  name="txtsiswa" value="" data-target="modalsearch">
					</div>
					<div class="col-sm-2">
					  <button  type="button"  title="Cari santri" class=" btn btn-block btn-flat btn-primary"  id="btnsearch"  ><span class="fa fa-search"></span> Cari</button>				
					</div>				
				  </div>				
				</div>				
			  </div><!-- /.row -->
			</div><!-- /.box-body -->
			
		</form>
	</div><!-- /.box -->
	
	<div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title titleAbsen">Raport Ubudiyah Santri</h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div><!-- /.box-header -->
		<!--./ box header-->
		<div class="box-body">
			<div class="jadwal-pelajaran">
			  
			  <div class="row">
			  
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>No Induk</b>
					</div>
					<div class="col-sm-8">
						<b class="control-label">: </b> <span id="detinduk"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Nama</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detnama"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tahun Akademik</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettapel"></span>
					</div>
				  </div>
				</div>
				<div class="col-md-6 form-horizontal">
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Jenjang</b>
					</div>
					<div class="col-sm-8">
						<b >: </b> <span id="detjenjang"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Tingkat</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="dettingkat"></span>
					</div>
				  </div>
				  <div class="form-group"> 
					<div class="col-sm-4 control-label">
						<b>Semester</b>
					</div>
					<div class="col-sm-8">
						<b>: </b> <span id="detsemester"></span>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="row">
				<form id="inputNilai">
				<input type="hidden" id="hiddensiswa">
				<input type="hidden" id="hiddenjenjang">
				<input type="hidden" id="hiddentingkat">
				<input type="hidden" id="crud">
				<div class="table-responsive">
				  <table id="raportSantri" class="table  table-bordered table-hover ">
					<thead>
					  <tr class="tableheader">
						<th style="width:45px" class="text-center">No</th>
						<th class="text-center">Kategori Materi</th>
						<th class="text-center">Materi </th>
						<th class="text-center">Nilai</th>
						<th class="text-center">Grade</th>
					  </tr>
					</thead>
					<tbody>
					
					</tbody>
				  </table>
				</div>
				</form>
			  </div>
			</div>	
		</div>
		<div class="box-footer ">
		  <div class="box-tools pull-right">
			<button type="button" title="Download raport" class="btn btn-success " id="btndownload" ><i class="fa fa-download"></i> Download</button><span id="infoproses"></span>
		  </div>
		</div><!-- /.box-footer -->
	</div><!-- /.box -->

</section><!-- /.content -->
	
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	   
	<script language="javascript">
		$(function () {
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
	
			//decimal();
			integer();
		});
		
		$(document).on('focus',".txttanggal", function(){
			$(this).datepicker({
				format: 'dd-mm-yyyy'
			});
			$(this).inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
		
		$('#txtasrama').on('change', function() {
			$('#kodesiswa').val('');
			$('#txtsiswa').val('');
			var value = {
				asrama: this.value,
				method : "get_kamar_asrama"
			};
			$.ajax({
				url : "../pesantren/c_kamar.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var respons = jQuery.parseJSON(data);
					if(respons.data.length>0){
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Pilih kamar </option>");
						$.each(respons.data, function (key, val) {
						  $('#txtkamar').append('<option value="'+val.auto+'">'+val.nama_kamar+'</option>');
						})
					}else{
						$('#txtkamar').html("");
						$('#txtkamar').html("<option value=''> Kamar tidak ditemukan </option>");
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		function integer(){
			$(document).on("blur",".decimal",function(){
				var angka=parseFloat($(this).val());
				if(isNaN(angka))
				{
					angka=0;
				}
				$(this).val(angka);		
			});

			$(".decimal").focus(function(e){
				if(e.which === 9){
					return false;
				}
				$(this).select();
			});
		}
		
		$(function () {
			
			$( "#txtsiswa" ).autocomplete({
				
				search  : function(){$(this).addClass('working');},
				open    : function(){$(this).removeClass('working');},
				source: function(request, response) {
					$.getJSON("autocomplete_ubudiyah.php", { term: $('#txtsiswa').val(), jenjang: $('#optjenjang').val(), tingkat: $('#opttingkat').val(), asrama:$('#txtasrama').val(),kamar:$('#txtkamar').val()}, 
						response); },
				minLength:1,
				select:function(event, ui){
						event.preventDefault();
						$('#txtsiswa').val(ui.item.nis+" - "+ui.item.nama);
						$('#kodesiswa').val(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
				return $( "<li>" )
				.append( "<dt>"+item.nis + " - "+item.nama+"</dt>"  )
				.appendTo( ul );
			};
		});
		
		$(document).on("click","#btnsearch",function(){
			var jenjang = $('#optjenjang').val();
			var tingkat = $('#opttingkat').val();
			var siswa = $('#kodesiswa').val();
			if( jenjang == null || jenjang == ''){
				$.notify({
					message: "Jenjang belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optjenjang").focus();
				return;
			}
			if( tingkat == null || tingkat == ''){
				$.notify({
					message: "Tingkat belum di pilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#opttingkat").focus();
				return;
			}
			if( siswa == null || siswa == ''){
				$.notify({
					message: "Santri tidak ada!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtsiswa").focus();
				return;
			}
			raport(jenjang,tingkat,siswa);
		});
		
		function raport(jenjang,tingkat,idSiswa){
			
			$('#raportSantri tbody').empty();
			var tbody = document.getElementById("raportSantri").tBodies[0];
			var value = {
				idSiswa: idSiswa,
				jenjang: jenjang,
				tingkat: tingkat,
				method : "getraportubudiyahsantri"
			};
			$.ajax(
			{
				url : "c_nilai_ubudiyah.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					
					$('#hiddensiswa').val(idSiswa);
					$('#hiddenjenjang').val(jenjang);
					$('#hiddentingkat').val(tingkat);
					$('#crud').val('N');
					
					$('#detinduk').html(hasil.nis);				
					$('#detnama').html(hasil.nama);
					$('#detjenjang').html(hasil.jenjang);
					$('#dettingkat').html(hasil.tingkat);
					$('#dettapel').html(hasil.tapel);
					$('#detsemester').html(hasil.semester);
					
					var no = 0;
					$.each(hasil.nilai, function (key, val) {
					  var row = tbody.insertRow(no);
					  var urut = row.insertCell(0);
					  var kategori = row.insertCell(1);
					  var materi = row.insertCell(2);
					  var nilai = row.insertCell(3);
					  var grade = row.insertCell(4);
					  
					  urut.innerHTML = no+1;	
					  kategori.innerHTML = val.kategori;	
					  materi.innerHTML = val.materi;
					  nilai.innerHTML = val.nilai;
					  grade.innerHTML = val.grade;
					  				  
					  no++;
					})
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		}
		
		function newitem(){
			$('#txtasrama').val('');				
			$('#txtkamar').val('');				
			$('#optjenjang').val('');				
			$('#opttingkat').val('');				
			$('#txtsiswa').val('');				
			$('#kodesiswa').val('');				
			$('#hiddensiswa').val('');				
			$('#hiddenjenjang').val('');				
			$('#hiddentingkat').val('');				
			$('#crud').val('N');
		}
		
		$(document).on( "click","#btndownload", function() {
			var jenjang = $('#hiddenjenjang').val();
			var tingkat = $('#hiddentingkat').val();
			var siswa = $('#hiddensiswa').val();
			
			if( siswa == null || siswa == ''){
				$.notify({
					message: "Santri belum dipilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtsiswa").focus();
				return;
			}
			var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "raport_ubudiyah.php";

			var txtjenjang = document.createElement("input");
			txtjenjang.type = "hidden";
			txtjenjang.name = "jenjang";
			txtjenjang.value = jenjang;
			mapForm.appendChild(txtjenjang);
			
			var txttingkat = document.createElement("input");
			txttingkat.type = "hidden";
			txttingkat.name = "tingkat";
			txttingkat.value = tingkat;
			mapForm.appendChild(txttingkat);
			
			var txtsiswa = document.createElement("input");
			txtsiswa.type = "hidden";
			txtsiswa.name = "siswa";
			txtsiswa.value = siswa;
			mapForm.appendChild(txtsiswa);
			
			document.body.appendChild(mapForm);
			mapForm.submit();
		});
	</script>
</body>
</html>
