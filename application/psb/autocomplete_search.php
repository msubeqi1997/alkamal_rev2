<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_psb();

$data = $pos->autoCompleteSearch($term);
foreach ($data[1] as $row) {
	$row['nomor']=htmlentities(stripslashes($row['no_pendaftaran']));
	$row['nama']=htmlentities(stripslashes($row['nama_lengkap']));
	$row['siswa_id']=htmlentities(stripslashes($row['uuid']));
	$row['jalur']=htmlentities(stripslashes($row['jalur_name']));
	
	$row_set[] = $row;
}

echo json_encode($row_set);
?>