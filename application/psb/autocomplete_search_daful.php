<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new model_psb();

$data = $pos->autoCompleteSearchDaful($term);
foreach ($data[1] as $rows) {
	$row['nomor']=htmlentities(stripslashes($rows['no_pendaftaran']));
	$row['nama']=htmlentities(stripslashes($rows['nama_lengkap']));
	$row['siswa_id']=htmlentities(stripslashes($rows['uuid']));
	$row['tingkat']=htmlentities(stripslashes($rows['jenjang_pendidikan']));
	$row['kelamin']=htmlentities(stripslashes($rows['kelamin']));
	
	$row_set[] = $row;
}
echo json_encode($row_set);
?>