<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
include '../../plugins/excel/vendor/autoload.php';

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$reader->setReadDataOnly(true);

	$pos = new model_psb();
	
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
		
	if($method == 'save')
	{
		$tmp_siswa = $_FILES['photo_siswa']['tmp_name'];
		$tipe_siswa = $_FILES['photo_siswa']['type'];
		$nama_siswa = $_FILES['photo_siswa']['name'];
		if(empty($tmp_siswa)){
			$output['stat'] = 'File kosong';
		}else{
			$value=array();
			$spreadsheet = $reader->load($tmp_siswa);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			$output['stat'] = 'ada';
			unset($sheetData[0]);
			foreach($sheetData as $row){
				$value[]="('".$row[0]."','".$tapel."','".$row[1]."')";
			}
			/*
			echo "<pre>";
			print_r($sheetData);
			*/
			
			$pos->deleteNilaiSeleksi($tapel);
			$insert = $pos->insertNilaiSeleksi($value);
			$result['result'] = $insert[0];
			$result['msg'] = $insert[1];
			echo json_encode($result);
			
		}
		
		$respon=json_encode($output);
		echo $respon;
	}
	if($method == 'getdata'){
		$array = $pos->getNilaiSeleksi();
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'data'){
		$array = $pos->getDataServerside();
		
		$data = $array['data'];
		$i=0;
		foreach ($data as $row) {
			$data[$i]['urutan'] = $row['urutan'];
			$data[$i]['id'] = $row['id'];
			$data[$i]['no_pendaftaran'] = $row['no_pendaftaran'];
			$data[$i]['nama_lengkap'] = $row['nama_lengkap'];
			$data[$i]['alamat'] = $row['alamat'];
			$data[$i]['sekolah_asal'] = $row['sekolah_asal'];
			$data[$i]['nilai'] = $row['nilai'];
			$data[$i]['jalur_name'] = $row['jalur_name'];
			$i++;
		}
		
		$result['draw'] = $array['draw'];
		$result['recordsTotal'] = $array['recordsTotal'];
		$result['recordsFiltered'] = $array['recordsFiltered'];
		$result['data'] = $data;
		echo json_encode($result);
	}
