<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'getdata'){
		$array = $pos->getJalurPendaftaran();
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['jalur_id'].'"  title="Tombol edit jalur pendaftaran" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['jalur_id'].'"  ><i class="fa fa-edit"></i></button>';
			if($key['aktif'] == '1'){$active = '<button  type="submit" id_item="'.$key['jalur_id'].'"  title="Jalur pendaftaran aktif" class="btn btn-sm btn-success btnactive "  id="btnactive'.$key['jalur_id'].'"  >active</button>';}
			else{$active = '<button  type="submit" id_item="'.$key['jalur_id'].'"  title="Aktifasi jalur pendaftaran" class="btn btn-sm btn-warning btnactive "  id="btnactive'.$key['jalur_id'].'"  >activate</button>';}
			$data[$i]['mdk'] = ($key['jenis']=='mdk')?'MDK':'Bukan MDK';
			$data[$i]['first'] = display_to_report($key['tgl_mulai']);
			$data[$i]['last'] = display_to_report($key['tgl_sampai']);
			$data[$i]['button'] = $button;
			$data[$i]['active'] = $active;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$mdk = $_POST['mdk'];
		$plot = $_POST['plot'];
		$kelengkapan = $_POST['kelengkapan'];
		$first = display_to_sql($_POST['first']);
		$last = display_to_sql($_POST['last']);
		$aktif = $_POST['aktif'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJalurPendaftaran($name,$mdk,$plot,$kelengkapan,$tapel,$first,$last,$aktif);
			if($array[0] == true)
			{
				$result['item'] = $array[2];
			}
		}
		else
		{
			$array = $pos->updateJalurPendaftaran($iditem,$name,$mdk,$plot,$kelengkapan,$first,$last,$aktif);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailJalurPendaftaran($id_item);
		
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		$array['first'] = display_to_report($data[1]['tgl_mulai']);
		$array['last'] = display_to_report($data[1]['tgl_sampai']);
		//$datax = array('data' => $array);
		echo json_encode($array);
	}
	
	if($method == 'activate')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->activateJalurPendaftaran($id_item);
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}