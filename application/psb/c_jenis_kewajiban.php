<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'getdata'){
		$array = $pos->getJenisKewajiban();
		$value = $array[1];
		$i=0;
		foreach ($value as $key => $val) {
			$button = '<button  type="submit" id_item="'.$key.'"  title="Tombol edit jenis kewajiban" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key.'"  ><i class="fa fa-edit"></i></button>';
			$data[$i]['button'] = $button;
			$data[$i]['urutan'] = $i+1;
			$data[$i]['name'] = $val;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveJenisKewajiban($name,$tapel);
			if($array[0] == true)
			{
				$result['item'] = $array[2];
			}
		}
		else
		{
			$array = $pos->updateJenisKewajiban($iditem,$name);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailJenisKewajiban($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
} else {
	exit('No direct access allowed.');
}