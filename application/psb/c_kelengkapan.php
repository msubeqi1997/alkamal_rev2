<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	
	if($method == 'getdata'){
		$array = $pos->getKelengkapan();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['kelengkapan_id'].'"  title="Tombol edit kelengkapan" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['kelengkapan_id'].'"  ><i class="fa fa-edit"></i></button>';
			if($key['aktif'] == '1'){$active = '<button  type="submit" id_item="'.$key['kelengkapan_id'].'"  title="Kelengkapan aktif" class="btn btn-sm btn-success btnactive "  id="btnactive'.$key['kelengkapan_id'].'"  >active</button>';}
			else{$active = '<button  type="submit" id_item="'.$key['kelengkapan_id'].'"  title="Aktifasi kelengkapan" class="btn btn-sm btn-warning btnactive "  id="btnactive'.$key['kelengkapan_id'].'"  >activate</button>';}
			$data[$i]['button'] = $button;
			$data[$i]['active'] = $active;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$jenis = $_POST['jenis'];
		$aktif = $_POST['aktif'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveKelengkapan($name,$jenis,$aktif);
			if($array[0] == true)
			{
				$result['item'] = $array[2];
			}
		}
		else
		{
			$array = $pos->updateKelengkapan($iditem,$name,$jenis,$aktif);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailKelengkapan($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'activate')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->activateKelengkapanPSB($id_item);
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}