<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'getdata'){
		$jenis = $pos->getJenisKewajiban();
		$plot = $jenis[1];
		$array = $pos->getMasterKewajiban();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_item="'.$key['id'].'"  title="Tombol edit kewajiban" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['id'].'"  ><i class="fa fa-edit"></i></button>';
			if($key['aktif'] == '1'){$active = '<button  type="submit" id_item="'.$key['id'].'"  title="Kewajiban aktif" class="btn btn-sm btn-success btnactive "  id="btnactive'.$key['id'].'"  >active</button>';}
			else{$active = '<button  type="submit" id_item="'.$key['id'].'"  title="Aktifasi kewajiban" class="btn btn-sm btn-warning btnactive "  id="btnactive'.$key['id'].'"  >activate</button>';}
			$data[$i]['plot'] = $plot[$key['jenis_kewajiban_id']];
			$data[$i]['nominal'] = number_format($key['nominal']);
			$data[$i]['button'] = $button;
			$data[$i]['active'] = $active;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item')
	{
		$iditem = $_POST['id_item'];
		$name = $_POST['item_name'];
		$plot = $_POST['plot'];
		$nominal = $_POST['nominal'];
		$aktif = $_POST['aktif'];
		$crud=$_POST['crud'];
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveMasterKewajiban($name,$plot,$nominal,$aktif,$tapel);
			if($array[0] == true)
			{
				$result['item'] = $array[2];
			}
		}
		else
		{
			$array = $pos->updateMasterKewajiban($iditem,$name,$plot,$nominal,$aktif);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'get_detail_item')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->getDetailKewajiban($id_item);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	
	if($method == 'activate')
	{
		$id_item=$_POST['id_item'];
		$data = $pos->activateMasterKewajiban($id_item);
		$array['result'] = $data[0];
		echo json_encode($array);
	}
} else {
	exit('No direct access allowed.');
}