<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'save_item')
	{
		$tapelPSB = $pos->activeTapelPSB();
		$tahunaktif = $tapelPSB[1]['thn_ajaran_id'];
		$unique = getId();
		$uuid = '';
		$jalur = isset($_POST['txtjalur']) ? $_POST['txtjalur'] : '';
		$data['sekolah'] = isset($_POST['sekolah']) ? $_POST['sekolah'] : '';
		$jenis= isset($_POST['jenis']) ? $_POST['jenis'] : '';
		$data['tingkat'] = isset($_POST['tingkat']) ? $_POST['tingkat'] : '';
		$data['nama_lengkap'] = isset($_POST['nama_lengkap']) ? $_POST['nama_lengkap'] : '';
		$data['kelamin'] = isset($_POST['kelamin']) ? $_POST['kelamin'] : '';
		$data['tempat_lahir'] = isset($_POST['tempat_lahir']) ? $_POST['tempat_lahir'] : '';
		$data['tanggal_lahir'] = isset($_POST['tanggal_lahir']) ? display_to_sql($_POST['tanggal_lahir']) : '';
		$data['no_hp'] = isset($_POST['no_hp']) ? $_POST['no_hp'] : '';
		$data['input_at'] = date ('Y-m-d H:i:s');
		$data['input_by'] = $_SESSION['sess_id'];
		$status = 'calon';
		$crud = $_POST['inputcrud'];
		$tahun = substr($tapelPSB[1]['thn_ajaran'],0,4);
		$id_siswa = isset($_POST['id_siswa']) ? $_POST['id_siswa'] : '';;
		
		$arrjenjang = $pos->getJenjangPendidikan($data['sekolah']);
		$jenjang = $arrjenjang[1]['satuan_pendidikan'];
		  
		if($_POST['jenis'] == 'tidak')
		{
			$uuid = $unique;
			$data['uuid'] = $uuid;
			$savesiswa = $pos->savePendaftar($data);
			if($savesiswa[0] == true){
				$query = $pos->inputjenjangpsb($tahunaktif,$uuid,$jenjang,$jenis,$data['sekolah'],$tahun,$status,$jalur,$data['input_by']);
			}
		}
		else
		{
			$uuid = base64_decode($id_siswa);
			$query = $pos->inputjenjangpsb($tahunaktif,$uuid,$jenjang,$jenis,$data['sekolah'],$tahun,$status,$jalur,$data['input_by']);
		}
		$result['token'] = $uuid;
		if($query[0] == true){
			$result['status']='OK';
			$result['no_pendaftaran']=$query[2];
		}else{
			$result['status']='NO';
		}
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}