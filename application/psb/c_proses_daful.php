<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$posdata = new model_data();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'select_siswa_pendaftar'){
		
		$idc = $_POST['siswa_id'];
		$array = $pos->getSiswaPendaftar($idc);
		$jenjang=$array[1]['jenjang_pendidikan']-1;
		$check_aktif = $pos->checkSiswaAktif($idc,$jenjang);
		
		$html_riwayat = '';
		$riwayat = $pos->getListKewajiban($array[1]['jalur'],$array[1]['uuid']);
		$num=1;
		foreach($riwayat[1] as $row){
			$kurang = $row['nominal']-$row['paid'];
			$form = ($kurang <= 0 )?'Lunas':'<input type="text" class="form-control input-sm nombayar" id="bayar'.$row['id'].'" name="bayar'.$row['id'].'" value="'.$kurang.'" onkeypress="return isNumber(event)">';
			$check= ($kurang <= 0 )?'Lunas':'<input type="checkbox" class="chkbox" id="seleksi_from" name="seleksi_from[]" value="'.$row['id'].'">';
			$html_riwayat .='<tr>
								<td>'.$num.'</td>
								<td>'.$row['kewajiban'].'</td>
								<td>'.$row['nominal'].'</td>
								<td>'.$row['paid'].'</td>
								<td>'.$form.'</td>
								<td>'.$check.'</td>
							</tr>';
			$num++;
		}
		$html_riwayat .='<tfoote>
							<td class="pull-right">Total : </td>
							<td></td><td></td><td></td>
							<td id="txttotal">Rp. 0</td>
						</tfooter>';
			
		$html_kel = '';
		$kelengkapan = $pos->getBerkasSiswa($array[1]['uuid'],$array[1]['jalur']);
		$no=1;
		foreach($kelengkapan[1] as $row){
			$html_kel .='<div class="form-group">
								<div class="col-sm-3">
									<b>'.$row['kelengkapan'].'</b>
								</div>
								<div class="col-sm-9">
								 <span><input type="checkbox" name="lengkap[]" value="'.$row['kelengkapan_id'].'"> Lengkap</span>
								</div>
							</div>';$no++;
			}
		$array[1]['kode_pendaftaran']=base64_encode($array[1]['kode_pendaftaran']);
		$result['kelengkapan'] = $html_kel;
		$result['riwayat'] = $html_riwayat;
		
		$result['data'] = $array[1];
		$result['aktif'] = ($check_aktif[1]['status']=='siswa')?'aktif':'tidak';
		$result['result'] = $array[0];
		echo json_encode($result);
	}
	
	if($method == 'getdata'){
		$array = $pos->getAllPendaftar();
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$data[$i]['daftar'] = ''.$key['no_pendaftaran'].' - '.$key['nama_lengkap'].'';;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'save_item'){
		$token = $_POST['txttoken'];
		$jenjang = $_POST['valjenjang'];
		$kode = base64_decode($_POST['txtpendaftaran']);
		$kelengkapan = isset($_POST['lengkap'])?$_POST['lengkap']:'';
		$seleksi_from = isset($_POST['seleksi_from'])?$_POST['seleksi_from']:'';
		$daful = 'sudah';
		$stat = 'siswa';
		$lengkap = '0';
		$check_aktif = $pos->checkSiswaAktif($token,$jenjang);
		$aktif = ($check_aktif[1]['status']=='siswa')?'aktif':'tidak';
		if($aktif == 'aktif'){
			$result['result'] = 'Status santri masih aktif.';
		}else{
			$array = $pos->updateSiswaDaful($kode,$stat,$daful);
			if($array[0] == true){
			  $data = $pos->getSiswaPendaftar($token);
			  $jenjang['tujuan_sekolah'] = $data[1]['tujuan_sekolah'];
			  $jenjang['nis'] ='';
			  $jenjang['jenis'] = $data[1]['jenis'];
			  $jenjang['jenjang_pendidikan'] = $data[1]['jenjang_pendidikan'];
			  $jenjang['tingkat_kelas'] = '1';
			  $jenjang['status_kesiswaan'] = 'siswa';
			  $jenjang['uuid']=$token;
			  $jenjang['id_pendaftaran']=$kode;
			  $tingkat['id_siswa']=$token;
			  $tingkat['tingkat']='1';
			  $insert_jenjang = $pos->insertJenjangSiswa($jenjang);
			  $tingkat = $pos->insertTingkatSiswa($tingkat);
			  if($kelengkapan !=''){
				foreach($kelengkapan as $val){
					$pos->updateKelengkapanDaful($token,$val,$lengkap);
				}
			  }
			  
			  //payment
			  $input = '';
			  $detail_payment = '';
			  $payment_id = getId();
			  $id_user = $_SESSION['sess_id'];
			  $id_siswa = $token;
			  $payment_date = date('Y-m-d');
			  
			  $payment = 'daful';
			  $lunas = '0';
			  
			  $jumlah = array();
			  if(!empty($seleksi_from)){
				$insert = array();
				foreach($seleksi_from as $k => $v){
					$jumlah[]=$_POST['bayar'.$v];
					$insert[]=array('payment_id'=>$payment_id,'id_kewajiban'=>$v,'nominal'=>$_POST['bayar'.$v]);
				}
				$paid = array_sum($jumlah);
				$input = $pos->inputPaymentDaful($payment_id,$id_user,$id_siswa,$kode,$payment_date,$paid,$payment,$lunas);
				if($input[0] == true){
				  $detail_payment = $pos->inputDetailPayment($insert);  
				}
				$result['payment'] = $input[1];
			  }
			  
			}
			
			$result['result'] = $array[0];
		}
		//$result['payment_detail'] = $detail_payment[1];
		echo json_encode($result);
	}
	
	if($method == 'pembatalan'){
		$token = $_POST['token'];
		$kode = base64_decode($_POST['kode']);
		$daful = 'belum';
		$stat = 'calon';
		$lengkap = '0';
		
		$array = $pos->updateSiswaDaful($kode,$stat,$daful);
		if($array[0] == true){
		  $jenjang = $pos->deleteJenjangSiswa($token);
		  $tingkat = $pos->deleteTingkatSiswa($token);
		  $berkas = $pos->deleteKelengkapanDaful($token,$kode);
			
		  $payment = 'daful';
		  $delete = $pos->deletePaymentDaful($token,$kode);
		}
		
		$result['result'] = $array[0];
		$result['payment'] = $delete[1];
		$result['berkas'] = $berkas[1];
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}