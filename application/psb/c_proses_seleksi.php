<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	if($method == 'select_siswa_pendaftar'){
		
		$idc = $_POST['siswa_id'];
		$array = $pos->getSiswaPendaftar($idc);
		
		$html_riwayat = '';
		$riwayat = $pos->getRiwayatPendidikan($array[1]['uuid']);
		$num=1;
		foreach($riwayat[1] as $row){
			$html_riwayat .='<div class="form-group">
								<div class="col-sm-2">
									<b>Pendidikan:</b>
								</div>
								<div class="col-sm-5">
									<span>'.$row['penyelenggara'].'</span>
								</div>
							  <div class="col-sm-5">
								<div class="col-sm-7">
									<b>Lulus tahun:</b>
								</div>
								<div class="col-sm-5">
									<span>'.$row['tahun_selesai'].'</span>
								</div>
							  </div>
							</div>';$num++;
		}
			
		$html_kel = '';
		$kelengkapan = $pos->getBerkasSiswa($array[1]['uuid'],$array[1]['jalur']);
		$no=1;
		foreach($kelengkapan[1] as $row){
			if($row['berkas'] == NULL){$berkas='';}
			else{ $berkas = '<a id="berkas'.$row['kelengkapan_id'].'" src="../../files/images_pendaftar/'.$row['berkas'].'" onclick="return bigimage(berkas'.$row['kelengkapan_id'].');">[Click to view]</a>';}
			$html_kel .='<div class="form-group">
								<div class="col-sm-3">
									<b>'.$row['kelengkapan'].'</b>
								</div>
								<div class="col-sm-9">
									<b>: </b> <span>'.$berkas.'</span>
								</div>
							</div>';$no++;
			}
		
		$array[1]['kode_pendaftaran']=base64_encode($array[1]['kode_pendaftaran']);
		$result['kelengkapan'] = $html_kel;
		$result['riwayat'] = $html_riwayat;
		
		$result['data'] = $array[1];
		$result['result'] = $array[0];
		echo json_encode($array);
	}
	
	if($method == 'getdata'){
		$array = $pos->getAllPendaftar();
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$data[$i]['daftar'] = ''.$key['no_pendaftaran'].' - '.$key['nama_lengkap'].'';
			$data[$i]['seleksi'] = $key['seleksi'];
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'seleksi_siswa'){
		$token = base64_decode($_POST['kode_pendaftaran']);
		$opt = $_POST['opt'];
		$user=$_SESSION['sess_id'];
		if($opt == 'reject'){
			$stat='tolak';
			
			$array = $pos->updateSeleksiSiswa($token,$stat,$user);
		}else if($opt == 'lolos'){
			$stat='lolos';
			$array = $pos->updateSeleksiSiswa($token,$stat,$user);
		}
		
		$result['result'] = $array[0];
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}