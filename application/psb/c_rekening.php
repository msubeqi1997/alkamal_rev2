<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=isset($_POST['method'])?$_POST['method']:'';
	
	if($method == 'get_rekening')
	{
		$result = $pos->get_rekening_bank();

		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'update_rekening')
	{
		$norek = $_POST['norek'];
		$bank = $_POST['bank'];
		$cabang = $_POST['cabang'];
		
		$array = $pos->update_rekening_bank($norek,$bank,$cabang);

		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
} else {
	exit('No direct access allowed.');
}