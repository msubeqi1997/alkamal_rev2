<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	if($method == 'getdata'){
		
		$var = array();
		if($_POST['first'] != ''){$var['t.payment_date >']=display_to_sql($_POST['first']);}
		if($_POST['last']  != ''){$var['t.payment_date <']=display_to_sql($_POST['last']);}
		
		$array = $pos->getFilterPayment($var);
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$data[$i]['urutan'] = $i+1;
			$data[$i]['tanggal'] = display_to_report($key['payment_date']);
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'download'){
		
	}
	
} else {
	exit('No direct access allowed.');
}