<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_psb();
	$method=$_POST['method'];
	$tahun = $pos->activeTapelPSB();
	$tapel = $tahun[1]['thn_ajaran_id'];
	
	$sex = array('putra' => '1','putri' => '0');
	$arrjenjang = array('ula' => '2', 'wustho' => '3');
	$kelamin = array('1' => 'Putra','0' => 'Putri');
	$jenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	$daful = array('sudah'=>'Sudah','belum'=>'Belum');
	$seleksi = array('lolos'=>'Diterima','tolak'=>'Tidak diterima',''=>'Belum seleksi');
	
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	
	if($method == 'getdata'){
		
		$var = array();
		if($_POST['jalur'] != 'all'){
			$jalur= explode(' ',$_POST['jalur']);
			$var['jenis_kelamin']=$jalur[1];
			$var['jenjang']=$jalur[0];
		}
		if($_POST['stat'] != 'all'){$var['seleksi']=$_POST['stat'];}
		if($_POST['first'] != ''){$var['DATE(input_at) >']=display_to_sql($_POST['first']);}
		if($_POST['last']  != ''){$var['DATE(input_at) <']=display_to_sql($_POST['last']);}
		
		$array = $pos->getFilterSeleksi($var);
		
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$data[$i]['urutan'] = $i+1;
			$data[$i]['alamat_lengkap'] = "".$key['alamat']." ".ucwords(strtolower($key['kabupaten_name']))." ".ucwords(strtolower($key['propinsi_name']))."";
			$data[$i]['status_seleksi'] = $seleksi[$key['seleksi']];
			$data[$i]['jenjang'] = $key['jenjang'];
			$data[$i]['mdk'] = $key['jenis'];
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	
	if($method == 'download'){
		
	}
	
} else {
	exit('No direct access allowed.');
}