<?php
//error_reporting(0);
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
include '../../plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf as Dompdf;
$dompdf = new Dompdf();

$idc = isset($_GET['token'])?$_GET['token']:'';
$no_pendaftaran = isset($_GET['nomor'])?$_GET['nomor']:'';
$pos = new model_psb();
$array = $pos->getSiswaPendaftar($idc);
$data = $array[1];
$tgl_ttd= tanggal_indo($data['tertanggal']);
$tgl_lahir_ayah = (empty($data['tanggal_lahir_ayah']) || $data['tanggal_lahir_ayah'] == '')?'':date('d-m-Y',strtotime($data['tanggal_lahir_ayah']));
$ttl_ayah = $data['tempat_lahir_ayah'].', '.$tgl_lahir_ayah;

$kelamin = array('0'=>'Perempuan','1'=>'Laki-laki');
$stopword = array('KOTA','KABUPATEN');

$rekening = $pos->get_rekening_bank();
$tingkatpend = $pos->getTingkatPendidikan();


  
$html = '<!DOCTYPE html>
  <html lang="id">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <div class="kertas">
      <div class="kop-surat">
        <img src="../../image/kop-alkamal.jpg" width="738" height="103" style="margin-top:-30px" alt="Kop surat">
      </div>
      <h1>FORMULIR ONLINE PENDAFTARAN SANTRI BARU</h1>
      <table class="formulir">
		<thead>

        </thead>
		<tbody>
		  <tr style="padding-top:10px;">
            <td class="label" style="width:145px;">No. pendaftaran</td>
            <td style="width:5px">:</td>
            <td colspan="7"> '.$no_pendaftaran.'</td>
          </tr>
		  <tr style="background-color:#000;color:#fff;text-align:center;">
            <th colspan="11"><h2>IDENTITAS CALON SANTRI</h2></th>
          </tr>
		  <tr style="padding-top:10px;">
            <td class="label" style="width:145px;">Nama lengkap</td>
            <td style="width:5px">:</td>
            <td colspan="7"> '.$data['nama_lengkap'].'</td>
          </tr>
		  <tr>
            <td class="label">Tempat & tanggal lahir</td>
            <td>:</td>
            <td colspan="7"> '.$data['tempat_lahir'].', '.date('d-m-Y',strtotime($data['tanggal_lahir'])).'</td>
          </tr>
		  <tr>
            <td class="label">Jenis kelamin</td>
            <td>:</td>
            <td style="width:260px"> '.$kelamin[$data['kelamin']].'</td>
          </tr>
		  <tr>
            <td class="label">Tujuan sekolah formal</td>
            <td>:</td>
            <td> '.$data['tujuan'].'</td>
            
          </tr>
		  
		  <tr><td style="margin-bottom:5px;">&nbsp;</td></tr>

		</tbody>
	  </table>
	  <table>
		<tr>
          <td></td>
          <td style="text-align:left;"> Blitar, '.$tgl_ttd.'</td>
        </tr>
        <tr>
		  <td style="width:458px; vertical-align:bottom;" rowspan="2">
			
		  </td>
          <td> Calon Santri PPTA </td>
        </tr>
		<tr>
          <td style="vertical-align:top;padding-top:48px;">( '.$data['nama_lengkap'].' )</td>
        </tr>
		<tr><td style="margin-bottom:12px;">&nbsp;</td></tr>
		<tr>
            <td colspan="2" class="rekening-judul">
              Pembayaran daftar ulang
            </td>
        </tr>
		<tr>
			<td class="rekening-detail">
			  Bank '.$rekening[1]['cabang'].' a.n '.$rekening[1]['nama_rekening'].' No. rekening '.$rekening[1]['no_rekening'].' 
			</td>
            <td class="rekening-detail">
              Nominal <br/>
              Rp. '.$data['nominal'].'
            </td>
        </tr>
	  </table>
	</div>

	<style>
          @page {
            /* width = 21cm */
            size: 8.5in 12.99in;
            margin: 1cm 1cm 0.5cm;

          }
          @media print{
            table{
              width: 715px;
            }

          }
          .kertas.ganti-halaman{
            page-break-before: always;
          }
          table{
            width: 100%;
            border-spacing:0;
            border-collapse: collapse;
  		}
          table tr td{
            padding: 0;
            vertical-align: top;
            text-align: left;
  		  font-size:10pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  line-height: 1.6;
          }
          table tr th{
            padding: -11px 0;
            margin-bottom: 15px
            font-size: 12pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  font-weight:bold;
          }
          table.formulir tr td.label{
            font-weight: reguler;
          }
          table.formulir-slim tr td.label{
            width: 5px;
          }
          table tr td.has-child{
            padding: 0;
          }
          table tr td.has-child table tr td{
            padding: 2px 0;
          }
          table tr td.sub-label{
            width: 20px;
          }
          table tr td.label + td{
            width: 10px;
          }
          table.formulir{
            margin-bottom:47px;
          }
          h1{
            text-align: center;
            font-size: 12pt;
  		  font-family:Arial, Helvetica, sans-serif !important;
  		  font-weight:bold;
          }
  		h2{
            text-align: center;
            font-size: 12pt;
  		}
          table.paraf{

          }
          table.paraf tr td{
            text-align: center;
  		  width: 9cm;
          }
          .tanda-tangan td{
            height: 100px;
            vertical-align: bottom;
            text-align: center;
            padding: 0;
  		  font-family:"Times New Roman", Times, serif !important;
  		  font-size: 12pt;
          }
          .garis-bawah td{
            vertical-align: top;
            font-size: 11pt;
            padding: 0;
  		  width:50%;
  		  text-align:center;
          }
          .garis-bawah td .garis-ttd{
            display: inline-block;
            padding: 0 0;
            border-top: 1px solid #000;
            width: 150px;
  		  font-size: 9pt;
  		  font-style:italic;
          }
          .tgl-surat{
            text-align: right;
          }
          .materai{
            width: 45px;
            border:1px solid #454545;
            padding: 3px;
            font-size: 8pt;
            color: #454545;
            margin-left: -30px;
            margin-bottom: 5px;
          }

		  .bingkai{
            border:1px solid #a12d28;
            padding: 3px;
            font-size: 10pt;
            color: #454545;

          }
          .center{
            text-align: center;
          }
          .pernyataan{
            font-size: 12pt;
  		  font-style:italic;
  		  font-family:"Times New Roman", Times, serif !important;
          }
          .rekening-judul{
            padding: 0 5px;
            font-weight:bold;
            text-transform:uppercase;
            border:1px solid #000;
            background-color: #f1f1f1;

          }
          .rekening-detail{
            padding:0 5px;
            border:1px solid #000;

          }
          </style>

      </body>
    </html>
';

$dompdf->load_html($html);

$dompdf->set_paper('Legal','portrait');

$dompdf->render();
$nama_file=''.$data['nama_lengkap'].'.pdf';
$dompdf->stream($nama_file);

//echo $html;
function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}