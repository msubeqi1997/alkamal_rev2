<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
		
	$pos = new model_psb();
	$method=$_POST['method'];
	
	$var = array();
	if($_POST['first'] != ''){$var['t.payment_date >']=display_to_sql($_POST['first']);}
	if($_POST['last']  != ''){$var['t.payment_date <']=display_to_sql($_POST['last']);}
		
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$array = $pos->getFilterPayment($var);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'NO')
	->setCellValue('B1', 'NO PENDAFTARAN')
	->setCellValue('C1', 'NAMA SANTRI')
	->setCellValue('D1', 'TANGGAL BAYAR')
	->setCellValue('E1', 'JUMLAH BAYAR')
	->setCellValue('F1', 'PEGAWAI')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=2; 
	foreach($data as $row) {
	  $spreadsheet->setActiveSheetIndex(0)
	  ->setCellValue('A'.$i, $num)
	  ->setCellValue('B'.$i, $row['no_pendaftaran'])
	  ->setCellValue('C'.$i, $row['nama_lengkap'])
	  ->setCellValue('D'.$i, display_to_report($row['payment_date']))
	  ->setCellValue('E'.$i, $row['paid'])
	  ->setCellValue('F'.$i, $row['username'])
	  ;
	$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	