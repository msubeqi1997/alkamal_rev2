<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_psb.php");
include '../../plugins/excel/vendor/autoload.php';

		use PhpOffice\PhpSpreadsheet\Helper\Sample;
		use PhpOffice\PhpSpreadsheet\IOFactory;
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Style\Protection;
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
		
	$pos = new model_psb();
	$method=$_POST['method'];
	$sex = array('putra' => '1','putri' => '0');
	$arrjenjang = array('ula' => '2', 'wustho' => '3');
	$kelamin = array('1' => 'Putra','0' => 'Putri');
	$jk = array('1' => 'Laki-laki','0' => 'Perempuan');
	$jenjang = array('2' => 'Ula', '3' => 'Wustho');
	
	$daful = array('sudah'=>'Sudah','belum'=>'Belum');
	$seleksi = array('lolos'=>'Diterima','tolak'=>'Tidak diterima',''=>'Belum seleksi');
	$pendidikan = array(''=>'','1'=>'SD/Sederajat','2'=>'SMP/Sederajat','3'=>'SMA/Sederajat','4'=>'Diploma','5'=>'S1','6'=>'S2','7'=>'S3');
	
	$var = array();
	if($_POST['jalur'] != 'all'){
		$jalur= explode(' ',$_POST['jalur']);
		$var['jenis_kelamin']=$jalur[1];
		$var['jenjang']=$jalur[0];
	}
	if($_POST['status'] != 'all'){$var['seleksi']=$_POST['status'];}
	if($_POST['first'] != ''){$var['DATE(input_at) >']=display_to_sql($_POST['first']);}
	if($_POST['last']  != ''){$var['DATE(input_at) <']=display_to_sql($_POST['last']);}
 
	// Create new Spreadsheet object
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Al Kamal')
	->setLastModifiedBy('al kamal')
	->setTitle('Office 2007 XLSX ')
	->setSubject('Office 2007 XLSX Document')
	->setDescription('Generated using PHP classes.')
	->setKeywords('office 2007 openxml php')
	->setCategory('File');

	// Add some data
	$array = $pos->getFilterPendaftar($var);
	$data = $array[1];
	
	$spreadsheet->setActiveSheetIndex(0)
	->setCellValue('A1', 'JALUR PENDAFTARAN')
	->setCellValue('B1', 'NO PENDAFTARAN')
	->setCellValue('C1', 'JENJANG')
	->setCellValue('D1', 'MDK')
	->setCellValue('E1', 'SEKOLAH TUJUAN')
	->setCellValue('F1', 'NAMA LENGKAP')
	->setCellValue('G1', 'TEMPAT LAHIR')
	->setCellValue('H1', 'TANGGAL LAHIR')
	->setCellValue('I1', 'KELAMIN')
	->setCellValue('J1', 'ALAMAT')
	->setCellValue('K1', 'KABUPATEN')
	->setCellValue('L1', 'PROPINSI')
	->setCellValue('M1', 'JUMLAH SAUDARA')
	->setCellValue('N1', 'ANAK KE')
	->setCellValue('O1', 'RIWAYAT PENYAKIT')
	->setCellValue('P1', 'NO HP')
	->setCellValue('Q1', 'KODE POS')
	->setCellValue('R1', 'NAMA AYAH')
	->setCellValue('S1', 'PENDIDIKAN AYAH')
	->setCellValue('T1', 'PEKERJAAN AYAH')
	->setCellValue('U1', 'HP AYAH')
	->setCellValue('V1', 'NAMA IBU')
	->setCellValue('W1', 'PENDIDIKAN IBU')
	->setCellValue('X1', 'PEKERJAAN IBU')
	->setCellValue('Y1', 'HP IBU')
	->setCellValue('Z1', 'NAMA WALI')
	->setCellValue('AA1', 'PEKERJAAN WALI')
	->setCellValue('AB1', 'HP WALI')
	->setCellValue('AC1', 'ALAMAT WALI')
	->setCellValue('AD1', 'HUBUNGAN KELUARGA')
	->setCellValue('AE1', 'STATUS SELEKSI')
	->setCellValue('AF1', 'INPUT BY')
	->setCellValue('AG1', 'SELEKSI BY')
	;

	// Miscellaneous glyphs, UTF-8
	$num=1;
	$i=2; 
	foreach($data as $row) {
	  $admin = (!empty($row['admin']))?$row['admin']:' ';
	  $status_seleksi = $seleksi[$row['seleksi']];
	  $spreadsheet->setActiveSheetIndex(0)
	  ->setCellValue('A'.$i, $row['jalur'])
	  ->setCellValue('B'.$i, $row['no_pendaftaran'])
	  ->setCellValue('C'.$i, $row['jenjang'].' '.$row['jenis_kelamin'])
	  ->setCellValue('D'.$i, $row['jenis'])
	  ->setCellValue('E'.$i, $row['sekolah_tujuan'])
	  ->setCellValue('F'.$i, $row['nama_lengkap'])
	  ->setCellValue('G'.$i, $row['tempat_lahir'])
	  ->setCellValue('H'.$i, display_to_report($row['tanggal_lahir']))
	  ->setCellValue('I'.$i, $jk[$row['kelamin']])
	  ->setCellValue('J'.$i, $row['alamat'])
	  ->setCellValue('K'.$i, $row['kabupaten_name'])
	  ->setCellValue('L'.$i, $row['propinsi_name'])
	  ->setCellValue('M'.$i, $row['saudara'])
	  ->setCellValue('N'.$i, $row['anak_ke'])
	  ->setCellValue('O'.$i, $row['abk'])
	  ->setCellValue('P'.$i, $row['no_hp'])
	  ->setCellValue('Q'.$i, $row['kode_pos'])
	  ->setCellValue('R'.$i, $row['ayah'])
	  ->setCellValue('S'.$i, $pendidikan[$row['pendidikan_ayah']])
	  ->setCellValue('T'.$i, $row['pekerjaan_ayah'])
	  ->setCellValue('U'.$i, $row['hp_ayah'])
	  ->setCellValue('V'.$i, $row['ibu'])
	  ->setCellValue('W'.$i, $pendidikan[$row['pendidikan_ibu']])
	  ->setCellValue('X'.$i, $row['pekerjaan_ibu'])
	  ->setCellValue('Y'.$i, $row['hp_ibu'])
	  ->setCellValue('Z'.$i, $row['wali'])
	  ->setCellValue('AA'.$i, $row['pekerjaan_wali'])
	  ->setCellValue('AB'.$i, $row['telp_wali'])
	  ->setCellValue('AC'.$i, $row['alamat_wali'])
	  ->setCellValue('AD'.$i, $row['hubungan_wali'])
	  ->setCellValue('AE'.$i, $status_seleksi)
	  ->setCellValue('AF'.$i, $admin)
	  ->setCellValue('AG'.$i, $row['selektor']);
	$i++; $num++;
	}
		
	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$spreadsheet->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
	ob_end_clean();
	$writer->save('php://output');
	exit;
	