<?php 
$titlepage="Pendaftaran Manual";
$idsmenu=67;
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php"; 
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_psb();
$array = $pos->getJalurPendaftaranByDate();
$jalur = $array[1];

$arr_pddk = $pos->getTingkatPendidikan();
$pendidikan = $arr_pddk[1];

$propinsi = $pos->getPropinsi();
?>

<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Pendaftaran Santri Baru</h3>
		</div>
		<!--./ box header-->
		<div class="box-body form-horizontal">
			<?php if(!empty($jalur)){ ?>
			<form method="post" id="target" action="c_pendaftaran.php" enctype="multipart/form-data">
			  <div class="row">
				<div class="col-md-8">
					<div class="form-group"> <label class="col-sm-3  control-label-l">Jalur pendaftaran</label>
						<div class="col-sm-7">
							<select class="form-control" id="jalur_daftar" name="txtjalur" required>
								<option value=""> Pilih jalur </option>
								<?php
								foreach ($jalur as $opt) {
								echo "<option value='".$opt['jalur_id']."'> ".$opt['jalur']." </option>";
								}
								?>
							</select>
							<input type="hidden" id="pendaftaran" name="pendaftaran" value="save_item">
							<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
							<input type="hidden" id="id_siswa" name="id_siswa" class="" value="">
							<input type="hidden" name="method" class="" value="save_item">
								
						</div>
					</div>
					<div class="form-group"> <label class="col-sm-3  control-label"> Apakah MDK? </label>
						<div class="col-sm-9" id="tipe"> <cite>Pilih jalur terlebih dahulu.</cite></div>
					</div>
					<div class="form-group"> <label class="col-sm-3  control-label"> Sekolah tujuan <span class="text-danger">*</span></label>
					  <div class="col-sm-9" id="tujuan"> 
						<cite>Pilih jalur terlebih dahulu.</cite>
					  </div>
					</div>
					<div class="form-group"> <label class="col-sm-3  control-label"> Jenjang pendidikan</label>
						<div class="col-sm-9" id="jenjang"> </div>
					</div>
					<div class="form-group"> <label class="col-sm-3  control-label">No Induk Santri<span class="text-danger">*</span></label>
								<div class="col-sm-9"><input type="text" class="form-control " id="no_induk" name="no_induk" value="" placeholder="Diisi jika alumni pesantren (MDK)"> </div>
							</div>
					<div class="form-group"> <label class="col-sm-3  control-label">Nama lengkap<span class="text-danger">*</span></label>
						<div class="col-sm-9"><input type="text" class="form-control " id="nama_lengkap" name="nama_lengkap" value="" placeholder="Isikan nama lengkap" required=""> </div>
					</div>
					<div class="form-group">
					  <label for="j-kelamin" class="control-label-l col-sm-3">Jenis kelamin<span class="text-danger"> *</span></label>
					  <div class="col-sm-9">
						<div class="radio">
						  <label><input type="radio" id="kelamin" name="kelamin" value="1" required> Laki-laki</label>
						  <label><input type="radio" id="kelamin" name="kelamin" value="0" required> Perempuan</label>
						</div>
					  </div>
					</div>
					<div class="form-group">
					  <label for="tempatlahir" class="control-label-l col-sm-3">Tempat & Tanggal lahir <span class="text-danger"> *</span></label>
					  <div class="col-sm-5">
						<input type="text" class="form-control letters" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" required>
					  </div>
					  <div class="col-sm-4">
						<div class="input-group date" id="datepicker">
						  <input type="text" class="form-control txtperiode tgl" name="tanggal_lahir" id="tanggal_lahir" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
						  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					  </div>
					</div>
					<div class="form-group">
					  <label for="no_hp" class="control-label-l col-sm-3">Handphone yang aktif</label>
					  <div class="col-sm-9">
						<input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Isikan nomor handphone" onkeypress="return isNumber(event)">
					  </div>
					</div>
					
				</div><!-- /.col -->
				
			  </div><!-- /.row -->
			  
			  <div class="box-footer">
				<button type="submit" id="btnsave" class="btn btn-info pull-right"><i class="fa fa-print"></i> Simpan & Cetak Formulir </button>
				<span id="infoproses"></span>
			  </div><!-- /.box-footer -->
			</form> 
			<?php } else {?>
			
			  <h3>
				Maaf, belum ada jadwal pendaftaran.
			  </h3>
				
		   <?php }?>
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
	  $(function () {
		$('#tanggal_lahir').datepicker({
				format: 'dd-mm-yyyy',
			});
			
		//Datemask dd/mm/yyyy
		$("#tanggal_lahir").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		
		$('#jalur_daftar').on('change', function() {
			$('#tipe').html('');
			$('#tujuan').html('');
			$('#list-kel').html('');
			if(this.value == '' || this.value == null){$("#form-kelengkapan").hide();}	else{$("#form-kelengkapan").show();}
			var id_item = this.value;
			var value = {
				id_item: id_item,
				method : "get_form"
			};
			$.ajax(
			{
				url : "../pendaftaran/formulir-online_process.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					$('#list-kel').append(hasil.kelengkapan);
					
					if(id_item == '' || id_item == null){
						$('#tipe, #tujuan').append('<cite>Pilih jalur terlebih dahulu.</cite>'); 
					}else{ 
						$('#tipe').append(hasil.jenis);
						$('#tujuan').append(hasil.tujuan);
						if(hasil.mdk =='ya'){
							newitem();
							$("#no_induk").show();
							$("#no_induk").val('');
							$("#inputcrud").val('E');
							$("#jenis").val('mdk');
							$("#nama_lengkap").val('');
							$("#nama_lengkap").prop("readonly", true);
						}else{
							$("#no_induk").hide();
							newitem();
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
			get_jenjang();
		});
		
		$("div#tujuan").on("change", "select[name='sekolah']",function() {
			get_jenjang();
		});
		
		$("input[name='kelamin']").on("change", function() {
			get_jenjang();
		});
		
		$("#no_induk").hide();
		
		//searching nama by no induk
		$(document).on("blur","#no_induk",function(){
			$("#nama_lengkap").val('');
			var nomor = $(this).val();
			var value = {
				nomor:nomor,
				method : "get_name"
			};
			$.ajax(
			{
				url : "../pendaftaran/formulir-online_process.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.status == 'exist'){
						$("#nama_lengkap").val(data.nama);
						$("#kelamin").prop("readonly", true);
						$("#tempat_lahir").prop("readonly", true);
						$("#tanggal_lahir").prop("readonly", true);
						$("#no_hp").prop("readonly", true);
						$("#tempat_lahir").val(data.tempat_lahir);
						$("input[name='kelamin'][value='"+data.kelamin+"']").prop('checked', true);
						$("input[name='kelamin']").attr('disabled', true);
						$("#tanggal_lahir").val(data.tgl);
						$("#no_hp").val(data.no_hp);
						$("#id_siswa").val(data.id_siswa);
						get_jenjang();
					}else{
						bootbox.alert('No induk tidak terdaftar');
						$("#no_induk").val('');
						$("#nama_lengkap").val('');
						$("input[name='kelamin']").prop('checked', false);
						$("#tempat_lahir").val('');
						$("#tanggal_lahir").val('');
						$("#no_hp").val('');
						$("#id_siswa").val('');
						
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
	
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  $("#btnsave").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#infoproses").html("");
			  }else{
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	  });
	  
	  $(document).ready(function(){
		$(".letters").keypress(function(event){
			var inputValue = event.which;
            // allow letters and whitespaces only.
            if(!(inputValue >= 65 && inputValue <= 123) && (inputValue != 32 && inputValue != 0) && inputValue != 8 ) { 
                event.preventDefault(); 
            }
            console.log(inputValue);
		});
	});

	function get_jenjang(){
		$('#jenjang').html('');
			
		var kelamin = $("input[name='kelamin']:checked").val();
		var sekolah = $("select[name='sekolah']").find('option:selected').val();
		var value = {
			kelamin: kelamin,
			sekolah: sekolah,
			method : "get_jenjang"
		};
		$.ajax(
		{
			url : "../pendaftaran/formulir-online_process.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				var hasil = jQuery.parseJSON(data);
				$('#jenjang').append(hasil.jenjang);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	function newitem(){
		$("#nama_lengkap").prop("readonly", false);
		$("#kelamin").prop("readonly", false);
		$("#tempat_lahir").prop("readonly", false);
		$("#tanggal_lahir").prop("readonly", false);
		$("#no_hp").prop("readonly", false);
		
		$("input[name='kelamin']").prop('checked', false);
		$("#inputcrud").val('N');
		$("#jenis").val('tidak');
		$("#nama_lengkap").val('');
		$("#tempat_lahir").val('');
		$("#tanggal_lahir").val('');
		$("#no_hp").val('');
		$("#id_siswa").val('');
	}
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		/*window.open('formulir-do-pdf.php?token='+respon.token+'&nomor='+respon.no_pendaftaran, '_blank');
		bootbox.alert("Berhasil. Data pendaftaran anda telah disimpan.");
		setTimeout(function(){
		  location.href='v_formulir-manual.php';
		},0);*/
		bootbox.alert({size: "small",
						title: "Nomor pendaftaran",
						message:"<p>Nomor pendaftaran anda </p><p><h2>"+respon.no_pendaftaran+"</h2></p>",
						callback: function(result){location.href='v_formulir-manual.php';}
					});
	  }
	  else{
		bootbox.alert(msg);
	  }
	}
	
	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	</script>

</body>
</html>
