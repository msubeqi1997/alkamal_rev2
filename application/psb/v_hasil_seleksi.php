<?php 
$titlepage="Nilai Seleksi Pendaftar";
$idsmenu=1; 
include "../../library/config.php";
require_once("../model/dbconn.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
  <div class="content-wrapper">
  <div class="container">
    <section class="content-header">
	  <h1>
		Nilai Seleksi Calon Siswa
	  </h1>
	  
	</section>
	<section class="content">
	  <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Upload nilai</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
			<form method="post" id="target" action="c_hasil_seleksi.php" enctype="multipart/form-data">
            <div class="box-body">
              <div class="row">
			    
                <input type="hidden" name="method" value="save">
				<div class="col-md-6">
                  <div class="form-group">
                    <label>Import nilai</label>
                    <input type="file" name="photo_siswa" id="upload_siswa" required />
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
			  <div class="pull-right">
			  	<button type="submit" id="btnimport" data-btn="import" class="btn btn-success"><i class="fa fa-download"></i> Import </button>
				<button type="button" id="btnsearch" data-btn="search" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				<span id="infoproses"></span>
			  </div>
            </div>
			</form>
          </div><!-- /.box -->
		  
		  <div class="box box-info">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Peserta pendaftar</h3>
				</div> <!-- end of box-header -->
				<div class="box-body table-responsive">
					<table id="table_item" class="table  table-bordered table-hover ">
					  <thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th width="15%">No Pendaftaran </th>
							<th>Jalur Pendaftaran </th>
							<th >Nama Lengkap </th>
							<th width="30%">Alamat </th>
							<th>Sekolah Asal </th>
							<th>Nilai </th>
						</tr>
					  </thead>
					  <tbody></tbody>
					</table>
				</div> <!-- end of box-body -->
			</div>
		</div><!-- end of div box info -->
	</section><!-- /.content -->

  </div>
  </div>
    <?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
	
	function tabel(){
		var value = {
			method : "data"
		};
		$('#table_item').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax": {
				"url": "c_hasil_seleksi.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "no_pendaftaran" },
			{ "data": "jalur_name" },
			{ "data": "nama_lengkap" },
			{ "data": "alamat" },
			{ "data": "sekolah_asal" },
			{ "data": "nilai" },
			]
		});
		$("#table_item_filter").addClass("pull-right");
	}
	
	$(document).on( "click","#btnsearch", function() {
				
			$("#table_item").DataTable().destroy();
			tabel();
		
	});
	
	$(function () {
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  $("#btnimport").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#btnimport").prop('disabled', false);
				$("#infoproses").html("");
			  }else{
				$("#btnimport").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.status == 'OK') {
		window.open('formulir-do-pdf.php?token='+respon.token, '_blank');
		bootbox.alert("Berhasil. Data pendaftaran anda telah disimpan.");
		setTimeout(function(){
		  location.href='terima-kasih.php';
		},0);
	  }
	  else if(respon.status == 'EXIST'){
		bootbox.alert('Gagal. Email yang anda gunakan sudah terdaftar. Harap gunakan email pribadi anda.');
	  }
	  else{
		bootbox.alert(msg);
	  }
	};
	</script>
</body>
</html>
