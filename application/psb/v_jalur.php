<?php 
$titlepage="Jalur Pendaftaran";
$idsmenu=9;
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php"; 
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_psb();
$array = $pos->getKelengkapan();
$kelengkapan = $array[1];
$arr_kewajiban = $pos->getJenisKewajiban();
$kewajiban = $arr_kewajiban[1];
?>

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content">
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Jalur Pendaftaran</h3>
    </div><!--./ box header-->
    <div class="box-body">
      <button type="submit" class="btn btn-primary " id="btnadd" name="btnadd"><i class="fa fa-plus"></i> Tambah Jalur</button>
      <br>
      <div class="box-body table-responsive no-padding">
        <table id="table_item" class="table  table-bordered table-hover ">
          <thead>
            <tr class="tableheader">
              <th style="width:30px">#</th>
              <th style="width:150px">Jalur</th>
              <th>Tapel</th>
              <th>MDK</th>
              <th>Tgl mulai</th>
              <th>Tgl sampai</th>
              <th>Kelengkapan</th>
              <th>Kewajiban</th>
              <th style="width:38px">Aktif</th>
              <th style="width:30px">Edit</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div><!-- /.box -->
</section><!-- /.content -->
	
	<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Tambah Jalur Pendaftaran</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Jalur pendaftaran</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="ex: reguler">
								<input type="hidden" id="kelengkapan" name="" class="" value="">
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Jalur Seleksi</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optmdk" name="optmdk" >
									<option value=""> Pilih seleksi </option>
									<option value="tidak"> Bukan MDK </option>
									<option value="mdk"> MDK </option>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kelengkapan</label>
							<div class="col-sm-9">
							  <div class="row" >
								<?php
								$num=1;
								$menuleft='';
								$menuright='';
								foreach ($kelengkapan as $key) {
								  if($num==1)
								  {
									$menuleft .= '<li class="list-group-item">
									  <div class="input-group">
										<input type="checkbox"  id="check-'.$key["kelengkapan_id"].'" class="chkbox" value="'.$key['kelengkapan_id'].'" > <strong>'.$key['kelengkapan'].'</strong>
									  </div>
									</li>'; 
								  }else{
									$menuright .= '<li class="list-group-item">
									  <div class="input-group">
										<input type="checkbox"  id="check-'.$key["kelengkapan_id"].'" class="chkbox" value="'.$key['kelengkapan_id'].'" > <strong>'.$key['kelengkapan'].'</strong>
									  </div>
									</li>'; 
									$num=0;
								  }
								  $num++;
								}
								?>
								
								<div class="col-xs-6" style="padding-left:13px">
								  <ul class="list-group">
									<?php echo $menuleft; ?>
								  </ul>
								</div>
								<div class="col-xs-6" style="padding-left:0px">
								  <ul class="list-group">
									<?php echo $menuright; ?>
								  </ul>
								</div>
							  </div>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Kewajiban</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optplot" name="optplot" >
									<option value=""> Pilih plotting </option>
									<?php
									foreach ($kewajiban as $key => $opt) {
									echo "<option value='".$key."'> ".$opt." </option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Tanggal</label>
							<div class="col-sm-9">
								<input readonly="" type="text" class="form-control txtperiode tgl" id="txtfirstperiod"  value=""  style="width:100px "> -
								<input readonly="" type="text" class="form-control txtperiode tgl" id="txtlastperiod"  value=""  style="width:100px ">
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Aktif</label>
							<div class="col-sm-9">
								<div class="radio">
									<label>
									  <input type="radio" name="aktif" id="aktif" value="1">
									  Aktif
									</label>
								</div>
								<div class="radio">
									<label>
									  <input type="radio" name="aktif" id="pasif" value="0">
									  Pasif
									</label>
								</div>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
		</div>
			<!--modal-content-->
	</div>
		<!--modal-dialog modal-lg-->
	</div>
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>

<script language="javascript">
		$(document).ready( function () 
		{
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_jalur.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "jalur" },
				{ "data": "tapel" },
				{ "data": "mdk" },
				{ "data": "first" },
				{ "data": "last" },
				{ "data": "kelengkapan" },
				{ "data": "kewajiban" },
				{ "data": "active" },
				{ "data": "button" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
			
			$('#txtfirstperiod,#txtlastperiod').datepicker({
				format: 'dd-mm-yyyy',
			});
		});
		
		$(document).on( "click","#btnadd", function() {
			$("#modalmasteritem").modal('show');
			newitem();
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#optmdk").val("");
			$("#optplot").val("");
			$("#txtfirstperiod").val("");
			$("#txtlastperiod").val("");
			$('.chkbox').prop('checked', false);
			$("input[name='aktif'][value='1']").prop('checked', true);
			set_focus("#txtname");
		}
		
		$(document).on("click",".chkbox",function(){
		  get_check_value();
		});
		
		function get_check_value(){
		  var values = [];
		  $('.chkbox:checked').each(function() {
			values.push($(this).val());
		  });
		  $('#kelengkapan').val(values.join(','));
		}
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var kelengkapan = $("#kelengkapan").val();
			var mdk = $("#optmdk").val(); 
			var plot = $("#optplot").val(); 
			var first = $("#txtfirstperiod").val(); 
			var last = $("#txtlastperiod").val(); 
			var aktif = document.querySelector('input[name="aktif"]:checked').value;
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Nama jalur kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			if(mdk == '' || mdk== null ){
				$.notify({
					message: "Jalur seleksi belum dipilih!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#optmdk").focus();
				return;
			}			
			if(first == '' || last == '' ){
				$.notify({
					message: "Tanggal pendaftaran kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtfirstperiod").focus();
				return;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				kelengkapan: kelengkapan,
				mdk:mdk,
				plot: plot,
				first: first,
				last:last,
				aktif: aktif,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_jalur.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.ceksat == 0){
						$.notify(data.error);
					}else{
						if(data.crud == 'N'){
							if(data.result == 1){
								$.notify('Proses simpan berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								newitem();				
							}else{
								$.notify({
									message: "Proses simpan gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});
								set_focus("#txtname");
							}
						}else if(data.crud == 'E'){
							if(data.result == 1){
								$.notify('Proses update berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								$("#modalmasteritem").modal("hide");
								newitem();	
							}else{
								$.notify({
									message: "Proses update gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});					
								set_focus("#txtname");
							}
						}else{
							$.notify({
								message: "Invalid request"
							},{
								type: 'danger',
								delay: 8000,
							});	
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on( "click",".btnedit", function() {
			newitem();
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "get_detail_item"
			};
			$.ajax(
			{
				url : "c_jalur.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					var plot = data.kelengkapan_id;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.jalur_id);
					$("#txtname").val($.trim(data.jalur));
					$("#txtfirstperiod").val(hasil.first); 
					$("#txtlastperiod").val(hasil.last); 
					var res = plot.split(",");
					for (i = 0; i < res.length; i++) { 
						$("#check-"+res[i]).prop('checked', true);
					}
					$("#optmdk option[value='"+data.jenis+"']").prop('selected', true);
					$("#optplot option[value='"+data.kewajiban_id+"']").prop('selected', true);
					$("input[name='aktif'][value='"+data.aktif+"']").prop('checked', true);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
					get_check_value();
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btnactive", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "activate"
			};
			$.ajax(
			{
				url : "c_jalur.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil.result == true){
						$.notify('Proses aktifasi berhasil');
						var table = $('#table_item').DataTable(); 
						table.ajax.reload( null, false );
					}else{
						$.notify({
							message: "Proses aktifasi gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
	</script>
</body>
</html>
