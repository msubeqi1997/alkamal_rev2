<?php 
$titlepage="Kewajiban Calon Siswa";
$idsmenu=8; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_psb();
$array = $pos->getJenisKewajiban();
$jenis = $array[1];

?>

<section class="content">
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Master Kewajiban Calon Siswa</h3>
		</div>
		<!--./ box header-->
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary " id="btnadd" name=""><i class="fa fa-plus"></i> Tambah Kewajiban</button>
					<br>
				</div>
			</div>
			<div class="box-body table-responsive no-padding" style="max-width:1124px;">
				<table id="table_item" class="table  table-bordered table-hover ">
					<thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th>Kewajiban </th>
							<th>Tapel </th>
							<th>Plotting </th>
							<th>Nominal </th>
							<th style="width:240px">Aktif</th>
							<th style="width:120px">Edit</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>		
		</div>
	</div><!-- /.box -->

</section><!-- /.content -->

<div id="modalmasteritem" class="modal fade ">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Tambah Kewajiban</h4>
			</div>
			<!--modal header-->
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-group"> <label class="col-sm-3  control-label">Kewajiban</label>
							<div class="col-sm-9">
								<input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
								<input type="hidden" id="txtiditem" name="txtiditem" class="">
								<input type="text" class="form-control " id="txtname" name="txtname" value="" placeholder="ex: uang pangkal"> 
							</div>
						</div>
						<div class="form-group new"> <label class="col-sm-3  control-label">Plotting</label>
							<div class="col-sm-9">
								<select class="form-control autocomplete" id="optplot" name="optplot" >
									<option value=""> Pilih plotting </option>
									<?php
									foreach ($jenis as $key => $opt) {
									echo "<option value='".$key."'> ".$opt." </option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Nominal</label>
							<div class="col-sm-9">
								<input type="text" class="form-control money" id="txtnominal" name="txtnominal" value="" placeholder="ex: 1000000"> 
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label">Aktif</label>
							<div class="col-sm-9">
								<div class="radio">
									<label>
									  <input type="radio" name="aktif" id="aktif" value="1" checked>
									  Aktif
									</label>
								</div>
								<div class="radio">
									<label>
									  <input type="radio" name="aktif" id="pasif" value="0">
									  Pasif
									</label>
								</div>
							</div>
						</div>
						<div class="form-group"> <label class="col-sm-3  control-label"></label>
							<div class="col-sm-9"><button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name=""><i class="fa fa-save"></i> Simpan</button> <span id="infoproses"></span> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
				<!--modal footer-->
			</div>
			<!--modal-content-->
		</div>
		<!--modal-dialog modal-lg-->
	</div>


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			money();
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"pageLength": 50,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_kewajiban.php",
					"type": "POST",
					"data":value,
				},
				"columns": [
				{ "data": "urutan" },
				{ "data": "kewajiban" },
				{ "data": "tapel" },
				{ "data": "plot" },
				{ "data": "nominal" },
				{ "data": "active" },
				{ "data": "button" },
				]
			});
			$("#table_item_filter").addClass("pull-right");
		});
		
		$(document).on( "click","#btnadd", function() {
			$("#modalmasteritem").modal('show');
			newitem();
		});
		
		function newitem()
		{
			$("#txtiditem").val("");
			$("#inputcrud").val("N");
			$("#txtname").val("");
			$("#optplot").val("");
			$("#txtnominal").val("");
			$("input[name='aktif'][value='1']").prop('checked', true);
			set_focus("#txtname");
			
		}
		
		$(document).on( "click","#btnsaveitem", function() {
			var id_item = $("#txtiditem").val();
			var item_name = $("#txtname").val();
			var plot = $("#optplot").val(); 
			var nominal = parseInt(cleanString($("#txtnominal").val()));
			var aktif = document.querySelector('input[name="aktif"]:checked').value;
			var crud=$("#inputcrud").val();
			if(item_name == '' || item_name== null ){
				$.notify({
					message: "Nama kewajiban kosong!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				$("#txtname").focus();
				return;
			}
			if(isNaN(nominal))
			{
				nominal = 0;
			}
			
			var value = {
				id_item: id_item,
				item_name: item_name,
				plot:plot,
				nominal:nominal,
				aktif: aktif,
				crud: crud,
				method : "save_item"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_kewajiban.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnsaveitem").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					if(data.ceksat == 0){
						$.notify(data.error);
					}else{
						if(data.crud == 'N'){
							if(data.result == 1){
								$.notify('Proses simpan berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								newitem();				
							}else{
								$.notify({
									message: "Proses simpan gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});
								set_focus("#txtname");
							}
						}else if(data.crud == 'E'){
							if(data.result == 1){
								$.notify('Proses update berhasil');
								var table = $('#table_item').DataTable(); 
								table.ajax.reload( null, false );
								$("#modalmasteritem").modal("hide");
							}else{
								$.notify({
									message: "Proses update gagal, error :"+data.error
								},{
									type: 'danger',
									delay: 8000,
								});					
								set_focus("#txtname");
							}
						}else{
							$.notify({
								message: "Invalid request"
							},{
								type: 'danger',
								delay: 8000,
							});	
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnsaveitem").prop('disabled', false);
				}
			});
		});
		
		$(document).on( "click",".btnedit", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "get_detail_item"
			};
			$.ajax(
			{
				url : "c_kewajiban.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#inputcrud").val("E");
					$("#txtiditem").val(data.id);
					$("#txtname").val($.trim(data.kewajiban));
					$("#optplot option[value='"+data.jenis_kewajiban_id+"']").prop('selected', true);
					$("#txtnominal").val(addCommas(Math.round(data.nominal)));
					$("input[name='aktif'][value='"+data.aktif+"']").prop('checked', true);
					$("#modalmasteritem").modal('show');
					set_focus("#txtname");
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
		
		$(document).on( "click",".btnactive", function() {
			var id_item = $(this).attr("id_item");
			var value = {
				id_item: id_item,
				method : "activate"
			};
			$.ajax(
			{
				url : "c_kewajiban.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					if(hasil.result == true){
						$.notify('Proses aktifasi berhasil');
						var table = $('#table_item').DataTable(); 
						table.ajax.reload( null, false );
					}else{
						$.notify({
							message: "Proses aktifasi gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
	</script>
</body>
</html>
