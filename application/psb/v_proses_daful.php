<?php 
$titlepage="Daftar Ulang";
$idsmenu=13; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

 
?>
  <div class="content">
    <section class="content-header">
	  <h1>
		Daftar ulang calon santri
	  </h1>
	  
	</section>
	<section class="content">
	<div class="row">
    
	<div class="col-md-12">
	  <div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">Pencarian santri</h3>
					</div> <!-- end of box-header -->

					<div class="row">
						<div class=" col-xs-12" style="padding-left:25px;padding-right:25px">
							<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nomor pendaftaran...">
						</div>
					</div>				
					<div class="box-body">
							
					</div> <!-- end of box-body -->
				</div>
			</div><!-- end of div box info -->
		</div><!-- end of col-md-12 -->
	  </div>
	  <form method="post" id="target" action="c_proses_daful.php">
		<div class="row">
			<div class="col-md-12">
				<!-- Identitas siswa -->
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/student.png" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Identitas santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
				  
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-9">
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nomor pendaftaran</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtnopendaftaran"></span>
									<input type="hidden" name="txttoken" id="txttoken" value="">
									<input type="hidden" name="txtpendaftaran" id="txtpendaftaran" value="">
									<input type="hidden" name="method" id="method" value="save_item">
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Sekolah tujuan</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txttujuan"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Jenjang Pendidikan</b>
								</div>
								<div class="col-sm-8">
									<input type="hidden" name="valjenjang" id="valjenjang">
									<b>: </b> <span id="txtjenjang"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-3">
									<b>Nama lengkap</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtnama"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<b>Tempat & Tanggal lahir</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtttl"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<b>Alamat tempat tinggal</b><small> (sesuai KK)</small>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtalamat"></span>
								</div>
							</div>
						</div><!-- /.col -->
						<div class="col-md-3">
							<div class="hero-widget box-profile" style="margin:0 0 10px 0">
								<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_siswa" alt="foto santri">
							</div><!-- /.box -->
							
							<div class="hero-widget box-profile">
								<img class="profile-user-img img-responsive" width="120px" height="150px" id="preview_wali" alt="foto wali">
							</div><!-- /.box -->
						</div>
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				</div><!-- /identitas siswa-->
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/berkas.png" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Berkas kelengkapan santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-12" id="form-kelengkapan">
						  
						</div>
					  </div>
					</div>
				</div>
				
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/idr.jpg" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Kewajiban santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding" >
					  <table class="table table-striped" id="income">
						<thead>
							<tr>
								<th>No</th>
								<th>Kewajiban</th>
								<th>Nominal</th>
								<th>Terbayar</th>
								<th>Jumlah Bayar</th>
								<th><input type="checkbox" id="selectall_from" onClick="selectAll(this,'seleksi')"/></th>
							</tr>
						</thead>
						<tbody id="form-riwayat">
							
						</tbody>
					  </table>
					</div>
				</div>
			</div>
		</div>
		<div class="box box-widget">
		  <div class="box-footer">
			<button type="submit" id="btnsave" data-btn="lolos" class="btn btn-primary pull-right"><i class="fa fa-mail-forward"></i> Proses</button>&nbsp;
			
			<button type="button" id="btndelete" data-btn="batal" class="btn btn-danger padding"><i class="fa fa-mail-reply"></i> Pembatalan</button>
		    
			<span id="infoproses"></span>
		  </div><!-- /.box-footer -->
		</div><!-- /.box -->
		</form>
	</div>
	</div>
	</section><!-- /.content -->
	
  </div>
  
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
	function bigimage(id,url){
		//var x = document.getElementsById().getAttribute("class");
		$('#'+id).attr('src', url);
	}
		
	var kelamin = {'0': 'Perempuan', '1': 'Laki-laki'};
	var jk = {'0': 'Putri', '1': 'Putra'};
	var jenjang = {'2':'Ula','3':'Wustho'};
	var pendidikan = {'':'','1':'SD/Sederajat','2':'SMP/Sederajat','3':'SMA/Sederajat','4':'Diploma','5':'S1','6':'S2','7':'S3'};
 
	$(document).ready( function () 
	{
		
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_search_daful.php", { term: $('#txtsearchitem').val() }, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dt>"+item.nomor + " - "+item.nama+"</dt>"+jenjang[item.tingkat]+" "+jk[item.kelamin]+""  )
			.appendTo( ul );
		};
	});
	
	function temptabel(id){
		var value = {
			siswa_id: id,
			method : "select_siswa_pendaftar"
		};
		$("#btnsave").prop('disabled', false);
		$.ajax(
		{
			url : "c_proses_daful.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var hasil = jQuery.parseJSON(data);
				var data = hasil.data;
				var aktif = hasil.aktif;
				if(aktif == 'aktif'){
					$("#btnsave").prop('disabled', true);
				}
				$('#txttoken').val(data.uuid);				
				$('#txtpendaftaran').val(data.kode_pendaftaran);				
				$('#txttujuan').html(data.tujuan);			
				$('#txtjenjang').html(""+jenjang[data.jenjang_pendidikan]+" "+jk[data.kelamin]+"");			
				$('#txtnopendaftaran').html(data.no_pendaftaran);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtttl').html(''+data.tempat_lahir+', '+sql_to_report(data.tanggal_lahir)+'');						
				$('#txtalamat').html(data.alamat);
				
				$('#valjenjang').html(data.jenjang_pendidikan);
				
				$('#form-kelengkapan').html(hasil.kelengkapan);
				$('#form-riwayat').html(hasil.riwayat);
				var urls = '../../files/images_pendaftar/'+data.photo_siswa;
				var urlw = '../../files/images_pendaftar/'+data.photo_wali;
				bigimage('preview_siswa',urls);
				bigimage('preview_wali',urlw);
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	
	$(function () {
		
		var options = {
			success: suksesDialog
		  }
		$("#target").submit(function( event ) {
		  var token = $("#txttoken").val();
		  
		  if(token == '' || token== null){
			$.notify({
				message: "Siswa belum dipilih"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsearchitem");
			return false;
		  }
		  $("#btnsave").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  bootbox.confirm("Apakah anda yakin semua data telah sesuai dan lanjut daftar ulang?",
			function (result) {
			  if (result == true) {
				$('#target').ajaxSubmit(options);
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
				
			  }else{
				$("#btnsave").prop('disabled', false);
				$("#infoproses").html("");
			  }
			});
			return false;
		});
	});
	
	function suksesDialog(msg){
	  var respon = $.parseJSON(msg);
	  if (respon.result == '1') {
		bootbox.alert("Berhasil. Daftar ulang telah berhasil.");
		newitem();
	  }
	  else{
		bootbox.alert(msg);
	  }
	};
	
	$(document).on( "click","#btndelete", function() {
		var token = $("#txttoken").val();
		var kode = $("#txtpendaftaran").val();
		var opt = $(this).attr('data-btn');
		
		if(token == '' || token== null){
			$.notify({
				message: "Santri belum dipilih"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsearchitem");
			return;
		}
		swal({   
		title: "Pembatalan daftar ulang",   
		text: "Apakah anda yakin akan menghapus daftar ulang?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Lanjut",   
		closeOnConfirm: true }, 
		function(){ 
		  var value = {
			token: token,
			kode: kode,
			opt: opt,
			method : "pembatalan"
		  };
		  $("#btnsave").prop('disabled', true);
		  $("#btndelete").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_proses_daful.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsave").prop('disabled', false);
				$("#btndelete").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Pembatalan berhasil');
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Pembatalan gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsave, #btndelete").prop('disabled', false);
			}
		  });
		});
	});
	
	function newitem(){
		$('#txttoken').val('');				
		$('#txtpendaftaran').val('');				
		$('#txttujuan').html('');			
		$('#txtjenjang').html('');			
		$('#txtnopendaftaran').html('');				
		$('#txtnama').html('');
		$('#txtttl').html('');						
		$('#txtalamat').html('');
		$('#form-kelengkapan').html('');
		$('#form-riwayat').html('');
		$('#selectall_from').prop('checked', false);
		$('#preview_siswa').attr('src', '');
		$('#preview_wali').attr('src', '');
	}
	
	function sql_to_report(tgl){
		var thn = tgl.substr(0, 4);
		var bln = tgl.substr(5, 2);
		var dy = tgl.substr(8, 2);
		var date = dy+'-'+bln+'-'+thn;
		return date;
	}
	
	function selectAll(source) {
		checkboxes = document.getElementsByName('seleksi_from[]');
		for(var i in checkboxes)
		checkboxes[i].checked = source.checked;
		get_check_value();
	}
	
	function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	  }
	  return true;
	}
	
	function get_check_value(){
	  var values = 0;
	  $('.chkbox:checked').each(function() {
		var id= $(this).val();
		values += parseInt($("#bayar"+id).val());
	  });
	  
	  $('#txttotal').html("Rp. "+addCommas(values));
	}
	
	$(document).on("blur",".nombayar", function() {
		get_check_value();
	});
	$(document).on("click",".chkbox",function(){
	  get_check_value();
	});
	</script>
</body>
</html>
