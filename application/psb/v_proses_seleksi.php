<?php 
$titlepage="Jalur Seleksi";
$idsmenu=12; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

 
?>
  <div class="content">
    <section class="content-header">
	  <h1>
		Seleksi pendaftaran calon santri
	  </h1>
	  
	</section>
	<section class="content">
	<div class="row">
    <div class="col-md-3">
	  <div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">Peserta pendaftar</h3>
					</div> <!-- end of box-header -->
					<div class="box-body table-responsive no-padding" style="height: 1136px; overflow-y: scroll;">
						<table id="table_item" class="table  table-bordered table-hover ">
						  <tbody></tbody>
						</table>
					</div> <!-- end of box-body -->
				</div>
			</div><!-- end of div box info -->
		</div><!-- end of col-md-12 -->
	  </div>
	</div>
	
	<div class="col-md-9">
	  <div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">Pencarian santri</h3>
					</div> <!-- end of box-header -->

					<div class="row">
						<div class=" col-xs-12" style="padding-left:25px;padding-right:25px">
							<input type="text" class="form-control " id="txtsearchitem" placeholder="Cari nama atau nomor pendaftaran...">
						</div>
					</div>				
					<div class="box-body">
							
					</div> <!-- end of box-body -->
				</div>
			</div><!-- end of div box info -->
		</div><!-- end of col-md-12 -->
	  </div>
		<div class="row">
			<div class="col-md-8">
				<!-- Identitas siswa -->
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/student.png" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Identitas santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
				  
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-12">
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>Jalur pendaftaran</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtjalur"></span>
									<input type="hidden" name="txttoken" id="txttoken" value="">
									<input type="hidden" name="txtpendaftaran" id="txtpendaftaran" value="">
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>Nomor pendaftaran</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtnopendaftaran"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>Sekolah tujuan</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txttujuan"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>Jenjang Pendidikan</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtjenjang"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>MDK</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtmdk"></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-4">
									<b>Nama lengkap</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtnama"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Email</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtemail"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Jenis kelamin</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtkelamin"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Tempat & Tanggal lahir</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtttl"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Jumlah saudara</b>
								</div>
								<div class="col-sm-2">
									<b>: </b> <span id="txtsaudara"></span>
								</div>
							  <div class="col-sm-5">
								<div class="col-sm-6">
									<b>Anak ke</b>
								</div>
								<div class="col-sm-4">
									<b>: </b> <span id="txtanakke"></span>
								</div>
							  </div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Riwayat penyakit</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtabk"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Alamat tempat tinggal</b><small> (sesuai KK)</small>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtalamat"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Kabupaten</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtkabupaten"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Propinsi</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtpropinsi"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Nomor Handphone</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtnohp"></span>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<b>Kode POS</b>
								</div>
								<div class="col-sm-8">
									<b>: </b> <span id="txtkodepos"></span>
								</div>
							</div>
						</div><!-- /.col -->
						
						
					  </div><!-- /.row -->
					</div><!-- /.box-body -->
				</div><!-- /identitas siswa-->
				
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/berkas.png" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Riwayat sekolah</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-12" id="form-riwayat">
						  
						</div>
					  </div>
					</div>
				</div>
				
				<div class="box box-primary">
					<div class="box-header with-border">
					  <div class="pull-left user-block">
						<img src="../../image/berkas.png" class="img-circle" alt="User Image">
					  </div>
					  <h3 class="box-title">Berkas kelengkapan santri</h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div><!-- /.box-header -->
					<div class="box-body form-horizontal">
					  <div class="row">
						<div class="col-md-12" id="form-kelengkapan">
						  
						</div>
					  </div>
					</div>
				</div>
				
			</div>
			<div class="col-md-4">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
				  <div class="post clearfix">
				  <div class="user-block">
					<img class="img-circle img-bordered-sm" src="../../image/male.png" alt="user image">
					<span class="username">
					  Data Ayah
					</span>
				  </div>
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Nama ayah :</b><br/>
					  <span id="txtnamaayah"></span>
					</li>
					<li class="list-group-item">
                      <b>Tempat & Tanggal lahir:</b><br/>
					  <span id="txtttlayah"></span>
                    </li>
					<li class="list-group-item">
                      <b>Pendidikan ayah:</b><br/>
					  <span id="txtpendidikanayah"></span>
                    </li>
                    <li class="list-group-item">
                      <b>Pekerjaan ayah:</b><br/>
					  <span id="txtpekerjaanayah"></span>
                    </li>
					<li class="list-group-item">
                      <b>No. HP aktif :</b><br/>
					  <span id="txthpayah"></span>
                    </li>
				  </ul>
				  </div>
				  <div class="post clearfix">
				  <div class="user-block">
					<img class="img-circle img-bordered-sm" src="../../image/female.png" alt="user image">
					<span class="username">
					  Data Ibu
					</span>
				  </div>
				  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Nama ibu :</b><br/>
					  <span id="txtnamaibu"></span>
					</li>
					<li class="list-group-item">
                      <b>Tempat & Tanggal lahir:</b><br/>
					  <span id="txtttlibu"></span>
                    </li>
					<li class="list-group-item">
                      <b>Pendidikan ibu:</b><br/>
					  <span id="txtpendidikanibu"></span>
                    </li>
                    <li class="list-group-item">
                      <b>Pekerjaan ibu:</b><br/>
					  <span id="txtpekerjaanibu"></span>
                    </li>
					<li class="list-group-item">
                      <b>No. HP aktif :</b><br/>
					  <span id="txthpibu"></span>
                    </li>
				  </ul>
				  
				  </div>
				  <div class="post clearfix">
				  <div class="user-block">
					<img class="img-circle img-bordered-sm" src="../../image/wali.png" alt="user image">
					<span class="username">
					  Data Wali
					</span>
				  </div>
				  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Nama wali :</b><br/>
					  <span id="txtnamawali"></span>
					</li>
					<li class="list-group-item">
                      <b>Tempat & Tanggal lahir:</b><br/>
					  <span id="txtttlwali"></span>
                    </li>
                    <li class="list-group-item">
                      <b>Pekerjaan wali:</b><br/>
					  <span id="txtpekerjaanwali"></span>
                    </li>
					<li class="list-group-item">
                      <b>No. HP aktif :</b><br/>
					  <span id="txthpwali"></span>
                    </li>
					<li class="list-group-item">
                      <b>Hubungan keluarga :</b><br/>
					  <span id="txthubungan"></span>
                    </li>
					<li class="list-group-item">
                      <b>Alamat wali :</b><br/>
					  <span id="txtalamatwali"></span>
                    </li>
				  </ul>
				  </div>
				  
				</div><!-- /.box-body -->
              </div><!-- /.box -->
			</div>
		</div>
		<div class="box box-widget">
		  <div class="box-footer">
			<button type="button" id="btnreject" data-btn="reject" class="btn btn-danger"><i class="fa fa-hand-paper-o"></i> Tolak</button>
			&nbsp;&nbsp;
			<button type="button" id="btnprint" data-btn="print" class="btn btn-info"><i class="fa fa-print"></i> Cetak data</button>
			<button type="button" id="btnsave" data-btn="lolos" class="btn btn-primary pull-right"><i class="fa fa-mail-forward"></i> Loloskan</button>
		    <span id="infoproses"></span>
		  </div><!-- /.box-footer -->
		</div><!-- /.box -->
	</div>
	</div>
	</section><!-- /.content -->
	
  </div>
  
  <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
  </div>
	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script language="javascript">
		$(document).ready( function () 
		{
			var value = {
				method : "getdata"
			};
			$('#table_item').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": false,
				"ordering": false,
				"info": false,
				"responsive": true,
				"autoWidth": false,
				"dom": '<"top"f>rtip',
				"ajax": {
					"url": "c_proses_seleksi.php",
					"type": "POST",
					"data":value,
				},
				"createdRow": function( row, data, dataIndex){
					if( data['seleksi'] ==  'tolak'){
						$(row).css('background-color', 'Red');
					}
				},
				"columns": [
				{ "data": "daftar" },
				
				]
			});
			$("#table_item_filter").addClass("pull-right");
			
			$('#txtfirstperiod,#txtlastperiod').datepicker({
				format: 'dd-mm-yyyy',
			});
		});
		
	function bigimage(id){
		//var x = document.getElementsById().getAttribute("class");
		$('.imagepreview').attr('src', $(id).attr('src'));
		$('#imagemodal').modal('show');   
	}
		
	var kelamin = {'0': 'Perempuan', '1': 'Laki-laki'};
	var jenjang = {'2':'Ula','3':'Wustho'};
	var pendidikan = {'':'','1':'SD/Sederajat','2':'SMP/Sederajat','3':'SMA/Sederajat','4':'Diploma','5':'S1','6':'S2','7':'S3'};
 
	$(document).ready( function () 
	{
		
		$( "#txtsearchitem" ).autocomplete({
			search  : function(){$(this).addClass('working');},
			open    : function(){$(this).removeClass('working');},
			source: function(request, response) {
				$.getJSON("autocomplete_search.php", { term: $('#txtsearchitem').val() }, 
					response); },
				minLength:1,
				select:function(event, ui){
					temptabel(ui.item.siswa_id);
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<dt>"+item.nomor + " - "+item.nama+"</dt>"+item.jalur+""  )
			.appendTo( ul );
		};
	});
	
	function temptabel(id){
		var value = {
			siswa_id: id,
			method : "select_siswa_pendaftar"
		};
		$.ajax(
		{
			url : "c_proses_seleksi.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				newitem();
				var hasil = jQuery.parseJSON(data);
				var data = hasil[1];
				
				$('#txtpendaftaran').val(data.kode_pendaftaran);				
				$('#txttoken').val(data.uuid);				
				$('#txtjalur').html(data.jalur_name);
				$('#txttujuan').html(data.tujuan);			
				$('#txtjenjang').html(jenjang[data.jenjang_pendidikan]);			
				$('#txtmdk').html(data.jenis);
				$('#txtnopendaftaran').html(data.no_pendaftaran);				
				$('#txtnama').html(data.nama_lengkap);
				$('#txtemail').html(data.email);					
				$('#txtkelamin').html(kelamin[data.kelamin]);					
				$('#txtttl').html(''+data.tempat_lahir+', '+sql_to_report(data.tanggal_lahir)+'');						
				$('#txtsaudara').html(data.saudara);					
				$('#txtanakke').html(data.anak_ke);					
				$('#txtabk').html(data.abk);						
				$('#txtpropinsi').html(data.propinsi_name);					
				$('#txtkabupaten').html(data.kabupaten_name);				
				$('#txtalamat').html(data.alamat);
				$('#txtnohp').html(data.no_hp);				
				$('#txtkodepos').html(data.kode_pos);
				
				$('#txtnamaayah').html(data.ayah);
				$('#txtpekerjaanayah').html(data.pekerjaan_ayah);
				$('#txtpendidikanayah').html(pendidikan[data.pendidikan_ayah]);
				$('#txthpayah').html(data.hp_ayah);
				
				$('#txtnamaibu').html(data.ibu);
				$('#txtpendidikanibu').html(pendidikan[data.pendidikan_ibu]);
				$('#txtpekerjaanibu').html(data.pekerjaan_ibu);
				$('#txthpibu').html(data.hp_ibu);
				
				$('#txtnamawali').html(data.wali);
				$('#txtpekerjaanwali').html(data.pekerjaan_wali);
				$('#txtalamatwali').html(data.alamat_wali);
				$('#txthpwali').html(data.telp_wali);
				$('#txthubungan').html(data.hubungan_wali);
				
				$('#form-kelengkapan').html(hasil.kelengkapan);
				$('#form-riwayat').html(hasil.riwayat);
				
				
				
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
			}
		});
	}
	$(document).on( "click","#btnsave, #btnreject, #btnprint", function() {
		var token = $("#txttoken").val();
		var kode_pendaftaran = $("#txtpendaftaran").val();
		var opt = $(this).attr('data-btn');
		
		if(token == '' || token== null){
			$.notify({
				message: "Siswa belum dipilih"
			},{
				type: 'warning',
				delay: 8000,
			});		
			set_focus("#txtsearchitem");
			return;
		}
		if(opt == 'print'){
			window.open('//localhost/alkamal_v2/application/pendaftaran/formulir-do-pdf.php?token='+token, '_blank');
		}else{
		  var value = {
			token: token,
			kode_pendaftaran: kode_pendaftaran,
			opt:opt,
			method : "seleksi_siswa"
		  };
		  $("#btnsave, #btnprint, #btnreject").prop('disabled', true);
		  proccess_waiting("#infoproses");
		  $.ajax(
		  {
			url : "c_proses_seleksi.php",
			type: "POST",
			data : value,
			success: function(data, textStatus, jqXHR)
			{
				$("#btnsave, #btnprint, #btnreject").prop('disabled', false);
				$("#infoproses").html("");
				var data = jQuery.parseJSON(data);
				if(data.result == true){
					$.notify('Update calon siswa berhasil');
					var table = $('#table_item').DataTable(); 
					table.ajax.reload( null, false );
					newitem();
					set_focus("#txtsearchitem");
				}else{
					$.notify({
						message: "Update calon siswa gagal, error :"+data.error
					},{
						type: 'danger',
						delay: 8000,
					});					
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				$("#btnsave, #btnprint, #btnreject").prop('disabled', false);
			}
		  });
		}
	});
		
	function newitem(){
		$('#txtpendaftaran').val('');				
		$('#txttoken').val('');				$('#txtnamaayah').html('');
		$('#txtnopendaftaran').html('');	$('#txtttlayah').html('');
		$('#txtjalur').html('');			$('#txtpendidikanayah').html('');
		$('#txttujuan').html('');			$('#txtpekerjaanayah').html('');
		$('#txtjenjang').html('');			$('#txthpayah').html('');
		$('#txtmdk').html('');				
		$('#txtnama').html('');				$('#txtnamaibu').html('');
		$('#txtemail').html('');			$('#txtttlibu').html('');
		$('#txtkelamin').html('');			$('#txtpendidikanibu').html('');
		$('#txtttl').html('');				$('#txtpekerjaanibu').html('');
		$('#txtsaudara').html('');			$('#txthpibu').html('');
		$('#txtanakke').html('');									
		$('#txtabk').html('');				$('#txtnamawali').html('');
		$('#txtpropinsi').html('');			$('#txtttlwali').html('');
		$('#txtkabupaten').html('');		$('#txtpekerjaanwali').html('');
		$('#txtalamat').html('');			$('#txtalamatwali').html('');
		$('#txtnohp').html('');				$('#txthpwali').html('');
		$('#txtkodepos').html('');			$('#txthubungan').html('');
											
		$('#form-kelengkapan').html('');
		$('#form-riwayat').html('');			
											
			
	}
	
	function sql_to_report(tgl){
		var thn = tgl.substr(0, 4);
		var bln = tgl.substr(5, 2);
		var dy = tgl.substr(8, 2);
		var date = dy+'-'+bln+'-'+thn;
		return date;
	}
	</script>
</body>
</html>
