<?php 
$titlepage="Setting Rekening";
$idsmenu=7; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content">
	<div class="row">
            <div class="col-md-6">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Rekening Bank</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                    <label>Nomor rekening:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bank"></i>
                      </div>
                      <input type="text" class="form-control" id="norek" name="norek">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Atas nama rekening:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bank"></i>
                      </div>
                      <input type="text" class="form-control" id="bank" name="bank">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Kantor cabang:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-bank"></i>
                      </div>
                      <input type="text" class="form-control" id="cabang" name="cabang">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnrefsystem" class="btn btn-primary pull-right">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
	</div>
</section><!-- /.content -->


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script>
		$(document).ready(function () {
			refsystem();
		});
		
		$(document).on("click","#btnrefsystem",function(){
			var norek = $("#norek").val();
			var bank = $("#bank").val();
			var cabang = $("#cabang").val();
			value={
				norek : norek,
				bank : bank,
				cabang : cabang,
				method : 'update_rekening',
			}
			$.ajax(
			{
				url : "c_rekening.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					refsystem();
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		function refsystem(){
			value={
				method : 'get_rekening',
			}
			$.ajax(
			{
				url : "c_rekening.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#bank").val(data.nama_rekening);
					$("#norek").val(data.no_rekening);
					$("#cabang").val(data.cabang);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
	</script>
</body>
</html>
