<?php 
$titlepage="Laporan daftar ulang";
$idsmenu=16; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_psb.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

$pos = new model_psb();

?>
  <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
  <div class="content">
  
    <section class="content-header">
	  <h1>
		Laporan daftar ulang santri
	  </h1>
	  
	</section>
	<section class="content">
	  <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Filter</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
			<form method="post" id="target" target="_blank" action="import_daful.php" >
            <div class="box-body">
              <div class="row">
			    
                <input type="hidden" name="method" value="excel">
				<div class="col-md-6">
                  <div class="form-group">
                    <label>Jenjang pendidikan</label>
                    <select class="form-control" id="txtjalur" name="jalur" style="width: 100%;">
                      <option value="all">Semua</option>
                      <option value="ula putra">Ula Putra</option>
                      <option value="ula putri">Ula Putri</option>
                      <option value="wustho putra">Wustho Putra</option>
                      <option value="wustho putri">Wustho Putri</option>
					</select>
                  </div><!-- /.form-group -->
				  <div class="form-group"> 
					<label >Tanggal daftar</label>
					<input type="text" class="form-control txtperiode tgl" name="first" id="txtfirstperiod"  value=""  data-inputmask="'alias': 'dd-mm-yyyy'" data-mask> 
				  </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Status daftar ulang</label>
                    <select class="form-control" id="txtstatus" name="status" style="width: 100%;">
                      <option value="all">Semua</option>
                      <option value="sudah">Sudah</option>
                      <option value="belum">Belum</option>
                    </select>
                  </div><!-- /.form-group -->
				  <div class="form-group"> 
					<label>Sampai tanggal</label>
					<input type="text" class="form-control txtperiode tgl" name="last" id="txtlastperiod"  value="" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask>
				  </div>
                </div><!-- /.col -->
				
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
			  <div class="pull-right">
			  	<button type="button" id="btnsearch" data-btn="search" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				<button type="submit" id="btndownloadx" data-btn="download" class="btn btn-success"><i class="fa fa-download"></i> Download excel</button>
				
				<span id="infoproses"></span>
			  </div>
            </div>
			</form>
          </div><!-- /.box -->
		  
		  <div class="box box-info">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Daftar ulang santri</h3>
				</div> <!-- end of box-header -->
				<div class="box-body table-responsive">
					<table id="table_item" class="table  table-bordered table-hover ">
					  <thead>
						<tr class="tableheader">
							<th style="width:50px">#</th>
							<th width="15%">No Pendaftaran </th>
							<th>Jenjang </th>
							<th>MDK </th>
							<th >Nama Lengkap </th>
							<th width="30%">Alamat </th>
							<th>Sekolah Tujuan </th>
							<th>Daful </th>
						</tr>
					  </thead>
					  <tbody></tbody>
					</table>
				</div> <!-- end of box-body -->
			</div>
		</div><!-- end of div box info -->
	</section><!-- /.content -->

  </div>
    <?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script language="javascript">
				
		$(document).ready( function () 
		{		
			$('#txtfirstperiod,#txtlastperiod').datepicker({
				format: 'dd-mm-yyyy',
			});
			//Datemask dd/mm/yyyy
			$("#txtfirstperiod, #txtlastperiod").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
		});
	function tabel(){
		var jalur = $('#txtjalur').val();
		var stat = $('#txtstatus').val();
		var first =  $('#txtfirstperiod').val();
		var last = $('#txtlastperiod').val();
	
		var value = {
			jalur: ""+jalur+"",
			stat: ""+stat+"",
			first: ""+first+"",
			last: ""+last+"",
			method : "getdata"
		};
		$('#table_item').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": false,
			"responsive": true,
			"autoWidth": false,
			"pageLength": 50,
			"dom": '<"top"f>rtip',
			"ajax": {
				"url": "c_report_daful.php",
				"type": "POST",
				"data":value,
			},
			"columns": [
			{ "data": "urutan" },
			{ "data": "no_pendaftaran" },
			{ "data": "jenjang" },
			{ "data": "mdk" },
			{ "data": "nama_lengkap" },
			{ "data": "alamat_lengkap" },
			{ "data": "sekolah_tujuan" },
			{ "data": "status_daful" },
			]
		});
		$("#table_item_filter").addClass("pull-right");
	}
	
	$(document).on( "click","#btndownload, #btnsearch", function() {
		var opt = $(this).attr('data-btn');
		var jalur = $('#txtjalur').val();
		var stat = $('#txtstatus').val();
		if(opt == 'search'){
			$("#table_item").DataTable().destroy();
			tabel();
		}else{
		    var mapForm = document.createElement("form");
			mapForm.target = "Map";
			mapForm.method = "POST"; // or "post" if appropriate
			mapForm.action = "import_pendaftar.php";

			var method = document.createElement("input");
			method.type = "text";
			method.name = "method";
			method.value = "excel";
			mapForm.appendChild(method);
			
			var txtjalur = document.createElement("input");
			txtjalur.type = "text";
			txtjalur.name = "jalur";
			txtjalur.value = jalur;
			mapForm.appendChild(txtjalur);
			
			var txtstat = document.createElement("input");
			txtstat.type = "text";
			txtstat.name = "stat";
			txtstat.value = stat;
			mapForm.appendChild(txtstat);

			document.body.appendChild(mapForm);

			map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

			if (map) {
				mapForm.submit();
			} else {
				alert('You must allow popups for this map to work.');
			}
		}
	});
	</script>
</body>
</html>
