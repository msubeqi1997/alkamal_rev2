<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/model_data.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new model_data();
	$method=isset($_POST['method'])?$_POST['method']:'';
	
	function display_to_sql($date){
		return substr($date,6,4).'-'.substr($date,3,2).'-'.substr($date,0,2);
	}
	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
	if($method == 'get_detail')
	{
		$id_item=$_POST['tahun'];
		$data = $pos->getDetailTapel($id_item);
		
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		$array['ganjilstart'] = display_to_report($data[1]['ganjilmulai']);
		$array['ganjilfinish'] = display_to_report($data[1]['ganjilselesai']);
		$array['genapstart'] = display_to_report($data[1]['genapmulai']);
		$array['genapfinish'] = display_to_report($data[1]['genapselesai']);
		
		echo json_encode($array);
	}
	
	if($method == 'update_akademik')
	{
		$tahun = $_POST['tahun'];
		$ganjilmulai = display_to_sql($_POST['ganjilmulai']);
		$ganjilselesai = display_to_sql($_POST['ganjilselesai']);
		$genapmulai = display_to_sql($_POST['genapmulai']);
		$genapselesai = display_to_sql($_POST['genapselesai']);
		$semester = $_POST['smt'];
		$hari_masuk = $_POST['hari_masuk'];
		
		$array = $pos->updateTanggalAkademik($tahun,$ganjilmulai,$ganjilselesai,$genapmulai,$genapselesai,$semester,$hari_masuk);
		$activate = $pos->activateTapel($tahun);
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}