
<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/utility.php");
$method=$_POST['method'];
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new utility();
	if($method == 'delete_user'){
		$id_user=strtoupper($_POST['id_user']);
		$pos = new utility();
		$array = $pos->deleteUser($id_user);
		$data['result'] = $array[0];
		$data['error'] = $array[1];
		echo json_encode($data);
	}

	if($method == 'getdata'){
		$pos = new utility();
		$array = $pos->getListUser();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {

			$data[$i]['button'] = '
			<button type="submit" id_user="'.$key['id_user'].'" class="btn btn-warning btnpass  btn-sm" id="btnpass'.$key['id_user'].'"  ><i class="fa fa-key"></i>
			</button>
			<button type="submit" id_user="'.$key['id_user'].'" username="'.$key['username'].'" h_menu="'.$key['h_menu'].'" t_menu="'.$key['level_access'].'" page="'.$key['default_page'].'" class="btn btn-primary btnedituser btn-sm " id="btnedit'.$key['id_user'].'"  ><i class="fa fa-edit"></i>
			</button>
			<button type="submit" id_user="'.$key['id_user'].'" class="btn  btn-danger btndelete  btn-sm " id="btndelete'.$key['id_user'].'" ><i class="fa fa-remove"></i>
				</button';


				$i++;
			}
			$datax = array('data' => $data);
			echo json_encode($datax);
		}
		
	if($method == 'get_user_detail'){
		$id_user=strtoupper($_POST['id_user']);
		$pos = new utility();
		$array = $pos->detailUser($id_user);
		$data['data'] = $array[1];
		$data['error'] = $array[0];
		echo json_encode($data);
	}
	
		if($method == 'reset_password')
		{
			$id_user= $_POST['id_user'];
			$newpass = $_POST['new_pass'];
			$pos = new utility();
			$array = $pos->resetPass($id_user,$newpass);
			$result['result'] = $array[0];
			$result['error'] = $array[1];

			echo json_encode($result);
		}

		if($method == 'save_user'){
			$id_user=$_POST['id_user'];
			$page=$_POST['page'];
			$username=strtoupper($_POST['username']);
			$pass_user=strtoupper($_POST['pass_user']);
			$t_menu=strtoupper($_POST['t_menu']);
			$h_menu=strtoupper($_POST['h_menu']);
			$pos = new utility();
			if($_POST['crud'] == 'N'){
				$array = $pos->saveUser($username,$pass_user,$t_menu,$h_menu,$page);
			}else{
				$array = $pos->updateUser($id_user,$username,$t_menu,$h_menu,$page);
			}
			$result['result'] = $array[0];
			$result['error'] = $array[1];
			$result['crud'] = $_POST['crud'];
			echo json_encode($result);
		}
		
		if($method == 'change_pass')
		{
			$id_user= $_POST['user'];
			$pass = $_POST['pass'];
			$newpass = $_POST['newpass'];
			$pos = new utility();
			$data = $pos->checkPassword($id_user,$pass);
			if(!empty($data[1])){
				$array = $pos->resetPass($id_user,$newpass);
				$result['result'] = $array[0];
				$result['error'] = $array[1];
			}else{
				$result['result'] = "Password wrong!";
			}
			echo json_encode($result);
		}
		if($method == 'upload_user')
		{
			$id_user= $_POST['user'];
			$dir = "../../files/images_user/";
			move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
			$array = $pos->updatePhotoUser($id_user,$_FILES["image"]["name"]);
		}
	} else {
		exit('No direct access allowed.');
	}