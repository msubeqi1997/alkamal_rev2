<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/pos_master.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new pos_master();
	$method=isset($_POST['method'])?$_POST['method']:'';
	
	if($method == 'get_receipt')
	{
		$result = $pos->get_receipt();

		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_printer')
	{
		$result = $pos->get_printer();

		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_refsystem')
	{
		$result = $pos->get_ref_system();

		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'get_stokmin')
	{
		$result = $pos->get_stokmin();
		echo json_encode(array('data'=>$result[1]));
	}
	
	if($method == 'update_refsystem')
	{
		$shop = $_POST['shop'];
		$address = $_POST['address'];
		$telp = $_POST['telp'];
		
		$array = $pos->update_refsystem($shop,$address,$telp);

		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'update_receipt')
	{
		$header1 = $_POST['header1'];
		$header2 = $_POST['header2'];
		$footer = $_POST['footer'];
		
		$array = $pos->update_receipt($header1,$header2,$footer);

		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'update_printer')
	{
		$printer = $_POST['printer'];
		
		$array = $pos->update_printer($printer);

		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
	
	if($method == 'update_stokmin')
	{
		$stokmin = $_POST['stokmin'];
		
		$array = $pos->update_printer($stokmin);

		$result['result'] = $array[0];
		$result['error'] = $array[1];
		echo json_encode($result);
	}
} else {
	exit('No direct access allowed.');
}