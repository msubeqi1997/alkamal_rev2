<?php 
$titlepage="Tambah User";
$idsmenu=101; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/utility.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

if(isset($_POST['edit'])){ 
	echo '<input type="hidden" name="iduser" id="txtiduser" value="'.$_POST['edit'].'">'; 
}
?>


<section class="content-header">
  <h1>
	User
	<small>new user</small>
  </h1>
</section>
		
<section class="content">
  <div class="row">
	<!-- left column -->
	<div class="col-md-12">
	  <div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">New User</h3>
		</div>
		<!--./ box header-->
		<form class="form-horizontal">
		<div class="box-body">
			<div class="form-group"> <label class="col-sm-3  control-label">Username</label>
			  <div class="col-sm-6">
				<input type="text" class="form-control text-uppercase" id="txtusername" name="" value="" placeholder="">
                <input type="hidden" id="inputcrud" name="inputcrud" class="" value="N">
                <input type="hidden" id="topmenu" name="" class="" value="">
                <input type="hidden" id="hmenu" name="" class="" value="">
			  </div>
			</div>
			<div class="form-group"> <label class="col-sm-3  control-label">Password</label>
			  <div class="col-sm-6">
				<input type="password" class="form-control " id="txtpass" name="" value="" placeholder="">
			  </div>
			</div>
		</div>
		<!--form menuk-->
            <?php
			 $pos = new utility();
			/*
           
            $top = $pos->getTopMenu();
			$mymenu = $pos->getMenu();
            $num=1;
            $menuku='';
            foreach ($mymenu[1] as $key) {
              if($num==1)
              {
                $menuku .= '<div class="row" >';
                $menuku .= '<div class="col-xs-6" style="padding-left:0px"><h4>'.$key['name_menu'].'</h4>';
                $submenuk = $pos->getSubMenu($key['id_menu']);
                $menuku .= '<ul class="list-group">'; 
                foreach ($submenuk[1] as $keys) {
                  $menuku .= '<li class="list-group-item">
				  <div class="input-group">
                    <input type="checkbox"  id="check-'.$keys["id_sub_menu"].'" class="chkbox" value="'.$keys['id_sub_menu'].'" > <strong>'.$keys['name_sub_menu'].'</strong>
					<span class="input-group-addon" style="border-color:#fff;">
					   <input name="default" class="default" id="default'.$keys["id_sub_menu"].'" value="'.$keys["id_sub_menu"].'" type="radio">
					</span>
                  </div>
                  
				  </li>'; 
                }
                $menuku .= '</ul>'; 
                $menuku .= '</div>';
              }else{
                $menuku .= '<div class="col-xs-6" style="padding-left:0px"><h4>'.$key['name_menu'].'</h4>';
                $submenuk = $pos->getSubMenu($key['id_menu']);
                $menuku .= '<ul class="list-group">'; 
                foreach ($submenuk[1] as $keys) {
                  $menuku .= '<li class="list-group-item">
								<div class="input-group">
								   <input type="checkbox" id="check-'.$keys["id_sub_menu"].'" class="chkbox" value="'.$keys['id_sub_menu'].'" > <strong>'.$keys['name_sub_menu'].'</strong>
								<span class="input-group-addon" style="border-color:#fff;">
								   <input name="default" class="default" id="default'.$keys["id_sub_menu"].'" value="'.$keys["id_sub_menu"].'" type="radio">
								</span>
							  </div>
						  </li>'; 
                }
                $menuku .= '</ul>';
                $menuku .= '</div>';
                $menuku .= '</div>';
                $num=0;
              }
              $num++;
            }
			*/
            ?>
		</form>
	  </div><!-- /.box -->
	</div>
	
		<div class="col-md-12">
		  <div class="box box-primary">
			<div class="box-header">
			  <i class="fa fa-edit"></i>
			  <h3 class="box-title">Menu Access</h3>
			</div>
			<div class="box-body" >
			  <div class="row">
				  <div class="col-xs-6">
					<input name="default" class="default" id="default0" value="0" type="radio" checked> Default Page [Dashboard]
				  </div>
			  </div>
			  <div style="overflow: auto;">
			  <p>
				
			  </p>
			  <?php
			  foreach($top[1] as $nav){ ?>
				<div class="col-md-4 col-sm-4">
				  <table class="table table-bordered" >
					<tr>
					  <?php
						echo '<th class="text-center"><input type="checkbox"  class="checktop" id="checktop-'.$nav['id'].'" data-id="'.$nav['id'].'" value="'.$nav['id'].'"> <strong>'.$nav['menu_name'].'</strong></th>';
					  ?>
					</tr>
					<tr>
					  <td>
					  <?php
						$mymenu = $pos->getMenu($nav['id']);
						foreach ($mymenu[1] as $key) {
						  echo '<div style="padding-left:0px"><h4>'.$key['name_menu'].'</h4>';
						  $submenuk = $pos->getSubMenu($key['id_menu']);
						  foreach ($submenuk[1] as $keys){
							echo' <div class="input-group">
								<input type="checkbox"  id="check-'.$keys["id_sub_menu"].'" data-sub="'.$nav['id'].'" class="chkbox" value="'.$keys['id_sub_menu'].'" > <strong>'.$keys['name_sub_menu'].'</strong>
								<span class="input-group-addon" style="border-color:#fff;">
								   <input name="default" class="default" id="default'.$keys["id_sub_menu"].'" value="'.$keys["id_sub_menu"].'" type="radio">
								</span>
							 </div>';							   
						  }
						}
					  ?>
					  </td>
					</tr>
				  </table>
				</div>
			  <?php } ?>
			  </div>
			</div><!-- /.box -->
		  </div>
		</div><!-- /.col -->
	  
	<div class="col-md-12">
      <div class="box">
		<!-- form start -->
		<form class="form-horizontal">
		  
		  <div class="box-footer">
			<button type="button" title="Save Button" class="btn btn-primary " id="btnsaveuser" key="back" name=""><i class="fa fa-save"></i> Save</button> <span id="infoproses"></span>
			<button type="button" title="Back Button" class="btn btn-primary " onclick="goBack()" name=""><i class="fa fa-back"></i> Back</button>
		  </div><!-- /.box-footer -->
		</form>
	  </div><!-- /.box -->
	</div>	
  </div>	

</section><!-- /.content -->
	
<?php include "../layout/footer.php"; //footer template ?> 
<?php include "../layout/bottom-footer.php"; //footer template ?> 
<script src="j_mstuser.js"></script>
<script>
$(document).ready( function () 
{
	if( $('#txtiduser').length){
		var idb = $('#txtiduser').val();
		edit(idb);
	}else{
		newitem();
	}
});

function newitem()
{
  $("#txtiduser").val(0);
  $("#txtusername").val("");
  $("#inputcrud").val("N");
  $("#topmenu").val("");
  $("#hmenu").val("");
  set_focus("#txtusername");
  $('.chkbox').prop('checked', false);
  $('#default0').prop('checked', true);
}

function edit(idb) {
	var param = {
		id_user : idb,
		method : "get_user_detail"
	};
	//request the JSON data and parse into the select element
	$.ajax({
		url : "c_mstuser.php",
		type: "POST",
		data : param,
		dataType: 'JSON',
		success: function (data) {
		  data = data.data;
		  
		  $("#inputcrud").val("E");
		  $("#txtiduser").val(data.id_user);
		  $("#txtusername").val(data.username);
		  $("#txtpass").val("***********");
		  $("#default"+data.default_page).prop('checked', true);
		  $("#topmenu").val(data.level_access);
		  $("#hmenu").val(data.h_menu);
		  $('.chkbox').prop('checked', false);
		  var res = data.h_menu.split(",");
		  for (i = 0; i < res.length; i++) { 
		   $("#check-"+res[i]).prop('checked', true);
		  }
		  
		  var top = data.level_access.split(",");
		  for (i = 0; i < top.length; i++) { 
		   $("#checktop-"+top[i]).prop('checked', true);
		  }
		},
		error: function () {
			//if there is an error append a 'none available' option
		}
	});
}
$(document).on("click",".checktop",function(){
  var id = $(this).data('id');
  if ($(this).is(':checked')){
	$('input[data-sub='+id+']:enabled').prop('checked', true);
  }else{
	$('input[data-sub='+id+']').prop('checked', false);
  }
  get_check_value();
  
  var values = [];
  $('.checktop:checked').each(function() {
    values.push($(this).val());
  });
  $('#topmenu').val(values.join(','));
});
function goBack() {
    window.history.back();
}
</script>
	