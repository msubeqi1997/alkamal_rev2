<?php 
$titlepage="Setting Akademik";
$idsmenu=1; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/model_data.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 
error_reporting(0);
$pos = new model_data();
$array = $pos->getTapel();
$data = $array[1];
$active = $pos->activeTapel();
$hari = $pos->hari();

$checked=explode(',', $active[1]['hari_masuk']);
$val_check=array();
foreach ($checked as $key => $value) {
  # code...
  $val_check[$value]=$value;
}

	function display_to_report($date){
		return substr($date,8,2).'-'.substr($date,5,2).'-'.substr($date,0,4);
	}
	
?>
<link rel="stylesheet" href="../../dist/css/bootstrap-switch.min.css">
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<section class="content">
	<div class="row">
            <div class="col-md-12">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Akademik</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body form-horizontal">
                    <div class="form-group"> <label class="col-sm-3  control-label">Tahun Akademik</label>
						<div class="col-sm-4">
						  <select class="form-control autocomplete" id="optakademik" name="optakademik" >
							<?php
							foreach($data as $opt){
							  $select=($opt['thn_ajaran_id'] == $active[1]['thn_ajaran_id'])?"selected":"";
							  echo '<option value="'.$opt['thn_ajaran_id'].'" '.$select.'>'.$opt['thn_ajaran'].'</option>'; 
							}
							?>
						  </select>
						</div>
					</div>
					<div class="form-group">
					  <label for="first" class="control-label col-sm-3">Semester Ganjil <span class="text-danger"> *</span></label>
					  <div class="col-sm-3 ">
						<div class="input-group">
						  <input readonly="" type="text" class="form-control txtperiode tgl" name="txtganjilmulai" id="txtganjilmulai"  value="<?php echo display_to_report($active[1]['ganjilmulai']);?>"  required> 
						  <div class="input-group-addon">
						    <i class="fa fa-calendar"></i>
						  </div>
						</div>
					  </div>
					  <div class="col-sm-1">
						<label class="control-label col-sm-1">s/d</label>
					  </div>
					  <div class="col-sm-3">
						<div class="input-group">
						  <input readonly="" type="text" class="form-control txtperiode tgl" name="txtganjilselesai" id="txtganjilselesai"  value="<?php echo display_to_report($active[1]['ganjilselesai']);?>" required>
						  <div class="input-group-addon">
						    <i class="fa fa-calendar"></i>
						  </div>
						</div>
					  </div>
					</div>
					<div class="form-group">
					  <label for="first" class="control-label col-sm-3">Semester Genap <span class="text-danger"> *</span></label>
					  <div class="col-sm-3 ">
						<div class="input-group">
						  <input readonly="" type="text" class="form-control txtperiode tgl" name="txtgenapmulai" id="txtgenapmulai"  value="<?php echo display_to_report($active[1]['genapmulai']);?>"  required> 
						  <div class="input-group-addon">
						    <i class="fa fa-calendar"></i>
						  </div>
						</div>
					  </div>
					  <div class="col-sm-1">
						<label class="control-label col-sm-1">s/d</label>
					  </div>
					  <div class="col-sm-3">
						<div class="input-group">
						  <input readonly="" type="text" class="form-control txtperiode tgl" name="txtgenapselesai" id="txtgenapselesai"  value="<?php echo display_to_report($active[1]['genapselesai']);?>" required>
						  <div class="input-group-addon">
						    <i class="fa fa-calendar"></i>
						  </div>
						</div>
					  </div>
					</div>					
					<div class="form-group">
					  <label for="first" class="control-label col-sm-3">Semester Aktif <span class="text-danger"> *</span></label>
					  <div class="col-sm-9">
						<div class="radio">
							<label>
							  <input type="radio" name="semester" id="ganjil" value="ganjil" <?php if($active[1]['semester'] =='ganjil' || $active[1]['semester'] ==NULL) echo 'checked';?>>
							  Ganjil
							</label>
						</div>
						<div class="radio">
							<label>
							  <input type="radio" name="semester" id="genap" value="genap" <?php if($active[1]['semester'] =='genap') echo 'checked';?>>
							  Genap
							</label>
						</div>
					  </div>
					</div>	
					<div class="form-group">
					  <label class="control-label col-sm-3">Hari Aktif</label>
					  <div class="col-sm-9" id="list_hari">
						<?php
						foreach ($hari[1] as $k=>$v) { ?>
							  <label for="" class="checkbox-inline">
								<input type="checkbox" class="chkbox" name="hari[]" id="<?php echo 'hari_'.$k;?>" <?php if($val_check[$k]) echo 'checked'; ?> value="<?php echo $k; ?>" >
									<?php echo $v;?>
							  </label>
						<?php } ?>
					  </div>
					</div>
					
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnupdate" class="btn btn-primary pull-right">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->

</section><!-- /.content -->


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?>
	<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
	
	<script>
		$(document).ready(function () {
			$('#txtganjilmulai,#txtganjilselesai,#txtgenapmulai,#txtgenapselesai').datepicker({
				format: 'dd-mm-yyyy',
			});
			
		});
		
		
		$(document).on("click","#btnupdate", function() {
			var tahun = $("#optakademik").val();
			var ganjilmulai = $("#txtganjilmulai").val();
			var ganjilselesai = $("#txtganjilselesai").val();
			var genapmulai = $("#txtgenapmulai").val();
			var genapselesai = $("#txtgenapselesai").val();
			var smt = document.querySelector('input[name="semester"]:checked').value;
			var values = [];
			$('.chkbox:checked').each(function() {
			  values.push($(this).val());
			});
			var hari_masuk = values.join(',');
			
			if(smt == '' || smt == null ){
				$.notify({
					message: "Semester aktif harus diisi!"
				},{
					type: 'warning',
					delay: 8000,
				});		
				return;
			}
			
			var value = {
				tahun: tahun,
				ganjilmulai: ganjilmulai,
				ganjilselesai: ganjilselesai,
				genapmulai:genapmulai,
				genapselesai: genapselesai,
				smt: smt,
				hari_masuk: hari_masuk,
				method : "update_akademik"
			};
			$(this).prop('disabled', true);
			proccess_waiting("#infoproses");
			$.ajax(
			{
				url : "c_akademik.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$("#btnupdate").prop('disabled', false);
					$("#infoproses").html("");
					var data = jQuery.parseJSON(data);
					
					if(data.result == 1){
						$.notify('Proses simpan berhasil');
					}else{
						$.notify({
							message: "Proses simpan gagal, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$("#btnupdate").prop('disabled', false);
				}
			});
		});
		
		$(document).on("change","#optakademik",function(){
			var tahun = $(this).val();
			value={
				tahun : tahun,
				method : 'get_detail',
			}
			$.ajax(
			{
				url : "c_akademik.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					var plot = data.hari_masuk;
					$('input[name="hari[]"]').removeAttr("checked");
					$("#txtganjilmulai").val(hasil.ganjilstart);
					$("#txtganjilselesai").val(hasil.ganjilfinish);
					$("#txtgenapmulai").val(hasil.genapstart);
					$("#txtgenapselesai").val(hasil.genapfinish);
					$("input[name='semester'][value='"+data.semester+"']").prop('checked', true);
					var res = plot.split(",");
					for (i = 0; i < res.length; i++) { 
						$("#hari_"+res[i]).prop('checked', true);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		
		
	</script>
</body>
</html>
