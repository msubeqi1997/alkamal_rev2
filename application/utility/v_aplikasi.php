<?php 
$titlepage="Setting";
$idsmenu=1; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/pos_report.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content">
	<div class="row">
            <div class="col-md-6">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Aplikasi</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                    <label>Nama  toko:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-home"></i>
                      </div>
                      <input type="text" class="form-control" id="shop" name="shop">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Alamat:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-road"></i>
                      </div>
                      <input type="text" class="form-control" id="address" name="address">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Telp:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" id="telp" name="telp">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnrefsystem" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
			
			
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Stok minimal barang</h3>
                </div>
				<form role="form">
                <div class="box-body">
                  <!-- Date range -->
                  <div class="form-group">
                    <label>Jumlah:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-toggle-down"></i>
                      </div>
                      <input type="text" class="form-control pull-right" name="stokmin" id="stokmin">
                    </div><!-- /.input group -->
                
				</div><!-- /.box-body -->
              </div><!-- /.box -->
			  <div class="box-footer">
                    <button type="button" id="btnupdateprinter" class="btn btn-primary">Simpan</button>
                  </div>
              </form>
			</div><!-- /.col (right) -->
		</div><!-- /.row -->
		<div class="col-md-6">
		  <div class="box box-primary">
			<div class="box-header">
			  <h3 class="box-title">Printer</h3>
			</div>
			<form role="form">
			<div class="box-body">
			  <!-- Date range -->
			  <div class="form-group">
				<label>IP Mask:</label>
				<div class="input-group">
				  <div class="input-group-addon">
					<i class="fa fa-laptop"></i>
				  </div>
				  <input type="text" class="form-control pull-right" name="" id="ipprinter">
				</div><!-- /.input group -->
			
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		  <div class="box-footer">
				<button type="button" id="btnupdateprinter" class="btn btn-primary">Simpan</button>
			  </div>
		  </form>
		  </div><!-- /.col (right) -->
		</div>
		<div class="col-md-6">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Receipt</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                    <label>Header I:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-header"></i>
                      </div>
                      <input type="text" class="form-control" id="header_1" name="header_1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Header II:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-header"></i>
                      </div>
                      <input type="text" class="form-control" id="header_2" name="header_2">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>footer:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-commenting-o"></i>
                      </div>
                      <input type="text" class="form-control" id="footer" name="footer">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnupdatereceipt" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
			
			
            
	</div>
</section><!-- /.content -->


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script>
		$(document).ready(function () {
			datareceipt();
			dataprinter();
			refsystem();
			stokmin()
		});
		
		$(document).on("click","#btnrefsystem",function(){
			var shop = $("#shop").val();
			var address = $("#address").val();
			var telp = $("#telp").val();
			value={
				shop : shop,
				address : address,
				telp : telp,
				method : 'update_refsystem',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					refsystem();
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		$(document).on("click","#btnupdatereceipt",function(){
			var header1 = $("#header_1").val();
			var header2 = $("#header_2").val();
			var footer = $("#footer").val();
			value={
				header1 : header1,
				header2 : header2,
				footer : footer,
				method : 'update_receipt',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					datareceipt();
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		$(document).on("click","#btnupdateprinter",function(){
			var printer = $("#ipprinter").val();
			value={
				printer : printer,
				method : 'update_printer',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					dataprinter();					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		$(document).on("click","#stokmin",function(){
			var stokmin = $("#stokmin").val();
			value={
				stokmin : stokmin,
				method : 'update_stokmin',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					stokmin();					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		function refsystem(){
			value={
				method : 'get_refsystem',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#shop").val(data.name_shop);
					$("#address").val(data.address_shop);
					$("#telp").val(data.phone_shop);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
		
		function datareceipt(){
			value={
				method : 'get_receipt',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#header_1").val(data.header_1);
					$("#header_2").val(data.header_2);
					$("#footer").val(data.footer);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
				
		function dataprinter(){
			value={
				method : 'get_printer',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#ipprinter").val(data.printer);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
		
		function stokmin(){
			value={
				method : 'get_stokmin',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#stokmin").val(data);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
	</script>
</body>
</html>
