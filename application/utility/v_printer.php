<?php 
$titlepage="Laporan penjualan";
$idsmenu=1; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/pos_report.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content">
	<div class="row">
            <div class="col-md-6">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Receipt</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                    <label>Header I:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-header"></i>
                      </div>
                      <input type="text" class="form-control" id="header_1" name="header_1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Header II:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-header"></i>
                      </div>
                      <input type="text" class="form-control" id="header_2" name="header_2">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>footer:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-commenting-o"></i>
                      </div>
                      <input type="text" class="form-control" id="footer" name="footer">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnupdatereceipt" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
			
			
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Printer</h3>
                </div>
				<form role="form">
                <div class="box-body">
                  <!-- Date range -->
                  <div class="form-group">
                    <label>IP Mask:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-laptop"></i>
                      </div>
                      <input type="text" class="form-control pull-right" name="" id="ipprinter">
                    </div><!-- /.input group -->
                
				</div><!-- /.box-body -->
              </div><!-- /.box -->
			  <div class="box-footer">
                    <button type="button" id="btnupdateprinter" class="btn btn-primary">Simpan</button>
                  </div>
              </form>
			</div><!-- /.col (right) -->
          </div><!-- /.row -->

</section><!-- /.content -->


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script>
		$(document).ready(function () {
			datareceipt();
			dataprinter();
		});
		
		$(document).on("click","#btnupdatereceipt",function(){
			var header1 = $("#header_1").val();
			var header2 = $("#header_2").val();
			var footer = $("#footer").val();
			value={
				header1 : header1,
				header2 : header2,
				footer : footer,
				method : 'update_receipt',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					datareceipt();
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		$(document).on("click","#btnupdateprinter",function(){
			var printer = $("#ipprinter").val();
			value={
				printer : printer,
				method : 'update_printer',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					$.notify('Update berhasil');
					dataprinter();					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		function datareceipt(){
			value={
				method : 'get_receipt',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#header_1").val(data.header_1);
					$("#header_2").val(data.header_2);
					$("#footer").val(data.footer);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
		
		function dataprinter(){
			value={
				method : 'get_printer',
			}
			$.ajax(
			{
				url : "c_printer.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var hasil = jQuery.parseJSON(data);
					data = hasil.data;
					$("#ipprinter").val(data.printer);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		}
	</script>
</body>
</html>
