<?php 
$titlepage="Ubah Password";
$idsmenu=100; 
include "../../library/config.php";
require_once("../model/dbconn.php");
require_once("../model/utility.php");
include "../layout/top-header.php";
include "../../library/check_login.php";
include "../../library/check_access.php";
include "../layout/header.php"; 

?>
<section class="content">
	<div class="row">
            <div class="col-md-6">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Change your password</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                    <label>Your password:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                      </div>
                      <input type="password" class="form-control" id="pass" name="pass">
					  <input type="hidden" id="txtiduser" name="txtiduser" class="" value="<?php echo $_SESSION['sess_id'];?>">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Type your new password:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                      </div>
                      <input type="password" class="form-control" id="newpass" name="newpass">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="form-group">
                    <label>Re-type your new password:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                      </div>
                      <input type="password" class="form-control" id="renewpass" name="renewpass">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" id="btnupdate" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
			<div class="col-md-3">
				<!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Photo Profile</h3>
                </div><!-- /.box-header -->
                <div class="hero-widget well well-md box-profile">
					<img class="profile-user-img img-responsive" width="100px" height="150px" id="preview_user" alt="Photo">
					<input type="file" name="photo_user" id="upload_user" accept="image/*"  />
				</div><!-- /.box -->
                <div class="box-footer">
                    <button type="button" id="btnupload" class="btn btn-primary">Upload</button>
                </div>
              </div><!-- /.box -->
			  
			</div><!-- /.col (left) -->
			
            
</section><!-- /.content -->


	<?php include "../layout/footer.php"; //footer template ?> 
	<?php include "../layout/bottom-footer.php"; //footer template ?> 
	<script>
		
		function changeImage(input,output) {
		  var reader;
		  
		  if (input.files && input.files[0]) {
			if(input.files[0].size > 120000){
			  alert("File Terlalu besar. Maksimal 100 KB!");
			  input.value = "";
			}else{
			  reader = new FileReader();
			  reader.onload = function(e) {
				output.setAttribute('src', e.target.result);
			  }
			  reader.readAsDataURL(input.files[0]);
			}
		  }
		}
		
		var fileTag = document.getElementById("upload_user"),
		preview = document.getElementById("preview_user");

		fileTag.addEventListener("change", function() {
		  changeImage(this,preview);
		});
	
		$(document).on("click","#btnupdate",function(){
			var user = $("#txtiduser").val();
			var pass = $("#pass").val();
			var newpass = $("#newpass").val();
			var renewpass = $("#renewpass").val();
			
			if(pass == '' || pass == null){
				$.notify({
					message: "Please fill out your password!"
				},{
					type: 'warning',
					delay: 10000,
				});
				set_focus("#pass");
				return;
			}
			
			if(newpass == '' || newpass == null){
				$.notify({
					message: "Please fill out your new password!"
				},{
					type: 'warning',
					delay: 10000,
				});
				set_focus("#newpass");
				return;
			}
			
			if(newpass != renewpass){
				$.notify({
					message: "Re-type New password not match with new password!"
				},{
					type: 'warning',
					delay: 10000,
				});
				set_focus("#renewpass");
				return;
			}
			value={
				user : user,
				pass : pass,
				newpass : newpass,
				method : "change_pass"
			}
			$.ajax(
			{
				url : "c_mstuser.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result == true){
						$.notify('Password was reset');
					}else{
						$.notify({
						message: data.result
						},{
							type: 'warning',
							delay: 5000,
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					
				}
			});
		});
		
		$(document).on("click","#btnupload",function(){
		  var input = document.getElementById("upload_user");
		  var user = $("#txtiduser").val();
		  file = input.files[0];
		  if(file != undefined){
			formData= new FormData();
			if(!!file.type.match(/image.*/)){
			  formData.append("image", file);
			  formData.append("user", user);
			  formData.append("method", 'upload_user');
			  value={
				user : user,
				method : "upload_user"
			  }
			  $.ajax({
				url: "c_mstuser.php",
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function(data){
					alert('success');
				}
			  });
			}else{
			  alert('Not a valid image!');
			}
		  }else{
			alert('Input something!');
		  }
		});
		
	</script>
</body>
</html>
