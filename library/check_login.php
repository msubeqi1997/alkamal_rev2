<?php
if (
	!isset($_SESSION['sess_username']) 
	or !isset($_SESSION['sess_id']) 
	or !isset($_SESSION['sess_uniqid']) 
	or  !isset($_SESSION['sess_h_menu']) )
{
	header('Location: '.$sitename.'application/main/login.php');
}
?>