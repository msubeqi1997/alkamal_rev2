<?php 
session_start();
include "config.php";
if (!isset($_SESSION['sess_username']) 
	or !isset($_SESSION['sess_id']) 
	or !isset($_SESSION['sess_uniqid']) 
	or  !isset($_SESSION['sess_h_menu']) )
{
	$data['result'] = '-1';
	$data['url'] = $sitename.'application/main/login.php?error=session_die';
}else{
	$data['result'] = '1';
	$data['url'] = 'access granted';
}
echo json_encode($data);
?>